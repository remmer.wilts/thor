package threads.thor.work

import android.content.Context
import android.os.Environment
import androidx.core.net.toUri
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.OutOfQuotaPolicy
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import threads.thor.App
import threads.thor.model.DOWNLOADS_TAG
import threads.thor.model.downloadsUri
import java.io.InputStream
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.UUID

class DownloadFileWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {


    fun copy(
        inputStream: InputStream, outputStream: OutputStream,
        read: Long, size: Long, consumer: (Int) -> Unit = {}
    ): Long {
        var x = read
        val buf = ByteArray(Short.MAX_VALUE.toInt())
        var remember = 0
        var n: Int
        while ((inputStream.read(buf).also { n = it }) > 0) {
            outputStream.write(buf, 0, n)
            x += n.toLong()

            if (size > 0) {
                val percent = ((x * 100.0f) / size).toInt()
                if (percent > remember) {
                    remember = percent
                    consumer.invoke(percent)
                }
            }
        }
        return x
    }

    override suspend fun doWork(): Result {
        val taskId = inputData.getLong(TID, 0)
        val app = applicationContext as App
        val tasks = app.tasks()

        tasks.tasksDao().active(taskId)
        withContext(Dispatchers.IO) {

            val task = tasks.tasksDao().task(taskId)
            val uri = task.uri.toUri()

            val urlCon = URL(uri.toString())

            val downloads = downloadsUri(
                applicationContext, task.mimeType, task.name,
                Environment.DIRECTORY_DOWNLOADS
            )
            checkNotNull(downloads)
            val contentResolver = applicationContext.contentResolver

            try {
                contentResolver.openOutputStream(downloads).use { os ->
                    checkNotNull(os)
                    HttpURLConnection.setFollowRedirects(false)

                    val huc = urlCon.openConnection() as HttpURLConnection

                    huc.readTimeout = 30000 // 30 sec
                    huc.connect()

                    huc.inputStream.use { fis ->
                        copy(fis, os, 0L, task.size) { progress: Int ->
                            check(!isStopped)
                            launch {
                                tasks.tasksDao().progress(taskId, progress / 100.0f)
                            }
                        }
                    }
                }
                tasks.tasksDao().finished(taskId, downloads.toString())
            } catch (throwable: Throwable) {
                contentResolver.delete(downloads, null, null)
                throw throwable
            }
        }

        if (isStopped) {
            CancelWorker.cancel(applicationContext, taskId)
            return Result.retry()
        }
        return Result.success()
    }

    companion object {
        private const val TID = "tid"

        private fun getWork(taskId: Long): OneTimeWorkRequest {
            val builder: Constraints.Builder = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)

            val data: Data.Builder = Data.Builder()
            data.putLong(TID, taskId)

            return OneTimeWorkRequestBuilder<DownloadFileWorker>()
                .addTag(DOWNLOADS_TAG)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build()
        }

        fun download(context: Context, taskId: Long): UUID {
            val work = getWork(taskId)
            WorkManager.getInstance(context).enqueueUniqueWork(
                taskId.toString(),
                ExistingWorkPolicy.KEEP, work
            )
            return work.id
        }
    }
}
