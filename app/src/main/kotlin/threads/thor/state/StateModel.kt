package threads.thor.state

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.Intent.CATEGORY_BROWSABLE
import android.graphics.Bitmap
import android.graphics.drawable.AdaptiveIconDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.webkit.CookieManager
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.net.toUri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.WorkManager
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.Peeraddr
import tech.lp2p.dark.createRequest
import tech.lp2p.dark.extractPeerId
import tech.lp2p.halo.Response
import tech.lp2p.loki.parseMagnetUri
import tech.lp2p.thor.data.Bookmark
import tech.lp2p.thor.data.Task
import tech.lp2p.thor.model.MimeType
import tech.lp2p.thor.model.homepage
import tech.lp2p.thor.model.homepageUri
import tech.lp2p.thor.model.removeHomepage
import threads.thor.App
import threads.thor.R
import threads.thor.model.DOWNLOADS_TAG
import threads.thor.model.Link
import threads.thor.model.MDNS
import threads.thor.model.bitmap
import threads.thor.model.bytesBitmap
import threads.thor.model.debug
import threads.thor.model.mdns
import threads.thor.model.mimeType
import threads.thor.model.workUUID
import threads.thor.ui.infoTitle
import threads.thor.work.CancelWorker
import threads.thor.work.DownloadContentWorker
import threads.thor.work.DownloadFileWorker
import threads.thor.work.DownloadMagnetWorker
import java.io.File
import java.util.concurrent.ConcurrentHashMap
import kotlin.concurrent.thread

class StateModel(private val application: Application) :
    AndroidViewModel(application) {


    private val locals = ConcurrentHashMap<PeerId, Peeraddr>()
    private val mdns: MDNS = mdns(application)

    val links = mutableStateListOf<Link>()
    var online: Boolean by mutableStateOf(true)


    init {
        debug("StateModel", "Init StateModel")
        mdns.startDiscovery(onServiceFound = { peeraddr: Peeraddr, name: String, icon: Long? ->
            locals[peeraddr.peerId] = peeraddr

            var title = name
            if (name.isEmpty()) {
                title = peeraddr.peerId.toBase58()
            }
            var bitmap: Bitmap? = null
            if (icon != null) {
                bitmap = loadIcon(peeraddr, icon)
            }

            val uri = createRequest(peeraddr)
            val link = Link(uri.toString(), title, bitmap)

            if (!links.contains(link)) {
                links.add(link)
            }

        }, onServiceLost = { peerId ->
            try {
                locals.remove(peerId)
                var found = mutableListOf<Link>()

                links.forEach { link ->
                    val uri = link.url.toUri()
                    if (uri.scheme == "pns") {
                        try {
                            val cmp = extractPeerId(link.url)
                            if (cmp == peerId) {
                                found.add(link)
                            }
                        } catch (throwable: Throwable) {
                            debug("StateModel", throwable)
                        }
                    }
                }
                if (found.isNotEmpty()) {
                    links.removeAll(found)
                }
            } catch (throwable: Throwable) {
                debug("StateModel", throwable)
            }
        })

        checkOnline()
        addThorLink()

    }

    fun activeTasks(): Flow<Boolean> {
        val app = application as App
        return app.tasks().tasksDao().active()
    }

    // todo maybe no blocking here
    private fun loadIcon(peeraddr: Peeraddr, icon: Long): Bitmap? = runBlocking {
        try {
            val app = application as App
            val request = createRequest(peeraddr, icon)
            val data = app.dark().fetchData(request)
            bitmap(data)
        } catch (throwable: Throwable) {
            debug("StateModel", throwable)
        }
        null
    }

    fun addThorLink() {
        try {
            val res = application.resources
            val theme = application.theme
            val adaptiveIcon = ResourcesCompat.getDrawable(
                res, R.mipmap.ic_launcher, theme
            ) as? AdaptiveIconDrawable

            val bitmap: Bitmap? = adaptiveIcon?.toBitmap()

            links.add(
                Link(
                    "https://gitlab.com/lp2p/thor",
                    application.getString(R.string.app_name),
                    bitmap
                )
            )
        } catch (throwable: Throwable) {
            debug("StateModel", throwable)
        }
    }

    override fun onCleared() {
        super.onCleared()
        mdns.stop()
    }

    fun tasks(pid: Long): Flow<List<Task>> {
        val app = application as App
        return app.tasks().tasksDao().tasks(pid)
    }


    fun tabIcon(url: String?): Int {
        if (url.isNullOrBlank()) {
            return R.drawable.https
        }
        val uri = url.toUri()
        return when (uri.scheme) {

            "pns" -> {
                R.drawable.pns
            }

            else -> {
                R.drawable.https
            }
        }
    }

    fun bookmarks(): Flow<List<Bookmark>> {
        val app = application as App
        return app.bookmarks().bookmarkDao().bookmarks()
    }

    fun cancelTask(task: Task) {
        val uuid = workUUID(task)
        if (uuid != null) {
            WorkManager.getInstance(application).cancelWorkById(uuid)
        }
        CancelWorker.cancel(application, task.id)
    }

    fun startTask(task: Task) {
        val app = application as App
        viewModelScope.launch {
            val uri = task.uri.toUri()

            val scheme = uri.scheme
            if (scheme == "magnet") {
                app.tasks().tasksDao().active(task.id)
                val uuid = DownloadMagnetWorker.download(application, task.id)
                app.tasks().tasksDao().work(task.id, uuid.toString())
            } else if (scheme == "pns") {
                app.tasks().tasksDao().active(task.id)
                val uuid = DownloadContentWorker.download(application, task.id)
                app.tasks().tasksDao().work(task.id, uuid.toString())
            } else if (scheme == "http") {
                app.tasks().tasksDao().active(task.id)
                val uuid = DownloadFileWorker.download(application, task.id)
                app.tasks().tasksDao().work(task.id, uuid.toString())
            } else if (scheme == "https") {
                app.tasks().tasksDao().active(task.id)
                val uuid = DownloadFileWorker.download(application, task.id)
                app.tasks().tasksDao().work(task.id, uuid.toString())
            }
        }
    }

    fun hasBookmark(url: String?): Flow<Boolean> {
        val app = application as App
        return app.bookmarks().bookmarkDao().hasBookmark(url)
    }

    fun alterBookmark(
        url: String, title: String?, bitmap: Bitmap?,
        onWarningRequest: (String) -> Unit = {}
    ) {
        val app = application as App
        viewModelScope.launch {
            var bookmark = app.bookmarks().bookmarkDao().bookmark(url)
            val bookmarkTitle = webViewTitle(url, title)
            if (bookmark != null) {
                app.bookmarks().bookmarkDao().delete(bookmark)
                onWarningRequest.invoke(
                    application.getString(
                        R.string.bookmark_removed, bookmarkTitle
                    )
                )

            } else {
                bookmark = Bookmark(0, url, bookmarkTitle, bytesBitmap(bitmap))
                app.bookmarks().bookmarkDao().insert(bookmark)
                onWarningRequest.invoke(
                    application.getString(
                        R.string.bookmark_added, bookmarkTitle
                    )
                )

            }
        }
    }


    fun removeBookmark(bookmark: Bookmark) {
        val app = application as App
        viewModelScope.launch {
            app.bookmarks().bookmarkDao().delete(bookmark)
        }
    }


    private fun local(peerId: PeerId?): Peeraddr {
        return checkNotNull(locals[peerId])
    }

    private fun hasLocal(peerId: PeerId?): Boolean {
        return locals.containsKey(peerId)
    }


    // todo maybe no blocking here
    fun response(uri: Uri, onWarningRequest: (String) -> Unit): Response = runBlocking {
        val app = application as App

        try {

            val peerId = extractPeerId(uri.toString())

            // check if local is available
            if (hasLocal(peerId)) {
                // local uri contains peeraddr (so direct access)
                app.dark().reachable(local(peerId))
            } else {
                onWarningRequest.invoke(application.getString(R.string.loading_page))
            }
        } catch (throwable: Throwable) {
            debug("StateModel", throwable)
        }

        app.dark().response(uri.toString())

    }


    internal val isNetworkConnected: Boolean
        get() {
            val connectivityManager = application.getSystemService(
                Context.CONNECTIVITY_SERVICE
            ) as ConnectivityManager
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw)
            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))
        }

    fun reset() {
        // cancel all jobs
        WorkManager.getInstance(application).cancelAllWorkByTag(DOWNLOADS_TAG)
        thread { // todo check again

            // clean tasks
            val app = application as App
            app.tasks().clearAllTables()

            // Clear all the cookies
            CookieManager.getInstance().removeAllCookies(null)
            CookieManager.getInstance().flush()

            // Clear cache data
            cleanCache()

            // clean all downloads
            WorkManager.getInstance(application).pruneWork()
        }
    }

    private fun cleanCache() {
        cleanCache(application.cacheDir, false)
    }


    private fun cleanCache(file: File, deleteWhenDir: Boolean) {
        try {
            if (file.isDirectory) {
                val children = file.listFiles()
                if (children != null) {
                    for (child in children) {
                        cleanCache(child, true)
                    }
                }
                if (deleteWhenDir) {
                    file.delete()
                }
            } else {
                file.delete()
            }
        } catch (throwable: Throwable) {
            debug("Provider", throwable)
        }
    }


    fun checkOnline() {
        this.online = isNetworkConnected
    }

    fun contentDownloader(uri: Uri, name: String) {
        val app = application as App
        viewModelScope.launch {
            val mimeType = mimeType(name)
            val taskId = app.tasks().tasksDao().insert(
                Task(
                    0, 0, name, mimeType,
                    uri.toString(), 0, null,
                    active = true,
                    finished = false,
                    progress = 0f
                )
            )

            val uuid = DownloadContentWorker.download(application, taskId)
            app.tasks().tasksDao().work(taskId, uuid.toString())
        }
    }

    fun magnetDownloader(uri: Uri) {
        val app = application as App
        viewModelScope.launch {

            val magnetUri = parseMagnetUri(uri.toString())

            var name = uri.toString()
            if (magnetUri.displayName != null) {
                name = checkNotNull(magnetUri.displayName)
            }

            val taskId = app.tasks().tasksDao().insert(
                Task(
                    0, 0, name, MimeType.DIR_MIME_TYPE.name,
                    uri.toString(), -1, null,
                    active = true,
                    finished = false,
                    progress = 0f
                )
            )
            val uuid = DownloadMagnetWorker.download(application, taskId)
            app.tasks().tasksDao().work(taskId, uuid.toString())

        }
    }

    fun fileDownloader(
        uri: Uri, filename: String, mimeType: String, size: Long
    ) {
        val app = application as App
        viewModelScope.launch {
            val taskId = app.tasks().tasksDao().insert(
                Task(
                    0, 0, filename, mimeType, uri.toString(), size,
                    null,
                    active = true,
                    finished = false,
                    progress = 0f
                )
            )
            val uuid = DownloadFileWorker.download(application, taskId)
            app.tasks().tasksDao().work(taskId, uuid.toString())
        }
    }


    fun resetHomepage() {
        viewModelScope.launch {
            val app = application as App
            removeHomepage(app.datastore())
        }
    }

    fun homePage(uri: String, title: String?, bitmap: Bitmap?) {
        viewModelScope.launch {
            val app = application as App
            homepage(app.datastore(), uri, infoTitle(title, uri), bytesBitmap(bitmap))
        }
    }


    fun homepageUri(default: String): Flow<String> {
        val app = application as App
        return homepageUri(app.datastore(), default)
    }

    fun pruneWork() {
        WorkManager.getInstance(application).pruneWork()
        val app = application as App
        viewModelScope.launch {
            app.tasks().tasksDao().purge()
        }
    }

    private fun webViewTitle(url: String, webViewTitle: String?): String {
        var title = webViewTitle
        if (title == null) {
            title = url.toUri().authority ?: ""
        }
        return title
    }


    fun invokeTask(task: Task, onWarningRequest: (String) -> Unit = {}) {
        try {
            val intent = Intent(Intent.ACTION_VIEW, task.uri.toUri())
            intent.addCategory(CATEGORY_BROWSABLE)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            application.startActivity(intent)
        } catch (_: Throwable) {
            onWarningRequest.invoke(
                application.getString(R.string.no_activity_found_to_handle_uri)
            )
        }
    }


    fun isAd(uri: Uri): Boolean {
        return threads.thor.model.isAd(application, uri)
    }

    fun removeTask(task: Task) {
        val uuid = workUUID(task)
        if (uuid != null) {
            WorkManager.getInstance(application).cancelWorkById(uuid)
        }
        val app = application as App
        viewModelScope.launch {
            app.tasks().tasksDao().delete(task)
        }
    }

}