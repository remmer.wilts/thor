package threads.thor.model

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdManager.DiscoveryListener
import android.net.nsd.NsdManager.ServiceInfoCallback
import android.net.nsd.NsdServiceInfo
import android.os.Build
import androidx.annotation.RequiresApi
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.Peeraddr
import tech.lp2p.lite.createPeeraddr
import tech.lp2p.lite.decodePeerId
import java.net.InetAddress
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.io.encoding.ExperimentalEncodingApi

class MDNS(private val nsdManager: NsdManager) {
    @Volatile
    private var discoveryService: DiscoveryService? = null

    fun startDiscovery(
        onServiceFound: (Peeraddr, String, Long?) -> Unit,
        onServiceLost: (PeerId) -> Unit
    ) {
        try {
            discoveryService = DiscoveryService(nsdManager, onServiceFound, onServiceLost)

            nsdManager.discoverServices(
                MDNS_SERVICE, NsdManager.PROTOCOL_DNS_SD,
                discoveryService
            )
        } catch (throwable: Throwable) {
            debug("MDNS", throwable)
        }
    }

    fun stop() {
        try {
            if (discoveryService != null) {
                nsdManager.stopServiceDiscovery(discoveryService)
            }
        } catch (throwable: Throwable) {
            debug("MDNS", throwable) // not ok when fails
        }
    }


    private data class DiscoveryService(
        val nsdManager: NsdManager,
        val onServiceFound: (Peeraddr, String, Long?) -> Unit,
        val onServiceLost: (PeerId) -> Unit,
    ) : DiscoveryListener {
        override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
            debug("MDNS", "onStartDiscoveryFailed $serviceType")
        }

        override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
            debug("MDNS", "onStopDiscoveryFailed $serviceType")
        }

        override fun onDiscoveryStarted(serviceType: String) {
            debug("MDNS", "onDiscoveryStarted $serviceType")
        }

        override fun onDiscoveryStopped(serviceType: String) {
            debug("MDNS", "onDiscoveryStopped $serviceType")
        }

        override fun onServiceFound(serviceInfo: NsdServiceInfo) {
            debug("MDNS", "onServiceFound ")

            if (Build.VERSION.SDK_INT >= 34) {
                nsdManager.registerServiceInfoCallback(
                    serviceInfo, EXECUTOR,
                    object : ServiceInfoCallback {
                        override fun onServiceInfoCallbackRegistrationFailed(i: Int) {
                            debug("MDNS", "onServiceInfoCallbackRegistrationFailed $i")
                        }

                        override fun onServiceUpdated(nsdServiceInfo: NsdServiceInfo) {
                            evaluateHosts(nsdServiceInfo)
                        }

                        override fun onServiceLost() {
                            debug("MDNS", "onServiceLost")

                            try {
                                val peerId = decodePeerId(serviceInfo.serviceName)
                                onServiceLost.invoke(peerId)
                            } catch (throwable: Throwable) {
                                debug("MDNS", throwable) // is ok when it fails
                            }
                        }

                        override fun onServiceInfoCallbackUnregistered() {
                            debug("MDNS", "onServiceInfoCallbackUnregistered")
                        }
                    })
            } else {
                nsdManager.resolveService(serviceInfo, object : NsdManager.ResolveListener {
                    override fun onResolveFailed(nsdServiceInfo: NsdServiceInfo, i: Int) {
                        debug("MDNS", "onResolveFailed $nsdServiceInfo")
                    }

                    override fun onServiceResolved(nsdServiceInfo: NsdServiceInfo) {
                        evaluateHost(nsdServiceInfo)
                    }
                })
            }
        }

        override fun onServiceLost(serviceInfo: NsdServiceInfo) {
            debug("MDNS", "onServiceLost $serviceInfo")

            try {
                val peerId = decodePeerId(serviceInfo.serviceName)
                onServiceLost.invoke(peerId)
            } catch (throwable: Throwable) {
                debug("MDNS", throwable) // is ok when it fails
            }

        }

        @OptIn(ExperimentalEncodingApi::class)
        private fun evaluateHost(serviceInfo: NsdServiceInfo) {
            try {
                val peerId = decodePeerId(serviceInfo.serviceName)
                val inetAddress = serviceInfo.host

                val peeraddr = createPeeraddr(
                    peerId, inetAddress.address, serviceInfo.port.toUShort()
                )


                var data = serviceInfo.attributes["name"]
                var name = ""
                if (data != null) {
                    name = String(data)
                }
                var icon: Long? = null
                data = serviceInfo.attributes["icon"]

                try {
                    if (data != null) {
                        icon = java.lang.Long.parseLong(String(data))
                    }
                } catch (throwable: Throwable) {
                    debug("MDNS", throwable)
                }
                onServiceFound.invoke(peeraddr, name, icon)
            } catch (throwable: Throwable) {
                debug("MDNS", throwable) /// is ok when it fails
            }
        }

        @OptIn(ExperimentalEncodingApi::class)
        @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
        private fun evaluateHosts(serviceInfo: NsdServiceInfo) {
            try {
                val peerId = decodePeerId(serviceInfo.serviceName)

                val peeraddrs = mutableListOf<Peeraddr>()
                for (inetAddress in serviceInfo.hostAddresses) {
                    val peeraddr = createPeeraddr(
                        peerId, inetAddress.address, serviceInfo.port.toUShort()
                    )

                    peeraddrs.add(peeraddr)
                }

                var data = serviceInfo.attributes["name"]
                var name = ""
                if (data != null) {
                    name = data.toString(Charsets.UTF_8)
                }

                var icon: Long? = null
                data = serviceInfo.attributes["icon"]

                try {
                    if (data != null) {
                        icon = data.toString(Charsets.UTF_8).toLong()
                    }
                } catch (throwable: Throwable) {
                    debug("MDNS", throwable)
                }


                if (!peeraddrs.isEmpty()) {
                    val peeraddr = bestReachablePeeraddr(peeraddrs)
                    if (peeraddr != null) {
                        onServiceFound.invoke(peeraddr, name, icon)
                    }
                }
            } catch (throwable: Throwable) {
                debug("MDNS", throwable) // is ok, fails for kubo
            }
        }

    }
}

const val MDNS_SERVICE: String = "_p2p._udp." // default libp2p [or kubo] service name

@RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
private val EXECUTOR: ExecutorService = Executors.newSingleThreadExecutor()


private fun removeLoopbacks(peeraddrs: MutableList<Peeraddr>) {
    val iterator = peeraddrs.iterator()

    while (iterator.hasNext()) {
        val peeraddr = iterator.next()

        if (peeraddrs.size == 1) {
            return
        }
        try {
            val inetAddress = InetAddress.getByAddress(peeraddr.address)
            if (inetAddress.isLoopbackAddress) {
                iterator.remove()
            }
        } catch (_: Throwable) {
            iterator.remove()
        }
    }
}

private fun removeLinkLocals(peeraddrs: MutableList<Peeraddr>) {
    val iterator = peeraddrs.iterator()

    while (iterator.hasNext()) {
        val peeraddr = iterator.next()

        if (peeraddrs.size == 1) {
            return
        }
        try {
            val inetAddress = InetAddress.getByAddress(peeraddr.address)
            if (inetAddress.isLinkLocalAddress) {
                iterator.remove()
            }
        } catch (_: Throwable) {
            iterator.remove()
        }
    }
}

private fun removeSiteLocals(peeraddrs: MutableList<Peeraddr>) {
    val iterator = peeraddrs.iterator()

    while (iterator.hasNext()) {
        val peeraddr = iterator.next()

        if (peeraddrs.size == 1) {
            return
        }
        try {
            val inetAddress = InetAddress.getByAddress(peeraddr.address)
            if (inetAddress.isSiteLocalAddress) {
                iterator.remove()
            }
        } catch (_: Throwable) {
            iterator.remove()
        }
    }
}

fun bestReachablePeeraddr(peeraddrs: MutableList<Peeraddr>): Peeraddr? {
    if (peeraddrs.size > 1) {
        removeLoopbacks(peeraddrs)
    }
    if (peeraddrs.size > 1) {
        removeLinkLocals(peeraddrs)
    }
    if (peeraddrs.size > 1) {
        removeSiteLocals(peeraddrs)
    }

    for (peer in peeraddrs) {
        val inetAddress = InetAddress.getByAddress(peer.address)
        if (inetAddress.isReachable(200)) { // only reachable
            return peer
        }
    }

    if (peeraddrs.isNotEmpty()) {
        return peeraddrs.first()
    }

    return null
}

fun mdns(context: Context): MDNS {
    val nsdManager = context.getSystemService(Context.NSD_SERVICE) as NsdManager
    return MDNS(nsdManager)
}