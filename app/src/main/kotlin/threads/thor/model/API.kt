package threads.thor.model

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.webkit.MimeTypeMap
import android.webkit.WebResourceResponse
import androidx.core.net.toUri
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tech.lp2p.loki.parseMagnetUri
import tech.lp2p.thor.data.Bookmark
import tech.lp2p.thor.data.Task
import tech.lp2p.thor.data.Tasks
import tech.lp2p.thor.model.MimeType
import threads.thor.R
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.io.OutputStream
import java.net.Inet6Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.Stack
import java.util.UUID


const val QWANT = "https://www.qwant.com/"

fun mediaResource(mimeType: String): Int {
    if (mimeType.isNotEmpty()) {
        if (mimeType == MimeType.TORRENT_MIME_TYPE.name) {
            return R.drawable.arrow_up_down_bold
        }
        if (mimeType == MimeType.OCTET_MIME_TYPE.name) {
            return R.drawable.file_star
        }
        if (mimeType == MimeType.JSON_MIME_TYPE.name) {
            return R.drawable.json
        }
        if (mimeType == MimeType.PLAIN_MIME_TYPE.name) {
            return R.drawable.file
        }
        if (mimeType == MimeType.PDF_MIME_TYPE.name) {
            return R.drawable.pdf
        }
        if (mimeType == MimeType.OPEN_EXCEL_MIME_TYPE.name) {
            return R.drawable.microsoft_excel
        }
        if (mimeType == MimeType.CSV_MIME_TYPE.name) {
            return R.drawable.microsoft_excel
        }
        if (mimeType == MimeType.EXCEL_MIME_TYPE.name) {
            return R.drawable.microsoft_excel
        }
        if (mimeType == MimeType.WORD_MIME_TYPE.name) {
            return R.drawable.microsoft_word
        }
        if (mimeType.startsWith(MimeType.TEXT.name)) {
            return R.drawable.file
        }
        if (mimeType == MimeType.DIR_MIME_TYPE.name) {
            return R.drawable.folder
        }
        if (mimeType.startsWith(MimeType.VIDEO.name)) {
            return R.drawable.movie_outline
        }
        if (mimeType.startsWith(MimeType.IMAGE.name)) {
            return R.drawable.camera
        }
        if (mimeType.startsWith(MimeType.AUDIO.name)) {
            return R.drawable.audio
        }
        if (mimeType.startsWith(MimeType.PGP_KEYS_MIME_TYPE.name)) {
            return R.drawable.fingerprint
        }
        if (mimeType.startsWith(MimeType.APPLICATION.name)) {
            return R.drawable.application
        }

        return R.drawable.settings
    }

    return R.drawable.help
}


suspend fun uploadDirectory(
    context: Context,
    tasks: Tasks,
    taskId: Long,
    dir: File,
    dirs: Stack<String>,
    uuid: UUID
) {
    dirs.add(dir.name)
    val children = dir.list()

    if (children != null) {
        for (child in children) {
            val fileChild = File(dir, child)
            if (fileChild.isDirectory) {
                val subTaskId = tasks.createOrGet(
                    taskId, fileChild.name,
                    MimeType.DIR_MIME_TYPE.name, fileChild.toURI().toString(),
                    fileChild.length(), uuid.toString()
                )
                tasks.tasksDao().active(subTaskId)
                uploadDirectory(context, tasks, subTaskId, fileChild, dirs, uuid)
            } else {
                val subTaskId = tasks.createOrGet(
                    taskId, fileChild.name,
                    mimeType(fileChild.name), fileChild.toURI().toString(),
                    fileChild.length(), uuid.toString()
                )
                tasks.tasksDao().active(subTaskId)
                uploadFile(context, tasks, subTaskId, fileChild, dirs)
            }
        }
    }
    dirs.pop()

    val mimeType = mimeType(dir.name)
    val path = Environment.DIRECTORY_DOWNLOADS + File.separator + java.lang.String.join(
        (File.separator), dirs
    )
    val targetUri = downloadsUri(context, mimeType, dir.name, path)
    checkNotNull(targetUri)
    tasks.tasksDao().finished(taskId, targetUri.toString())
}

suspend fun uploadFile(
    context: Context, tasks: Tasks, taskId: Long,
    file: File, dirs: Stack<String>
) {
    val name = file.name
    checkNotNull(name)

    val mimeType = mimeType(name)
    val path = Environment.DIRECTORY_DOWNLOADS + File.separator + java.lang.String.join(
        (File.separator), dirs
    )

    val targetUri = downloadsUri(context, mimeType, name, path)
    checkNotNull(targetUri)
    val contentResolver = context.contentResolver

    try {
        withContext(Dispatchers.IO) {
            FileInputStream(file).use { fis ->
                contentResolver.openOutputStream(targetUri).use { os ->
                    checkNotNull(os)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        fis.transferTo(os)
                    } else {
                        copy(fis, os)
                    }
                }
            }
        }
        tasks.tasksDao().finished(taskId, targetUri.toString())
    } catch (throwable: Throwable) {
        contentResolver.delete(targetUri, null, null)
        throw throwable
    }
}

fun copy(source: InputStream, sink: OutputStream) {
    val buf = ByteArray(Short.MAX_VALUE.toInt())
    var n: Int
    while ((source.read(buf).also { n = it }) > 0) {
        sink.write(buf, 0, n)
    }
}

fun mimeType(name: String): String {
    val mimeType = evaluateMimeType(name)
    if (mimeType != null) {
        return mimeType
    }
    return MimeType.OCTET_MIME_TYPE.name
}

private fun getExtension(filename: String): String {
    if (filename.contains(".")) {
        return filename.substring(filename.lastIndexOf('.') + 1)
    }
    return ""
}


private fun evaluateMimeType(filename: String): String? {
    try {
        val extension = getExtension(filename)
        if (extension.isNotEmpty()) {
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            if (mimeType != null) {
                return mimeType
            }
        } else {
            // no extension directory assumed
            return MimeType.DIR_MIME_TYPE.name
        }
    } catch (throwable: Throwable) {
        debug(TAG, throwable)
    }
    return null
}

const val DOWNLOADS_TAG: String = "downloads"
private const val NO_NAME = "download-file.bin"
const val CONTENT_DOWNLOAD: String = "Content-Download"


/*
const val DUCKDUCKGO = "https://start.duckduckgo.com/"
const val GOOGLE = "https://www.google.com/"
const val ECOSIA = "https://www.ecosia.org/"
const val BING = "https://www.bing.com/"
)*/


private const val TAG: String = "API"


fun getFileName(uri: Uri): String {
    val paths = uri.pathSegments
    return if (paths.isNotEmpty()) {
        paths[paths.size - 1]
    } else {
        NO_NAME
    }
}

// todo evaluate the function again
fun getUriTitle(url: String?): String {
    if (url.isNullOrBlank()) {
        return ""
    }
    val uri = url.toUri()
    val scheme = uri.scheme
    if (scheme == "magnet") {
        val magnetUri = parseMagnetUri(uri.toString())

        var name = uri.toString()
        if (magnetUri.displayName != null) {
            name = magnetUri.displayName!!
        }
        return name
    } else if (scheme == "pns") {
        val name = uri.getQueryParameter(CONTENT_DOWNLOAD)
        if (!name.isNullOrBlank()) {
            return name
        }
        val host = checkNotNull(uri.host)
        return host
    } else {
        val host = uri.host
        if (!host.isNullOrBlank()) {
            return host
        } else {
            val paths = uri.pathSegments
            return if (paths.isNotEmpty()) {
                paths[paths.size - 1]
            } else {
                ""
            }
        }
    }
}


fun createEmptyResource(): WebResourceResponse {
    return WebResourceResponse(
        MimeType.PLAIN_MIME_TYPE.name,
        Charsets.UTF_8.name(),
        ByteArrayInputStream(byteArrayOf())
    )
}


fun downloadsUri(context: Context, mimeType: String, name: String, path: String): Uri? {
    val contentValues = ContentValues()
    debug(TAG, "$mimeType $name $path")
    contentValues.put(MediaStore.Downloads.DISPLAY_NAME, name)
    contentValues.put(MediaStore.Downloads.MIME_TYPE, mimeType)
    contentValues.put(MediaStore.Downloads.RELATIVE_PATH, path)

    val contentResolver = context.contentResolver
    return contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues)
}


fun bitmap(icon: ByteArray?): Bitmap? {
    var bitmap: Bitmap? = null
    try {
        if (icon != null) {
            bitmap = BitmapFactory.decodeByteArray(icon, 0, icon.size)
        }
    } catch (throwable: Throwable) {
        debug("Preferences", throwable)
    }
    return bitmap
}


fun workUUID(task: Task): UUID? {
    if (task.work != null) {
        return UUID.fromString(task.work)
    }
    return null
}

fun bytesBitmap(bitmap: Bitmap?): ByteArray? {
    if (bitmap == null) {
        return null
    }
    val config = checkNotNull(bitmap.config)
    val copy = bitmap.copy(config, true)
    val stream = ByteArrayOutputStream()
    copy.compress(Bitmap.CompressFormat.PNG, 100, stream)
    val byteArray = stream.toByteArray()
    copy.recycle()
    return byteArray
}


@Suppress("SameReturnValue")
private val isDebug: Boolean get() = true


fun debug(tag: String, message: String) {
    if (isDebug) {
        Log.e(tag, message)
    }
}


fun debug(tag: String, throwable: Throwable) {
    if (isDebug) {
        Log.e(tag, throwable.localizedMessage, throwable)
    }
}

data class Link(val url: String, val title: String, val bitmap: Bitmap? = null) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Link

        return url == other.url
    }

    override fun hashCode(): Int {
        return url.hashCode()
    }
}


internal fun isLanAddress(inetAddress: InetAddress): Boolean {
    return inetAddress.isAnyLocalAddress
            || inetAddress.isLinkLocalAddress
            || (inetAddress.isLoopbackAddress)
            || (inetAddress.isSiteLocalAddress)
}


internal fun publicAddresses(): List<InetAddress> {
    val inetAddresses: MutableList<InetAddress> = ArrayList()

    try {
        val interfaces = NetworkInterface.getNetworkInterfaces()

        for (networkInterface in interfaces) {
            if (networkInterface.isUp) {
                val addresses = networkInterface.inetAddresses
                for (inetAddress in addresses) {
                    if (inetAddress is Inet6Address) {
                        if (!isLanAddress(inetAddress)) {
                            inetAddresses.add(inetAddress)
                        }
                    }
                }
            }
        }
    } catch (throwable: Throwable) {
        throw IllegalStateException(throwable)
    }
    return inetAddresses
}

fun iconBitmap(bookmark: Bookmark): Bitmap? {
    return bitmap(bookmark.icon)
}


// Reservation feature is only possible when a public Inet6Address is available
fun reservationFeaturePossible(): Boolean {
    return publicAddresses().isNotEmpty()
}
