package threads.thor.ui

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.content.Intent.CATEGORY_BROWSABLE
import android.content.Intent.makeMainActivity
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Message
import android.print.PrintAttributes
import android.print.PrintManager
import android.provider.DocumentsContract
import android.view.View
import android.view.ViewGroup
import android.webkit.ConsoleMessage
import android.webkit.CookieManager
import android.webkit.HttpAuthHandler
import android.webkit.JsPromptResult
import android.webkit.PermissionRequest
import android.webkit.SslErrorHandler
import android.webkit.ValueCallback
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.History
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Share
import androidx.compose.material.icons.outlined.Bookmarks
import androidx.compose.material.icons.outlined.Downloading
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.Print
import androidx.compose.material.icons.outlined.SearchOff
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material.icons.outlined.StarOutline
import androidx.compose.material3.BottomSheetDefaults
import androidx.compose.material3.BottomSheetScaffold
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.SheetValue
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.contentColorFor
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.material3.rememberBottomSheetScaffoldState
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.material3.rememberStandardBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.graphics.createBitmap
import androidx.core.net.toUri
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.launch
import tech.lp2p.thor.Stream
import tech.lp2p.thor.model.MimeType
import tech.lp2p.thor.parseContentDisposition
import threads.thor.R
import threads.thor.model.CONTENT_DOWNLOAD
import threads.thor.model.QWANT
import threads.thor.model.createEmptyResource
import threads.thor.model.debug
import threads.thor.model.getFileName
import threads.thor.model.reservationFeaturePossible
import threads.thor.state.StateModel

class Activity : ComponentActivity() {

    @Composable
    fun KeepScreenOn() {
        val currentView = LocalView.current
        DisposableEffect(Unit) {
            currentView.keepScreenOn = true
            onDispose {
                currentView.keepScreenOn = false
            }
        }
    }


    fun network(stateModel: StateModel, connectivityManager: ConnectivityManager) {
        val networkCallback: NetworkCallback =
            object : NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    stateModel.online = true
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    stateModel.online = false
                }
            }

        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
            .build()

        connectivityManager.registerNetworkCallback(networkRequest, networkCallback)

    }

    @Composable
    fun AppTheme(useDarkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
        val context = LocalContext.current

        val lightingColorScheme = lightColorScheme()

        val colorScheme =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                if (useDarkTheme)
                    dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
            } else {
                lightingColorScheme
            }

        MaterialTheme(
            colorScheme = colorScheme,
            content = content
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        enableEdgeToEdge()

        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager

        setContent {
            val stateModel: StateModel = viewModel()
            network(stateModel, connectivityManager)
            KeepScreenOn()

            AppTheme {
                Box(modifier = Modifier.safeDrawingPadding()) {
                    MainView(stateModel, savedInstanceState)
                }
            }
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun MainView(stateModel: StateModel, savedInstanceState: Bundle?) {

        var url by remember { mutableStateOf("") }
        val homepage by stateModel.homepageUri(QWANT).collectAsState("")

        val uri by remember { mutableStateOf(intentUri(intent)) }
        LaunchedEffect(uri) {
            if (uri != null) {
                url = uri!!.toString()
            }
        }
        if (uri == null) {
            LaunchedEffect(homepage.isNotEmpty()) {
                url = homepage
            }
        }

        if (url.isNotEmpty()) {
            WebPage(stateModel, savedInstanceState, url)
        }
    }

    private fun instance() {
        try {
            val intent = makeMainActivity(ComponentName(this, threads.thor.ui.Activity::class.java))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
            startActivity(intent)
        } catch (throwable: Throwable) {
            debug("share", throwable)
        }
    }

    private fun share(url: String) {
        try {
            val names = arrayOf(ComponentName(this, threads.thor.ui.Activity::class.java))

            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link))
            intent.putExtra(Intent.EXTRA_TEXT, url)
            intent.setType(MimeType.PLAIN_MIME_TYPE.name)
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)


            val chooser = Intent.createChooser(intent, getText(R.string.share))
            chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names)
            startActivity(chooser)
        } catch (throwable: Throwable) {
            debug("share", throwable)
        }
    }


    private fun createPrintJob(webView: WebView) {
        try {
            val printManager = getSystemService(PRINT_SERVICE) as PrintManager
            val jobName = webView.title ?: application.getString(android.R.string.untitled)
            val printDocumentAdapter = webView.createPrintDocumentAdapter(jobName)
            printManager.print(
                jobName, printDocumentAdapter,
                PrintAttributes.Builder().build()
            )
        } catch (throwable: Throwable) {
            debug("createPrintJob", throwable)
        }
    }


    private inner class CustomWebViewClient(
        private val stateModel: StateModel,
        private val onDownloadRequest: () -> Unit = {},
        private val onAuthRequest: (AuthRequest) -> Unit = {},
        private val onWarningRequest: (String) -> Unit = {}
    ) : AccompanistWebViewClient() {

        private val adsUrls: MutableMap<Uri, Boolean> = HashMap()


        override fun onReceivedHttpError(
            view: WebView, request: WebResourceRequest,
            errorResponse: WebResourceResponse
        ) {
            super.onReceivedHttpError(view, request, errorResponse)
            debug("Activity", "onReceivedHttpError " + errorResponse.reasonPhrase)
        }

        override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
            super.onReceivedSslError(view, handler, error)
            debug("Activity", "onReceivedSslError $error")
        }


        override fun onPageCommitVisible(view: WebView, url: String) {
            super.onPageCommitVisible(view, url)
            debug("Activity", "onPageCommitVisible $url")
        }

        override fun onReceivedHttpAuthRequest(
            view: WebView,
            handler: HttpAuthHandler,
            host: String,
            realm: String
        ) {
            super.onReceivedHttpAuthRequest(view, handler, host, realm)
            onAuthRequest.invoke(AuthRequest(handler, host, realm))
        }


        override fun onLoadResource(view: WebView, url: String) {
            super.onLoadResource(view, url)
            debug("Activity", "onLoadResource : $url")
        }

        override fun doUpdateVisitedHistory(view: WebView, url: String, isReload: Boolean) {
            super.doUpdateVisitedHistory(view, url, isReload)
            debug("Activity", "doUpdateVisitedHistory : $url $isReload")
        }

        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            debug("Activity", "onPageStarted : $url")
        }


        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            debug("Activity", "onPageFinished : $url")
            stateModel.checkOnline()
        }

        override fun onReceivedError(
            view: WebView,
            request: WebResourceRequest,
            error: WebResourceError
        ) {
            super.onReceivedError(view, request, error)
            debug(
                "Activity", "onReceivedError " + view.url + " " +
                        error.description + " " + error.errorCode
            )
        }


        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            try {
                val uri = request.url

                debug("Activity", "shouldOverrideUrlLoading : $uri")

                when (uri.scheme) {
                    "about" -> {
                        debug("Activity", "about scheme $uri")
                        return true
                    }

                    "magnet" -> {
                        stateModel.magnetDownloader(uri)
                        onDownloadRequest.invoke()
                        return true
                    }

                    "pns" -> {
                        val name = uri.getQueryParameter(CONTENT_DOWNLOAD)
                        if (name != null) {
                            stateModel.contentDownloader(uri, name)
                            onDownloadRequest.invoke()
                            return true
                        }
                        return false
                    }

                    "http", "https" -> {
                        return false
                    }

                    else -> {
                        // all other stuff
                        try {
                            val intent = Intent(Intent.ACTION_VIEW, uri)
                            intent.addCategory(CATEGORY_BROWSABLE)
                            // todo support intent.addFlags(Intent.FLAG_ACTIVITY_REQUIRE_NON_BROWSER)
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        } catch (_: Throwable) {
                            debug("Activity", "Not handled uri $uri")
                        }
                        return true
                    }
                }
            } catch (throwable: Throwable) {
                debug("Activity", throwable)
            }

            return false
        }


        override fun shouldInterceptRequest(
            view: WebView,
            request: WebResourceRequest
        ): WebResourceResponse? {
            val uri = request.url

            debug("Activity", "shouldInterceptRequest : $uri")

            if (uri.scheme == "http" || uri.scheme == "https") {
                var advertisement = false
                if (!adsUrls.containsKey(uri)) {
                    advertisement = stateModel.isAd(uri)
                    adsUrls[uri] = advertisement
                } else {
                    val value = adsUrls[uri]
                    if (value != null) {
                        advertisement = value
                    }
                }

                return if (advertisement) {
                    createEmptyResource()
                } else {
                    null
                }
            } else if (uri.scheme == "pns") {


                val response = stateModel.response(uri, onWarningRequest)
                if (response.status == 200) {
                    return WebResourceResponse(
                        response.mimeType, response.encoding, response.status,
                        response.reason, response.headers, Stream(response.channel)
                    )
                } else {
                    var issueMsg = false
                    if (stateModel.isNetworkConnected) {
                        if (!reservationFeaturePossible()) {
                            issueMsg = true

                            onWarningRequest(
                                getString(R.string.pns_service_limitation_ip)
                            )

                        }
                    }
                    if (!issueMsg) {
                        val msg = response.reason
                        if (msg.isNotBlank()) {
                            onWarningRequest(msg)
                        }
                    }
                    return WebResourceResponse(
                        response.mimeType, response.encoding, response.status,
                        response.reason, response.headers, Stream(response.channel)
                    )

                }

            }
            return null
        }
    }

    private inner class CustomWebChromeClient(
        private val onFilePickerRequest: (ValueCallback<Array<Uri>>) -> Unit = {}
    ) : AccompanistWebChromeClient() {
        private var mCustomView: View? = null
        private var mCustomViewCallback: CustomViewCallback? = null


        override fun onHideCustomView() {
            super.onHideCustomView()


            (this@Activity.window.decorView as FrameLayout).removeView(this.mCustomView)
            this.mCustomView = null

            mCustomViewCallback!!.onCustomViewHidden()
            this.mCustomViewCallback = null
        }

        override fun onCreateWindow(
            view: WebView,
            dialog: Boolean,
            userGesture: Boolean,
            resultMsg: Message
        ): Boolean {
            val result = view.hitTestResult
            val data = result.extra
            val context = view.context
            val browserIntent =
                Intent(Intent.ACTION_VIEW, data?.toUri(), context, Activity::class.java)
            context.startActivity(browserIntent)
            return false
        }

        override fun onRequestFocus(view: WebView) {
            debug("Activity", "onRequestFocus " + view.url)
        }

        override fun onCloseWindow(window: WebView) {
            debug("Activity", "onCloseWindow ")
        }

        override fun onJsPrompt(
            view: WebView, url: String, message: String,
            defaultValue: String, result: JsPromptResult
        ): Boolean {
            debug("Activity", "onJsPrompt ")
            return false
        }

        override fun onShowCustomView(
            paramView: View,
            paramCustomViewCallback: CustomViewCallback
        ) {
            super.onShowCustomView(paramView, paramCustomViewCallback)


            this.mCustomView = paramView
            this.mCustomViewCallback = paramCustomViewCallback
            (this@Activity.window.decorView as FrameLayout)
                .addView(
                    this.mCustomView, FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
                    )
                )

        }

        override fun getDefaultVideoPoster(): Bitmap {
            return createBitmap(10, 10)
        }

        override fun onReceivedTitle(view: WebView, title: String) {
            super.onReceivedTitle(view, title)
            debug("Activity", "onReceivedTitle " + title + " " + view.url)
        }


        override fun onReceivedTouchIconUrl(
            view: WebView, url: String,
            precomposed: Boolean
        ) {
            debug("Activity", "onReceivedTouchIconUrl")
        }

        override fun onReceivedIcon(view: WebView, icon: Bitmap) {
            super.onReceivedIcon(view, icon)
            debug("Activity", "onReceivedIcon " + view.url)
        }

        override fun onShowFileChooser(
            webView: WebView,
            filePathCallback: ValueCallback<Array<Uri>>,
            fileChooserParams: FileChooserParams
        ): Boolean {
            onFilePickerRequest.invoke(filePathCallback)
            return true
        }

        override fun onPermissionRequest(request: PermissionRequest) {
            debug("Activity", "onPermissionRequest $request")
            request.deny()
            return
        }

        override fun onPermissionRequestCanceled(request: PermissionRequest) {
            super.onPermissionRequestCanceled(request)
            debug("Activity", "onPermissionRequestCanceled $request")
        }

        override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
            debug("Activity", "onConsoleMessage " + consoleMessage.message())
            return super.onConsoleMessage(consoleMessage)
        }

        override fun getVideoLoadingProgressView(): View? {
            debug("Activity", "getVideoLoadingProgressView ")
            return null
        }

        override fun getVisitedHistory(callback: ValueCallback<Array<String>>) {
            debug("Activity", "getVisitedHistory ")
        }

    }

    @OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
    @Composable
    fun WebPage(
        stateModel: StateModel,
        savedInstanceState: Bundle?,
        initPage: String
    ) {
        val webViewState = rememberSaveableWebViewState()
        webViewState.viewState = savedInstanceState
        val webViewNavigator = rememberWebViewNavigator()


        var filePickRequest: ValueCallback<Array<Uri>>? by remember { mutableStateOf(null) }

        val filePickerLauncher = rememberLauncherForActivityResult(
            ActivityResultContracts.GetContent()
        ) { uri ->
            if (filePickRequest != null) {
                if (uri != null) {
                    filePickRequest!!.onReceiveValue(arrayOf(uri))
                } else {
                    filePickRequest!!.onReceiveValue(null)
                }
            }
            filePickRequest = null
        }
        if (filePickRequest != null) {
            filePickerLauncher.launch(MimeType.ALL.name)
        }

        var authRequest: AuthRequest? by remember { mutableStateOf(null) }
        if (authRequest != null) {
            HttpAuthDialog(
                authRequest!!,
                onDismissRequest = { authRequest = null })
        }

        val hasActiveTasks by stateModel.activeTasks().collectAsState(false)

        val hasBookmark by stateModel
            .hasBookmark(webViewState.lastLoadedUrl)
            .collectAsState(false)

        LaunchedEffect(stateModel.online) {
            webViewState.webView?.setNetworkAvailable(stateModel.online)
        }


        val snackbarHostState = remember { SnackbarHostState() }
        var warning: String by remember { mutableStateOf("") }

        if (warning.isNotBlank()) {
            LaunchedEffect(warning) {
                snackbarHostState.showSnackbar(
                    message = warning,
                    duration = SnackbarDuration.Short
                )
            }
        }
        val links = remember { stateModel.links }

        var showResetDialog: Boolean by remember { mutableStateOf(false) }
        var showInfo: Boolean by remember { mutableStateOf(false) }
        var showHistory: Boolean by remember { mutableStateOf(false) }
        var showBookmarks: Boolean by remember { mutableStateOf(false) }
        var showTasks: Boolean by remember { mutableStateOf(false) }
        var findInPage: Boolean by remember { mutableStateOf(false) }


        val scope = rememberCoroutineScope()
        val bottomSheetState = rememberStandardBottomSheetState(
            initialValue = SheetValue.Expanded
        )

        val scaffoldState = rememberBottomSheetScaffoldState(
            bottomSheetState = bottomSheetState,
            snackbarHostState = snackbarHostState
        )

        val isVisible: Boolean = WindowInsets.ime.getBottom(LocalDensity.current) > 0

        LaunchedEffect(key1 = isVisible) {

            if (isVisible) {
                scaffoldState.bottomSheetState.partialExpand()
            } else {
                scaffoldState.bottomSheetState.expand()
                findInPage = false
            }
        }


        BottomSheetScaffold(
            sheetPeekHeight = 0.dp,
            scaffoldState = scaffoldState,
            snackbarHost = {
                SnackbarHost(
                    hostState = snackbarHostState
                )
            },
            sheetContainerColor = Color.Black.copy(0.3f),
            sheetShape = BottomSheetDefaults.ExpandedShape,
            sheetSwipeEnabled = false,
            sheetContent = {
                val scrollState = rememberScrollState()
                val primaryAction = MaterialTheme.colorScheme.primary
                val secondaryAction = MaterialTheme.colorScheme.secondary
                val tertiaryAction = MaterialTheme.colorScheme.tertiary


                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                        .horizontalScroll(scrollState, enabled = true)
                ) {

                    FloatingActionButton(
                        containerColor = primaryAction,
                        contentColor = contentColorFor(primaryAction),
                        modifier = Modifier.padding(8.dp),
                        onClick = {
                            scope.launch {
                                stateModel.homepageUri(QWANT).collect { value ->
                                    webViewNavigator.stopLoading()
                                    webViewNavigator.loadUrl(value)
                                }
                            }
                        }) {
                        Icon(
                            imageVector = Icons.Outlined.Home,
                            contentDescription = stringResource(id = R.string.home_page),
                        )
                    }

                    FloatingActionButton(
                        modifier = Modifier.padding(8.dp),
                        containerColor = secondaryAction,
                        contentColor = contentColorFor(secondaryAction),
                        onClick = {
                            showBookmarks = true
                        }) {
                        Icon(
                            imageVector = Icons.Outlined.Bookmarks,
                            contentDescription = stringResource(id = R.string.bookmarks)
                        )
                    }





                    FloatingActionButton(
                        containerColor = tertiaryAction,
                        contentColor = contentColorFor(tertiaryAction),
                        modifier = Modifier.padding(8.dp),
                        onClick = { showInfo = true }) {
                        Icon(
                            painter = painterResource(
                                id = stateModel.tabIcon(webViewState.lastLoadedUrl)
                            ),
                            contentDescription = stringResource(id = R.string.information)
                        )
                    }

                    FloatingActionButton(
                        modifier = Modifier.padding(8.dp),
                        containerColor = primaryAction,
                        contentColor = contentColorFor(primaryAction),
                        onClick = {
                            instance()
                        }) {
                        Icon(
                            imageVector = Icons.Default.Add,
                            contentDescription = stringResource(id = R.string.add_tab)
                        )
                    }


                    FloatingActionButton(
                        modifier = Modifier.padding(8.dp),
                        containerColor = secondaryAction,
                        contentColor = contentColorFor(secondaryAction),
                        onClick = {
                            showHistory = true
                        }) {
                        Icon(
                            Icons.Filled.History,
                            contentDescription = stringResource(id = R.string.history)
                        )
                    }

                    var downloadColor = secondaryAction
                    if (hasActiveTasks) {
                        downloadColor = primaryAction
                    }

                    FloatingActionButton(
                        containerColor = downloadColor,
                        contentColor = contentColorFor(downloadColor),
                        modifier = Modifier.padding(8.dp),
                        onClick = { showTasks = true }) {
                        Icon(
                            imageVector = Icons.Outlined.Downloading,
                            contentDescription = stringResource(id = R.string.downloads)
                        )
                    }



                    AnimatedVisibility(webViewNavigator.canGoBack) {
                        FloatingActionButton(
                            containerColor = tertiaryAction,
                            contentColor = contentColorFor(tertiaryAction),
                            modifier = Modifier.padding(8.dp),
                            onClick = { webViewNavigator.navigateBack() }
                        ) {
                            Icon(
                                Icons.AutoMirrored.Filled.ArrowBack,
                                contentDescription = stringResource(id = R.string.previous_page)
                            )
                        }
                    }

                    FloatingActionButton(
                        containerColor = tertiaryAction,
                        contentColor = contentColorFor(tertiaryAction),
                        modifier = Modifier.padding(8.dp),
                        onClick = { webViewNavigator.reload() }) {
                        Icon(
                            imageVector = Icons.Default.Refresh,
                            contentDescription = stringResource(id = R.string.reload_page)
                        )
                    }

                    AnimatedVisibility(webViewNavigator.canGoForward) {
                        FloatingActionButton(
                            containerColor = tertiaryAction,
                            contentColor = contentColorFor(tertiaryAction),
                            modifier = Modifier.padding(8.dp),
                            onClick = { webViewNavigator.navigateForward() }
                        ) {
                            Icon(
                                imageVector = Icons.AutoMirrored.Filled.ArrowForward,
                                contentDescription = stringResource(id = R.string.next_page)
                            )
                        }
                    }

                    AnimatedVisibility(
                        webViewState.loadingState == LoadingState.Finished
                                && canBookmark(webViewState.lastLoadedUrl)
                    ) {
                        FloatingActionButton(

                            modifier = Modifier.padding(8.dp),
                            onClick = {
                                stateModel.alterBookmark(
                                    webViewState.lastLoadedUrl!!,
                                    webViewState.pageTitle,
                                    webViewState.pageIcon,
                                    onWarningRequest = { warning = it }
                                )
                            }) {
                            Icon(
                                imageVector =
                                    if (hasBookmark) {
                                        Icons.Outlined.Star
                                    } else {
                                        Icons.Outlined.StarOutline
                                    },
                                contentDescription = stringResource(id = R.string.bookmark)
                            )
                        }
                    }

                    FloatingActionButton(

                        modifier = Modifier.padding(8.dp),
                        onClick = { findInPage = true }) {
                        Icon(
                            imageVector = Icons.Outlined.SearchOff,
                            contentDescription = stringResource(id = R.string.find_page)
                        )
                    }

                    AnimatedVisibility(webViewState.loadingState == LoadingState.Finished) {
                        FloatingActionButton(

                            modifier = Modifier.padding(8.dp),
                            onClick = {
                                share(cleanUrl(webViewState.lastLoadedUrl))
                            }) {
                            Icon(
                                Icons.Filled.Share,
                                contentDescription = stringResource(id = R.string.share)
                            )
                        }
                    }

                    FloatingActionButton(
                        modifier = Modifier.padding(8.dp),

                        onClick = {
                            createPrintJob(webViewState.webView!!)
                        }) {
                        Icon(
                            Icons.Outlined.Print,
                            contentDescription = stringResource(id = R.string.print)
                        )
                    }

                    FloatingActionButton(
                        modifier = Modifier.padding(8.dp),
                        containerColor = MaterialTheme.colorScheme.errorContainer,
                        contentColor = MaterialTheme.colorScheme.onErrorContainer,
                        onClick = {
                            showResetDialog = true
                        }) {
                        Icon(
                            painterResource(id = R.drawable.broom),
                            contentDescription = stringResource(id = R.string.action_cleanup)
                        )
                    }
                    links.forEach { link ->

                        FloatingActionButton(
                            modifier = Modifier.padding(8.dp),
                            containerColor = MaterialTheme.colorScheme.secondaryContainer,
                            contentColor = MaterialTheme.colorScheme.onSecondaryContainer,
                            onClick = {
                                webViewNavigator.stopLoading()
                                webViewNavigator.loadUrl(link.url)
                            }) {
                            if (link.bitmap != null) {
                                Image(
                                    bitmap = link.bitmap.asImageBitmap(),
                                    contentDescription = link.url,
                                    modifier = Modifier.size(24.dp)
                                )
                            } else {
                                Icon(
                                    painterResource(id = R.drawable.view),
                                    contentDescription = link.url,
                                )
                            }
                        }

                    }
                }

            },
            topBar = {
                if (!findInPage) {
                    if (webViewState.isLoading) {
                        LinearProgressIndicator(
                            modifier = Modifier.fillMaxWidth(),
                            progress = { webViewState.progress }
                        ) {}
                    }

                    AnimatedVisibility(
                        visible = !stateModel.online,
                        enter = slideInVertically() + expandVertically(expandFrom = Alignment.Top),
                        exit = slideOutVertically() + shrinkVertically()
                    ) {
                        Text(
                            text = stringResource(id = R.string.offline_mode),
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(MaterialTheme.colorScheme.errorContainer),
                            maxLines = 1,
                            color = MaterialTheme.colorScheme.onErrorContainer,
                            style = MaterialTheme.typography.labelLarge,
                            textAlign = TextAlign.Center
                        )
                    }
                }
            }) { padding ->

            Box(
                modifier = Modifier
                    .padding(padding)
                    .fillMaxSize()
            ) {

                WebView(
                    modifier = Modifier.matchParentSize(),
                    state = webViewState,
                    navigator = webViewNavigator,
                    chromeClient = remember {
                        CustomWebChromeClient(
                            onFilePickerRequest = {
                                filePickRequest = it
                            })
                    },
                    client = remember {
                        CustomWebViewClient(
                            stateModel = stateModel,
                            onDownloadRequest = { showTasks = true },
                            onAuthRequest = { authRequest = it },
                            onWarningRequest = { warning = it })
                    },
                    onSwipeDown = {
                        findInPage = false
                        scope.launch {
                            scaffoldState.bottomSheetState.expand()
                        }
                    },
                    onSwipeUp = {
                        findInPage = false
                        scope.launch {
                            scaffoldState.bottomSheetState.partialExpand()
                        }
                    },
                    onInitPage = {
                        initPage
                    },
                    onCreated = { view ->

                        CookieManager.getInstance().setAcceptThirdPartyCookies(view, false)


                        view.setDownloadListener { downloadUrl: String,
                                                   userAgent: String?,
                                                   contentDisposition: String?,
                                                   mimeType: String,
                                                   contentLength: Long ->
                            try {
                                // todo use external downloader
                                val uri = downloadUrl.toUri()

                                var filename = parseContentDisposition(contentDisposition)

                                if (filename.isNullOrEmpty()) {
                                    filename = getFileName(uri)
                                }

                                stateModel.fileDownloader(
                                    uri, filename, mimeType, contentLength
                                )
                                showTasks = true

                            } catch (throwable: Throwable) {
                                debug("Activity", throwable)
                            }
                        }
                    },
                    onBackPressed = {
                        scope.launch {
                            findInPage = false
                            scaffoldState.bottomSheetState.expand()
                        }
                    })

                if (findInPage) {
                    Box(
                        contentAlignment = Alignment.TopCenter,
                        modifier = Modifier
                            .align(Alignment.TopCenter)
                            .fillMaxWidth()
                    ) {
                        FindInPageView(webViewState.webView!!)
                    }
                }
            }
        }




        if (showTasks) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = true
            )
            ModalBottomSheet(
                containerColor = Color.Black.copy(0.3f),
                modifier = Modifier
                    .fillMaxHeight()
                    .safeDrawingPadding(),
                sheetState = sheetState,
                onDismissRequest = { showTasks = false }) {

                TasksView(
                    stateModel,
                    onWarningRequest = { warning = it })
            }
        }


        if (showBookmarks) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = true
            )
            ModalBottomSheet(
                containerColor = Color.Black.copy(0.3f),
                modifier = Modifier
                    .fillMaxHeight()
                    .safeDrawingPadding(),
                sheetState = sheetState,
                onDismissRequest = { showBookmarks = false }) {

                BookmarksView(stateModel, onShowRequest = { bookmark ->
                    webViewNavigator.stopLoading()
                    webViewNavigator.loadUrl(bookmark.url)
                    showBookmarks = false
                })
            }
        }

        if (showHistory) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = true
            )
            ModalBottomSheet(
                containerColor = Color.Black.copy(0.3f),
                modifier = Modifier
                    .fillMaxHeight()
                    .safeDrawingPadding(),
                sheetState = sheetState,
                onDismissRequest = { showHistory = false }) {
                val list = transform(
                    webViewState.webView!!.copyBackForwardList()
                )
                HistoriesView(
                    list,
                    onShowRequest = { history ->
                        webViewNavigator.stopLoading()
                        webViewNavigator.loadUrl(history.url)
                        showHistory = false
                    }
                )
            }
        }
        if (showResetDialog) {
            ResetDialog(
                onResetRequest = {
                    // clear web view data
                    webViewState.webView!!.clearHistory()
                    webViewState.webView!!.clearCache(true)
                    webViewState.webView!!.clearFormData()
                    stateModel.reset()
                },
                onDismissRequest = {
                    showResetDialog = false
                })
        }




        if (showInfo) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = true
            )
            ModalBottomSheet(
                containerColor = Color.Black.copy(0.3f),
                modifier = Modifier
                    .safeDrawingPadding(),
                sheetState = sheetState,
                onDismissRequest = { showInfo = false }) {
                InfoView(stateModel, webViewState)
            }
        }

    }
}


data class AuthRequest(val handler: HttpAuthHandler, val host: String, val realm: String)