package threads.thor.ui

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.webkit.WebView
import threads.thor.model.debug
import kotlin.math.abs

class NestedWebView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = android.R.attr.webViewStyle,
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {}
) : WebView(
    context!!, attrs, defStyleAttr
) {


    val gd = GestureDetector(context!!, object : GestureDetector.OnGestureListener {
        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onShowPress(e: MotionEvent) {

        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            return false
        }

        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            return false
        }

        override fun onLongPress(e: MotionEvent) {

        }

        override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent,
            velocityX: Float,
            velocityY: Float
        ): Boolean {

            val distanceX: Float = e2.x - (e1?.x ?: e2.x)
            val distanceY: Float = e2.y - (e1?.y ?: e2.y)
            if (abs(distanceX) > abs(distanceY) &&
                abs(distanceX) > SWIPE_DISTANCE &&
                abs(velocityX) > SWIPE_VELOCITY
            ) {
                if (distanceX > 0) {
                    debug("NestedWebView", "Swipe right")
                } else {
                    debug("NestedWebView", "Swipe left")
                }
                return true
            }
            if (abs(distanceY) > abs(distanceX) &&
                abs(distanceY) > SWIPE_DISTANCE &&
                abs(velocityY) > SWIPE_VELOCITY
            ) {
                if (distanceY > 0) {
                    debug("NestedWebView", "Swipe down")
                    onSwipeDown.invoke()
                } else {
                    debug("NestedWebView", "Swipe up")
                    onSwipeUp.invoke()
                }
                return true
            }

            return false
        }

    })

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        gd.onTouchEvent(event)
        return super.onTouchEvent(event)
    }


}

const val SWIPE_DISTANCE: Int = 100
const val SWIPE_VELOCITY: Int = 100