package threads.thor.ui

import android.webkit.WebView
import android.webkit.WebViewDatabase
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.BottomSheetDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import threads.thor.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ResetDialog(onResetRequest: () -> Unit, onDismissRequest: () -> Unit) {
    BasicAlertDialog(onDismissRequest = { onDismissRequest.invoke() }) {
        Card(
            elevation = CardDefaults.cardElevation(
                defaultElevation = 6.dp
            )
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
            ) {
                Text(
                    text = stringResource(id = R.string.warning),
                    style = MaterialTheme.typography.titleLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )
                Text(
                    text = stringResource(id = R.string.browser_cleanup_message),
                    style = MaterialTheme.typography.bodyMedium,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth()
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp, 0.dp)
                ) {
                    Button(onClick = { onDismissRequest.invoke() }) {
                        Text(
                            text = stringResource(id = android.R.string.cancel),
                        )
                    }
                    Box(modifier = Modifier.weight(1.0f, true))
                    Button(onClick = {
                        onResetRequest.invoke()
                        onDismissRequest.invoke()
                    }
                    ) {
                        Text(
                            text = stringResource(id = android.R.string.ok),
                        )
                    }
                }
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HttpAuthDialog(
    authRequest: AuthRequest,
    onDismissRequest: () -> Unit
) {

    val database = WebViewDatabase.getInstance(LocalContext.current)
    val data = database.getHttpAuthUsernamePassword(authRequest.host, authRequest.realm)


    var storedName: String by remember {
        mutableStateOf(
            if (data != null) {
                data[0]
            } else {
                ""
            }
        )
    }
    var storedPass: String by remember {
        mutableStateOf(
            if (data != null) {
                data[1]
            } else {
                ""
            }
        )
    }

    BasicAlertDialog(onDismissRequest) {

        Card(
            elevation = CardDefaults.cardElevation(
                defaultElevation = 6.dp
            )
        ) {
            val focusRequester = remember { FocusRequester() }
            LaunchedEffect(focusRequester) {
                focusRequester.requestFocus()
            }

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                TextField(

                    modifier = Modifier
                        .focusRequester(focusRequester)
                        .fillMaxWidth(),
                    label = {
                        Text(text = stringResource(R.string.user_name))
                    },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        autoCorrectEnabled = true,
                        keyboardType = KeyboardType.Text,
                    ),
                    value = storedName, maxLines = 1, onValueChange = { value ->
                        storedName = value
                    })

                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = {
                        Text(text = stringResource(R.string.password))
                    },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        autoCorrectEnabled = true,
                        keyboardType = KeyboardType.Password
                    ),
                    value = storedPass, maxLines = 1, onValueChange = { value ->
                        storedPass = value
                    })
                Row(
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                ) {
                    Button(onClick = {
                        authRequest.handler.cancel()
                        onDismissRequest.invoke()
                    }) {
                        Text(
                            text = stringResource(id = android.R.string.cancel),
                        )
                    }
                    Box(modifier = Modifier.weight(1.0f, true))
                    Button(onClick = {
                        database.setHttpAuthUsernamePassword(
                            authRequest.host, authRequest.realm, storedName, storedPass
                        )

                        authRequest.handler.proceed(storedName, storedPass)
                        onDismissRequest.invoke()
                    }
                    ) {
                        Text(
                            text = stringResource(id = android.R.string.ok),
                        )
                    }
                }
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FindInPageView(webView: WebView) {

    var numbers by remember { mutableStateOf("0/0") }
    var text by remember { mutableStateOf("") }
    val backgroundColor = MaterialTheme.colorScheme.primaryContainer
    val textColor = MaterialTheme.colorScheme.onPrimaryContainer

    val focusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current

    LaunchedEffect(focusRequester) {
        focusRequester.requestFocus()
    }

    Row(
        modifier = Modifier
            .shadow(
                BottomSheetDefaults.Elevation,
                shape = MaterialTheme.shapes.large.copy(
                    topStart = CornerSize(0.dp),
                    topEnd = CornerSize(0.dp)
                )
            )
            .widthIn(16.dp, BottomSheetDefaults.SheetMaxWidth)
            .width(BottomSheetDefaults.SheetMaxWidth)
            .background(backgroundColor)

    ) {

        TextField(
            colors = TextFieldDefaults.colors(
                focusedTextColor = textColor,
                unfocusedTextColor = textColor,
                focusedContainerColor = backgroundColor,
                unfocusedContainerColor = backgroundColor,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent,
            ),
            textStyle = MaterialTheme.typography.titleSmall,

            modifier = Modifier
                .padding(8.dp)
                .focusRequester(focusRequester)
                .weight(1.0f, true),

            placeholder = {
                Text(
                    text = stringResource(R.string.enter_search),
                    style = MaterialTheme.typography.titleSmall,
                    color = textColor
                )
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                autoCorrectEnabled = true,
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(onDone = {
                focusManager.clearFocus()
            }),
            value = text,
            maxLines = 1,
            onValueChange = { value ->
                text = value
                webView.findAllAsync(value)
            })

        Text(
            modifier = Modifier
                .padding(8.dp)
                .minimumInteractiveComponentSize()
                .align(Alignment.CenterVertically),
            style = MaterialTheme.typography.titleSmall,
            text = numbers,
            color = textColor
        )


        IconButton(
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .minimumInteractiveComponentSize(),
            onClick = {
                webView.findNext(false)
            }) {
            Icon(
                painter = painterResource(id = R.drawable.chevron_up),
                contentDescription = stringResource(id = R.string.previous_search),
                tint = textColor
            )
        }

        IconButton(
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .minimumInteractiveComponentSize(),
            onClick = {
                webView.findNext(true)
            }) {
            Icon(
                painter = painterResource(id = R.drawable.chevron_down),
                contentDescription = stringResource(id = R.string.next_search),
                tint = textColor
            )
        }

    }


    DisposableEffect(focusRequester) {
        webView.setFindListener { activeMatchOrdinal: Int, numberOfMatches: Int, _: Boolean ->
            numbers = "$activeMatchOrdinal/$numberOfMatches"
        }
        onDispose {
            webView.clearMatches()
            webView.setFindListener(null)
        }
    }


}