package threads.thor.ui

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.outlined.RestartAlt
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SwipeToDismissBox
import androidx.compose.material3.SwipeToDismissBoxState
import androidx.compose.material3.SwipeToDismissBoxValue
import androidx.compose.material3.Text
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.material3.rememberSwipeToDismissBoxState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import tech.lp2p.thor.data.Task
import tech.lp2p.thor.model.MimeType
import threads.thor.R
import threads.thor.model.mediaResource
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TasksView(
    stateModel: StateModel,
    onWarningRequest: (String) -> Unit = {}
) {
    var pid: Long by remember { mutableLongStateOf(0L) }
    val tasks by stateModel.tasks(pid).collectAsState(emptyList())
    val stack = remember { mutableStateListOf<Long>() }
    val threshold = .5f

    Scaffold(
        floatingActionButton = {
            if (!stack.isEmpty()) {
                FloatingActionButton(
                    containerColor = MaterialTheme.colorScheme.secondary,
                    contentColor = MaterialTheme.colorScheme.onSecondary,
                    modifier = Modifier.padding(8.dp),
                    onClick = {
                        pid = stack.removeAt(stack.size - 1)
                    }) {
                    Icon(
                        imageVector = Icons.AutoMirrored.Default.ArrowBack,
                        contentDescription = stringResource(R.string.previous_page)
                    )
                }
            } else {
                FloatingActionButton(
                    containerColor = MaterialTheme.colorScheme.secondary,
                    contentColor = MaterialTheme.colorScheme.onSecondary,
                    modifier = Modifier.padding(8.dp),
                    onClick = {
                        stateModel.pruneWork()
                    }) {
                    Icon(
                        painter = painterResource(id = R.drawable.broom),
                        contentDescription = stringResource(R.string.cleanup_downloads)
                    )
                }
            }
        },
        topBar = {
            Text(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.secondary)
                    .fillMaxWidth(),
                style = MaterialTheme.typography.titleLarge,
                textAlign = TextAlign.Center,
                text = stringResource(id = R.string.downloads),
                color = MaterialTheme.colorScheme.onSecondary
            )

        }, content = { padding ->
            LazyColumn(modifier = Modifier.padding(padding)) {
                items(
                    items = tasks,
                    key = { task ->
                        task.id
                    }) { task ->

                    var dismissState: SwipeToDismissBoxState? = null

                    dismissState = rememberSwipeToDismissBoxState(
                        confirmValueChange = { value ->
                            if (value == SwipeToDismissBoxValue.EndToStart
                                && dismissState!!.progress > threshold
                            ) {
                                stateModel.removeTask(task)
                                true
                            } else {
                                false
                            }
                        },
                        positionalThreshold = { it * threshold }
                    )

                    SwipeToDismissBox(
                        modifier = Modifier.animateContentSize(),
                        state = dismissState,
                        backgroundContent = {

                            Box(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .background(MaterialTheme.colorScheme.error),
                                contentAlignment = Alignment.CenterEnd

                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Delete,
                                    modifier = Modifier.padding(8.dp),
                                    contentDescription = stringResource(id = android.R.string.cancel),
                                    tint = MaterialTheme.colorScheme.onError
                                )
                            }
                        },
                        enableDismissFromEndToStart = !task.active,
                        enableDismissFromStartToEnd = false,
                    ) {
                        TaskItem(
                            stateModel, task, onWarningRequest,
                            onDirectoryRequest = {
                                stack.add(pid)
                                pid = task.id
                            })
                    }
                }
            }
        }
    )
}

@Composable
fun TaskItem(
    stateModel: StateModel,
    task: Task,
    onWarningRequest: (String) -> Unit = {},
    onDirectoryRequest: (Task) -> Unit = {}
) {

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(enabled = task.finished) {
                when (task.mimeType) {
                    MimeType.DIR_MIME_TYPE.name -> {
                        onDirectoryRequest.invoke(task)
                    }

                    else -> {
                        stateModel.invokeTask(task, onWarningRequest)
                    }
                }
            }
            .background(MaterialTheme.colorScheme.surface)
    ) {
        Row(
            modifier = Modifier
                .padding(0.dp, 8.dp)
                .fillMaxWidth()
        ) {
            Box(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .size(48.dp)
            ) {
                Icon(
                    painter = painterResource(id = mediaResource(task.mimeType)),
                    contentDescription = stringResource(id = android.R.string.untitled),
                    modifier = Modifier.align(Alignment.Center),
                    tint = MaterialTheme.colorScheme.secondary
                )

                if (task.active) {
                    if (task.progress > 0) {
                        CircularProgressIndicator(
                            progress = {
                                task.progress
                            },
                            strokeWidth = 2.dp,
                            modifier = Modifier.align(Alignment.Center)
                        )
                    } else {
                        CircularProgressIndicator(
                            strokeWidth = 2.dp,
                            modifier = Modifier.align(Alignment.Center)
                        )
                    }
                }
            }

            Text(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f, true),
                text = task.name,
                maxLines = 2,
                style = MaterialTheme.typography.labelLarge,
                softWrap = true
            )

            if (!task.finished) {
                IconButton(
                    modifier = Modifier.align(Alignment.CenterVertically),
                    onClick = {
                        if (task.active) {
                            stateModel.cancelTask(task)
                        } else {
                            stateModel.startTask(task)
                        }
                    }) {
                    Icon(
                        imageVector =
                            if (task.active) {
                                Icons.Default.Pause
                            } else {
                                Icons.Outlined.RestartAlt
                            },
                        contentDescription = stringResource(id = android.R.string.untitled),

                        )
                }
            } else {
                Spacer(modifier = Modifier.minimumInteractiveComponentSize())
            }
        }
    }
}