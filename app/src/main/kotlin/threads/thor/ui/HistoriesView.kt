package threads.thor.ui

import android.webkit.WebHistoryItem
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import threads.thor.R
import threads.thor.model.getUriTitle


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HistoriesView(
    list: List<WebHistoryItem>,
    onShowRequest: (WebHistoryItem) -> Unit = {},
) {

    Scaffold(
        topBar = {
            Text(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.secondary)
                    .fillMaxWidth(),
                style = MaterialTheme.typography.titleLarge,
                textAlign = TextAlign.Center,
                text = stringResource(id = R.string.history),
                color = MaterialTheme.colorScheme.onSecondary
            )
        }, content = { padding ->
            LazyColumn(
                modifier = Modifier.padding(padding)
            ) {
                items(items = list) { item ->
                    WebHistoryItem(item, onShowRequest)
                }
            }
        }
    )
}

@Composable
fun WebHistoryItem(
    history: WebHistoryItem,
    onShowRequest: (WebHistoryItem) -> Unit = {},
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.surface)
            .clickable {
                onShowRequest.invoke(history)
            },
    ) {
        Row(
            modifier = Modifier
                .padding(12.dp)
                .fillMaxWidth()
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .size(48.dp)
                    .background(
                        color = MaterialTheme.colorScheme.secondaryContainer,
                        shape = RoundedCornerShape(30.dp)
                    )

            ) {
                if (history.favicon != null) {
                    Image(
                        bitmap = history.favicon!!.asImageBitmap(),
                        contentDescription = stringResource(id = R.string.history),
                        modifier = Modifier.size(24.dp)
                    )
                } else {
                    Icon(
                        painter = painterResource(id = R.drawable.view),
                        contentDescription = stringResource(id = R.string.history),
                        modifier = Modifier.size(24.dp)
                    )
                }
            }
            Column(modifier = Modifier.absolutePadding(16.dp)) {
                Text(
                    text = if (history.title.isNullOrBlank()) {
                        getUriTitle(history.url)
                    } else {
                        history.title
                    },
                    maxLines = 1,
                    modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.bodyMedium, softWrap = false
                )
                Text(
                    text = cleanUrl(history.url), maxLines = 2,
                    modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.labelSmall, softWrap = false
                )
            }
        }
    }
}
