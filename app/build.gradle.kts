import java.io.FileInputStream
import java.util.Properties

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.ksp)
    alias(libs.plugins.kotlin.compose)
}


val keystoreProperties = Properties()
val keystorePropertiesFile = rootProject.file("release-keystore.properties")
if (keystorePropertiesFile.exists()) {
    keystoreProperties.load(FileInputStream(keystorePropertiesFile))
}

android {
    compileSdk = 35
    namespace = "threads.thor"

    signingConfigs {
        create("release") {
            keyAlias = keystoreProperties["keyAlias"].toString()
            keyPassword = keystoreProperties["keyPassword"].toString()
            storeFile = keystoreProperties["storeFile"]?.let { file(it) }
            storePassword = keystoreProperties["storePassword"].toString()
        }
    }
    defaultConfig {
        applicationId = "threads.thor"
        minSdk = 29
        targetSdk = 35
        versionCode = 176
        versionName = "1.7.6"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_21
        targetCompatibility = JavaVersion.VERSION_21
    }
    kotlinOptions {
        jvmTarget = "21"
    }

    buildFeatures {
        compose = true
    }
}


dependencies {
    implementation(project(":shared"))

    // Android (LICENSE done)
    implementation(libs.androidx.material.icons.core)

    // Android (LICENSE done)
    implementation(libs.androidx.material.icons.extended)

    // Android (LICENSE done)
    implementation(libs.androidx.material3)

    // Android (LICENSE done)
    implementation(libs.androidx.lifecycle.viewmodel.compose)

    // Android (LICENSE done)
    implementation(libs.androidx.activity.compose)

    // Android (LICENSE done)
    implementation(libs.androidx.datastore.preferences.core)

    // Android (LICENSE done)
    implementation(libs.androidx.datastore.preferences)

    // Google API (LICENSE done)
    implementation(libs.core)

    // API (LICENSE done)
    implementation(libs.kotlinx.io.core)

    // Android (LICENSE done)
    implementation(libs.androidx.work.runtime)

    // Android (LICENSE done)
    implementation(libs.androidx.room.ktx)

    // API (LICENSE done)
    implementation(libs.atomicfu)

    // Testing
    androidTestImplementation(libs.kotlin.test)
    androidTestImplementation(libs.androidx.core)
    androidTestImplementation(libs.androidx.runner)
    androidTestImplementation(libs.androidx.espresso.core)
}
