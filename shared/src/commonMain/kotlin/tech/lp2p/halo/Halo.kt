package tech.lp2p.halo

import kotlinx.io.Buffer
import kotlinx.io.Sink
import kotlinx.io.Source
import kotlinx.io.buffered
import kotlinx.io.files.Path
import kotlinx.io.files.SystemFileSystem
import kotlinx.io.files.SystemTemporaryDirectory
import tech.lp2p.halo.core.Fid
import tech.lp2p.halo.core.FidChannel
import tech.lp2p.halo.core.Raw
import tech.lp2p.halo.core.RawChannel
import tech.lp2p.halo.core.createRaw
import tech.lp2p.halo.core.decodeNode
import tech.lp2p.halo.core.fetchData
import tech.lp2p.halo.core.removeNode
import tech.lp2p.halo.core.storeSource
import kotlin.concurrent.Volatile
import kotlin.random.Random
import kotlin.uuid.ExperimentalUuidApi
import kotlin.uuid.Uuid


interface Channel {
    fun size(): Long
    fun seek(offset: Long)
    suspend fun transferTo(sink: Sink, read: (Int) -> Unit)
    suspend fun readAllBytes(): ByteArray
    suspend fun next(): Buffer?
}

data class Response(
    val mimeType: String,
    val encoding: String,
    val status: Int,
    val reason: String,
    val headers: Map<String, String>,
    val channel: Channel
)

const val MAX_CHARS_SIZE = 4096
const val SPLITTER_SIZE = UShort.MAX_VALUE
const val HALO_ROOT = 0L


interface Node {
    fun cid(): Long
    fun size(): Long
    fun name(): String
    fun mimeType(): String
}

interface Halo : Fetch {

    fun reset()

    fun delete()

    fun directory(): Path

    fun hasBlock(cid: Long): Boolean

    override suspend fun fetchBlock(cid: Long): Buffer

    fun deleteBlock(cid: Long)

    fun nextCid(): Long

    fun storeBlock(cid: Long, buffer: Buffer)

    fun root(data: ByteArray)

    fun root(node: Node)

    fun root(): Node

    fun tempFile(): Path

    suspend fun response(request: Long?): Response {
        val cid = request ?: root().cid()

        // from here should be part of halo
        val node = info(cid)
        return if (node is Fid) {
            val channel = channel(node)
            contentResponse(channel, node)
        } else {
            val channel = channel(node)
            contentResponse(channel, "OK", 200)
        }
    }

    suspend fun info(cid: Long): Node {
        val block = fetchBlock(cid)
        return decodeNode(cid, block)
    }

    // Note: remove the cid block (add all links blocks recursively)
    fun delete(node: Node) {
        removeNode(this, node)
    }

    fun storeData(data: ByteArray): Node {
        return createRaw(this, data) {
            nextCid()
        }
    }

    fun storeText(data: String): Node {
        return storeData(data.encodeToByteArray())
    }

    fun storeFile(path: Path, mimeType: String): Node {
        require(SystemFileSystem.exists(path)) { "Path does not exists" }
        val metadata = SystemFileSystem.metadataOrNull(path)
        checkNotNull(metadata) { "Path has no metadata" }
        require(metadata.isRegularFile) { "Path is not a regular file" }
        require(mimeType.isNotBlank()) { "MimeType is blank" }
        SystemFileSystem.source(path).buffered().use { source ->
            return storeSource(source, path.name, mimeType)
        }

    }

    suspend fun transferTo(node: Node, path: Path) {
        SystemFileSystem.sink(path, false).buffered().use { sink ->
            channel(node).transferTo(sink) {}
        }
    }

    fun channel(node: Node): Channel {
        return createChannel(node, this)
    }

    fun storeSource(source: Source, name: String, mimeType: String): Node {
        return storeSource(source, this, name, mimeType) {
            nextCid()
        }
    }

    suspend fun fetchData(node: Node): ByteArray {
        return fetchData(node, this)
    }

    suspend fun fetchText(node: Node): String {
        return fetchData(node).decodeToString()
    }

}

private class HaloImpl(private val directory: Path) : Halo {
    @Volatile
    private var root: Node = createRaw(this, byteArrayOf()) {
        nextCid()
    }

    override fun directory(): Path {
        return directory
    }

    override fun root(data: ByteArray) {
        root(createRaw(this, data) {
            nextCid()
        })
    }

    override fun root(node: Node) {
        root = node
    }

    override fun root(): Node {
        return root
    }

    override fun reset() {
        root(byteArrayOf())
        cleanupDirectory(directory)
    }

    override fun delete() {
        cleanupDirectory(directory)
        SystemFileSystem.delete(directory, false)
    }

    override fun hasBlock(cid: Long): Boolean {
        require(cid != HALO_ROOT) { "Invalid Cid" }
        return SystemFileSystem.exists(path(cid))
    }

    override suspend fun fetchBlock(cid: Long): Buffer {
        require(cid != HALO_ROOT) { "Invalid Cid" }
        val file = path(cid)
        require(SystemFileSystem.exists(file)) { "Block does not exists" }
        val size = SystemFileSystem.metadataOrNull(file)!!.size
        SystemFileSystem.source(file).use { source ->
            val buffer = Buffer()
            buffer.write(source, size)
            return buffer
        }
    }


    override fun storeBlock(cid: Long, buffer: Buffer) {
        require(cid != HALO_ROOT) { "Invalid Cid" }
        val file = path(cid)
        if (!SystemFileSystem.exists(file)) {
            SystemFileSystem.sink(file, false).use { sink ->
                sink.write(buffer, buffer.size)
            }
        }

    }

    @OptIn(ExperimentalUuidApi::class)
    override fun tempFile(): Path {
        return Path(directory, Uuid.random().toHexString())
    }

    @OptIn(ExperimentalStdlibApi::class)
    private fun path(cid: Long): Path {
        require(cid != HALO_ROOT) { "Invalid Cid" }
        return Path(directory, cid.toHexString())
    }

    override fun deleteBlock(cid: Long) {
        require(cid != HALO_ROOT) { "Invalid Cid" }
        val file = path(cid)
        SystemFileSystem.delete(file, false)
    }

    override fun nextCid(): Long {
        val cid = Random.nextLong()
        if (cid != HALO_ROOT) {
            val exists = hasBlock(cid)
            if (!exists) {
                return cid
            }
        }
        return nextCid()
    }
}


fun contentResponse(
    channel: Channel, msg: String, status: Int
): Response {
    return Response(
        "text/html", "UTF-8", status,
        msg, emptyMap(), channel
    )
}

const val CONTENT_TYPE: String = "Content-Type"
const val CONTENT_LENGTH: String = "Content-Length"
const val CONTENT_TITLE: String = "Content-Title"

fun contentResponse(channel: Channel, node: Node): Response {
    val mimeType = node.mimeType()
    val responseHeaders: MutableMap<String, String> = mutableMapOf()
    responseHeaders[CONTENT_LENGTH] = node.size().toString()
    responseHeaders[CONTENT_TYPE] = mimeType
    responseHeaders[CONTENT_TITLE] = node.name()
    return Response(
        mimeType, "UTF-8", 200,
        "OK", responseHeaders, channel
    )
}

private fun cleanupDirectory(dir: Path) {
    if (SystemFileSystem.exists(dir)) {
        val files = SystemFileSystem.list(dir)
        for (file in files) {
            SystemFileSystem.delete(file)
        }
    }
}

fun newHalo(): Halo {
    return newHalo(tempDirectory())
}

@OptIn(ExperimentalUuidApi::class)
private fun tempDirectory(): Path {
    val path = Path(SystemTemporaryDirectory, Uuid.random().toHexString())
    SystemFileSystem.createDirectories(path)
    return path
}


fun newHalo(directory: Path): Halo {
    require(SystemFileSystem.exists(directory)) { "Directory does not exists." }
    require(
        SystemFileSystem.metadataOrNull(directory)?.isDirectory == true
    ) {
        "Path is not a directory."
    }
    return HaloImpl(directory)
}

fun decodeNode(cid: Long, block: Buffer): Node {
    return decodeNode(cid, block)
}

interface Fetch {
    suspend fun fetchBlock(cid: Long): Buffer
}

fun createChannel(node: Node, fetch: Fetch): Channel {
    val size = node.size()
    if (node is Fid) {
        return FidChannel(node, size, fetch)
    }
    val raw = node as Raw
    val buffer = Buffer()
    buffer.write(raw.data())
    return RawChannel(buffer)
}


