package tech.lp2p.thor.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Bookmark::class], version = 1, exportSchema = false)
abstract class Bookmarks : RoomDatabase() {
    abstract fun bookmarkDao(): BookmarkDao
}
