package tech.lp2p.thor.data

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [Task::class], version = 1, exportSchema = false)
abstract class Tasks : RoomDatabase() {
    abstract fun tasksDao(): TasksDao

    suspend fun createOrGet(
        pid: Long, name: String, mimeType: String, uri: String,
        size: Long, uuid: String
    ): Long {
        val id = tasksDao().parent(pid, name)
        if (id != null) {
            return id
        }
        return tasksDao().insert(
            Task(
                0, pid, name, mimeType, uri,
                size, uuid,
                active = false,
                finished = false,
                progress = 0f
            )
        )
    }
}
