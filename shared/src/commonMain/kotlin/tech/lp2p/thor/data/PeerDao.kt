package tech.lp2p.thor.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import tech.lp2p.asen.PeerId

@Dao
interface PeerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(peer: Peer)

    @Query("SELECT * FROM Peer ORDER BY RANDOM() LIMIT :limit")
    fun randomPeers(limit: Int): Flow<List<Peer>>

    @Query("DELETE FROM Peer WHERE peerId = :peerId")
    suspend fun delete(peerId: PeerId)
}
