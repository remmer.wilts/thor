package tech.lp2p.loki

import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertNotNull
import tech.lp2p.loki.data.DataDescriptor
import tech.lp2p.loki.data.Storage
import tech.lp2p.loki.protocol.data
import tech.lp2p.loki.protocol.decodeMetadata
import tech.lp2p.loki.protocol.encodeMetadata
import tech.lp2p.loki.torrent.buildTorrent
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.file.Files
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TorrentParserTest {
    private val path: String = "src/test/resources"

    @Test
    @Throws(IOException::class)
    fun parseTorrent() {
        val resources = File(path)

        val file = File(resources, "lubuntu-24.04-desktop-amd64.iso.torrent")

        val dir = Files.createTempDirectory("test").toFile()
        assertTrue(dir.exists())
        val storage = Storage(dir)
        FileInputStream(file).use { inputStream ->
            val bytes = inputStream.readAllBytes()
            val torrent = buildTorrent(bytes)
            assertNotNull(torrent)

            assertEquals(torrent.chunkHashes.size, 12576)

            val dataDescriptor = DataDescriptor(storage, torrent)
            assertNotNull(dataDescriptor)

            storage.finish()
            val files = dir.listFiles()
            assertEquals(files!!.size, torrent.files.size)

        }
        dir.deleteRecursively()
    }

    @Test
    fun testStorage() {
        val dir = Files.createTempDirectory("test").toFile()
        assertTrue(dir.exists())
        val storage = Storage(dir)

        storage.markVerified(0)
        storage.markVerified(5)

        val bitfield = storage.verifiedPieces(10)
        assertTrue(bitfield.isVerified(0))
        assertTrue(bitfield.isVerified(5))
        assertFalse(bitfield.isVerified(1))
        assertFalse(bitfield.isVerified(9))

        storage.close()

        storage.finish()
    }

    @Test
    fun testMetadata() {
        val maxData = 1000
        val data = ByteBuffer.allocate(maxData)
        val utMetadata = data(0, maxData, ByteArray(500))
        assertTrue(encodeMetadata(utMetadata, data))
        val length = data.position()
        val payload = ByteArray(length)
        data.flip()
        data.get(payload)
        val result = decodeMetadata(payload)
        assertEquals(result.consumed, length)
        assertEquals(result.message!!, utMetadata)
    }
}
