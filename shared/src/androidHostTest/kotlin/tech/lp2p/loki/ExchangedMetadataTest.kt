package tech.lp2p.loki

import org.junit.Test
import tech.lp2p.loki.torrent.ExchangedMetadata
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class ExchangedMetadataTest {

    @Test
    fun createAndValidate() {

        val totalSize = META_EXCHANGE_BLOCK_SIZE * 5 + 100

        val meta = ExchangedMetadata(totalSize, META_EXCHANGE_BLOCK_SIZE)

        assertFalse(meta.isComplete)

        val pieces = totalSize / META_EXCHANGE_BLOCK_SIZE
        val rest = totalSize.mod(META_EXCHANGE_BLOCK_SIZE)
        assertEquals(rest, totalSize - (pieces * META_EXCHANGE_BLOCK_SIZE))

        repeat(pieces) { i ->
            meta.setBlock(i, Random.nextBytes(ByteArray(META_EXCHANGE_BLOCK_SIZE)))
        }

        if (rest > 0) {
            assertFalse(meta.isComplete)
            meta.setBlock(pieces, Random.nextBytes(ByteArray(rest)))
        }
        assertTrue(meta.isComplete)

        val digest = meta.digest()
        assertNotNull(digest)

        assertTrue(digest.contentEquals(meta.digest()))
    }
}