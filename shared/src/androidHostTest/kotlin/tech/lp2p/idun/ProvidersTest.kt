package tech.lp2p.idun

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import tech.lp2p.idun.core.Peer
import tech.lp2p.idun.core.SHA1_HASH_LENGTH
import tech.lp2p.idun.core.createRandomKey
import kotlin.test.Test

class ProvidersTest {


    @Test
    fun randomPeers(): Unit = runBlocking(Dispatchers.IO) {
        val channel = Channel<Peer>()
        val idun = newIdun()
        idun.startup()

        val infoHash = createRandomKey(SHA1_HASH_LENGTH) // note random key (probably nobody has)
        launch {
            idun.peers(infoHash, channel)
        }
        for (peer in channel) {
            System.err.println(peer.toString())
        }
        idun.shutdown()
    }

}