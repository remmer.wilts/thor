package tech.lp2p.thor

import androidx.room.Room
import androidx.room.RoomDatabase
import tech.lp2p.thor.data.Bookmarks
import tech.lp2p.thor.data.Tasks


fun tasksBuilder(): RoomDatabase.Builder<Tasks> {
    val dbFilePath = documentDirectory() + "/tasks.db"
    return Room.databaseBuilder<Tasks>(
        name = dbFilePath,
    )
}


fun bookmarksBuilder(): RoomDatabase.Builder<Bookmarks> {
    val dbFilePath = documentDirectory() + "/bookmarks.db"
    return Room.databaseBuilder<Bookmarks>(
        name = dbFilePath,
    )
}


/*
fun createDataStore(): DataStore<Preferences> = createDataStore(

    producePath = {
        val documentDirectory: NSURL? = NSFileManager.defaultManager.URLForDirectory(
            directory = NSDocumentDirectory,
            inDomain = NSUserDomainMask,
            appropriateForURL = null,
            create = false,
            error = null,
        )
        requireNotNull(documentDirectory).path + "/$dataStoreFileName"
    }
)*/


private fun documentDirectory(): String {
    /*
    val documentDirectory = NSFileManager.defaultManager.URLForDirectory(
        directory = NSDocumentDirectory,
        inDomain = NSUserDomainMask,
        appropriateForURL = null,
        create = false,
        error = null,
    )
    return requireNotNull(documentDirectory?.path)
    */
    TODO("no apple available")
}