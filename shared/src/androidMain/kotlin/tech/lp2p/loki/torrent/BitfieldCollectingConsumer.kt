package tech.lp2p.loki.torrent

import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.protocol.Bitfield
import tech.lp2p.loki.protocol.Have
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Type
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

class BitfieldCollectingConsumer : Agent, Consumers {
    private val bitfields: ConcurrentMap<Peer, ByteArray> =
        ConcurrentHashMap()
    private val haves: ConcurrentMap<Peer, MutableSet<Int>> =
        ConcurrentHashMap()

    private fun doConsume(message: Message, messageContext: MessageContext) {
        if (message is Bitfield) {
            consume(message, messageContext)
        }
        if (message is Have) {
            consume(message, messageContext)
        }
    }

    override val consumers: List<MessageConsumer>
        get() {
            val list: MutableList<MessageConsumer> =
                ArrayList()
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Bitfield
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Have
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })
            return list
        }


    private fun consume(bitfieldMessage: Bitfield, context: MessageContext) {
        bitfields[context.peer] = bitfieldMessage.bitfield
    }

    private fun consume(have: Have, context: MessageContext) {
        val peer = context.peer
        val peerHaves =
            haves.computeIfAbsent(peer) { k: Peer? -> ConcurrentHashMap.newKeySet() }
        peerHaves.add(have.pieceIndex)
    }

    fun getBitfields(): Map<Peer, ByteArray> {
        return bitfields
    }

    fun getHaves(): Map<Peer, MutableSet<Int>> {
        return haves
    }
}
