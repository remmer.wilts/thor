package tech.lp2p.loki.torrent

import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Type
import java.util.concurrent.ConcurrentHashMap

class MessageRouter {

    private val typedConsumers: MutableMap<Type, MutableCollection<MessageConsumer>> =
        ConcurrentHashMap()
    private val producers: MutableSet<MessageProducer> = ConcurrentHashMap.newKeySet()

    fun clear() {
        typedConsumers.clear()
        producers.clear()
    }


    fun registerMessagingAgent(agent: Agent) {
        if (agent is Consumers) {
            addConsumers(agent.consumers)
        }

        if (agent is Produces) {
            producers.add(object : MessageProducer {
                override fun produce(
                    messageContext: MessageContext,
                    messageConsumer: (Message) -> Unit
                ) {
                    agent.produce(messageContext, messageConsumer)
                }
            })
        }
    }

    private fun addConsumers(messageConsumers: List<MessageConsumer>) {
        messageConsumers.forEach { consumer: MessageConsumer ->
            val consumedType = consumer.consumedType()
            typedConsumers.computeIfAbsent(consumedType) { ArrayList() }
                .add(consumer)

        }
    }


    fun consume(message: Message, context: MessageContext) {
        val consumers: Collection<MessageConsumer>? = typedConsumers[message.type]
        consumers?.forEach { consumer: MessageConsumer ->
            consumer.consume(
                message,
                context
            )
        }
    }

    fun produce(context: MessageContext, messageConsumer: (Message) -> Unit) {
        producers.forEach { producer: MessageProducer ->
            producer.produce(context, messageConsumer)
        }
    }

}
