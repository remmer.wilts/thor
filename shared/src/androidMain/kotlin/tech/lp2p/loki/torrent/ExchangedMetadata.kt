package tech.lp2p.loki.torrent

import org.kotlincrypto.hash.sha1.SHA1
import tech.lp2p.loki.data.MutableBlockSet
import tech.lp2p.loki.data.createMutableBlockSet
import java.nio.ByteBuffer


/**
 * BEP-9 torrent metadata, thread-safe
 */
internal data class ExchangedMetadata(
    val totalSize: Int,
    val blockSize: Int
) {

    val data: ByteBuffer = ByteBuffer.allocate(totalSize)
    private val metadataBlocks: MutableBlockSet =
        createMutableBlockSet(totalSize.toLong(), blockSize.toLong())

    fun isBlockPresent(blockIndex: Int): Boolean {
        return metadataBlocks.isPresent(blockIndex)
    }

    @Synchronized
    fun setBlock(blockIndex: Int, block: ByteArray) {
        validateBlockIndex(blockIndex)
        val offset = blockIndex * blockSize
        data.position(offset)
        data.put(block)
        metadataBlocks.markAvailable(offset.toLong(), block.size)
    }

    val blockCount: Int
        get() = metadataBlocks.blockCount

    val isComplete: Boolean
        get() = metadataBlocks.isComplete


    @Synchronized
    fun digest(): ByteArray {
        check(metadataBlocks.isComplete) { "Metadata is not complete" }
        val instance = SHA1()
        instance.update(data.array())
        return instance.digest()
    }


    private fun validateBlockIndex(blockIndex: Int) {
        val blockCount = metadataBlocks.blockCount
        require(!(blockIndex < 0 || blockIndex >= blockCount)) {
            "Invalid block index: $blockIndex; expected 0..$blockCount"
        }
    }

}
