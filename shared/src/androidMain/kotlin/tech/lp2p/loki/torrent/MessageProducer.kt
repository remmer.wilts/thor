package tech.lp2p.loki.torrent

import tech.lp2p.loki.protocol.Message

internal interface MessageProducer {
    fun produce(messageContext: MessageContext, messageConsumer: (Message) -> Unit)
}
