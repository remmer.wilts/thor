package tech.lp2p.loki.torrent

import android.annotation.SuppressLint
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Piece
import tech.lp2p.loki.protocol.Type
import java.util.Deque
import java.util.concurrent.LinkedBlockingDeque

data class RoutingPeerWorker(
    val connectionState: ConnectionState,
    val messageRouter: MessageRouter,
    val messageContext: MessageContext,
    val outgoingMessages: Deque<Message>
) {
    fun accept(message: Message) {
        messageRouter.consume(message, messageContext)
        updateConnection()
    }

    private fun postMessage(message: Message) {
        if (isUrgent(message)) {
            addUrgent(message)
        } else {
            add(message)
        }
    }

    private fun add(message: Message) {
        outgoingMessages.add(message)
    }

    private fun addUrgent(message: Message) {
        outgoingMessages.addFirst(message)
    }

    fun get(): Message? {
        if (outgoingMessages.isEmpty()) {
            messageRouter.produce(
                messageContext
            ) { message: Message -> this.postMessage(message) }
            updateConnection()
        }
        return postProcessOutgoingMessage(outgoingMessages.poll())
    }

    private fun postProcessOutgoingMessage(message: Message?): Message? {
        if (message == null) {
            return null
        }

        val messageType = message.type

        if (Type.Piece == messageType) {
            val piece = message as Piece
            // check that peer hadn't sent cancel while we were preparing the requested block
            if (isCancelled(piece)) {
                // dispose of message
                return null
            }
        }
        if (Type.Interested == messageType) {
            connectionState.isInterested = true
        }
        if (Type.NotInterested == messageType) {
            connectionState.isInterested = false
        }
        if (Type.Choke == messageType) {
            connectionState.setShouldChoke(true)
        }
        if (Type.Unchoke == messageType) {
            connectionState.setShouldChoke(false)
        }

        return message
    }

    @SuppressLint("ImplicitSamInstance")
    private fun isCancelled(piece: Piece): Boolean {
        val pieceIndex = piece.pieceIndex
        val offset = piece.offset
        val length = piece.length

        return connectionState.cancelledPeerRequests.remove(
            Key(pieceIndex, offset, length)
        )
    }

    private fun updateConnection() {
        handleConnection(connectionState) { message: Message -> postMessage(message) }
    }

}

fun createRoutingPeerWorker(
    peer: Peer,
    messageRouter: MessageRouter
): RoutingPeerWorker {
    val connectionState = ConnectionState()
    return RoutingPeerWorker(
        connectionState, messageRouter,
        MessageContext(peer, connectionState), LinkedBlockingDeque()
    )
}

private fun isUrgent(message: Message): Boolean {
    val messageType = message.type
    return Type.Choke == messageType || Type.Unchoke == messageType || Type.Cancel == messageType
}