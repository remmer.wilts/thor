package tech.lp2p.loki.torrent

import tech.lp2p.loki.Loki
import tech.lp2p.loki.MAX_CONCURRENT_ACTIVE_PEERS_TORRENT
import tech.lp2p.loki.MAX_PEER_CONNECTIONS_PER_TORRENT
import tech.lp2p.loki.TIMEOUT_PEER_BAN
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.protocol.Have
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Type
import tech.lp2p.loki.protocol.interested
import tech.lp2p.loki.protocol.notInterested
import java.util.Queue
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.LinkedBlockingQueue

internal class TorrentWorker(private val loki: Loki) {
    private val peerMap: ConcurrentMap<Peer, PieceAnnouncingPeerWorker> =
        ConcurrentHashMap()
    private val timeoutedPeers: MutableMap<Peer, Long> = ConcurrentHashMap()
    private val disconnectedPeers: Queue<Peer> = LinkedBlockingQueue()
    private val interestUpdates: MutableMap<Peer, Message> = ConcurrentHashMap()
    private var lastUpdatedAssignments: Long = 0

    fun clear() {
        peerMap.clear()
        timeoutedPeers.clear()
        disconnectedPeers.clear()
        interestUpdates.clear()
    }


    private fun addPeer(peer: Peer) {
        val worker = createPeerWorker(peer)
        val existing = peerMap.putIfAbsent(peer, worker)
        if (existing == null) {
            loki.messageDispatcher.addMessageConsumer(
                peer
            ) { message: Message -> consume(peer, message) }
            loki.messageDispatcher.addMessageSupplier(peer) { produce(peer) }
        }
    }

    private fun consume(peer: Peer, message: Message) {
        val worker = peerMap[peer]
        worker?.accept(message)
    }

    private fun produce(peer: Peer): Message? {
        var message: Message? = null

        val worker = peerMap[peer]
        if (worker != null) {

            val bitfield = loki.bitfield
            val assignments = loki.assignments

            if (bitfield != null && assignments != null &&
                (bitfield.piecesRemaining > 0 || assignments.count() > 0)
            ) {
                inspectAssignment(peer, worker, assignments)
                if (shouldUpdateAssignments(assignments)) {
                    processDisconnectedPeers(assignments)
                    processTimeoutedPeers()
                    updateAssignments(assignments)
                }
                val interestUpdate = interestUpdates.remove(peer)
                message = interestUpdate ?: worker.getMessage()
            } else {
                message = worker.getMessage()
            }
        }

        return message
    }


    private fun inspectAssignment(
        peer: Peer,
        peerWorker: PieceAnnouncingPeerWorker,
        assignments: Assignments
    ) {
        val connectionState: ConnectionState = peerWorker.connectionState
        val assignment = assignments.get(peer)
        if (assignment != null) {
            if (assignment.status == Assignment.Status.TIMEOUT) {
                timeoutedPeers[peer] = System.currentTimeMillis()
                assignments.remove(assignment)
            } else if (connectionState.isPeerChoking) {
                assignments.remove(assignment)
            }
        } else if (!connectionState.isPeerChoking) {
            if (mightCreateMoreAssignments(assignments)) {
                val newAssignment = assignments.assign(peer)
                newAssignment?.start(connectionState)
            }
        }
    }

    private fun shouldUpdateAssignments(assignments: Assignments): Boolean {
        return (timeSinceLastUpdated() > UPDATE_ASSIGNMENTS_OPTIONAL_INTERVAL
                && mightUseMoreAssignees(assignments))
                || timeSinceLastUpdated() > UPDATE_ASSIGNMENTS_MANDATORY_INTERVAL
    }

    private fun timeSinceLastUpdated(): Long {
        return System.currentTimeMillis() - lastUpdatedAssignments
    }

    private fun processDisconnectedPeers(assignments: Assignments?) {
        var disconnectedPeer: Peer?
        while ((disconnectedPeers.poll().also { disconnectedPeer = it }) != null) {
            checkNotNull(disconnectedPeer)
            if (assignments != null) {
                val assignment = assignments.get(disconnectedPeer)
                if (assignment != null) {
                    assignments.remove(assignment)
                }
            }
            timeoutedPeers.remove(disconnectedPeer)
            loki.pieceStatistics?.removeBitfield(disconnectedPeer)
        }
    }

    private fun processTimeoutedPeers() {
        timeoutedPeers.entries.removeIf { entry: Map.Entry<Peer, Long> ->
            System.currentTimeMillis() - entry.value >= TIMEOUT_PEER_BAN
        }
    }

    private fun updateAssignments(assignments: Assignments) {
        interestUpdates.clear()

        val ready: MutableSet<Peer> = HashSet()
        val choking: MutableSet<Peer> = HashSet()

        peerMap.forEach { (peer: Peer, worker: PieceAnnouncingPeerWorker) ->
            val timeouted = timeoutedPeers.containsKey(peer)
            val disconnected = disconnectedPeers.contains(peer)
            if (!timeouted && !disconnected) {
                if (worker.connectionState.isPeerChoking) {
                    choking.add(peer)
                } else {
                    ready.add(peer)
                }
            }
        }

        val interesting = assignments.update(ready, choking)

        ready.forEach { peer: Peer ->
            if (!interesting.contains(peer)) {
                val worker = peerMap[peer]
                if (worker != null) {
                    val connectionState: ConnectionState = worker.connectionState
                    if (connectionState.isInterested) {
                        interestUpdates[peer] = notInterested()
                        connectionState.isInterested = false
                    }
                }
            }
        }

        choking.forEach { peer: Peer ->
            val worker = peerMap[peer]
            if (worker != null) {
                val connectionState: ConnectionState = worker.connectionState
                if (interesting.contains(peer)) {
                    if (!connectionState.isInterested) {
                        interestUpdates[peer] = interested()
                        connectionState.isInterested = true
                    }
                } else if (connectionState.isInterested) {
                    interestUpdates[peer] = notInterested()
                    connectionState.isInterested = false
                }
            }
        }

        lastUpdatedAssignments = System.currentTimeMillis()
    }

    private fun createPeerWorker(peer: Peer): PieceAnnouncingPeerWorker {
        val routingWorker = createRoutingPeerWorker(
            peer,
            loki.messageRouter
        )
        return PieceAnnouncingPeerWorker(routingWorker)
    }

    private fun removePeer(peer: Peer) {
        val removed = peerMap.remove(peer)
        if (removed != null) {
            disconnectedPeers.add(peer)
        }
    }

    @Synchronized
    internal fun onPeerDiscovered(peer: Peer) {
        // TODO: Store discovered peers to use them later,
        // when some of the currently connected peers disconnects
        if (mightAddPeer()) {
            loki.connectionSource.connect(peer)
        }
    }

    @Synchronized
    internal fun onPeerConnected(peer: Peer) {
        if (mightAddPeer()) {
            addPeer(peer)
        }
    }

    private fun mightAddPeer(): Boolean {
        return peerMap.keys.size < MAX_PEER_CONNECTIONS_PER_TORRENT
    }

    @Synchronized
    internal fun onPeerDisconnected(peer: Peer) {
        removePeer(peer)
    }


    private inner class PieceAnnouncingPeerWorker(private val delegate: RoutingPeerWorker) {
        private val pieceAnnouncements: Queue<Have> = ConcurrentLinkedQueue()

        val connectionState: ConnectionState
            get() = delegate.connectionState


        fun accept(message: Message) {
            delegate.accept(message)
        }


        fun getMessage(): Message? {
            var message: Message? = pieceAnnouncements.poll()
            if (message != null) {
                return message
            }

            message = delegate.get()
            if (message != null && Type.Have == message.type) {
                val have = message as Have
                peerMap.values.forEach { worker: PieceAnnouncingPeerWorker ->
                    if (this !== worker) {
                        worker.pieceAnnouncements.add(have)
                    }
                }
            }
            return message
        }
    }
}

private const val UPDATE_ASSIGNMENTS_OPTIONAL_INTERVAL: Long = 1000
private const val UPDATE_ASSIGNMENTS_MANDATORY_INTERVAL: Long = 5000
private fun mightUseMoreAssignees(assignments: Assignments): Boolean {
    return assignments.count() < MAX_CONCURRENT_ACTIVE_PEERS_TORRENT
}

private fun mightCreateMoreAssignments(assignments: Assignments): Boolean {
    return assignments.count() < MAX_CONCURRENT_ACTIVE_PEERS_TORRENT
}