package tech.lp2p.loki.torrent

data class TorrentState(val descriptor: Descriptor) {
    val piecesTotal: Int
        get() = if (descriptor.dataDescriptor != null) {
            descriptor.dataDescriptor!!.bitfield.piecesTotal
        } else {
            -1
        }


    val piecesComplete: Int
        get() {
            return if (descriptor.dataDescriptor != null) {
                descriptor.dataDescriptor!!.bitfield.piecesComplete
            } else {
                -1
            }
        }

    val piecesRemaining: Int
        get() {
            return if (descriptor.dataDescriptor != null) {
                descriptor.dataDescriptor!!.bitfield.piecesRemaining
            } else {
                -1
            }
        }
}
