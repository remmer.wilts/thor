package tech.lp2p.loki.torrent

import tech.lp2p.loki.data.Bitfield
import tech.lp2p.loki.net.BufferedPieceRegistry
import tech.lp2p.loki.protocol.Have
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Piece
import tech.lp2p.loki.protocol.Type
import java.util.Queue
import java.util.concurrent.LinkedBlockingQueue

internal class PieceConsumer(
    private val bitfield: Bitfield,
    private val dataWorker: DataWorker,
    private val bufferedPieceRegistry: BufferedPieceRegistry
) : Produces, Consumers {
    private val completedPieces: Queue<Int> =
        LinkedBlockingQueue()

    private fun doConsume(message: Message, messageContext: MessageContext) {
        if (message is Piece) {
            consume(message, messageContext)
        }
    }

    override val consumers: List<MessageConsumer>
        get() {
            val list: MutableList<MessageConsumer> =
                ArrayList()
            list.add(object : MessageConsumer {
                override fun consumedType(): Type {
                    return Type.Piece
                }

                override fun consume(
                    message: Message,
                    messageContext: MessageContext
                ) {
                    doConsume(message, messageContext)
                }
            })


            return list
        }

    private fun consume(piece: Piece, context: MessageContext) {
        val connectionState = context.connectionState

        // check that this block was requested in the first place
        if (!checkBlockIsExpected(connectionState, piece)) {
            disposeOfBlock(piece)
            return
        }

        // discard blocks for pieces that have already been verified
        if (bitfield.isComplete(piece.pieceIndex)) {
            disposeOfBlock(piece)

            return
        }

        val block = addBlock(connectionState, piece)
        if (block == null) {
            disposeOfBlock(piece)
        } else {
            if (block.verified) {
                completedPieces.add(piece.pieceIndex)
            }
        }
    }

    private fun disposeOfBlock(piece: Piece) {
        val buffer = bufferedPieceRegistry.getBufferedPiece(piece.pieceIndex, piece.offset)
        buffer?.dispose()
    }

    private fun addBlock(
        connectionState: ConnectionState,
        piece: Piece
    ): BlockWrite? {
        val pieceIndex = piece.pieceIndex
        val offset = piece.offset
        val blockLength = piece.length

        val assignment = connectionState.currentAssignment
        if (assignment != null) {
            if (assignment.isAssigned(pieceIndex)) {
                assignment.check()
            }
        }

        val buffer = bufferedPieceRegistry.getBufferedPiece(pieceIndex, offset) ?: return null
        val blockWrite = dataWorker.addBlock(pieceIndex, offset, buffer)
        connectionState.pendingWrites[Key(pieceIndex, offset, blockLength)] = blockWrite
        return blockWrite
    }

    override fun produce(context: MessageContext, messageConsumer: (Message) -> Unit) {
        var completedPiece: Int?
        while ((completedPieces.poll().also { completedPiece = it }) != null) {
            messageConsumer.invoke(Have(completedPiece!!))
        }
    }
}

private fun checkBlockIsExpected(connectionState: ConnectionState, piece: Piece): Boolean {
    val key = Key(piece.pieceIndex, piece.offset, piece.length)
    return connectionState.pendingRequests.remove(key)
}