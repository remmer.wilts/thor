package tech.lp2p.loki.torrent

import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.updateAndGet
import tech.lp2p.loki.Loki
import tech.lp2p.loki.MAX_IO_QUEUE_SIZE
import tech.lp2p.loki.buffer.BufferedData
import tech.lp2p.loki.data.verify

internal class DataWorker(private val loki: Loki) {
    private val pendingTasksCount = atomic(0)

    fun addBlockRequest(
        pieceIndex: Int,
        offset: Int,
        length: Int
    ): BlockRead {
        val data = loki.descriptor.dataDescriptor!!
        if (!data.bitfield.isVerified(pieceIndex)) {
            return rejectedBlockRead(pieceIndex, offset, length)
        } else if (!tryIncrementTaskCount()) {
            throw IllegalStateException("Queue is overloaded")
        }
        try {
            val blockRange = loki.descriptor
                .dataDescriptor!!
                .chunkDescriptors[pieceIndex].data
            return readyBlockRead(pieceIndex, offset, length, blockRange)
        } finally {
            pendingTasksCount.decrementAndGet()
        }
    }


    fun addBlock(
        pieceIndex: Int,
        offset: Int,
        buffer: BufferedData
    ): BlockWrite {
        if (!tryIncrementTaskCount()) {
            buffer.dispose()
            return exceptionalBlockWrite(IllegalStateException("Queue is overloaded"))
        }
        try {
            val data = loki.descriptor.dataDescriptor!!
            val chunk = data.chunkDescriptors[pieceIndex]

            if (chunk.isComplete) {
                return rejectBlockWrite()
            }

            // todo ByteBuffer wrap remove
            chunk.data.getSubrange(offset.toLong()).putBytes(
                buffer.buffer.asReadOnlyBuffer()
            )
            chunk.blockSet.markAvailable(offset.toLong(), buffer.length)

            if (chunk.isComplete) {
                val verified = verify(chunk)
                if (verified) {
                    data.bitfield.markVerified(pieceIndex)
                    loki.storage.markVerified(pieceIndex)
                } else {
                    // reset data
                    chunk.clear()
                }
                return completeBlockWrite(verified)
            } else {
                return completeBlockWrite(false)
            }
        } catch (e: Throwable) {
            return exceptionalBlockWrite(e)
        } finally {
            pendingTasksCount.decrementAndGet()
            buffer.dispose()
        }

    }

    private fun tryIncrementTaskCount(): Boolean {
        val newCount = pendingTasksCount.updateAndGet { oldCount: Int ->
            if (oldCount == MAX_IO_QUEUE_SIZE) {
                return@updateAndGet oldCount
            } else {
                return@updateAndGet oldCount + 1
            }
        }
        return newCount < MAX_IO_QUEUE_SIZE
    }


}
