package tech.lp2p.loki.torrent

import tech.lp2p.loki.protocol.Cancel
import tech.lp2p.loki.protocol.Request


class ConnectionState internal constructor() {
    val cancelledPeerRequests: MutableSet<Key> = HashSet()

    /**
     * Get keys of block requests, that have been sent to the remote peer.
     *
     * @return Set of block request keys
     */
    val pendingRequests: MutableSet<Key> = HashSet()

    /**
     * Get pending block writes, mapped by keys of corresponding requests.
     *
     * @return Pending block writes, mapped by keys of corresponding requests.
     */
    val pendingWrites: MutableMap<Key, BlockWrite> = HashMap()

    val enqueuedPieces: MutableSet<Int> = HashSet()
    val requestQueue: ArrayDeque<Request> = ArrayDeque()

    @Volatile
    var isInterested: Boolean = false

    @Volatile
    var isPeerInterested: Boolean = false

    @Volatile
    private var choking = true

    @Volatile
    var isPeerChoking: Boolean = true
    private var shouldChoke: Boolean? = null

    /**
     * @return Last time connection was choked, 0 if it hasn't been choked yet.
     * Note that connections are initially choked when created.
     */
    var lastChoked: Long = 0
    var currentAssignment: Assignment? = null

    /**
     * @return true if the local client is choking the connection
     */
    fun isChoking(): Boolean {
        return choking
    }


    fun setChoking(choking: Boolean) {
        this.choking = choking
        this.shouldChoke = null
    }


    fun getShouldChoke(): Boolean? {
        return shouldChoke
    }

    fun setShouldChoke(shouldChoke: Boolean) {
        this.shouldChoke = shouldChoke
    }

    /**
     * Signal that remote peer has cancelled a previously issued block request.
     */
    fun onCancel(cancel: Cancel) {
        cancelledPeerRequests.add(
            Key(
                cancel.pieceIndex, cancel.offset, cancel.length
            )
        )
    }


    fun removeAssignment() {
        this.currentAssignment = null
    }
}
