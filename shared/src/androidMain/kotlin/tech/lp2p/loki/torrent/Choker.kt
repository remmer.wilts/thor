package tech.lp2p.loki.torrent

import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.choke
import tech.lp2p.loki.protocol.unchoke

/**
 * Inspects connection state and yields choke/unchoke messages when appropriate.
 */
fun handleConnection(
    connectionState: ConnectionState,
    messageConsumer: (Message) -> Unit
) {
    var shouldChoke = connectionState.getShouldChoke()
    val choking = connectionState.isChoking()
    val peerInterested = connectionState.isPeerInterested

    if (shouldChoke == null) {
        if (peerInterested && choking) {
            if (mightUnchoke(connectionState)) {
                shouldChoke = false // should unchoke
            }
        } else if (!peerInterested && !choking) {
            shouldChoke = true
        }
    }

    if (shouldChoke != null) {
        if (shouldChoke != choking) {
            if (shouldChoke) {
                // choke immediately
                connectionState.setChoking(true)
                messageConsumer.invoke(choke())
                connectionState.lastChoked = System.currentTimeMillis()
            } else if (mightUnchoke(connectionState)) {
                connectionState.setChoking(false)
                messageConsumer.invoke(unchoke())
            }
        }
    }
}

private fun mightUnchoke(connectionState: ConnectionState): Boolean {
    // unchoke depending on last choked time to avoid fibrillation
    return System.currentTimeMillis() - connectionState.lastChoked >= CHOKING_THRESHOLD
}

const val CHOKING_THRESHOLD: Long = 10000 // millis

