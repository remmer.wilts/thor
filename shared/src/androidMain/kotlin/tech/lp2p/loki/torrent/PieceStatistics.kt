package tech.lp2p.loki.torrent

import tech.lp2p.loki.data.Bitfield
import tech.lp2p.loki.data.Bitfield.PieceStatus
import tech.lp2p.loki.net.Peer

data class PieceStatistics(
    val localBitfield: Bitfield,
    val peerBitFields: MutableMap<Peer, Bitfield>,
    val pieceTotals: IntArray
) {
    /**
     * Add peer's bitfield.
     * For each piece, that the peer has, total count will be incremented by 1.
     */
    fun addBitfield(peer: Peer, bitfield: Bitfield) {
        validateBitfieldLength(bitfield)
        peerBitFields[peer] = bitfield

        for (i in pieceTotals.indices) {
            if (bitfield.getPieceStatus(i) == PieceStatus.COMPLETE_VERIFIED) {
                incrementPieceTotal(i)
            }
        }
    }

    @Synchronized
    private fun incrementPieceTotal(i: Int) {
        pieceTotals[i]++
    }

    /**
     * Remove peer's bitfield.
     * For each piece, that the peer has, total count will be decremented by 1.
     */
    fun removeBitfield(peer: Peer) {
        val bitfield = peerBitFields.remove(peer) ?: return

        for (i in pieceTotals.indices) {
            if (bitfield.getPieceStatus(i) == PieceStatus.COMPLETE_VERIFIED) {
                decrementPieceTotal(i)
            }
        }
    }

    @Synchronized
    private fun decrementPieceTotal(i: Int) {
        pieceTotals[i]--
    }

    private fun validateBitfieldLength(bitfield: Bitfield) {
        require(bitfield.piecesTotal == pieceTotals.size) {
            "Bitfield has invalid length (" + bitfield.piecesTotal +
                    "). Expected number of pieces: " + pieceTotals.size
        }
    }

    /**
     * Update peer's bitfield by indicating that the peer has a given piece.
     * Total count of the specified piece will be incremented by 1.
     */
    fun addPiece(peer: Peer, pieceIndex: Int) {
        var bitfield = peerBitFields[peer]
        if (bitfield == null) {
            bitfield = Bitfield(localBitfield.piecesTotal)
            val existing = peerBitFields.putIfAbsent(peer, bitfield)
            if (existing != null) {
                bitfield = existing
            }
        }

        markPieceVerified(bitfield, pieceIndex)
    }

    @Synchronized
    private fun markPieceVerified(bitfield: Bitfield, pieceIndex: Int) {
        if (!bitfield.isVerified(pieceIndex)) {
            bitfield.markVerified(pieceIndex)
            incrementPieceTotal(pieceIndex)
        }
    }

    fun getPeerBitfield(peer: Peer): Bitfield? {
        return peerBitFields[peer]
    }

    @Synchronized
    fun getCount(pieceIndex: Int): Int {
        return pieceTotals[pieceIndex]
    }

    val piecesTotal: Int
        get() = pieceTotals.size


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PieceStatistics

        if (localBitfield != other.localBitfield) return false
        if (peerBitFields != other.peerBitFields) return false
        if (!pieceTotals.contentEquals(other.pieceTotals)) return false
        if (piecesTotal != other.piecesTotal) return false

        return true
    }

    override fun hashCode(): Int {
        var result = localBitfield.hashCode()
        result = 31 * result + peerBitFields.hashCode()
        result = 31 * result + pieceTotals.contentHashCode()
        result = 31 * result + piecesTotal
        return result
    }
}
