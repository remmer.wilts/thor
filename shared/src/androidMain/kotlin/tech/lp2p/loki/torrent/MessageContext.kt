package tech.lp2p.loki.torrent

import tech.lp2p.loki.net.Peer

data class MessageContext(val peer: Peer, val connectionState: ConnectionState)
