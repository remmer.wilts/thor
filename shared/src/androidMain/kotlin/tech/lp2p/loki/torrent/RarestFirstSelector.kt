package tech.lp2p.loki.torrent

import java.util.PrimitiveIterator
import java.util.PriorityQueue
import java.util.Spliterator
import java.util.Spliterators
import java.util.stream.IntStream
import java.util.stream.StreamSupport
import kotlin.math.min
import kotlin.random.Random

/**
 * Implements the "rarest-first" piece selection algorithm.
 * As the name implies, pieces that appear less frequently
 * and are generally less available are selected in the first place.
 *
 *
 * There are two "flavours" of the "rarest-first" strategy: regular and randomized.
 *
 *
 * Regular rarest-first selects whichever pieces that are the least available
 * (strictly following the order of increasing availability).
 *
 *
 * Randomized rarest-first selects one of the least available pieces randomly
 * (which means that it does not always select THE least available piece, but rather looks at
 * some number N of the least available pieces and then randomly picks one of them).
 */
class RarestFirstSelector {
    private val random = Random(System.currentTimeMillis())

    fun getNextPieces(pieceStatistics: PieceStatistics): IntStream {
        return StreamSupport.intStream(
            Spliterators.spliteratorUnknownSize(
                createIterator(pieceStatistics),
                Spliterator.NONNULL or Spliterator.DISTINCT or Spliterator.ORDERED
            ), false
        )
    }

    private fun createIterator(pieceStatistics: PieceStatistics): PrimitiveIterator.OfInt {
        val queue = orderedQueue(pieceStatistics)
        return RandomizedIteratorOfInt(
            queue,
            random
        )
    }

    private class RandomizedIteratorOfInt(
        private val list: MutableList<Long>,
        private val random: Random
    ) :
        PrimitiveIterator.OfInt {
        private var position = 0
        private var limit: Int

        init {
            this.limit = calculateLimitAndShuffle(list, 0)
        }

        /**
         * Starting with a given position, iterates over elements of the list,
         * while one of the following holds true:
         * - each subsequent element's "count" is equal to the initial element's "count",
         *
         *
         * Shuffles the subrange of the list, starting with the initial element at `position`.
         * Each group where all elements have the same "count" is shuffled separately.
         *
         * @return index of the first element in the list with "count" different from the
         * initial element's "count"
         * or index of the element that is positions ahead in the list
         * than the initial element, whichever is greater
         * @see .getCount
         */
        fun calculateLimitAndShuffle(list: MutableList<Long>, pos: Int): Int {
            var position = pos
            if (position >= list.size) {
                return list.size
            }

            var limit = position + 1
            var count = getCount(list[position])
            var nextCount = count

            do {
                while (limit < list.size && (getCount(list[limit]).also {
                        nextCount = it
                    }) == count) {
                    limit++
                }
                // shuffle elements with the same "count" only,
                // because otherwise less available pieces may end up
                // being swapped with more available pieces
                // (i.e. pushed to the bottom of the queue)
                shuffle(
                    list, position,
                    min(limit.toDouble(), list.size.toDouble()).toInt()
                )

                if (limit >= list.size) {
                    break
                } else {
                    position = limit
                    count = nextCount
                }
            } while (true)

            return limit
        }

        /**
         * Shuffle a subrange of the given list, between 'begin' and 'end' (exclusively)
         *
         * @param begin index of the first element of the subrange
         * @param end   index of the first element after the last element of the subrange
         */
        fun shuffle(list: MutableList<Long>, begin: Int, end: Int) {
            var i = end
            while (--i > begin) {
                swap(list, i, begin + random.nextInt(i - begin))
            }
        }

        override fun nextInt(): Int {
            val result = getPieceIndex(list[position++])
            if (position == limit) {
                limit = calculateLimitAndShuffle(list, position)
            }
            return result
        }

        override fun remove() {
        }

        override fun hasNext(): Boolean {
            return position < list.size
        }

    }
}

private fun zip(pieceIndex: Int, count: Int): Long {
    return ((pieceIndex.toLong()) shl 32) + count
}

private fun getPieceIndex(zipped: Long): Int {
    return (zipped shr 32).toInt()
}

private fun getCount(zipped: Long): Int {
    return zipped.toInt()
}

// TODO: this is very inefficient when only a few pieces are needed,
// and this for sure can be moved to PieceStatistics (that will be responsible for maintaining an up-to-date list)
// UPDATE: giving this another thought, amortized costs of maintaining an up-to-date list (from the '# of operations' POV)
// are in fact likely to far exceed the costs of periodically rebuilding the statistics from scratch,
// esp. when there are many connects and disconnects, and statistics are slightly adjusted for each added or removed bitfield.
private fun orderedQueue(pieceStatistics: PieceStatistics): MutableList<Long> {
    val rarestFirst = PriorityQueue(object : Comparator<Long> {
        override fun compare(o1: Long, o2: Long): Int {
            return if (o1.toInt() > o2.toInt()) {
                1
            } else if (o1.toInt() < o2.toInt()) {
                -1
            } else {
                (o1 shr 32).compareTo(o2 shr 32)
            }
        }
    })
    val piecesTotal = pieceStatistics.piecesTotal
    for (pieceIndex in 0 until piecesTotal) {
        val count = pieceStatistics.getCount(pieceIndex)
        if (count > 0) {
            val zipped = zip(pieceIndex, count)
            rarestFirst.add(zipped)
        }
    }

    return rarestFirst.toMutableList()
}

private fun swap(list: MutableList<Long>, i: Int, j: Int) {
    if (i != j) {
        val temp = list[i]
        list[i] = list[j]
        list[j] = temp
    }
}