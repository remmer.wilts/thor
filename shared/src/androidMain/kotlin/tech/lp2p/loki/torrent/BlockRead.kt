package tech.lp2p.loki.torrent

import tech.lp2p.loki.data.DataRange


data class BlockRead(
    val isRejected: Boolean,
    val pieceIndex: Int,
    val offset: Int,
    val length: Int,
    val blockRange: DataRange?
)

fun readyBlockRead(pieceIndex: Int, offset: Int, length: Int, blockRange: DataRange): BlockRead {
    return BlockRead(false, pieceIndex, offset, length, blockRange)
}

fun rejectedBlockRead(pieceIndex: Int, offset: Int, length: Int): BlockRead {
    return BlockRead(true, pieceIndex, offset, length, null)
}
