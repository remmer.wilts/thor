package tech.lp2p.loki.torrent

import tech.lp2p.loki.MAX_OUTSTANDING_REQUESTS
import tech.lp2p.loki.data.Bitfield
import tech.lp2p.loki.data.ChunkDescriptor
import tech.lp2p.loki.protocol.Cancel
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.Request
import kotlin.math.min

data class RequestProducer(
    val bitfield: Bitfield, val chunks: List<ChunkDescriptor>
) : Produces {
    override fun produce(context: MessageContext, messageConsumer: (Message) -> Unit) {
        val connectionState = context.connectionState

        val assignment = connectionState.currentAssignment
        if (assignment == null) {
            resetConnection(connectionState, messageConsumer)
            return
        }

        val assignedPieces = assignment.pieces
        if (assignedPieces.isEmpty()) {
            resetConnection(connectionState, messageConsumer)
            return
        } else {
            var finishedPieces: MutableList<Int?>? = null
            for (assignedPiece in assignedPieces) {
                if (bitfield.isComplete(assignedPiece)) {
                    if (finishedPieces == null) {
                        finishedPieces = ArrayList(assignedPieces.size + 1)
                    }
                    // delay removing piece from assignments to avoid CME
                    finishedPieces.add(assignedPiece)
                } else if (connectionState.enqueuedPieces.add(assignedPiece)) {
                    addRequestsToQueue(connectionState, assignedPiece)
                }
            }
            finishedPieces?.forEach { finishedPiece: Int? ->
                assignment.finish(finishedPiece!!)
                connectionState.enqueuedPieces.remove(finishedPiece)
            }
        }

        val requestQueue = connectionState.requestQueue
        while (!requestQueue.isEmpty() && (connectionState.pendingRequests.size
                    <= MAX_OUTSTANDING_REQUESTS)
        ) {
            val request = requestQueue.removeFirst()
            val key = Key(request.pieceIndex, request.offset, request.length)
            messageConsumer.invoke(request)
            connectionState.pendingRequests.add(key)
        }
    }

    private fun addRequestsToQueue(connectionState: ConnectionState, pieceIndex: Int) {
        val requests = buildRequests(pieceIndex)
            .filter { request: Request? ->
                val key = Key(
                    request!!.pieceIndex, request.offset, request.length
                )
                if (connectionState.pendingRequests.contains(key)) {
                    return@filter false
                }

                val blockWrite = connectionState.pendingWrites[key]
                if (blockWrite == null) {
                    return@filter true
                }

                val failed = blockWrite.error != null
                if (failed) {
                    connectionState.pendingWrites.remove(key)
                }
                failed
            }.toMutableList()

        requests.shuffle()

        connectionState.requestQueue.addAll(requests)
    }

    private fun buildRequests(pieceIndex: Int): List<Request> {
        val requests: MutableList<Request> = ArrayList()
        val chunk = chunks[pieceIndex]
        val chunkSize = chunk.data.length()
        val blockSize = chunk.blockSize()

        for (blockIndex in 0 until chunk.blockCount()) {
            if (!chunk.isPresent(blockIndex)) {
                val offset = (blockIndex * blockSize).toInt()
                val length = min(blockSize, (chunkSize - offset)).toInt()
                requests.add(Request(pieceIndex, offset, length))
            }
        }

        return requests
    }
}


private fun resetConnection(
    connectionState: ConnectionState,
    messageConsumer: (Message) -> Unit
) {
    connectionState.requestQueue.clear()
    connectionState.enqueuedPieces.clear()
    connectionState.pendingRequests.forEach { key: Key ->
        messageConsumer.invoke(
            Cancel(
                key.pieceIndex,
                key.offset, key.length
            )
        )
    }
    connectionState.pendingRequests.clear()
    connectionState.pendingWrites.clear()
}
