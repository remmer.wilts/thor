package tech.lp2p.loki.buffer


interface ByteBufferView {

    fun position(): Int

    fun position(newPosition: Int)

    fun limit(): Int

    fun limit(newLimit: Int)

    fun hasRemaining(): Boolean

    fun remaining(): Int

    fun get(): Byte

    val short: Short

    val int: Int

    fun get(dst: ByteArray)

    fun duplicate(): ByteBufferView
}
