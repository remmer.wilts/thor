package tech.lp2p.loki.buffer

import java.nio.ByteBuffer

/**
 * Searches for the first pattern match in the provided buffer.
 * Buffer's position might change in the following ways after this methods returns:
 * - if a pattern was not found, then the buffer's position will not change
 * - if a pattern was found, then the buffer's position will be right after the last index
 * of the matching subrange (probably equal to buffer's limit)
 *
 * @return true if pattern was found in the provided buffer
 */
fun searchPattern(buf: ByteBuffer, pattern: ByteArray): Boolean {
    require(pattern.isNotEmpty()) { "Empty pattern" }
    if (buf.remaining() < pattern.size) {
        return false
    }

    val pos = buf.position()

    val len = pattern.size
    val p = 31
    var mult0 = 1
    for (i in 0 until len - 1) {
        mult0 *= p
    }
    var hash = 0
    for (i in 0 until len - 1) {
        hash += pattern[i].toInt()
        hash *= p
    }
    hash += pattern[len - 1].toInt()

    var bufHash = 0
    val bytes = ByteArray(len)
    var b: Byte
    for (i in 0 until len - 1) {
        b = buf.get()
        bytes[i] = b
        bufHash += b.toInt()
        bufHash *= p
    }
    b = buf.get()
    bytes[bytes.size - 1] = b
    bufHash += b.toInt()

    var found = false
    do {
        if (bufHash == hash && pattern.contentEquals(bytes)) {
            found = true
            break
        } else if (!buf.hasRemaining()) {
            break
        }
        val next = buf.get()
        bufHash -= (bytes[0] * mult0)
        bufHash *= p
        bufHash += next.toInt()
        System.arraycopy(bytes, 1, bytes, 0, len - 1)
        bytes[len - 1] = next
    } while (true)

    if (!found) {
        buf.position(pos)
    }
    return found
}

