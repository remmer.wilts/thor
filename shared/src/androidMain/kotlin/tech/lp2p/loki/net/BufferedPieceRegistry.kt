package tech.lp2p.loki.net

import tech.lp2p.loki.buffer.BufferedData
import java.util.concurrent.ConcurrentHashMap

class BufferedPieceRegistry : ConcurrentHashMap<Long, BufferedData>() {
    fun addBufferedPiece(pieceIndex: Int, offset: Int, buffer: BufferedData): Boolean {
        require(pieceIndex >= 0) { "Illegal piece index: $pieceIndex" }
        val existing = putIfAbsent(zip(pieceIndex, offset), buffer)
        return (existing == null)
    }

    fun getBufferedPiece(pieceIndex: Int, offset: Int): BufferedData? {
        return remove(zip(pieceIndex, offset))
    }
}

private fun zip(pieceIndex: Int, offset: Int): Long {
    return ((pieceIndex.toLong()) shl 32) + offset
}