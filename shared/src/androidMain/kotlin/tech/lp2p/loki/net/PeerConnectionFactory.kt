package tech.lp2p.loki.net

import tech.lp2p.loki.Loki
import tech.lp2p.loki.NETWORK_BUFFER_SIZE
import tech.lp2p.loki.PEER_HANDSHAKE_TIMEOUT
import tech.lp2p.loki.crypto.MSEHandshakeProcessor
import tech.lp2p.loki.debug
import tech.lp2p.loki.protocol.HANDSHAKE_RESERVED_LENGTH
import tech.lp2p.loki.protocol.Handshake
import tech.lp2p.loki.protocol.MSECipher
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.MessageHandler
import java.net.InetAddress
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.Selector
import java.nio.channels.SocketChannel
import javax.crypto.Cipher
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

internal class PeerConnectionFactory(
    private val peerId: ByteArray,
    private val selector: Selector,
    private val bufferedPieceRegistry: BufferedPieceRegistry,
    private val messageHandler: MessageHandler,
    private val loki: Loki,
    private val dataReceiver: DataReceiver,
    private val handshakeHandlers: Collection<HandshakeHandler>
) {
    private val cryptoHandshakeProcessor = MSEHandshakeProcessor(
        loki, messageHandler
    )

    private val localOutgoingSocketAddress = InetSocketAddress(0)

    private fun getChannel(inetAddress: InetAddress, port: Int): SocketChannel {
        val remoteAddress = InetSocketAddress(inetAddress, port)
        val outgoingChannel = selector.provider().openSocketChannel()
        outgoingChannel.socket().bind(localOutgoingSocketAddress)
        outgoingChannel.socket().soTimeout =
            socketTimeout.inWholeMilliseconds.toInt()
        outgoingChannel.socket().setSoLinger(false, 0)
        outgoingChannel.connect(remoteAddress)
        return outgoingChannel
    }

    private fun createConnection(
        peer: Peer,
        channel: SocketChannel,
        incoming: Boolean
    ): PeerConnection? {
        val inBB = ByteBuffer.allocate(NETWORK_BUFFER_SIZE)
        val outBB = ByteBuffer.allocate(NETWORK_BUFFER_SIZE)
        try {
            return createConnectionIntern(peer, channel, incoming, inBB, outBB)
        } catch (_: Exception) {
            closeQuietly(channel)
            return null
        }
    }


    private fun createConnectionIntern(
        peer: Peer,
        channel: SocketChannel,
        incoming: Boolean,
        inBB: ByteBuffer,
        outBB: ByteBuffer
    ): PeerConnection? {

        channel.configureBlocking(false)
        var cipher: MSECipher? = null


        try {
            cipher = if (incoming) {
                cryptoHandshakeProcessor.negotiateIncoming(peer, channel, inBB, outBB)
            } else {
                cryptoHandshakeProcessor.negotiateOutgoing(
                    channel,
                    inBB,
                    outBB
                )
            }
        } finally {
            debug("PeerConnectionFactory", "Cipher $cipher")
        }

        var decoders: List<Cipher> = emptyList()
        var encoders: List<Cipher> = emptyList()

        if (cipher != null) {
            decoders = listOf(cipher.decryptionCipher)
            encoders = listOf(cipher.encryptionCipher)
        }

        val channelHandler = SocketChannelHandler(
            peer,
            messageHandler,
            decoders,
            encoders,
            bufferedPieceRegistry,
            channel,
            inBB,
            outBB,
            dataReceiver
        )
        channelHandler.register()

        val remotePort = (channel.remoteAddress as InetSocketAddress).port
        val connection = PeerConnection(peer, remotePort, channelHandler)
        val inited = if (incoming) {
            handleIncomingConnection(connection)
        } else {
            handleOutgoingConnection(connection)
        }
        if (inited) {
            channelHandler.activate()
            return connection
        } else {
            connection.closeQuietly()
            return null
        }
    }

    private fun handleOutgoingConnection(
        connection: PeerConnection
    ): Boolean {
        val handshake = Handshake(
            ByteArray(HANDSHAKE_RESERVED_LENGTH), loki.torrentId, peerId
        )
        handshakeHandlers.forEach { handler: HandshakeHandler ->
            handler.processOutgoingHandshake(handshake)
        }
        try {
            connection.postMessage(handshake)
        } catch (_: Exception) {
            return false
        }

        var peerHandshake: Message? = null
        try {
            peerHandshake = connection.readMessage(PEER_HANDSHAKE_TIMEOUT)
        } catch (_: Throwable) {
            // ignore exception
        }

        if (peerHandshake is Handshake) {

            val incomingTorrentId = peerHandshake.torrentId
            if (loki.torrentId == incomingTorrentId) {

                handshakeHandlers.forEach { handler: HandshakeHandler ->
                    handler.processIncomingHandshake(
                        connection,
                        peerHandshake
                    )
                }

                return true
            }

        }
        return false
    }

    private fun handleIncomingConnection(connection: PeerConnection): Boolean {
        var peerHandshake: Message? = null
        try {
            peerHandshake = connection.readMessage(PEER_HANDSHAKE_TIMEOUT)
        } catch (_: Throwable) {
            // ignore exception
        }


        if (peerHandshake is Handshake) {

            val torrentId = peerHandshake.torrentId
            val descriptor = loki.getDescriptor(torrentId)
            // it's OK if descriptor is not present -- threads.torrent might be being fetched at the time
            if (loki.torrentId == torrentId
                && (descriptor == null || descriptor.isActive())
            ) {
                val handshake = Handshake(
                    ByteArray(HANDSHAKE_RESERVED_LENGTH), torrentId, peerId
                )
                handshakeHandlers.forEach { handler: HandshakeHandler ->
                    handler.processOutgoingHandshake(handshake)
                }

                try {
                    connection.postMessage(handshake)
                } catch (_: Exception) {
                    return false
                }

                handshakeHandlers.forEach { handler: HandshakeHandler ->
                    handler.processIncomingHandshake(
                        connection, peerHandshake
                    )
                }

                return true
            }
        }

        return false
    }


    fun createOutgoingConnection(peer: Peer): PeerConnection? {

        val inetAddress = peer.inetAddress
        val port = peer.port

        val channel: SocketChannel
        try {
            channel = getChannel(inetAddress, port)
        } catch (_: Exception) {
            return null
        }

        return createConnection(peer, channel, false)
    }

    fun createIncomingConnection(peer: Peer, channel: SocketChannel): PeerConnection? {
        return createConnection(peer, channel, true)
    }
}


private val socketTimeout: Duration = 30.toDuration(DurationUnit.SECONDS)


private fun closeQuietly(channel: SocketChannel?) {
    if (channel != null && channel.isOpen) {
        try {
            channel.close()
        } catch (_: Throwable) {
        }
    }
}
