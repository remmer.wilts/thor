package tech.lp2p.loki.net

interface ConnectionRoutine {
    fun establish(): PeerConnection?

    fun cancel()
}
