package tech.lp2p.loki.net

import kotlinx.atomicfu.atomic
import tech.lp2p.loki.buffer.BufferedData
import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.buffer.DelegatingByteBufferView
import tech.lp2p.loki.buffer.SplicedByteBufferView
import tech.lp2p.loki.crypto.mutateBuffer
import tech.lp2p.loki.debug
import tech.lp2p.loki.isDebug
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.protocol.MessageHandler
import tech.lp2p.loki.protocol.Piece
import java.nio.ByteBuffer
import java.nio.channels.SocketChannel
import java.util.ArrayDeque
import java.util.Queue
import java.util.concurrent.LinkedBlockingQueue
import javax.crypto.Cipher
import kotlin.math.min

class SocketChannelHandler(
    private val peer: Peer,
    private val messageHandler: MessageHandler,
    private val decoders: List<Cipher>,
    private val encoders: List<Cipher>,
    private val bufferedPieceRegistry: BufferedPieceRegistry,
    private val channel: SocketChannel,
    private val inboundBuffer: ByteBuffer,
    private val outboundBuffer: ByteBuffer,
    private val dataReceiver: DataReceiver
) {
    private val inboundBufferLock = Any()
    private val outboundBufferLock = Any()
    private val shutdown = atomic(false)
    private val bufferView: ByteBufferView = DelegatingByteBufferView(inboundBuffer)
    private val decodingView: DecodingBufferView = DecodingBufferView()
    private val messageQueue: Queue<Message> = LinkedBlockingQueue()
    private val bufferQueue: Queue<BufferedDataWithOffset> = ArrayDeque()
    private var regionA: Region?
    private var regionB: Region?
    private var undisposedDataOffset = -1


    init {
        require(
            !(inboundBuffer.position() != 0 || inboundBuffer.limit() != inboundBuffer.capacity()
                    || inboundBuffer.capacity() == 0)
        ) {
            ("Illegal buffer params (position: "
                    + inboundBuffer.position() + ", limit: " + inboundBuffer.limit() +
                    ", capacity: " + inboundBuffer.capacity() + ")")
        }

        this.regionA = Region()
        this.regionB = null


        // process existing data immediately (e.g. there might be leftovers from MSE handshake)
        processInboundData()
    }

    private fun notEncoded(message: Message): Boolean {
        return !writeMessageToBuffer(message, outboundBuffer)
    }

    private fun writeMessageToBuffer(message: Message, buffer: ByteBuffer): Boolean {
        val encodedDataLimit = buffer.position()
        val written = messageHandler.encode(peer, message, buffer)
        if (written) {
            val unencodedDataLimit = buffer.position()
            buffer.flip()
            encoders.forEach { cipher: Cipher ->
                buffer.position(encodedDataLimit)
                mutateBuffer(cipher, buffer)
            }
            buffer.clear()
            buffer.position(unencodedDataLimit)
        }
        return written
    }

    fun send(message: Message) {
        if (notEncoded(message)) {
            flush()
            check(!notEncoded(message)) { "Failed to send message: $message" }
        }
        flush()
    }

    fun receive(): Message? {
        return messageQueue.poll()
    }

    fun read(): Boolean {
        try {
            return processReadingData()
        } catch (e: Exception) {
            shutdown()
            throw RuntimeException("Unexpected error", e)
        }
    }

    fun register() {
        dataReceiver.registerChannel(channel, this)
    }

    private fun unregister() {
        dataReceiver.unregisterChannel(channel)
    }

    fun activate() {
        dataReceiver.activateChannel(channel)
    }

    private fun processReadingData(): Boolean {
        synchronized(inboundBufferLock) {
            var readLast: Int

            do {
                while ((channel.read(inboundBuffer).also { readLast = it }) > 0);

                val insufficientSpace = !inboundBuffer.hasRemaining()
                processInboundData()
                if (readLast == -1) {
                    error("EOF")
                } else if (!insufficientSpace) {
                    return true
                }
            } while (inboundBuffer.hasRemaining())
            return false

        }
    }

    private fun flush() {
        synchronized(outboundBufferLock) {

            outboundBuffer.flip()
            try {
                while (outboundBuffer.hasRemaining()) {
                    channel.write(outboundBuffer)
                }
                outboundBuffer.compact()

            } catch (e: Exception) {
                shutdown()
                throw e
            }
        }
    }

    fun close() {
        synchronized(inboundBufferLock) {
            synchronized(outboundBufferLock) {
                shutdown()
            }
        }
    }

    private fun shutdown() {
        if (shutdown.compareAndSet(false, true)) {
            try {
                unregister()
            } catch (e: Exception) {
                debug("SocketChannelHandler", e)
            }
            closeChannel()
        }
    }

    private fun closeChannel() {
        try {
            channel.close()
        } catch (e: Exception) {
            debug("SocketChannelHandler", e)
        }
    }


    private fun processInboundData() {
        try {
            if (regionB == null) {
                processA()
            } else {
                processAB()
            }
        } catch (e: Exception) {
            debug("SocketChannelHandler", e)
            printDebugInfo(e.message)
            throw e
        }
    }

    private fun printDebugInfo(message: String?) {
        if (isDebug) {
            var s = "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
            s += "\n  Debug message: $message"
            s += "\n  Peer: $peer"
            s += "\n  Buffer: $inboundBuffer"
            s += "\n  Region A: $regionA"
            s += "\n  Region B: $regionB"
            s += "\n  Decoding params: $decodingView"
            s += "\n  First undisposed data offset: $undisposedDataOffset"
            s += """
  Message queue size: ${messageQueue.size}"""
            if (!bufferQueue.isEmpty()) {
                s += """
  Undisposed data queue size: ${bufferQueue.size}"""
                val sBuilder = StringBuilder(s)
                for (data in bufferQueue) {
                    sBuilder.append("\n   * ").append(data)
                }
                s = sBuilder.toString()
            }
            s += "\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
            debug("InboundMessageProcessor", s)
        }
    }

    private fun processA() {
        reclaimDisposedBuffers()

        decodingView.undecodedLimit = inboundBuffer.position()
        decode()

        decodingView.unconsumedOffset += consumeA()

        // Resize region A
        regionA!!.setOffsetAndLimit(
            if ((undisposedDataOffset >= 0))
                undisposedDataOffset
            else
                decodingView.unconsumedOffset,
            decodingView.undecodedLimit
        )

        if (regionA!!.offset == regionA!!.limit) {
            // All data has been consumed, we can reset region A and decoding view
            inboundBuffer.limit(inboundBuffer.capacity())
            inboundBuffer.position(0)
            regionA!!.setOffsetAndLimit(0, 0)
            decodingView.unconsumedOffset = 0
            decodingView.undecodedOffset = 0
            decodingView.undecodedLimit = 0
        } else {
            // We need to compare the amount of free space
            // to the left and to the right of region A
            // and create region B, if there's more free space
            // to the left than to the right
            val freeSpaceBeforeOffset = regionA!!.offset - 1
            val freeSpaceAfterLimit = inboundBuffer.capacity() - regionA!!.limit
            if (freeSpaceBeforeOffset > freeSpaceAfterLimit) {
                regionB = Region()
                decodingView.undecodedOffset = regionB!!.offset
                decodingView.undecodedLimit = regionB!!.limit
                // Adjust buffer's position and limit,
                // so that it would be possible to append new data to it
                inboundBuffer.limit(regionA!!.offset - 1)
                inboundBuffer.position(0)
            } else {
                inboundBuffer.limit(inboundBuffer.capacity())
                inboundBuffer.position(decodingView.undecodedLimit)
            }
        }
    }

    private fun reclaimDisposedBuffers() {
        var buffer: BufferedDataWithOffset?
        while ((bufferQueue.peek().also { buffer = it }) != null) {
            checkNotNull(buffer)
            if (buffer.buffer.isDisposed) {
                bufferQueue.remove()
                if (bufferQueue.isEmpty()) {
                    undisposedDataOffset = -1
                }
            } else {
                undisposedDataOffset = buffer.offset
                break
            }
        }
    }

    private fun decode() {
        val dbw = decodingView

        if (dbw.undecodedOffset < dbw.undecodedLimit) {
            inboundBuffer.flip()
            decoders.forEach { cipher: Cipher ->
                inboundBuffer.position(dbw.undecodedOffset)
                mutateBuffer(cipher, inboundBuffer)
            }
            dbw.undecodedOffset = dbw.undecodedLimit
        }
    }

    private fun consumeA(): Int {
        val dbw = decodingView

        var consumed = 0
        if (dbw.unconsumedOffset < dbw.undecodedOffset) {
            bufferView.limit(dbw.undecodedOffset)
            bufferView.position(dbw.unconsumedOffset)

            var message: Message?
            var prevPosition = inboundBuffer.position()
            while (true) {
                message = deserialize(peer, messageHandler, bufferView)
                if (message == null) {
                    break
                } else {
                    if (message is Piece) {
                        val globalOffset = inboundBuffer.position() - message.length
                        // todo duplicate remove
                        processPieceMessage(message, bufferView.duplicate(), globalOffset)
                    }
                    // careful: post message only after having published buffered data
                    messageQueue.add(message)
                    consumed += (inboundBuffer.position() - prevPosition)
                    prevPosition = inboundBuffer.position()
                }
            }
        }
        return consumed
    }

    private fun processAB() {
        debug("InboundMessageProcessor", "processAB")
        reclaimDisposedBuffers()

        decodingView.undecodedLimit = inboundBuffer.position()
        decode()

        regionB!!.setCheckedLimit(decodingView.undecodedLimit)

        if (undisposedDataOffset >= regionA!!.offset) {
            regionA!!.setCheckedOffset(undisposedDataOffset)
        } else if (decodingView.unconsumedOffset >= regionA!!.offset) {
            regionA!!.setCheckedOffset(decodingView.unconsumedOffset)
        } else {
            regionA!!.setCheckedOffset(regionA!!.limit)
        }

        val consumed = consumeAB()
        val consumedA: Int
        val consumedB: Int
        if (decodingView.unconsumedOffset >= regionA!!.offset) {
            consumedA = min(
                consumed.toDouble(),
                (regionA!!.limit - decodingView.unconsumedOffset).toDouble()
            ).toInt()
            consumedB = consumed - consumedA // non-negative
            if (undisposedDataOffset < regionA!!.offset) {
                regionA!!.setCheckedOffset(regionA!!.offset + consumedA)
            }
            if (consumedB > 0) {
                decodingView.unconsumedOffset = regionB!!.offset + consumedB
            } else {
                decodingView.unconsumedOffset += consumedA
            }
        } else {
            consumedB = consumed
            decodingView.unconsumedOffset += consumedB
        }

        if (regionA!!.offset == regionA!!.limit) {
            // Data in region A has been fully processed,
            // so now we promote region B to become region A
            if (undisposedDataOffset < 0) {
                // There's no undisposed data, so we can shrink B
                regionB!!.setCheckedOffset(regionB!!.offset + consumedB)
                if (regionB!!.limit == decodingView.unconsumedOffset || regionB!!.limit == regionB!!.offset) {
                    // Or even reset it, if all of it has been consumed
                    regionB!!.setOffsetAndLimit(0, 0)
                    decodingView.unconsumedOffset = 0
                    decodingView.undecodedOffset = 0
                    decodingView.undecodedLimit = 0
                }
            } else {
                regionB!!.setCheckedOffset(undisposedDataOffset)
            }
            if (decodingView.unconsumedOffset == regionA!!.limit) {
                decodingView.unconsumedOffset = regionB!!.offset
            }
            regionA = regionB
            regionB = null
            inboundBuffer.limit(inboundBuffer.capacity())
        } else {
            inboundBuffer.limit(regionA!!.offset - 1)
        }
        inboundBuffer.position(decodingView.undecodedLimit)
    }

    private fun consumeAB(): Int {
        val dbw = decodingView

        val splicedBuffer: ByteBufferView
        val splicedBufferOffset: Int
        if (dbw.unconsumedOffset >= regionA!!.offset) {
            val regionAView = inboundBuffer.duplicate()
            regionAView.limit(regionA!!.limit)
            regionAView.position(dbw.unconsumedOffset)

            val regionBView = inboundBuffer.duplicate()
            regionBView.limit(dbw.undecodedOffset)
            regionBView.position(regionB!!.offset)

            debug("SplicedByteBufferView", "create SplicedByteBufferView")
            splicedBuffer = SplicedByteBufferView(regionAView, regionBView)
            splicedBufferOffset = dbw.unconsumedOffset
        } else {
            val regionBView = inboundBuffer.duplicate()
            regionBView.limit(dbw.undecodedLimit)
            regionBView.position(dbw.unconsumedOffset)

            splicedBuffer = DelegatingByteBufferView(regionBView)
            splicedBufferOffset = 0
        }

        var message: Message?
        var consumed = 0
        var prevPosition = splicedBuffer.position()
        while (true) {
            message = deserialize(peer, messageHandler, splicedBuffer)
            if (message == null) {
                break
            } else {
                if (message is Piece) {
                    var globalOffset =
                        splicedBufferOffset + (splicedBuffer.position() - message.length)
                    if (globalOffset >= regionA!!.limit) {
                        globalOffset = regionB!!.offset + (globalOffset - regionA!!.limit)
                    }
                    processPieceMessage(message, splicedBuffer.duplicate(), globalOffset)
                }
                // careful: post message only after having published buffered data
                messageQueue.add(message)

                consumed += (splicedBuffer.position() - prevPosition)
                prevPosition = splicedBuffer.position()
            }
        }

        return consumed
    }

    // todo optimize
    private fun processPieceMessage(piece: Piece, buffer: ByteBufferView, globalOffset: Int) {
        val offset = buffer.position() - piece.length
        buffer.limit(buffer.position())
        buffer.position(offset)


        // todo in the future make a slice
        val data: ByteBuffer
        if (buffer is DelegatingByteBufferView) {
            data = buffer.delegate
        } else {
            val raw = ByteArray(piece.length)
            buffer.get(raw)
            data = ByteBuffer.wrap(raw)
        }

        val bufferedData = BufferedData(data, piece.length)
        val added =
            bufferedPieceRegistry.addBufferedPiece(piece.pieceIndex, piece.offset, bufferedData)
        if (added) {
            if (bufferQueue.isEmpty()) {
                undisposedDataOffset = globalOffset
            }
            bufferQueue.add(BufferedDataWithOffset(bufferedData, globalOffset))
        }
    }

    val isClosed: Boolean
        get() = shutdown.value

    private class Region {
        var offset: Int = 0
        var limit: Int = 0

        fun setCheckedOffset(offset: Int) {
            require(offset <= limit) {
                ("offset greater than limit: "
                        + offset + " > " + limit)
            }
            this.offset = offset
        }

        fun setCheckedLimit(limit: Int) {
            require(limit >= offset) {
                ("limit smaller than offset: "
                        + limit + " < " + offset)
            }
            this.limit = limit
        }

        fun setOffsetAndLimit(offset: Int, limit: Int) {
            require(!(offset < 0 || limit < 0 || limit < offset)) {
                ("illegal offset ("
                        + offset + ") and limit (" + limit + ")")
            }
            this.offset = offset
            this.limit = limit
        }
    }

    private class DecodingBufferView {
        var unconsumedOffset: Int = 0
        var undecodedOffset: Int = 0
        var undecodedLimit: Int = 0
    }

    private data class BufferedDataWithOffset(val buffer: BufferedData, val offset: Int)
}

fun deserialize(peer: Peer, messageHandler: MessageHandler, buffer: ByteBufferView): Message? {
    val position = buffer.position()
    val limit = buffer.limit()

    var message: Message? = null
    val result = messageHandler.decode(peer, buffer)
    val consumed = result.consumed
    if (consumed > 0) {
        if (consumed > limit - position) {
            throw RuntimeException("Unexpected amount of bytes consumed: $consumed")
        }
        buffer.position(position + consumed)
        message = checkNotNull(result.message)
    } else {
        buffer.limit(limit)
        buffer.position(position)
    }
    return message
}