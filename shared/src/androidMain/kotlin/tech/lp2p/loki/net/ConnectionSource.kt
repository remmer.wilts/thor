package tech.lp2p.loki.net

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import tech.lp2p.loki.MAX_PEER_CONNECTIONS
import tech.lp2p.loki.debug

internal data class ConnectionSource(
    private val scope: CoroutineScope,
    val connectionFactory: PeerConnectionFactory,
    val connectionPool: PeerConnectionPool,
    val connectionAcceptor: SocketChannelConnectionAcceptor,

    ) {

    @Volatile
    private var shutdown = false

    fun startup() {

        scope.launch {
            var connectionRoutine: ConnectionRoutine
            while (!shutdown) {
                try {
                    connectionRoutine = connectionAcceptor.accept()
                } catch (e: Exception) {
                    debug("IncomingConnectionListener", "Unexpected error", e)
                    break
                }

                if (mightAddConnection()) {
                    establishConnection(connectionRoutine)
                } else {
                    connectionRoutine.cancel()
                }
            }
        }
    }

    fun shutdown() {
        shutdown = true
    }


    fun connect(peer: Peer) {

        if (connectionPool.count() >= MAX_PEER_CONNECTIONS) {
            return
        }

        val connection = connectionPool.getConnection(peer)
        if (connection != null) {
            return
        }

        scope.launch {
            val connection =
                connectionFactory.createOutgoingConnection(peer)
            if (connection != null) {
                val added = connectionPool.addConnectionIfAbsent(connection)
                if (added !== connection) {
                    connection.closeQuietly()
                }
            }
        }


    }

    private fun establishConnection(connectionRoutine: ConnectionRoutine) {
        scope.launch {
            var added = false
            if (!shutdown) {
                val connection = connectionRoutine.establish()
                if (connection != null) {
                    if (!shutdown && mightAddConnection()) {
                        val existing =
                            connectionPool.addConnectionIfAbsent(connection)
                        added = (connection === existing)
                    }
                }
            }
            if (!added) {
                connectionRoutine.cancel()
            }
        }
    }

    private fun mightAddConnection(): Boolean {
        return connectionPool.count() < MAX_PEER_CONNECTIONS
    }


}
