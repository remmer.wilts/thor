package tech.lp2p.loki.net

import tech.lp2p.loki.debug
import java.net.InetSocketAddress
import java.nio.channels.Selector
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel


internal class SocketChannelConnectionAcceptor(
    private val selector: Selector,
    private val connectionFactory: PeerConnectionFactory,
    private val localAddress: InetSocketAddress
) {

    private var serverChannel: ServerSocketChannel? = null
        /**
         * @return Local socket channel, used for accepting the incoming connections
         */
        get() {
            if (field == null) {
                val serverChannelIntern =
                    selector.provider().openServerSocketChannel()
                serverChannelIntern.bind(localAddress)
                serverChannelIntern.configureBlocking(true)
                field = serverChannelIntern
            }
            return field
        }


    fun accept(): ConnectionRoutine {

        val serverChannel: ServerSocketChannel = this.serverChannel!!


        var channel: SocketChannel?
        var remoteAddress: InetSocketAddress? = null

        do {
            channel = serverChannel.accept()
            if (channel != null) {
                try {
                    remoteAddress = channel.remoteAddress as InetSocketAddress
                } catch (e: Exception) {
                    debug(
                        "SocketChannelConnectionAcceptor",
                        "Failed to establish incoming connection",
                        e
                    )
                }
            }
        } while (channel == null || remoteAddress == null)

        return getConnectionRoutine(channel, remoteAddress)

    }

    private fun getConnectionRoutine(
        incomingChannel: SocketChannel,
        remoteAddress: InetSocketAddress
    ): ConnectionRoutine {
        return object : ConnectionRoutine {
            override fun establish(): PeerConnection? {
                return createConnection(incomingChannel, remoteAddress)
            }

            override fun cancel() {
                try {
                    incomingChannel.close()
                } catch (e: Exception) {
                    debug(
                        "SocketChannelConnectionAcceptor",
                        "Failed to close incoming channel: $remoteAddress", e
                    )
                }
            }
        }
    }

    private fun createConnection(
        incomingChannel: SocketChannel,
        remoteAddress: InetSocketAddress
    ): PeerConnection? {
        try {
            val peer: Peer = createPeer(remoteAddress.address, remoteAddress.port)
            return connectionFactory.createIncomingConnection(peer, incomingChannel)
        } catch (e: Exception) {
            debug(
                "SocketChannelConnectionAcceptor",
                "Failed to establish incoming connection from peer: $remoteAddress", e
            )
            try {
                incomingChannel.close()
            } catch (e1: Exception) {
                debug("SocketChannelConnectionAcceptor", e1)
            }
            return null
        }
    }

}
