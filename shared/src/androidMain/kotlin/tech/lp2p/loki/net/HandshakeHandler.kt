package tech.lp2p.loki.net

import tech.lp2p.loki.protocol.Handshake

interface HandshakeHandler {
    /**
     * Process an incoming handshake, received from a remote peer.
     * Implementations are free to send messages via the provided connection
     * and may choose to close it if some of their expectations about the handshake are not met.
     *
     *
     * Attempt to read from the provided connection will trigger an [UnsupportedOperationException].
     *
     */
    fun processIncomingHandshake(connection: PeerConnection, peerHandshake: Handshake)

    /**
     * Make amendments to an outgoing handshake, that will be sent to a remote peer.
     */
    fun processOutgoingHandshake(handshake: Handshake)
}
