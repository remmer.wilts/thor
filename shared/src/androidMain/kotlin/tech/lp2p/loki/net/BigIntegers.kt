package tech.lp2p.loki.net

import java.math.BigInteger
import java.nio.ByteBuffer

/**
 * Encode an arbitrary big integer to its' binary representation,
 * without unnecessary leading zeros that are used in two's-complement form.
 *
 * @param i         Arbitrary big integer
 * @param byteCount Number of bytes to use for encoding
 * @return Byte array containing the binary representation of the big integer left-padded
 * with zeros if necessary

 */
fun encodeUnsigned(i: BigInteger, byteCount: Int): ByteArray {
    require(byteCount >= 1) { "Invalid number of bytes: $byteCount" }
    var bytes = i.toByteArray()
    if (bytes[0].toInt() == 0) {
        // first byte is prepended for a sign bit
        bytes = bytes.copyOfRange(1, bytes.size)
    }
    check(bytes.size <= byteCount) { "Value truncation" }
    if (bytes.size < byteCount) {
        val bytesCopy = bytes
        bytes = ByteArray(byteCount)
        System.arraycopy(bytesCopy, 0, bytes, (bytes.size - bytesCopy.size), bytesCopy.size)
    }
    return bytes
}

/**
 * Decode an unsigned big integer from the provided buffer.
 * Leading zeros are stripped.
 *
 * @param length Number of bytes to read from the buffer; may include leading zeros
 * @return Non-negative big integer
 */

fun decodeUnsigned(buffer: ByteBuffer, length: Int): BigInteger {
    require(length >= 1) { "Invalid number of bytes: $length" }
    check(buffer.remaining() >= length) {
        ("Insufficient bytes in buffer: " + buffer.remaining()
                + ", requested: " + length)
    }
    // strip leading zeros
    var b: Byte
    var i = 0
    @Suppress("ControlFlowWithEmptyBody")
    while ((buffer.get().also { b = it }).toInt() == 0 && ++i < length);

    // all bytes were zeros
    if (i == length) {
        return BigInteger.ZERO
    }

    val len = length - i
    val bytes = ByteArray(len)
    bytes[0] = b
    buffer[bytes, 1, len - 1]
    return BigInteger(1, bytes)
}


