package tech.lp2p.loki.net

import kotlinx.atomicfu.atomic
import tech.lp2p.loki.debug
import tech.lp2p.loki.protocol.Message
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.math.min

class PeerConnection internal constructor(
    val remotePeer: Peer,
    val remotePort: Int,
    private val handler: SocketChannelHandler
) {

    private val lastActive = atomic(0L)

    private val readLock =
        ReentrantLock(true)
    private val condition: Condition = readLock.newCondition()


    @Synchronized
    fun readMessageNow(): Message? {
        val message = handler.receive()
        if (message != null) {
            updateLastActive()
        }
        return message
    }


    @Synchronized
    fun readMessage(timeout: Long): Message? {
        var message = readMessageNow()
        if (message == null) {
            var remaining = timeout

            // ... wait for the incoming message
            while (!handler.isClosed) {
                try {
                    readLock.lock()
                    try {
                        condition.await(
                            min(
                                timeout.toDouble(),
                                WAIT_BETWEEN_READS.toDouble()
                            ).toLong(),
                            TimeUnit.MILLISECONDS
                        )
                    } catch (e: InterruptedException) {
                        throw RuntimeException("Unexpectedly interrupted", e)
                    }
                    remaining -= WAIT_BETWEEN_READS
                    message = readMessageNow()
                    if (message != null) {
                        return message
                    } else if (remaining <= 0) {
                        return null
                    }
                } finally {
                    readLock.unlock()
                }
            }
        }
        return message
    }

    @Synchronized
    fun postMessage(message: Message) {
        updateLastActive()

        handler.send(message)
    }

    private fun updateLastActive() {
        lastActive.value = System.currentTimeMillis()
    }

    fun closeQuietly() {
        try {
            close()
        } catch (e: Throwable) {
            debug("SocketPeerConnection", e)
        }
    }

    fun close() {
        if (!isClosed) {
            handler.close()
        }
    }

    val isClosed: Boolean
        get() = handler.isClosed

    fun getLastActive(): Long {
        return lastActive.value
    }
}

private const val WAIT_BETWEEN_READS = 100L