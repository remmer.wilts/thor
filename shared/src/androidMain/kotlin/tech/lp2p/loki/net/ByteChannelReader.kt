package tech.lp2p.loki.net

import tech.lp2p.loki.buffer.searchPattern
import java.nio.ByteBuffer
import java.nio.channels.ReadableByteChannel
import kotlin.time.Duration

data class ByteChannelReader(
    private val channel: ReadableByteChannel,
    val timeout: Duration,
    val waitBetweenReads: Duration,
    val min: Int,
    val limit: Int
) {

    init {
        require(!(min < 0 || limit < 0 || limit < min)) { "Illegal arguments: min ($min), limit ($limit)" }
    }

    fun readAtLeast(minBytes: Int): ByteChannelReader {
        return ByteChannelReader(channel, timeout, waitBetweenReads, minBytes, limit)
    }

    fun readNoMoreThan(maxBytes: Int): ByteChannelReader {
        return ByteChannelReader(channel, timeout, waitBetweenReads, min, maxBytes)
    }

    fun readBetween(minBytes: Int, maxBytes: Int): ByteChannelReader {
        return ByteChannelReader(channel, timeout, waitBetweenReads, minBytes, maxBytes)
    }


    fun sync(buf: ByteBuffer, syncToken: ByteArray): Int {
        ensureSufficientSpace(buf)
        require(syncToken.isNotEmpty()) { "Empty synchronization token" }

        var searchpos = buf.position()
        val origlim = buf.limit()
        var found = false
        var matchpos = -1
        val t1 = System.currentTimeMillis()
        var readTotal = 0
        var read: Int
        val timeoutMillis = timeoutMillis

        do {
            read = channel.read(buf)
            if (read < 0) {
                throw RuntimeException("Received EOF, total bytes read: $readTotal, expected: $min..$limit")
            } else if (read > 0) {
                readTotal += read
                check(readTotal <= limit) { "More than $limit bytes received: $readTotal" }
                if (!found) {
                    val pos = buf.position()
                    buf.flip()
                    buf.position(searchpos)
                    if (buf.remaining() >= syncToken.size) {
                        if (searchPattern(buf, syncToken)) {
                            found = true
                            matchpos = buf.position()
                        } else {
                            searchpos = pos - syncToken.size + 1
                        }
                    }
                    buf.limit(origlim)
                    buf.position(pos)
                }
            }
            if (found && min > 0 && readTotal >= min) {
                break
            }
        } while (timeoutMillis == 0L || (System.currentTimeMillis() - t1 <= timeoutMillis))

        check(readTotal >= min) { "Less than $min bytes received: $readTotal" }
        check(found) { "Failed to synchronize: expected $min..$limit, received $readTotal" }

        buf.position(matchpos)
        return readTotal
    }


    fun read(buf: ByteBuffer): Int {
        ensureSufficientSpace(buf)

        val t1 = System.currentTimeMillis()
        var readTotal = 0
        var read: Int
        val timeoutMillis = timeoutMillis

        do {
            read = channel.read(buf)
            if (read < 0) {
                throw RuntimeException("Received EOF, total bytes read: $readTotal, expected: $min..$limit")
            } else {
                readTotal += read
            }
            check(readTotal <= limit) { "More than $limit bytes received: $readTotal" }
            if (min in 1..readTotal) {
                break
            }
        } while ((min > 0 && timeoutMillis == 0L) || (System.currentTimeMillis() - t1 <= timeoutMillis))

        check(readTotal >= min) { "Less than $min bytes received: $readTotal" }

        return readTotal
    }

    private val timeoutMillis: Long
        get() {
            return timeout.inWholeMilliseconds
        }


    private fun ensureSufficientSpace(buf: ByteBuffer) {
        require(buf.remaining() >= min) {
            "Insufficient space in buffer: " +
                    buf.remaining() + ", required at least: " + min
        }
    }

    companion object {
        fun forChannel(
            channel: ReadableByteChannel,
            timeout: Duration,
            waitBetweenReads: Duration
        ): ByteChannelReader {
            return ByteChannelReader(channel, timeout, waitBetweenReads, 0, Int.MAX_VALUE)
        }
    }
}
