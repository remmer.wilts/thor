package tech.lp2p.loki.net

import tech.lp2p.loki.protocol.Handshake
import tech.lp2p.loki.protocol.Port

data class DHTHandshakeHandler(val port: Int) : HandshakeHandler {
    override fun processIncomingHandshake(connection: PeerConnection, peerHandshake: Handshake) {
        // according to the spec, the client should immediately communicate its' DHT service port
        // upon receiving a handshake indicating DHT support
        if (peerHandshake.isReservedBitSet(DHT_FLAG_BIT_INDEX)) {
            connection.postMessage(Port(port))
        }
    }

    override fun processOutgoingHandshake(handshake: Handshake) {
        handshake.setReservedBit(DHT_FLAG_BIT_INDEX)
    }

}

private const val DHT_FLAG_BIT_INDEX = 63