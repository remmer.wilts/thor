package tech.lp2p.loki.net

import tech.lp2p.loki.ENCRYPTION_POLICY
import tech.lp2p.loki.Loki
import tech.lp2p.loki.bencoding.BEInteger
import tech.lp2p.loki.bencoding.BEMap
import tech.lp2p.loki.bencoding.BEObject
import tech.lp2p.loki.bencoding.BEString
import tech.lp2p.loki.debug
import tech.lp2p.loki.protocol.EncryptionPolicy
import tech.lp2p.loki.protocol.ExtendedHandshake
import tech.lp2p.loki.protocol.ExtendedMessageHandler
import tech.lp2p.loki.protocol.Handshake
import java.math.BigInteger

internal data class ExtendedProtocolHandshakeHandler(
    private val loki: Loki,
    private val extendedMessages: List<ExtendedMessageHandler>,
    private val tcpAcceptorPort: Int
) : HandshakeHandler {

    private val lazyHandshake = lazy { buildHandshake() }

    private fun buildHandshake(): ExtendedHandshake {
        val data: MutableMap<String, BEObject> = HashMap()
        var messageTypeMap: MutableMap<String, BEObject> = HashMap()


        when (ENCRYPTION_POLICY) {
            EncryptionPolicy.REQUIRE_PLAINTEXT, EncryptionPolicy.PREFER_PLAINTEXT -> {
                data[ENCRYPTION_PROPERTY] = BEInteger(BigInteger.ZERO)
            }

            EncryptionPolicy.PREFER_ENCRYPTED, EncryptionPolicy.REQUIRE_ENCRYPTED -> {
                data[ENCRYPTION_PROPERTY] = BEInteger(BigInteger.ONE)
            }
        }

        data[TCPPORT_PROPERTY] = BEInteger(BigInteger.valueOf(tcpAcceptorPort.toLong()))

        try {
            val source = loki.source
            if (source != null) {
                val metadataSize = source.capacity()
                data[UT_METADATA_SIZE_PROPERTY] = BEInteger(
                    BigInteger.valueOf(metadataSize.toLong())
                )
            }
        } catch (throwable: Throwable) {
            debug("ExtendedHandshakeFactory", throwable)
        }


        data[VERSION_PROPERTY] = BEString(version.toByteArray(Charsets.UTF_8))

        extendedMessages.forEach { handler: ExtendedMessageHandler ->

            if (messageTypeMap.containsKey(handler.localName())) {
                throw RuntimeException("Message type already defined: $handler.localName()")
            }

            messageTypeMap[handler.localName()] = BEInteger(
                BigInteger.valueOf(handler.localTypeId().toLong())
            )

        }
        if (!messageTypeMap.isEmpty()) {
            data[MESSAGE_TYPE_MAPPING_KEY] = BEMap(null, messageTypeMap)
        }
        return ExtendedHandshake(data.toMap())

    }

    override fun processIncomingHandshake(connection: PeerConnection, peerHandshake: Handshake) {
        val extendedHandshake = lazyHandshake.value
        // do not send the extended handshake
        // if local client does not have any extensions turned on
        if (extendedHandshake.data.isNotEmpty()) {
            connection.postMessage(extendedHandshake)
        }
    }

    override fun processOutgoingHandshake(handshake: Handshake) {
        val extendedHandshake = lazyHandshake.value
        // do not advertise support for the extended protocol
        // if local client does not have any extensions turned on
        if (extendedHandshake.data.isNotEmpty()) {
            handshake.setReservedBit(EXTENDED_FLAG_BIT_INDEX)
        }
    }

}

/**
 * Message type mapping key in the extended handshake.
 */
const val MESSAGE_TYPE_MAPPING_KEY: String = "m"

const val ENCRYPTION_PROPERTY: String = "e"
const val TCPPORT_PROPERTY: String = "p"
const val VERSION_PROPERTY: String = "v"

private const val EXTENDED_FLAG_BIT_INDEX = 43

private const val UT_METADATA_SIZE_PROPERTY = "metadata_size"

private const val VERSION_TEMPLATE = "IL %s"

private val version: String
    get() = String.format(
        VERSION_TEMPLATE,
        "0.5.0"
    )