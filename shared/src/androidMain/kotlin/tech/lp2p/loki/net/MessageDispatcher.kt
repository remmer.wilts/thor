package tech.lp2p.loki.net

import kotlinx.atomicfu.locks.reentrantLock
import kotlinx.atomicfu.locks.withLock
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import tech.lp2p.loki.MESSAGE_PROCESSING_INTERVAL
import tech.lp2p.loki.debug
import tech.lp2p.loki.protocol.Message
import tech.lp2p.loki.torrent.Descriptor
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.min

internal class MessageDispatcher(
    private val scope: CoroutineScope,
    private val pool: PeerConnectionPool,
    private val descriptor: Descriptor
) {
    private val consumers:
            MutableMap<Peer, MutableCollection<(Message) -> Unit>> = ConcurrentHashMap()
    private val suppliers:
            MutableMap<Peer, MutableCollection<(Any) -> Message?>> = ConcurrentHashMap()
    private val loopControl = LoopControl(MESSAGE_PROCESSING_INTERVAL)
    private val modificationLock = reentrantLock()


    fun startup() {
        scope.launch {
            try {
                while (!shutdown) {
                    if (consumers.isNotEmpty()) {
                        val iter: MutableIterator<Map.Entry<Peer, Collection<(Message) -> Unit>>> =
                            consumers.entries.iterator()
                        while (iter.hasNext()) {
                            val consumerMapByTorrent = iter.next()
                            val consumerMapByPeer = consumerMapByTorrent.value
                            if (consumerMapByPeer.isEmpty()) {
                                modificationLock.withLock {
                                    if (consumerMapByPeer.isEmpty()) {
                                        iter.remove()
                                    }
                                }
                            }
                            if (isSupportedAndActive()) {
                                processConsumerMap()
                            }
                        }
                    }

                    if (suppliers.isNotEmpty()) {
                        val iter: MutableIterator<Map.Entry<Peer, Collection<(Any) -> Message?>>> =
                            suppliers.entries.iterator()
                        while (iter.hasNext()) {
                            val supplierMapByTorrent = iter.next()
                            val supplierMapByPeer = supplierMapByTorrent.value
                            if (supplierMapByPeer.isEmpty()) {
                                modificationLock.withLock {
                                    if (supplierMapByPeer.isEmpty()) {
                                        iter.remove()
                                    }
                                }
                            }
                            if (isSupportedAndActive()) {
                                processSupplierMap()
                            }
                        }
                    }

                    loopControl.iterationFinished()
                }
            } catch (throwable: Throwable) {
                debug("MessageDispatcher", throwable)
            }
        }
    }

    @Volatile
    private var shutdown = false


    fun isSupportedAndActive(): Boolean {
        // it's OK if descriptor is not present --
        // torrent might be being fetched at the time
        return descriptor.isActive()
    }

    private fun processConsumerMap() {
        val iter: MutableIterator<Map.Entry<Peer, Collection<(Message) -> Unit>>> =
            consumers.iterator()
        while (iter.hasNext()) {
            val e = iter.next()
            val peer = e.key
            val peerConsumers = e.value
            if (peerConsumers.isEmpty()) {
                modificationLock.withLock {
                    if (peerConsumers.isEmpty()) {
                        iter.remove()
                    }
                }
            } else {
                val connection = pool.getConnection(peer)
                if (connection != null && !connection.isClosed) {
                    var message: Message?
                    while (true) {
                        try {
                            message = connection.readMessageNow()
                        } catch (throwable: Throwable) {
                            debug(
                                "MessageDispatcher",
                                "Error when reading message from peer connection: $peer",
                                throwable
                            )
                            break
                        }

                        if (message == null) {
                            break
                        }

                        loopControl.incrementProcessed()
                        for (consumer in peerConsumers) {
                            try {
                                consumer.invoke(message)
                            } catch (ex: Exception) {
                                debug("MessageDispatcher", "Error in message consumer", ex)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun processSupplierMap() {
        val iter: MutableIterator<Map.Entry<Peer, Collection<(Any) -> Message?>>> =
            suppliers.entries.iterator()
        while (iter.hasNext()) {
            val e = iter.next()
            val peer = e.key
            val peerSuppliers = e.value
            if (peerSuppliers.isEmpty()) {
                modificationLock.withLock {
                    if (peerSuppliers.isEmpty()) {
                        iter.remove()
                    }
                }
            } else {
                val connection = pool.getConnection(peer)
                if (connection != null && !connection.isClosed) {
                    for (messageSupplier in peerSuppliers) {
                        var message: Message?
                        try {
                            message = messageSupplier.invoke(Any())
                        } catch (ex: Exception) {
                            debug("MessageDispatcher", "Error in message supplier", ex)
                            continue
                        }

                        loopControl.incrementProcessed()
                        try {
                            if (message != null) {
                                connection.postMessage(message)
                            }
                        } catch (ex: Exception) {
                            debug("MessageDispatcher", "Error when writing message", ex)
                        }
                    }
                }
            }
        }
    }


    fun shutdown() {
        shutdown = true
        consumers.clear()
        suppliers.clear()

    }

    fun addMessageConsumer(peer: Peer, messageConsumer: (Message) -> Unit) {
        modificationLock.withLock {
            val peerConsumers =
                consumers.computeIfAbsent(peer) { _: Peer? -> ConcurrentHashMap.newKeySet() }
            peerConsumers.add(messageConsumer)
        }
    }

    fun addMessageSupplier(peer: Peer, messageSupplier: (Any) -> Message?) {
        modificationLock.withLock {
            val peerSuppliers =
                suppliers.computeIfAbsent(peer) { it: Peer? -> ConcurrentHashMap.newKeySet() }
            peerSuppliers.add(messageSupplier)
        }
    }

    /**
     * Controls the amount of time to sleep after each iteration of the main message processing loop.
     * It implements an adaptive strategy and increases the amount of time for the dispatcher to sleep
     * after each iteration during which no messages were either received or sent.
     * This strategy greatly reduces CPU load when there is little network activity.
     */
    internal class LoopControl(private val maxTimeToSleep: Long) {
        private var messagesProcessed = 0
        private var timeToSleep: Long = 1


        fun reset() {
            messagesProcessed = 0
            timeToSleep = 1
        }

        fun incrementProcessed() {
            messagesProcessed++
        }

        @Synchronized
        fun iterationFinished() {
            if (messagesProcessed > 0) {
                reset()
            } else {
                try {
                    (this as Object).wait(timeToSleep)
                } catch (_: InterruptedException) {
                    debug("MessageDispatcher", "Wait interrupted")
                }

                timeToSleep = if (timeToSleep < maxTimeToSleep) {
                    min((timeToSleep shl 1), maxTimeToSleep)
                } else {
                    maxTimeToSleep
                }
            }
        }
    }


}
