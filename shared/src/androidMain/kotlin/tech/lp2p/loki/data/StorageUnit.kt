package tech.lp2p.loki.data

import tech.lp2p.loki.debug
import tech.lp2p.loki.torrent.TorrentFile
import java.io.File
import java.nio.ByteBuffer
import java.nio.channels.SeekableByteChannel
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.StringTokenizer


class StorageUnit(storage: Storage, torrentFile: TorrentFile) : AutoCloseable {
    private val file: File = getFile(storage, torrentFile)
    private val capacity: Long = torrentFile.size
    private var sbc: SeekableByteChannel

    @Volatile
    private var closed = true

    init {
        require(file.exists()) { "File does not exists" }
        sbc = Files.newByteChannel(
            file.toPath(),
            StandardOpenOption.READ,
            StandardOpenOption.WRITE,
            StandardOpenOption.DSYNC
        )
        closed = false
    }


    private fun readToBuffer(buffer: ByteBuffer, offset: Long): Int {
        if (closed) {
            return -1
        }
        require(offset >= 0) { "Negative offset: $offset" }

        sbc.position(offset)
        return sbc.read(buffer)
    }

    @Synchronized
    fun readBuffer(buffer: ByteBuffer, offset: Long) {
        var read = 0
        var total = 0
        do {
            total += read
            read = readToBuffer(buffer, offset + total)
        } while (read >= 0 && buffer.hasRemaining())
    }

    private fun writeToBuffer(buffer: ByteBuffer, offset: Long): Int {
        if (closed) {
            return -1
        }

        require(offset >= 0) { "Negative offset: $offset" }
        require(offset <= capacity - buffer.remaining()) {
            "Received a request to write past the end of file (offset: " + offset +
                    ", block length: " + buffer.remaining() + ", file capacity: " + capacity
        }

        sbc.position(offset)
        return sbc.write(buffer)
    }

    @Synchronized
    fun writeBuffer(buffer: ByteBuffer, offset: Long) {
        var written = 0
        var total = 0
        do {
            total += written
            written = writeToBuffer(buffer, offset + total)
        } while (written >= 0 && buffer.hasRemaining())
    }

    fun capacity(): Long {
        return capacity
    }

    fun size(): Long {
        return file.length()
    }

    override fun toString(): String {
        return "(" + size() + " of " + capacity + " B) "
    }

    override fun close() {

        if (!closed) {
            try {
                sbc.close()
            } catch (e: Exception) {
                debug("ContentStorageUnit", e)
            } finally {
                closed = true
            }
        }
    }
}

internal object PathNormalizer {
    private val separator: String = File.separator


    fun normalize(path: String): String {
        var normalized = path.trim { it <= ' ' }
        if (normalized.isEmpty()) {
            return "_"
        }

        val tokenizer = StringTokenizer(normalized, separator, true)
        val buf = StringBuilder(normalized.length)
        var first = true
        while (tokenizer.hasMoreTokens()) {
            val element = tokenizer.nextToken()
            if (separator == element) {
                if (first) {
                    buf.append("_")
                }
                buf.append(separator)
                // this will handle inner slash sequences, like ...a//b...
                first = true
            } else {
                buf.append(normalizePathElement(element))
                first = false
            }
        }

        normalized = buf.toString()
        return replaceTrailingSlashes(normalized)
    }

    private fun normalizePathElement(pathElement: String): String {
        // truncate leading and trailing whitespaces
        var normalized = pathElement.trim { it <= ' ' }
        if (normalized.isEmpty()) {
            return "_"
        }

        // truncate trailing whitespaces and dots;
        // this will also eliminate '.' and '..' relative names
        val value = normalized.toCharArray()
        var to = value.size
        while (to > 0 && (value[to - 1] == '.' || value[to - 1] == ' ')) {
            to--
        }
        if (to == 0) {
            normalized = ""
        } else if (to < value.size) {
            normalized = normalized.substring(0, to)
        }

        return normalized.ifEmpty { "_" }
    }

    private fun replaceTrailingSlashes(pathFile: String): String {
        var path = pathFile
        if (path.isEmpty()) {
            return path
        }

        var k = 0
        while (path.endsWith(separator)) {
            path = path.substring(0, path.length - separator.length)
            k++
        }
        if (k > 0) {
            val separatorChars = separator.toCharArray()
            val value = CharArray(path.length + (separatorChars.size + 1) * k)
            System.arraycopy(path.toCharArray(), 0, value, 0, path.length)
            var offset = path.length
            while (offset < value.size) {
                System.arraycopy(separatorChars, 0, value, offset, separatorChars.size)
                value[offset + separatorChars.size] = '_'
                offset += separatorChars.size + 1
            }
            path = String(value)
        }

        return path
    }
}

private fun getFile(
    storage: Storage, torrentFile: TorrentFile
): File {
    val file = storage.rootFile
    require(file.exists()) { "Root directory does not exists" }
    var pathToFile = file
    for (path in torrentFile.pathElements) {
        val normalizePath = PathNormalizer.normalize(path)
        pathToFile = File(pathToFile, normalizePath)
    }
    if (pathToFile.exists()) {
        return pathToFile
    }
    Files.createDirectories(Paths.get(pathToFile.parent))
    Files.createFile(pathToFile.toPath())
    return pathToFile
}