package tech.lp2p.loki.data

import java.nio.ByteBuffer
import kotlin.math.min

/**
 * [fileOffsets] This is a "map" of this range's "virtual addresses" (offsets) into the range's files.
 * Size of this map is equal to the number of range's files.
 * The number x at some position n is the "virtual" offset that designates the beginning
 * of the n-th file. So, a request to build a range beginning with offset x,
 * will translate to offset 0 in the n-th file (i.e. the beginning of the file).
 *
 *
 * Obviously, the number at position 0 is always 0 and translates into the offset in the first
 * file in the range (this offset is set upon creation of the range).
 *
 *
 * Also, it's guaranted that the address at position n+1 is always greater than address at position n.
 */
data class DataRange(
    val units: List<StorageUnit>,
    val lastUnit: Int,
    val offsetInFirstUnit: Long,
    val limitInLastUnit: Long,
    val length: Long,
    val fileOffsets: LongArray
) {
    fun length(): Long {
        return length
    }

    fun getSubrange(offset: Long, length: Long): DataRange {
        require(length != 0L) { "Requested empty subrange, expected length of 1.." + length() }
        require(!(offset < 0 || length < 0)) { "Illegal arguments: offset ($offset), length ($length)" }
        require(offset < length()) { "Offset is too large: " + offset + ", expected 0.." + (length() - 1) }
        if (offset == 0L && length == length()) {
            return this
        }

        var firstRequestedFileIndex: Int
        val limitInLastRequestedFile: Long

        // determine the file that the requested block begins in
        firstRequestedFileIndex = -1
        for (i in units.indices) {
            if (offset < fileOffsets[i]) {
                firstRequestedFileIndex = i - 1
                break
            } else if (i == units.size - 1) {
                // reached the last file
                firstRequestedFileIndex = i
            }
        }

        var offsetInFirstRequestedFile = offset - fileOffsets[firstRequestedFileIndex]
        if (firstRequestedFileIndex == 0) {
            // if the first requested file is the first file in chunk,
            // then we need to begin from this chunk's offset in that file
            // (in case this chunk has access only to a portion of the file)
            offsetInFirstRequestedFile += offsetInFirstUnit
        }

        var lastRequestedFileIndex = firstRequestedFileIndex
        var remaining = length
        do {
            // determine which files overlap with the requested block
            remaining -= if (firstRequestedFileIndex == lastRequestedFileIndex) {
                (units[lastRequestedFileIndex].capacity() - offsetInFirstRequestedFile)
            } else {
                units[lastRequestedFileIndex].capacity()
            }
        } while (remaining > 0 && ++lastRequestedFileIndex < units.size)

        require(lastRequestedFileIndex < units.size) { "Insufficient data (offset: $offset, requested length: $length)" }
        // if remaining is negative now, then we need to
        // strip off some data from the last file
        limitInLastRequestedFile = units[lastRequestedFileIndex].capacity() + remaining

        if (lastRequestedFileIndex == units.size - 1) {
            require(limitInLastRequestedFile <= limitInLastUnit) {
                "Insufficient data (offset: $offset, requested length: $length)"
            }
        }


        val storageUnits = listOf(
            *units.toTypedArray<StorageUnit>()
                .copyOfRange(firstRequestedFileIndex, lastRequestedFileIndex + 1)
        )

        return createReadWriteDataRange(
            storageUnits,
            offsetInFirstRequestedFile,
            limitInLastRequestedFile
        )
    }

    fun getSubrange(offset: Long): DataRange {
        require(offset >= 0) { "Illegal arguments: offset ($offset)" }
        return if (offset == 0L) this else getSubrange(offset, length() - offset)
    }


    fun getBytes(buffer: ByteBuffer): Boolean {
        if (buffer.remaining() < length) {
            return false
        }
        transferToBuffer(buffer)
        return true
    }

    private fun transferToBuffer(buffer: ByteBuffer) {
        val offset = buffer.position()

        visitUnits(object : DataRangeVisitor {
            var offsetInBlock: Int = 0

            override fun visitUnit(unit: StorageUnit, off: Long, lim: Long) {
                val len = lim - off
                check(len <= Int.MAX_VALUE) { "Too much data requested" }

                check((offsetInBlock.toLong()) + len <= Int.MAX_VALUE) { "Integer overflow while constructing block" }

                buffer.limit(offset + offsetInBlock + len.toInt())
                buffer.position(offset + offsetInBlock)
                unit.readBuffer(buffer, off)
                check(!buffer.hasRemaining()) { "Failed to read data fully: " + buffer.remaining() + " bytes not read" }
                offsetInBlock += len.toInt()
            }
        })
    }

    fun putBytes(buffer: ByteBuffer) {
        if (!buffer.hasRemaining()) {
            return
        } else require(buffer.remaining() <= length()) { "Data does not fit in this range" }
        transferFromBuffer(buffer)
    }


    private fun transferFromBuffer(buffer: ByteBuffer) {
        val blockLength = buffer.remaining()
        val offset = buffer.position()

        visitUnits(object : DataRangeVisitor {
            var offsetInBlock: Int = 0
            var limitInBlock: Int = 0

            override fun visitUnit(unit: StorageUnit, off: Long, lim: Long) {
                val fileSize = lim - off
                check(fileSize <= Int.MAX_VALUE) { "Unexpected file size -- insufficient data in block" }

                limitInBlock = min(
                    blockLength.toDouble(),
                    (offsetInBlock + fileSize.toInt()).toDouble()
                ).toInt()
                buffer.limit(offset + limitInBlock)
                buffer.position(offset + offsetInBlock)
                unit.writeBuffer(buffer, off)
                check(!buffer.hasRemaining()) { "Failed to write data fully: " + buffer.remaining() + " bytes remaining" }
                offsetInBlock = limitInBlock
            }
        })
    }

    fun visitUnits(visitor: DataRangeVisitor) {
        var off: Long
        var lim: Long

        for (i in 0..lastUnit) {
            val file = units[i]
            off = if ((i == 0)) offsetInFirstUnit else 0
            lim = if ((i == lastUnit)) limitInLastUnit else file.capacity()

            visitor.visitUnit(file, off, lim)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DataRange

        if (lastUnit != other.lastUnit) return false
        if (offsetInFirstUnit != other.offsetInFirstUnit) return false
        if (limitInLastUnit != other.limitInLastUnit) return false
        if (length != other.length) return false
        if (units != other.units) return false
        if (!fileOffsets.contentEquals(other.fileOffsets)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = lastUnit
        result = 31 * result + offsetInFirstUnit.hashCode()
        result = 31 * result + limitInLastUnit.hashCode()
        result = 31 * result + length.hashCode()
        result = 31 * result + units.hashCode()
        result = 31 * result + fileOffsets.contentHashCode()
        return result
    }
}
