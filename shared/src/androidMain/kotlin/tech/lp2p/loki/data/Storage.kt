package tech.lp2p.loki.data

import tech.lp2p.loki.debug
import tech.lp2p.loki.torrent.TorrentFile
import java.io.File
import java.nio.ByteBuffer
import java.nio.channels.SeekableByteChannel
import java.nio.file.Files
import java.nio.file.StandardOpenOption

data class Storage(val root: File) : AutoCloseable {
    private val one = byteArrayOf(0x01.toByte())
    private val bitmask: File = File(root, "bitmask.db")
    private var sbc: SeekableByteChannel

    init {
        if (!bitmask.exists()) {
            Files.createFile(bitmask.toPath())
        }
        require(bitmask.exists()) { "File does not exists" }
        sbc = Files.newByteChannel(
            bitmask.toPath(),
            StandardOpenOption.READ,
            StandardOpenOption.WRITE,
            StandardOpenOption.DSYNC
        )
    }

    @Synchronized
    fun markVerified(piece: Int) {
        require(sbc.isOpen) { "Bitmask not open" }
        sbc.position(piece.toLong())
        sbc.write(ByteBuffer.wrap(one))
    }


    @Synchronized
    fun verifiedPieces(totalPieces: Int): Bitfield {
        require(sbc.isOpen) { "Bitmask not open" }
        sbc.position(0)

        var read = -1
        val data = ByteArray(totalPieces)
        val buffer = ByteBuffer.wrap(data)
        do {
            read = sbc.read(buffer)
        } while (buffer.hasRemaining() && read >= 0)

        val result = Bitfield(totalPieces)
        repeat(totalPieces) { i ->
            val verified = data[i].toInt() == 1
            if (verified) {
                result.markVerified(i)
            }
        }
        return result
    }

    override fun close() {
        debug("Storage", "close file exists " + bitmask.exists())

        try {
            if (sbc.isOpen) {
                sbc.close()
            }
        } catch (e: Exception) {
            debug("ContentStorageUnit", e)
        }
    }

    fun finish() {
        debug("Storage", "finish")
        close()
        bitmask.delete()
    }

    val rootFile: File
        get() {
            require(root.exists()) { "data directory does not exists" }
            return root
        }

    fun getUnit(torrentFile: TorrentFile): StorageUnit {
        return StorageUnit(this, torrentFile)
    }
}


