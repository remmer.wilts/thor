package tech.lp2p.loki.protocol

class Unchoke : Message {
    override val messageId: Byte
        get() = StandardBittorrentProtocol.UNCHOKE_ID
    override val type: Type
        get() = Type.Unchoke
}

private val instance = Unchoke()

fun unchoke(): Unchoke {
    return instance
}

