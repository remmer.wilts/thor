package tech.lp2p.loki.protocol

import tech.lp2p.loki.SHA1_HASH_LENGTH
import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import tech.lp2p.loki.torrent.TORRENT_ID_LENGTH
import tech.lp2p.loki.torrent.TorrentId
import tech.lp2p.loki.torrent.createTorrentId
import java.nio.ByteBuffer

class StandardBittorrentProtocol(extraHandlers: Map<Byte, MessageHandler>) :
    MessageHandler {
    private val handlers: Map<Byte, MessageHandler>
    private val uniqueTypes: Map<Byte, Type>
    private val handlersByType: Map<Type, MessageHandler>
    private val idMap: Map<Type, Byte>


    init {
        val handlers: MutableMap<Byte, MessageHandler> = HashMap()
        handlers[CHOKE_ID] = ChokeHandler()
        handlers[UNCHOKE_ID] = UnchokeHandler()
        handlers[INTERESTED_ID] = InterestedHandler()
        handlers[NOT_INTERESTED_ID] = NotInterestedHandler()
        handlers[HAVE_ID] = HaveHandler()
        handlers[BITFIELD_ID] = BitfieldHandler()
        handlers[REQUEST_ID] = RequestHandler()
        handlers[PIECE_ID] = PieceHandler()
        handlers[CANCEL_ID] = CancelHandler()

        extraHandlers.forEach { (messageId: Byte, handler: MessageHandler) ->
            if (handlers.containsKey(messageId)) {
                throw RuntimeException("Duplicate handler for message ID: $messageId")
            }
            handlers[messageId] = handler
        }

        val idMap: MutableMap<Type, Byte> = mutableMapOf()
        val handlersByType: MutableMap<Type, MessageHandler> = mutableMapOf()
        val uniqueTypes: MutableMap<Byte, Type> = HashMap()

        handlers.forEach { (messageId: Byte, handler: MessageHandler) ->
            if (handler.supportedTypes().isEmpty()) {
                throw RuntimeException("No supported types declared in handler: " + handler.javaClass.name)
            } else {
                uniqueTypes[messageId] = handler.supportedTypes().iterator().next()
            }
            handler.supportedTypes().forEach { messageType ->
                if (idMap.containsKey(messageType)) {
                    throw RuntimeException("Duplicate handler for message type: $messageType")
                }
                idMap[messageType] = messageId
                handlersByType[messageType] = handler
            }
        }

        this.handlers = handlers
        this.idMap = idMap
        this.handlersByType = handlersByType
        this.uniqueTypes = uniqueTypes
    }

    override fun supportedTypes(): Collection<Type> = emptySet()

    override fun readMessageType(buffer: ByteBufferView): Type? {

        if (!buffer.hasRemaining()) {
            return null
        }

        val position = buffer.position()
        val first = buffer.get()
        if (first.toInt() == PROTOCOL_NAME.length) {
            return Type.Handshake
        }
        buffer.position(position)

        val length: Int? = readInt(buffer)
        if (length == null) {
            return null
        } else if (length == 0) {
            return Type.KeepAlive
        }

        if (buffer.hasRemaining()) {
            val messageTypeId = buffer.get()
            var messageType: Type?

            messageType = uniqueTypes[messageTypeId]
            if (messageType == null) {
                val handler = handlers[messageTypeId]
                requireNotNull(handler) { "Unknown message type ID: $messageTypeId" }
                messageType = handler.readMessageType(buffer)
            }
            return messageType
        }

        return null
    }

    override fun decode(peer: Peer, buffer: ByteBufferView): DecodingResult {

        if (!buffer.hasRemaining()) {
            return DecodingResult(0, null)
        }

        val position = buffer.position()
        val messageType = readMessageType(buffer) ?: return DecodingResult(0, null)

        if (Type.Handshake == messageType) {
            buffer.position(position)
            return decodeHandshake(buffer)
        }
        if (Type.KeepAlive == messageType) {
            return DecodingResult(KEEPALIVE.size, keepAlive())
        }

        val handler = checkNotNull(handlersByType[messageType])
        buffer.position(position)
        return handler.decode(peer, buffer)
    }

    override fun encode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {

        val messageId = idMap[message.type]

        if (Handshake::class.java == message.javaClass) {
            val handshake = message as Handshake
            return writeHandshake(buffer, handshake.reserved, handshake.torrentId, handshake.peerId)
        }
        if (KeepAlive::class.java == message.javaClass) {
            return writeKeepAlive(buffer)
        }

        requireNotNull(messageId) { "Unknown message type: " + message.javaClass.simpleName }

        return checkNotNull(handlers[messageId]).encode(peer, message, buffer)
    }

    companion object {

        const val CHOKE_ID: Byte = 0x0

        const val UNCHOKE_ID: Byte = 0x1

        const val INTERESTED_ID: Byte = 0x2

        const val NOT_INTERESTED_ID: Byte = 0x3

        const val HAVE_ID: Byte = 0x4

        const val BITFIELD_ID: Byte = 0x5

        const val REQUEST_ID: Byte = 0x6

        const val PIECE_ID: Byte = 0x7

        const val CANCEL_ID: Byte = 0x8

        const val PORT_ID: Byte = 9


        /**
         * BitTorrent message ID size in bytes.
         */
        const val MESSAGE_TYPE_SIZE: Int = 1

        /**
         * BitTorrent message prefix size in bytes.
         */
        private const val MESSAGE_LENGTH_PREFIX_SIZE = 4

        /**
         * BitTorrent message prefix size in bytes.
         * Message prefix is a concatenation of message length prefix and message ID.
         */
        const val MESSAGE_PREFIX_SIZE: Int = MESSAGE_LENGTH_PREFIX_SIZE + MESSAGE_TYPE_SIZE
        private const val PROTOCOL_NAME = "BitTorrent protocol"

        private val PROTOCOL_NAME_BYTES = PROTOCOL_NAME.toByteArray(Charsets.US_ASCII)
        private val HANDSHAKE_PREFIX: ByteArray
        private val HANDSHAKE_RESERVED_OFFSET: Int
        private const val HANDSHAKE_RESERVED_LENGTH = 8

        private val KEEPALIVE = byteArrayOf(0, 0, 0, 0)

        init {
            val protocolNameLength = PROTOCOL_NAME_BYTES.size
            val prefixLength = 1

            HANDSHAKE_RESERVED_OFFSET = 1 + protocolNameLength
            HANDSHAKE_PREFIX = ByteArray(HANDSHAKE_RESERVED_OFFSET)
            HANDSHAKE_PREFIX[0] = protocolNameLength.toByte()
            System.arraycopy(
                PROTOCOL_NAME_BYTES,
                0,
                HANDSHAKE_PREFIX,
                prefixLength,
                protocolNameLength
            )
        }

        // keep-alive: <len=0000>
        private fun writeKeepAlive(buffer: ByteBuffer): Boolean {
            if (buffer.remaining() < KEEPALIVE.size) {
                return false
            }
            buffer.put(KEEPALIVE)
            return true
        }

        // handshake: <pstrlen><pstr><reserved><info_hash><peer_id>
        private fun writeHandshake(
            buffer: ByteBuffer,
            reserved: ByteArray,
            torrentId: TorrentId,
            peerId: ByteArray
        ): Boolean {
            require(reserved.size == HANDSHAKE_RESERVED_LENGTH) {
                ("Invalid reserved bytes: expected " + HANDSHAKE_RESERVED_LENGTH
                        + " bytes, received " + reserved.size)
            }

            val length = HANDSHAKE_PREFIX.size + HANDSHAKE_RESERVED_LENGTH +
                    TORRENT_ID_LENGTH + SHA1_HASH_LENGTH
            if (buffer.remaining() < length) {
                return false
            }

            buffer.put(HANDSHAKE_PREFIX)
            buffer.put(reserved)
            buffer.put(torrentId.bytes)
            buffer.put(peerId)

            return true
        }

        private fun decodeHandshake(buffer: ByteBufferView): DecodingResult {
            var consumed = 0
            var message: Message? = null
            val length = HANDSHAKE_RESERVED_LENGTH + TORRENT_ID_LENGTH + SHA1_HASH_LENGTH
            val limit = HANDSHAKE_RESERVED_OFFSET + length

            if (buffer.remaining() >= limit) {
                buffer.get() // skip message ID

                val protocolNameBytes = ByteArray(PROTOCOL_NAME.length)
                buffer.get(protocolNameBytes)
                require(PROTOCOL_NAME_BYTES.contentEquals(protocolNameBytes)) {
                    "Unexpected protocol name (decoded with ASCII): " + String(
                        protocolNameBytes,
                        Charsets.US_ASCII
                    )
                }

                val reserved = ByteArray(HANDSHAKE_RESERVED_LENGTH)
                buffer.get(reserved)

                val infoHash = ByteArray(TORRENT_ID_LENGTH)
                buffer.get(infoHash)

                val peerId = ByteArray(SHA1_HASH_LENGTH)
                buffer.get(peerId)

                message = Handshake(reserved, createTorrentId(infoHash), peerId)
                consumed = limit
            }

            return DecodingResult(consumed, message)
        }
    }
}
