package tech.lp2p.loki.protocol

import tech.lp2p.loki.torrent.TorrentId

/**
 * Standard handshake message.
 * This is the very first message that peers must send,
 * when initializing a new peer connection.
 *
 *
 * Handshake message includes:
 * - a constant header, specified in standard BitTorrent protocol
 * - threads.torrent ID
 * - peer ID
 * - 8 reserved bytes, that are used by extensions, e.g. BEP-10: Extension Protocol
 *
 */
data class Handshake(
    val reserved: ByteArray,
    val torrentId: TorrentId,
    val peerId: ByteArray
) :
    Message {
    /**
     * Check if a reserved bit is set.
     *
     * @param bitIndex Index of a bit to check (0..63 inclusive)
     * @return true if this bit is set to 1
     */
    fun isReservedBitSet(bitIndex: Int): Boolean {
        return getBit(reserved, BitOrder.LITTLE_ENDIAN, bitIndex) == 1
    }

    /**
     * Set a reserved bit.
     *
     * @param bitIndex Index of a bit to set (0..63 inclusive)
     */
    fun setReservedBit(bitIndex: Int) {
        if (bitIndex < 0 || bitIndex > UPPER_RESERVED_BOUND) {
            throw RuntimeException(
                "Illegal bit index: " + bitIndex +
                        ". Expected index in range [0.." + UPPER_RESERVED_BOUND + "]"
            )
        }
        setBit(reserved, BitOrder.LITTLE_ENDIAN, bitIndex)
    }

    override val type: Type
        get() = Type.Handshake

    override val messageId: Byte
        get() {
            throw UnsupportedOperationException()
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Handshake

        if (!reserved.contentEquals(other.reserved)) return false
        if (torrentId != other.torrentId) return false
        if (!peerId.contentEquals(other.peerId)) return false
        if (messageId != other.messageId) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = reserved.contentHashCode()
        result = 31 * result + torrentId.hashCode()
        result = 31 * result + peerId.contentHashCode()
        result = 31 * result + messageId
        result = 31 * result + type.hashCode()
        return result
    }
}
