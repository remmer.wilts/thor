package tech.lp2p.loki.protocol

class Choke : Message {
    override val messageId: Byte
        get() = StandardBittorrentProtocol.CHOKE_ID
    override val type: Type
        get() = Type.Choke


}

private val instance = Choke()

fun choke(): Choke {
    return instance
}