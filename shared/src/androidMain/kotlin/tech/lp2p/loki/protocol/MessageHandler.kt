package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

interface MessageHandler {
    /**
     * Tries to encode the provided message and place the result into the byte buffer.
     */
    fun encode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean

    /**
     * Tries to decode message from the byte buffer. If decoding is successful, then the
     * message is set into result
     */
    fun decode(peer: Peer, buffer: ByteBufferView): DecodingResult

    /**
     * @return All message types, supported by this protocol.
     */
    fun supportedTypes(): Collection<Type>

    /**
     * Tries to determine the message type based on the (part of the) message available in the byte buffer.
     *
     * @param buffer Byte buffer of arbitrary length containing (a part of) the message.
     * Decoding should be performed starting with the current position of the buffer.
     * @return Message type or @{code null} if the data is insufficient
     */
    fun readMessageType(buffer: ByteBufferView): Type?
}

interface ExtendedMessageHandler : MessageHandler {
    fun localTypeId(): Int
    fun localName(): String
}


const val UPPER_RESERVED_BOUND = 8 * 8 - 1
const val HANDSHAKE_RESERVED_LENGTH = 8