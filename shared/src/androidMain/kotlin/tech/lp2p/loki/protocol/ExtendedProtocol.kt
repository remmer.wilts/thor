package tech.lp2p.loki.protocol

import tech.lp2p.loki.HANDSHAKE_TYPE_ID
import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class ExtendedProtocol(messageHandlers: List<ExtendedMessageHandler>) :
    BaseMessageHandler() {

    private val extendedHandshakeHandler = ExtendedHandshakeMessageHandler()
    private val handlers: MutableMap<Type, MessageHandler> = HashMap()
    private val uniqueTypes: MutableMap<String, Type> = HashMap()
    private val handlersByTypeName: MutableMap<String, MessageHandler> = HashMap()
    private val nameMap: MutableMap<Int, String> = HashMap()
    private val typeMap: MutableMap<Type, String> = HashMap()


    init {
        handlers[Type.ExtendedHandshake] = extendedHandshakeHandler

        messageHandlers.forEach { handler ->
            nameMap[handler.localTypeId()] = handler.localName()
            handler.supportedTypes()
                .forEach { messageType -> typeMap[messageType] = handler.localName() }

            if (handler.supportedTypes().isEmpty()) {
                throw RuntimeException("No supported types declared in handler: " + handler.javaClass.name)
            } else {
                uniqueTypes[handler.localName()] = handler.supportedTypes().iterator().next()
            }
            handler.supportedTypes().forEach { messageType ->
                if (handlers.containsKey(messageType)) {
                    throw RuntimeException(
                        "Encountered duplicate handler for message type: $messageType"
                    )
                }
                handlers[messageType] = handler
            }
            this.handlersByTypeName[handler.localName()] = handler
        }
    }

    fun getTypeNameForId(typeId: Int): String {
        return nameMap[typeId]!!
    }

    fun getTypeNameForJavaType(type: Type): String {
        return typeMap[type]!!
    }

    override fun supportedTypes(): Collection<Type> = handlers.keys

    override fun readMessageType(buffer: ByteBufferView): Type? {
        if (!buffer.hasRemaining()) {
            return null
        }
        val messageTypeId = buffer.get().toInt()
        if (messageTypeId == HANDSHAKE_TYPE_ID) {
            return Type.ExtendedHandshake
        }

        var messageType: Type?
        val typeName = getTypeNameForId(messageTypeId)
        messageType = uniqueTypes[typeName]
        if (messageType == null) {
            messageType = checkNotNull(
                handlersByTypeName[typeName]
            ).readMessageType(buffer)
        }
        return messageType
    }

    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        val typeId = buffer.get().toInt()
        val handler: MessageHandler?
        if (typeId == HANDSHAKE_TYPE_ID) {
            handler = extendedHandshakeHandler
        } else {
            val extendedType = getTypeNameForId(typeId)
            handler = handlersByTypeName[extendedType]
        }

        val consumed = checkNotNull(handler).decode(peer, buffer)
        if (consumed.consumed > 0) {
            val newConsumed =
                consumed.consumed + 1 // type ID was trimmed when passing data to handler
            return DecodingResult(newConsumed, consumed.message)
        }
        return consumed
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        val extendedMessage = message as ExtendedMessage
        return doEncode(peer, extendedMessage, extendedMessage.type, buffer)
    }


    private fun doEncode(
        peer: Peer,
        message: Message,
        type: Type,
        buffer: ByteBuffer
    ): Boolean {
        if (!buffer.hasRemaining()) {
            return false
        }

        val begin = buffer.position()
        if (Type.ExtendedHandshake == type) {
            buffer.put(HANDSHAKE_TYPE_ID.toByte())
        } else {
            val typeName = getTypeNameForJavaType(type)
            var typeId: Int? = null
            for (entry in extendedHandshakeHandler.getPeerTypeMapping(peer)) {
                if (entry.value == typeName) {
                    typeId = entry.key
                }
            }
            checkNotNull(typeId) { "Peer does not support extension message: $typeName" }
            buffer.put(typeId.toByte())
        }

        val encoded = checkNotNull(handlers[type]).encode(peer, message, buffer)
        if (!encoded) {
            buffer.position(begin)
        }
        return encoded
    }

}
