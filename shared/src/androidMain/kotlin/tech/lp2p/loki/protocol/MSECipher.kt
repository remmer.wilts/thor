package tech.lp2p.loki.protocol

import android.annotation.SuppressLint
import org.kotlincrypto.hash.sha1.SHA1
import tech.lp2p.loki.torrent.TorrentId
import java.security.Key
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec


/**
 * RC4-drop1024 stream cipher, used in Message Stream Encryption protocol.
 *
 *
 * Ciphers that are returned by [.getEncryptionCipher] and [.getDecryptionCipher]
 * will be different, depending on which of the factory methods was used to build an instance of this class:
 * - connection initiating side should use [.forInitiator] factory method
 * - receiver of connection request should use [.forReceiver] factory method
 */
class MSECipher private constructor(
    sharedSecrets: ByteArray,
    torrentId: TorrentId,
    initiator: Boolean
) {
    /**
     * @return Cipher for decrypting incoming data
     */
    val decryptionCipher: Cipher

    /**
     * @return Cipher for encrypting outgoing data
     */
    val encryptionCipher: Cipher

    init {
        val initiatorKey = getInitiatorEncryptionKey(sharedSecrets, torrentId.bytes)
        val receiverKey = getReceiverEncryptionKey(sharedSecrets, torrentId.bytes)
        val outgoingKey = if (initiator) initiatorKey else receiverKey
        val incomingKey = if (initiator) receiverKey else initiatorKey
        this.decryptionCipher = createCipher(Cipher.DECRYPT_MODE, incomingKey)
        this.encryptionCipher = createCipher(Cipher.ENCRYPT_MODE, outgoingKey)
    }


    companion object {
        fun isKeySizeSupported(keySize: Int): Boolean {
            require(keySize > 0) { "Negative key size: $keySize" }

            val maxAllowedKeySizeBits: Int = Cipher.getMaxAllowedKeyLength("ARCFOUR/ECB/NoPadding")

            return (keySize * 8) <= maxAllowedKeySizeBits
        }

        /**
         * Create MSE cipher for connection initiator
         *
         * @param sharedSecrets         Shared secret
         * @param torrentId Torrent id
         * @return MSE cipher configured for use by connection initiator
         */
        fun forInitiator(sharedSecrets: ByteArray, torrentId: TorrentId): MSECipher {
            return MSECipher(sharedSecrets, torrentId, true)
        }

        /**
         * Create MSE cipher for receiver of the connection request
         *
         * @param sharedSecrets Shared secret
         * @param torrentId Torrent id
         * @return MSE cipher configured for use by receiver of the connection request
         */
        fun forReceiver(sharedSecrets: ByteArray, torrentId: TorrentId): MSECipher {
            return MSECipher(sharedSecrets, torrentId, false)
        }

        private fun getInitiatorEncryptionKey(sharedSecrets: ByteArray, skey: ByteArray): Key {
            return getEncryptionKey("keyA", sharedSecrets, skey)
        }

        private fun getReceiverEncryptionKey(sharedSecrets: ByteArray, skey: ByteArray): Key {
            return getEncryptionKey("keyB", sharedSecrets, skey)
        }

        private fun getEncryptionKey(
            value: String,
            sharedSecrets: ByteArray,
            skey: ByteArray
        ): Key {
            val digest = SHA1()
            digest.update(value.toByteArray(Charsets.US_ASCII))
            digest.update(sharedSecrets)
            digest.update(skey)
            return SecretKeySpec(digest.digest(), "ARCFOUR")
        }


        @SuppressLint("GetInstance")
        private fun createCipher(mode: Int, key: Key): Cipher {
            val cipher: Cipher = Cipher.getInstance("ARCFOUR/ECB/NoPadding")
            cipher.init(mode, key)
            cipher.update(ByteArray(1024)) // discard first 1024 bytes
            return cipher
        }
    }
}
