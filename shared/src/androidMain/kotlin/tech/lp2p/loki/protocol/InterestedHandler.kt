package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class InterestedHandler : UniqueMessageHandler(Type.Interested) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        verifyPayloadHasLength(Type.Interested, 0, buffer.remaining())
        return DecodingResult(0, interested())
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        return true
    }
}
