package tech.lp2p.loki.protocol


data class UtMetadata(
    val metaType: MetaType,
    val pieceIndex: Int,
    val totalSize: Int,
    val data: ByteArray
) : ExtendedMessage {

    override val type: Type
        get() = Type.UtMetadata

    constructor(type: MetaType, pieceIndex: Int) : this(type, pieceIndex, 0, BYTES_EMPTY)

    init {
        require(pieceIndex >= 0) { "Invalid piece index: $pieceIndex" }
        require(totalSize >= 0) { "Invalid total size: $totalSize" }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UtMetadata

        if (pieceIndex != other.pieceIndex) return false
        if (totalSize != other.totalSize) return false
        if (metaType != other.metaType) return false
        if (!data.contentEquals(other.data)) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = pieceIndex
        result = 31 * result + totalSize
        result = 31 * result + metaType.hashCode()
        result = 31 * result + data.contentHashCode()
        result = 31 * result + type.hashCode()
        return result
    }

}

@Suppress("unused")
enum class MetaType(val id: Int) {
    REQUEST(0),
    DATA(1),
    REJECT(2);
}

fun metaTypeForId(id: Int): MetaType {
    for (type in MetaType.entries) {
        if (type.id == id) {
            return type
        }
    }
    throw IllegalArgumentException("Unknown message id: $id")
}

private val BYTES_EMPTY = ByteArray(0)
private const val MSG_TYPE = "msg_type"
private const val PIECE = "piece"
private const val TOTAL_SIZE = "total_size"

fun messageTypeField(): String {
    return MSG_TYPE
}

fun pieceIndexField(): String {
    return PIECE
}

fun totalSizeField(): String {
    return TOTAL_SIZE
}

/**
 * Create metadata request for a given piece.
 */
fun request(pieceIndex: Int): UtMetadata {
    return UtMetadata(MetaType.REQUEST, pieceIndex)
}

/**
 * Create metadata response for a given piece.
 */
fun data(pieceIndex: Int, totalSize: Int, data: ByteArray): UtMetadata {
    return UtMetadata(MetaType.DATA, pieceIndex, totalSize, data)
}

/**
 * Create metadata rejection response for a given piece.
 */
fun reject(pieceIndex: Int): UtMetadata {
    return UtMetadata(MetaType.REJECT, pieceIndex)
}