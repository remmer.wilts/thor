package tech.lp2p.loki.protocol

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import kotlinx.io.writeUShort
import tech.lp2p.loki.bencoding.BEMap
import tech.lp2p.loki.bencoding.BEObject
import tech.lp2p.loki.bencoding.BEString
import tech.lp2p.loki.debug
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class PeerExchange(val added: Collection<Peer>, val dropped: Collection<Peer>) : ExtendedMessage {
    override val type: Type
        get() = Type.PeerExchange

    fun writeTo(out: ByteBuffer) {
        val message = BEMap(null, object : HashMap<String, BEObject>() {
            init {
                val inet4Peers = filterByAddressType(added, AddressType.IPV4)
                val inet6Peers = filterByAddressType(added, AddressType.IPV6)

                put(ADDED_IPV4_KEY, encodePeers(inet4Peers))
                put(ADDED_IPV4_FLAGS_KEY, encodePeerOptions(inet4Peers))
                put(ADDED_IPV6_KEY, encodePeers(inet6Peers))
                put(ADDED_IPV6_FLAGS_KEY, encodePeerOptions(inet6Peers))

                put(
                    DROPPED_IPV4_KEY,
                    encodePeers(filterByAddressType(dropped, AddressType.IPV4))
                )
                put(
                    DROPPED_IPV6_KEY,
                    encodePeers(filterByAddressType(dropped, AddressType.IPV6))
                )
            }
        })
        debug("PeerExchange", "is written")
        val buffer = Buffer()
        message.writeTo(buffer)
        out.put(buffer.readByteArray())
    }


    override fun toString(): String {
        return "[PeerExchange] added peers {$added}, dropped peers {$dropped}"
    }


    init {
        require(!(added.isEmpty() && dropped.isEmpty())) {
            "Can't create PEX message: no peers added/dropped"
        }
    }
}

private const val ADDED_IPV4_KEY = "added"
private const val ADDED_IPV4_FLAGS_KEY = "added.f"
private const val ADDED_IPV6_KEY = "added6"
private const val ADDED_IPV6_FLAGS_KEY = "added6.f"
private const val DROPPED_IPV4_KEY = "dropped"
private const val DROPPED_IPV6_KEY = "dropped6"

private const val CRYPTO_FLAG = 0x01


fun parse(message: BEMap): PeerExchange {
    val map = message.value

    val added: MutableCollection<Peer> = HashSet()
    extractPeers(
        map,
        ADDED_IPV4_KEY,
        ADDED_IPV4_FLAGS_KEY,
        AddressType.IPV4,
        added
    )
    extractPeers(
        map,
        ADDED_IPV6_KEY,
        ADDED_IPV6_FLAGS_KEY,
        AddressType.IPV6,
        added
    )

    val dropped: MutableCollection<Peer> = HashSet()
    extractPeers(map, DROPPED_IPV4_KEY, null, AddressType.IPV4, dropped)
    extractPeers(map, DROPPED_IPV6_KEY, null, AddressType.IPV6, dropped)
    debug("PeerExchange", "is parsed")
    return PeerExchange(added, dropped)
}

private fun extractPeers(
    map: Map<String, BEObject>,
    peersKey: String,
    flagsKey: String?,
    addressType: AddressType,
    destination: MutableCollection<Peer>
) {
    if (map.containsKey(peersKey)) {
        val peers = (checkNotNull(map[peersKey]) as BEString).content
        if (flagsKey != null && map.containsKey(flagsKey)) {
            val flags = (checkNotNull(map[flagsKey]) as BEString).content
            extractPeers(peers, flags, addressType, destination)
        } else {
            extractPeers(peers, addressType, destination)
        }
    }
}

private fun extractPeers(
    peers: ByteArray,
    flags: ByteArray,
    addressType: AddressType,
    destination: MutableCollection<Peer>
) {
    val cryptoFlags = ByteArray(flags.size)
    for (i in flags.indices) {
        cryptoFlags[i] = (flags[i].toInt() and CRYPTO_FLAG).toByte()
    }
    parsePeers(peers, addressType, destination, cryptoFlags)
}

private fun extractPeers(
    peers: ByteArray,
    addressType: AddressType,
    destination: MutableCollection<Peer>
) {
    parsePeers(peers, addressType, destination)
}

private fun filterByAddressType(
    peers: Collection<Peer>,
    addressType: AddressType
): Collection<Peer> {
    return peers.filter { peer: Peer -> peer.inetAddress.address.size == addressType.length }
}

private fun encodePeers(peers: Collection<Peer>): BEString {
    val bos = Buffer()
    for (peer in peers) {
        bos.write(peer.inetAddress.address)
        bos.writeUShort(peer.port.toUShort())
    }
    return BEString(bos.readByteArray())
}

private fun encodePeerOptions(peers: Collection<Peer>): BEString {
    val bos = Buffer()
    for (peer in peers) {
        var options: Byte = 0
        val encryptionPolicy = peer.encryptionPolicy
        if (encryptionPolicy == EncryptionPolicy.PREFER_ENCRYPTED
            || encryptionPolicy == EncryptionPolicy.REQUIRE_ENCRYPTED
        ) {
            options = CRYPTO_FLAG.toByte()
        }
        bos.writeInt(options.toInt())
    }
    return BEString(bos.readByteArray())
}