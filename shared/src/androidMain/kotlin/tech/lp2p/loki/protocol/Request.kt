package tech.lp2p.loki.protocol

data class Request(
    val pieceIndex: Int,
    val offset: Int,
    val length: Int
) : Message {
    override val type: Type
        get() = Type.Request

    override val messageId: Byte
        get() = StandardBittorrentProtocol.REQUEST_ID

    init {
        require(!(pieceIndex < 0 || offset < 0 || length <= 0)) {
            "Illegal arguments: piece index (" +
                    pieceIndex + "), offset (" + offset + "), length (" + length + ")"
        }
    }
}
