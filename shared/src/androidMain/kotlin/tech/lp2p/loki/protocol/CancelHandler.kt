package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class CancelHandler : UniqueMessageHandler(Type.Cancel) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        verifyPayloadHasLength(Type.Cancel, 12, buffer.remaining())
        return decodeCancel(buffer)
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        val cancel = message as Cancel
        return writeCancel(cancel.pieceIndex, cancel.offset, cancel.length, buffer)
    }
}

// cancel: <len=0013><id=8><index><begin><length>
private fun writeCancel(
    pieceIndex: Int,
    offset: Int,
    length: Int,
    buffer: ByteBuffer
): Boolean {
    require(!(pieceIndex < 0 || offset < 0 || length <= 0)) {
        ("Invalid arguments: pieceIndex (" + pieceIndex
                + "), offset (" + offset + "), length (" + length + ")")
    }
    if (buffer.remaining() < Integer.BYTES * 3) {
        return false
    }

    buffer.putInt(pieceIndex)
    buffer.putInt(offset)
    buffer.putInt(length)

    return true
}

private fun decodeCancel(buffer: ByteBufferView): DecodingResult {
    var consumed = 0
    val length = Integer.BYTES * 3
    var message: Message? = null
    if (buffer.remaining() >= length) {
        val pieceIndex = checkNotNull(readInt(buffer))
        val blockOffset = checkNotNull(readInt(buffer))
        val blockLength = checkNotNull(readInt(buffer))

        message = Cancel(pieceIndex, blockOffset, blockLength)
        consumed = length
    }

    return DecodingResult(consumed, message)
}