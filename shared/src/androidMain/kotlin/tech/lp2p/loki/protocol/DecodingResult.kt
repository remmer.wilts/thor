package tech.lp2p.loki.protocol

data class DecodingResult(val consumed: Int, val message: Message?)
