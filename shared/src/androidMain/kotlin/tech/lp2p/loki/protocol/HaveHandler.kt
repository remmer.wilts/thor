package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class HaveHandler : UniqueMessageHandler(Type.Have) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        verifyPayloadHasLength(Type.Have, 4, buffer.remaining())
        return decodeHave(buffer)
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        val have = message as Have
        return writeHave(have.pieceIndex, buffer)
    }

}

private fun writeHave(pieceIndex: Int, buffer: ByteBuffer): Boolean {
    require(pieceIndex >= 0) { "Invalid piece index: $pieceIndex" }
    if (buffer.remaining() < Integer.BYTES) {
        return false
    }

    buffer.putInt(pieceIndex)
    return true
}

private fun decodeHave(buffer: ByteBufferView): DecodingResult {
    var consumed = 0
    var message: Message? = null
    val length = Integer.BYTES

    if (buffer.remaining() >= length) {
        val pieceIndex = checkNotNull(readInt(buffer))
        message = Have(pieceIndex)
        consumed = length
    }

    return DecodingResult(consumed, message)
}