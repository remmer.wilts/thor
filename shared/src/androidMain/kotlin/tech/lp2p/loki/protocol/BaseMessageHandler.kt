package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

abstract class BaseMessageHandler : MessageHandler {
    override fun encode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        if (buffer.remaining() < StandardBittorrentProtocol.MESSAGE_PREFIX_SIZE) {
            return false
        }

        val begin = buffer.position()
        buffer.position(begin + StandardBittorrentProtocol.MESSAGE_PREFIX_SIZE)
        if (doEncode(peer, message, buffer)) {
            val end = buffer.position()
            val payloadLength = end - begin - StandardBittorrentProtocol.MESSAGE_PREFIX_SIZE
            if (payloadLength < 0) {
                throw RuntimeException("Unexpected payload length: $payloadLength")
            }
            buffer.position(begin)
            buffer.putInt(payloadLength + StandardBittorrentProtocol.MESSAGE_TYPE_SIZE)
            buffer.put(message.messageId)
            buffer.position(end)
            return true
        } else {
            buffer.position(begin)
            return false
        }
    }

    /**
     * Encode the payload of a message (excluding message prefix --
     * see [StandardBittorrentProtocol.MESSAGE_PREFIX_SIZE])
     * and write it into the provided buffer.
     */
    protected abstract fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean

    override fun decode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        if (buffer.remaining() < StandardBittorrentProtocol.MESSAGE_PREFIX_SIZE) {
            return DecodingResult(0, null)
        }

        val length = checkNotNull(readInt(buffer))
        if (buffer.remaining() < length) {
            return DecodingResult(0, null)
        }
        buffer.get() // skip message ID

        val initialLimit = buffer.limit()
        buffer.limit(buffer.position() + length - StandardBittorrentProtocol.MESSAGE_TYPE_SIZE)
        try {
            val decodingResult = doDecode(peer, buffer)
            if (decodingResult.message != null) {
                return DecodingResult(
                    decodingResult.consumed +
                            StandardBittorrentProtocol.MESSAGE_PREFIX_SIZE,
                    decodingResult.message
                )
            }
        } finally {
            buffer.limit(initialLimit)
        }
        return DecodingResult(0, null)
    }

    /**
     * Decode the payload of a message (excluding message prefix --
     * see [StandardBittorrentProtocol.MESSAGE_PREFIX_SIZE])
     */
    protected abstract fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult
}
