package tech.lp2p.loki.protocol

import tech.lp2p.loki.EXTENDED_MESSAGE_ID

interface ExtendedMessage : Message {
    override val messageId: Byte
        get() = EXTENDED_MESSAGE_ID
}
