package tech.lp2p.loki.protocol

import tech.lp2p.loki.buffer.ByteBufferView
import tech.lp2p.loki.net.Peer
import java.nio.ByteBuffer

class PieceHandler : UniqueMessageHandler(Type.Piece) {
    public override fun doDecode(peer: Peer, buffer: ByteBufferView): DecodingResult {
        return decodePiece(buffer)
    }

    public override fun doEncode(peer: Peer, message: Message, buffer: ByteBuffer): Boolean {
        val piece = message as Piece
        return writePiece(piece, buffer)
    }
}

// piece: <len=0009+X><id=7><index><begin><block>
private fun writePiece(message: Piece, buffer: ByteBuffer): Boolean {
    val pieceIndex = message.pieceIndex
    val offset = message.offset
    val length = message.length
    if (buffer.remaining() < Integer.BYTES * 2 + length) {
        return false
    }

    buffer.putInt(pieceIndex)
    buffer.putInt(offset)

    val bufferRemaining = buffer.remaining()
    check(
        message.blockRange!!.getSubrange(offset.toLong(), length.toLong()).getBytes(buffer)
    ) {
        "Failed to read data to buffer:" +
                " piece index {" + pieceIndex + "}," +
                " offset {" + offset + "}," +
                " length: {" + length + "}," +
                " buffer space {" + bufferRemaining + "}"
    }
    return true
}

private fun decodePiece(buffer: ByteBufferView): DecodingResult {
    var consumed = 0
    var message: Message? = null
    val pos = buffer.remaining()

    val pieceIndex = checkNotNull(readInt(buffer))
    val blockOffset = checkNotNull(readInt(buffer))
    val blockLength = pos - Integer.BYTES * 2
    buffer.position(buffer.position() + blockLength) // set to the end (todo)

    message = createPiece(pieceIndex, blockOffset, blockLength)
    consumed = pos


    return DecodingResult(consumed, message)
}