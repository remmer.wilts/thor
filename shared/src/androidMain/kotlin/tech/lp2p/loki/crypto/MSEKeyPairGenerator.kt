package tech.lp2p.loki.crypto

import dev.whyoleg.cryptography.random.CryptographyRandom
import tech.lp2p.loki.crypto.MSEKeyPairGenerator.MSEPrivateKey
import tech.lp2p.loki.crypto.MSEKeyPairGenerator.MSEPublicKey
import tech.lp2p.loki.net.decodeUnsigned
import tech.lp2p.loki.net.encodeUnsigned
import java.math.BigInteger
import java.nio.ByteBuffer


class MSEKeyPairGenerator(val privateKeySize: Int) {

    data class MSEKeys(val publicKey: MSEPublicKey, val privateKey: MSEPrivateKey)

    init {
        require(!(privateKeySize < 16 || privateKeySize > 512)) {
            "Illegal key size: $privateKeySize; expected 16..512 bytes"
        }

    }

    fun generateKeys(): MSEKeys {
        val privateKey = MSEPrivateKey(privateKeySize)
        val publicKey: MSEPublicKey = privateKey.publicKey!!
        return MSEKeys(publicKey, privateKey)
    }

    class MSEPublicKey(val value: BigInteger) {
        var encoded: ByteArray = encodeUnsigned(value, PUBLIC_KEY_BYTES)
    }

    class MSEPrivateKey(size: Int) {
        private val value: BigInteger = generatePrivateKey(size)
        private val lock: Any = Any()

        @Volatile
        var publicKey: MSEPublicKey? = null
            get() {
                if (field == null) {
                    synchronized(lock) {
                        if (field == null) {
                            field = MSEPublicKey(
                                G.modPow(
                                    value,
                                    P
                                )
                            )
                        }
                    }
                }
                return field
            }

        fun calculateSharedSecret(publicKey: MSEPublicKey): BigInteger {
            return publicKey.value.modPow(value, P)
        }

    }
}

const val PUBLIC_KEY_BYTES: Int = 96

// 768-bit prime
private val P = BigInteger(
    "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1" +
            "29024E088A67CC74020BBEA63B139B22514A08798E3404DD" +
            "EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245" +
            "E485B576625E7EC6F44C42E9A63A36210000000000090563", 16
)
private val G: BigInteger = BigInteger.valueOf(2)

fun calculateSharedSecret(publicKey: BigInteger, privateKey: MSEPrivateKey): BigInteger {
    return privateKey.calculateSharedSecret(MSEPublicKey(publicKey))
}

// private key: random N bit integer
private fun generatePrivateKey(size: Int): BigInteger {
    val bytes = ByteArray(size)
    for (i in 0 until size) {
        bytes[i] = CryptographyRandom.nextInt(256).toByte()
    }
    return decodeUnsigned(ByteBuffer.wrap(bytes), size)
}