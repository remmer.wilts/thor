package tech.lp2p.loki.crypto

import java.nio.ByteBuffer
import java.nio.channels.ByteChannel
import javax.crypto.Cipher


internal data class EncryptedChannel(
    val delegate: ByteChannel,
    val cipherIn: Cipher,
    val cipherOut: Cipher
) : ByteChannel {

    override fun read(dst: ByteBuffer): Int {
        var read = 0
        if (dst.hasRemaining()) {
            val position = dst.position()
            val limit = dst.limit()
            read = delegate.read(dst)
            if (read > 0) {
                dst.limit(dst.position())
                dst.position(position)
                var bytes = ByteArray(dst.remaining())
                dst[bytes]
                dst.limit(limit)
                dst.position(position)
                bytes = cipherIn.update(bytes)
                dst.put(bytes)
            }
        }
        return read
    }

    override fun write(src: ByteBuffer): Int {
        var written = 0
        if (src.hasRemaining()) {
            val position = src.position()
            var bytes = ByteArray(src.remaining())
            src[bytes]
            src.position(position)
            bytes = cipherOut.update(bytes)
            src.put(bytes)
            src.position(position)
            while (src.hasRemaining()) {
                // write fully
                written += delegate.write(src)
            }
        }
        return written
    }

    override fun isOpen(): Boolean {
        return delegate.isOpen
    }

    override fun close() {
        delegate.close()
    }
}
