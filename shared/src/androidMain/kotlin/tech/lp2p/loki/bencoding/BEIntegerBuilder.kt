package tech.lp2p.loki.bencoding

import java.math.BigInteger

internal class BEIntegerBuilder : BEPrefixedTypeBuilder() {
    private val buf = StringBuilder()

    override fun doAccept(b: Int): Boolean {
        val c = b.toChar()
        if (Character.isDigit(c) || buf.isEmpty() && c == '-') {
            buf.append(c)
            return true
        }
        throw IllegalArgumentException("Unexpected token while reading integer (as ASCII char): $c")
    }

    override fun acceptEOF(): Boolean {
        return true
    }

    override fun type(): BEType {
        return BEType.INTEGER
    }

    override fun doBuild(content: ByteArray): BEInteger {
        return BEInteger(BigInteger(buf.toString()))
    }
}
