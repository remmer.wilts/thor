package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer

data class BEString(val content: ByteArray) : BEObject {

    override fun writeTo(out: Buffer) {
        encode(this, out)
    }

    fun string(): String {
        return String(content, Charsets.UTF_8)
    }

    override fun hashCode(): Int {
        return content.contentHashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is BEString) {
            return false
        }

        if (other === this) {
            return true
        }

        return content.contentEquals(other.content)
    }


    override fun toString(): String {
        return string()
    }
}
