package tech.lp2p.loki.bencoding

internal class BEListBuilder : BEPrefixedTypeBuilder() {
    private val objects: MutableList<BEObject> = ArrayList()
    private var builder: BEObjectBuilder? = null

    override fun doAccept(b: Int): Boolean {
        if (builder == null) {
            val type = getTypeForPrefix(b.toChar())
            builder = builderForType(type)
        }
        if (!builder!!.accept(b)) {
            objects.add(builder!!.build())
            builder = null
            return accept(b, false)
        }
        return true
    }

    override fun acceptEOF(): Boolean {
        return builder == null
    }

    override fun doBuild(content: ByteArray): BEList {
        return BEList(objects)
    }

    override fun type(): BEType {
        return BEType.LIST
    }
}
