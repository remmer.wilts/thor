package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer

/**
 * BEncoded list. May contain objects of different types.
 */
class BEList(value: List<BEObject>) : BEObject {

    override fun writeTo(out: Buffer) {
        encode(this, out)
    }

    override fun equals(other: Any?): Boolean {
        if (other !is BEList) {
            return false
        }
        if (other === this) {
            return true
        }
        return value == other.value
    }


    override fun toString(): String {
        return value.toTypedArray().contentToString()
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    val value: List<BEObject> = value.toList()
}
