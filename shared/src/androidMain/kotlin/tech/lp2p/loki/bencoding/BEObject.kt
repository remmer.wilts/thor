package tech.lp2p.loki.bencoding

import kotlinx.io.Buffer

interface BEObject {
    fun writeTo(out: Buffer)
}
