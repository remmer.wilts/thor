package tech.lp2p.loki.bencoding

/**
 * Sorts byte arrays as if they were raw strings.
 *
 *
 * This means that negative numbers go after positive numbers,
 * because they represent higher order characters (128-255).
 */
class ByteStringComparator : Comparator<ByteArray> {
    override fun compare(o1: ByteArray, o2: ByteArray): Int {
        var i = 0
        var j = 0
        while (i < o1.size && j < o2.size) {
            val k = (o1[i].toInt() and 0xFF) - (o2[j].toInt() and 0xFF)
            if (k != 0) {
                return k
            }
            i++
            j++
        }
        return o1.size - o2.size
    }
}
