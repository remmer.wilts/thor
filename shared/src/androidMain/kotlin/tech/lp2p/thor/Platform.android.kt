package tech.lp2p.thor

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.room.Room
import androidx.room.RoomDatabase
import tech.lp2p.thor.data.Bookmarks
import tech.lp2p.thor.data.Peers
import tech.lp2p.thor.data.Tasks


fun peersBuilder(ctx: Context): RoomDatabase.Builder<Peers> {
    val appContext = ctx.applicationContext
    val dbFile = appContext.getDatabasePath("peers.db")
    return Room.databaseBuilder<Peers>(
        context = appContext,
        name = dbFile.absolutePath
    )
}

fun createPeers(ctx: Context): Peers {
    return peersDatabaseBuilder(peersBuilder(ctx))
}


fun tasksBuilder(ctx: Context): RoomDatabase.Builder<Tasks> {
    val appContext = ctx.applicationContext
    val dbFile = appContext.getDatabasePath("tasks.db")
    return Room.databaseBuilder<Tasks>(
        context = appContext,
        name = dbFile.absolutePath
    )
}

fun createDataStore(context: Context): DataStore<Preferences> = createDataStore(
    producePath = { context.filesDir.resolve(dataStoreFileName).absolutePath }
)


fun bookmarksBuilder(ctx: Context): RoomDatabase.Builder<Bookmarks> {
    val appContext = ctx.applicationContext
    val dbFile = appContext.getDatabasePath("bookmarks.db")
    return Room.databaseBuilder<Bookmarks>(
        context = appContext,
        name = dbFile.absolutePath
    )
}

fun createTasks(ctx: Context): Tasks {
    return tasksDatabaseBuilder(tasksBuilder(ctx))
}


fun createBookmarks(ctx: Context): Bookmarks {
    return bookmarksDatabaseBuilder(bookmarksBuilder(ctx))
}