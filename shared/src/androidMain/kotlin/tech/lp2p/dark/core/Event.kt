package tech.lp2p.dark.core

enum class Event {
    OUTGOING_RESERVE_EVENT, INCOMING_CONNECT_EVENT
}