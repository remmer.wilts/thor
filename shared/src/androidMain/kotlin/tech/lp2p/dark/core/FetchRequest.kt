package tech.lp2p.dark.core

import kotlinx.io.Buffer
import tech.lp2p.halo.Fetch
import tech.lp2p.halo.Halo
import tech.lp2p.lite.Lite


data class FetchRequest(
    val lite: Lite,
    val connector: Connector,
    val request: String,
    val halo: Halo?
) : Fetch {

    override suspend fun fetchBlock(cid: Long): Buffer {
        if (halo != null) {
            if (halo.hasBlock(cid)) {
                return halo.fetchBlock(cid)
            }
        }
        val pnsChannel = connector.connect(lite, request)
        return request(pnsChannel, cid)
    }


    private suspend fun request(channel: Channel, cid: Long): Buffer {
        val payload = channel.request(cid)
        if (halo != null) {
            if (!halo.hasBlock(cid)) {
                halo.storeBlock(cid, payload.copy())
            }
        }
        return payload
    }
}

