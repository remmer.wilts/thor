package tech.lp2p.dark.core

import com.eygraber.uri.Uri
import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.Peeraddr
import tech.lp2p.asen.parsePeeraddr
import tech.lp2p.dark.PNS
import tech.lp2p.dark.debug
import java.math.BigInteger
import java.net.InetAddress
import java.net.Socket
import javax.net.SocketFactory
import kotlin.io.encoding.ExperimentalEncodingApi


const val HASH_LENGTH: Int = 32
const val PEER_VERSION: Byte = 2
const val EOF: Int = -1


internal fun encodePeeraddr(peeraddr: Peeraddr): String {
    return BigInteger(1, peeraddr.encoded()).toString(36)
}

internal fun encodePeerId(peerId: PeerId): String {
    val allocate = Buffer()
    allocate.writeByte(PEER_VERSION)
    allocate.write(peerId.hash)
    return BigInteger(1, allocate.readByteArray()).toString(36)
}


@OptIn(ExperimentalStdlibApi::class)
internal fun encodeCid(cid: Long): String {
    return cid.toString(radix = 16)
}

// BASE 64
@OptIn(ExperimentalEncodingApi::class)
internal fun decodeCid(data: String): Long {
    return data.toLong(radix = 16)
}

internal fun extractCidFromRequest(request: String): Long? {
    val pns = Uri.parse(request)
    validate(pns)
    var path = pns.path
    if (path != null) {
        path = path.trim().removePrefix("/")
        if (path.isNotBlank()) {
            val cid = decodeCid(path)
            return cid
        }
    }
    return null
}


internal fun extractPeeraddrFromRequest(request: String): Peeraddr? {
    val pns = Uri.parse(request)
    val host = validate(pns)
    val userInfo = pns.userInfo
    if (userInfo != null && userInfo.isNotBlank()) {
        try {
            val raw = BigInteger(userInfo, 36).toByteArray()
            val peerId = decodePeerId(host)
            return parsePeeraddr(peerId, raw)
        } catch (throwable: Throwable) {
            debug(throwable)
        }
    }
    return null
}

internal fun validate(pns: Uri): String {
    checkNotNull(pns.scheme) { "Scheme not defined" }
    require(pns.scheme == PNS) { "Scheme not pns" }
    val host = pns.host
    checkNotNull(host) { "Host not defined" }
    require(host.isNotBlank()) { "Host is empty" }
    return host
}

internal fun extractPeerIdFromRequest(request: String): PeerId {
    val pns = Uri.parse(request)
    val host = validate(pns)
    return decodePeerId(host)
}

internal fun longToBytes(value: Long): ByteArray {
    val buffer = Buffer()
    buffer.writeLong(value)
    return buffer.readByteArray()
}


internal fun bytesToLong(value: ByteArray): Long {
    val buffer = Buffer()
    buffer.write(value)
    val value = buffer.readLong()
    require(buffer.exhausted())
    return value
}


internal fun bytesToInt(value: ByteArray): Int {
    val buffer = Buffer()
    buffer.write(value)
    val value = buffer.readInt()
    require(buffer.exhausted())
    return value
}

internal fun intToBytes(value: Int): ByteArray {
    val buffer = Buffer()
    buffer.writeInt(value)
    return buffer.readByteArray()
}

private fun decodePeerId(data: String): PeerId {
    val wrap = Buffer()
    wrap.write(BigInteger(data, 36).toByteArray())
    val version = wrap.readByte()
    require(version == PEER_VERSION) { "Invalid version" }
    val hash = wrap.readByteArray(HASH_LENGTH)
    require(wrap.exhausted()) { "Still data available" }
    return PeerId(hash)
}


internal fun socketClose(socket: Socket) {

    if (!socket.isClosed) {
        try {
            socket.close()
        } catch (throwable: Throwable) {
            debug(throwable)
        }
    }
}


fun openChannel(connector: Connector, peeraddr: Peeraddr): Channel {

    val socket = SocketFactory.getDefault().createSocket(
        InetAddress.getByAddress(peeraddr.address), peeraddr.port.toInt()
    )
    checkNotNull(socket)

    val channel = Channel(peeraddr, connector, socket)
    connector.registerChannel(channel)
    return channel
}

