package tech.lp2p.dark

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import tech.lp2p.asen.Keys
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.PeerStore
import tech.lp2p.asen.Peeraddr
import tech.lp2p.dark.core.Connector
import tech.lp2p.dark.core.Event
import tech.lp2p.dark.core.FetchRequest
import tech.lp2p.dark.core.bytesToLong
import tech.lp2p.dark.core.encodeCid
import tech.lp2p.dark.core.encodePeerId
import tech.lp2p.dark.core.encodePeeraddr
import tech.lp2p.dark.core.extractCidFromRequest
import tech.lp2p.dark.core.extractPeerIdFromRequest
import tech.lp2p.dark.core.extractPeeraddrFromRequest
import tech.lp2p.dark.core.intToBytes
import tech.lp2p.dark.core.longToBytes
import tech.lp2p.dark.core.socketClose
import tech.lp2p.halo.Channel
import tech.lp2p.halo.HALO_ROOT
import tech.lp2p.halo.Halo
import tech.lp2p.halo.Node
import tech.lp2p.halo.Response
import tech.lp2p.halo.contentResponse
import tech.lp2p.halo.core.Fid
import tech.lp2p.halo.core.Raw
import tech.lp2p.halo.core.RawChannel
import tech.lp2p.halo.createChannel
import tech.lp2p.halo.decodeNode
import tech.lp2p.lite.MemoryPeers
import tech.lp2p.lite.generateKeys
import tech.lp2p.lite.newLite
import tech.lp2p.lite.sign
import java.net.ServerSocket
import java.net.Socket
import javax.net.ServerSocketFactory


const val HTML_OK: Int = 200
internal const val RESOLVE_TIMEOUT: Int = 60
internal const val PNS = "pns"


class Dark internal constructor(
    keys: Keys = generateKeys(),
    bootstrap: List<Peeraddr> = emptyList(),
    peerStore: PeerStore = MemoryPeers(),
    val events: (Event) -> Unit = {}
) {

    private val incoming: MutableSet<Socket> = mutableSetOf()
    private val signatures: MutableMap<Long, ByteArray> = mutableMapOf()
    private val mutex = Mutex()
    private val lite = newLite(keys, bootstrap, peerStore) {
        events.invoke(Event.OUTGOING_RESERVE_EVENT)
    }
    private val connector = Connector()
    private var serverSocket: ServerSocket? = null
    private val scope = CoroutineScope(Dispatchers.IO)


    fun runService(halo: Halo, port: Int) {

        serverSocket = ServerSocketFactory.getDefault().createServerSocket(port)

        scope.launch {
            try {
                while (isActive) {

                    // accept a connection
                    var socket: Socket = serverSocket!!.accept()


                    registerIncoming(socket)
                    scope.launch {
                        handleConnection(halo, socket)
                    }
                }
            } catch (_: Throwable) {
            }
        }
    }

    private suspend fun signature(cid: Long): ByteArray {
        mutex.withLock {
            var signature = signatures[cid]
            if (signature != null) {
                return signature
            }
            // create and insert
            val data = longToBytes(cid)
            signature = sign(lite.keys(), data)
            signatures.put(cid, signature)
            return signature
        }
    }


    private suspend fun handleConnection(halo: Halo, socket: Socket) {
        try {
            val payload = ByteArray(Long.SIZE_BYTES)
            socket.getInputStream().use { inputStream ->
                socket.getOutputStream().use { outputStream ->
                    while (scope.isActive) {

                        val read = inputStream.read(payload)
                        if (read != Long.SIZE_BYTES) {
                            break
                        }

                        try {
                            val cid = bytesToLong(payload)
                            val root = halo.root()
                            if (cid == HALO_ROOT) { // root request
                                // root cid
                                val data = longToBytes(root.cid())
                                outputStream.write(intToBytes(data.size))
                                outputStream.write(data)
                                // signature
                                val signature = signature(root.cid())
                                outputStream.write(signature.size)
                                outputStream.write(signature)

                            } else {
                                require(halo.hasBlock(cid)) { "Data not available" }

                                val block = halo.fetchBlock(cid)
                                outputStream.write(intToBytes(block.size.toInt()))
                                outputStream.write(block.readByteArray())
                            }
                            outputStream.flush()
                        } catch (throwable: Throwable) {
                            debug(throwable)
                            removeIncoming(socket)
                            socketClose(socket)
                        }
                    }
                }
            }
        } catch (throwable: Throwable) {
            debug(throwable)
        } finally {
            removeIncoming(socket)
            socketClose(socket)
        }
    }

    private suspend fun registerIncoming(socket: Socket) {
        mutex.withLock {
            incoming.add(socket)
        }
        try {
            events.invoke(Event.INCOMING_CONNECT_EVENT)
        } catch (throwable: Throwable) {
            debug(throwable) // should not occur
        }
    }

    private suspend fun removeIncoming(socket: Socket) {
        mutex.withLock {
            incoming.remove(socket)
        }
        try {
            events.invoke(Event.INCOMING_CONNECT_EVENT)
        } catch (throwable: Throwable) {
            debug(throwable) // should not occur
        }
    }

    /**
     * Find the peer addresses of given target peer ID via the relay.
     *
     * @param relay address of the relay which should be used to the relay connection
     * @param target the target peer ID which addresses should be retrieved

     * @return list of the peer addresses (usually one IPv6 address)
     */
    suspend fun findPeer(relay: Peeraddr, target: PeerId): List<Peeraddr> {
        val peeraddrs = lite.findPeer(relay, target)
        return inet6Public(peeraddrs)
    }

    /**
     * Find the peer addresses of given target peer ID via the **libp2p** relay mechanism.
     *
     * @param target the target peer ID which addresses should be retrieved
     * @param timeout in seconds
     * @return list of the peer addresses (usually one IPv6 address)
     */
    @Suppress("unused")
    suspend fun findPeer(target: PeerId, timeout: Long): List<Peeraddr> {
        val peeraddrs = lite.findPeer(target, timeout)
        return inet6Public(peeraddrs)
    }

    fun keys(): Keys {
        return lite.keys()
    }

    fun peerStore(): PeerStore {
        return lite.peerStore()
    }

    fun reservations(): List<Peeraddr> {
        return lite.reservations()
    }

    fun hasReservations(): Boolean {
        return lite.hasReservations()
    }

    fun numReservations(): Int {
        return lite.numReservations()
    }

    suspend fun numIncomingConnections(): Int {
        return incomingConnections().size
    }

    suspend fun incomingConnections(): List<String> {
        mutex.withLock {
            val result = mutableListOf<String>()
            for (connection in incoming) {
                if (!connection.isClosed) {
                    result.add(connection.remoteSocketAddress.toString())
                } else {
                    incoming.remove(connection)
                }
            }
            return result
        }
    }

    fun numOutgoingConnections(): Int {
        return connector.channels().size
    }

    internal suspend fun fetchRoot(request: String): String {
        val pnsChannel = connector.connect(lite, request)
        val payload = pnsChannel.request(HALO_ROOT)
        val cid = payload.readLong()
        return createRequest(pnsChannel.remotePeeraddr, cid)
    }


    @Suppress("unused")
    suspend fun response(request: String, halo: Halo? = null): Response {
        try {
            val node = info(request, halo) // is resolved
            return if (node is Fid) {
                val channel = channel(request, halo)
                contentResponse(channel, node)
            } else {
                val buffer = Buffer()
                buffer.write((node as Raw).data())

                contentResponse(RawChannel(buffer), "OK", HTML_OK)
            }
        } catch (throwable: Throwable) {
            var message = throwable.message
            if (message.isNullOrEmpty()) {
                message = "Service unavailable"
            }
            return contentResponse(RawChannel(Buffer()), message, 500)

        }
    }


    suspend fun channel(request: String, halo: Halo? = null): Channel {
        val node = info(request, halo)
        if (node is Fid) {
            val fetch = FetchRequest(lite, connector, request, halo)
            return createChannel(node, fetch)
        }
        throw Exception("Stream on directory or raw not possible")
    }

    fun peerId(): PeerId {
        return lite.peerId()
    }

    /**
     * Makes a reservation o relay nodes with the purpose that other peers can fin you via
     * the nodes peerId
     *
     * @param maxReservation number of max reservations
     * @param timeout in seconds
     */
    suspend fun makeReservations(
        peeraddrs: List<Peeraddr>,
        maxReservation: Int,
        timeout: Int,
        running: (Boolean) -> Unit = {}
    ) {
        return lite.makeReservations(peeraddrs, maxReservation, timeout, running)
    }


    suspend fun info(request: String, halo: Halo? = null): Node {
        val cid = extractCid(request)
        if (cid != null) {
            val fetch = FetchRequest(lite, connector, request, halo)
            return decodeNode(cid, fetch.fetchBlock(cid))
        } else {
            return info(fetchRoot(request), halo)
        }
    }

    suspend fun fetchData(request: String, halo: Halo? = null): ByteArray {
        val node = info(request, halo)
        if (node is Raw) {
            return node.data()
        }
        throw Exception("Uri does not reference raw node")
    }

    suspend fun reachable(peeraddr: Peeraddr): Boolean {
        try {
            connector.connect(lite, createRequest(peeraddr))
            return true
        } catch (throwable: Throwable) {
            debug(throwable)
        }
        return false
    }


    suspend fun shutdown() {

        lite.shutdown()

        connector.shutdown()

        incoming.forEach { socket: Socket -> socketClose(socket) }
        incoming.clear()


        try {
            scope.cancel()
        } catch (throwable: Throwable) {
            debug(throwable)
        }
        try {
            serverSocket?.close()
        } catch (throwable: Throwable) {
            debug(throwable)
        }
    }
}


fun newDark(
    keys: Keys = generateKeys(),
    bootstrap: List<Peeraddr> = emptyList(),
    peerStore: PeerStore = MemoryPeers(),
    events: (Event) -> Unit = {}
): Dark {
    return Dark(keys, bootstrap, peerStore, events)
}


private fun inet6Public(peeraddrs: List<Peeraddr>): List<Peeraddr> {
    val result = mutableListOf<Peeraddr>()
    for (peer in peeraddrs) {
        if (peer.inet6()) {
            if (!peer.isLanAddress()) {
                result.add(peer)
            }
        }
    }
    return result
}


fun createRequest(peeraddr: Peeraddr): String {
    return PNS + "://" + encodePeeraddr(peeraddr) + "@" + encodePeerId(peeraddr.peerId)
}

fun createRequest(peerId: PeerId): String {
    return PNS + "://" + encodePeerId(peerId)
}

fun createRequest(peerId: PeerId, cid: Long): String {
    return createRequest(peerId) + relativePath(cid)
}

fun createRequest(peeraddr: Peeraddr, cid: Long): String {
    return createRequest(peeraddr) + relativePath(cid)
}

fun createRequest(peerId: PeerId, node: Node): String {
    return createRequest(peerId, node.cid())
}

fun createRequest(peeraddr: Peeraddr, node: Node): String {
    return createRequest(peeraddr, node.cid())
}

fun relativePath(cid: Long): String {
    return "/" + encodeCid(cid)
}

fun extractPeerId(request: String): PeerId? {
    return extractPeerIdFromRequest(request)
}

fun extractPeeraddr(request: String): Peeraddr? {
    return extractPeeraddrFromRequest(request)
}

fun extractCid(request: String): Long? {
    return extractCidFromRequest(request)
}


fun debug(throwable: Throwable) {
    if (DEBUG) {
        throwable.printStackTrace()
    }
}

private const val DEBUG: Boolean = true
