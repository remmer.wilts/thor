package tech.lp2p.lite.quic

enum class FrameType {
    AckFrame, CryptoFrame, StreamFrame, ConnectionCloseFrame,
    DataBlockedFrame, StreamDataBlockedFrame, HandshakeDoneFrame,
    MaxDataFrame, PingFrame, PaddingFrame, NewTokenFrame,
    ResetStreamFrame, MaxStreamDataFrame, MaxStreamsFrame,
    NewConnectionIdFrame, PathChallengeFrame, PathResponseFrame,
    RetireConnectionIdFrame, StopSendingFrame, StreamsBlockedFrame
}
