package tech.lp2p.lite.quic


data class PacketStatus(val packet: Packet, val timeSent: Long, val size: Int)

