package tech.lp2p.lite.quic

import kotlinx.io.Buffer


data class AlpnResponder(
    val stream: Stream,
    val responder: Responder,
    val streamState: Libp2pState
) : StreamHandler {
    override suspend fun data(data: Buffer) {
        try {
            StreamState.iteration(streamState, stream, data)
        } catch (_: Exception) {
            stream.resetStream(Settings.PROTOCOL_NEGOTIATION_FAILED.toLong())
        } catch (_: Throwable) {
            stream.resetStream(Settings.INTERNAL_ERROR.toLong())
        }
    }

    override fun terminated() {
        streamState.reset()
    }

    override fun fin() {
        streamState.reset()
    }


    override fun readFully(): Boolean {
        return false
    }


    class Libp2pState(private val responder: Responder) : StreamState() {
        var protocol: String? = null

        public override suspend fun accept(stream: Stream, frame: ByteArray) {
            if (frame.isNotEmpty()) {
                if (isProtocol(frame)) {
                    protocol = String(frame, 0, frame.size - 1)
                    responder.protocol(stream, protocol!!)
                } else {
                    responder.data(stream, protocol!!, frame)
                }
            }
        }
    }
}

