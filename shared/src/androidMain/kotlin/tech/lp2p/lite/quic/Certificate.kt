package tech.lp2p.lite.quic

import tech.lp2p.lite.cert.ASN1Encodable
import tech.lp2p.lite.cert.ASN1Object
import tech.lp2p.lite.cert.ASN1OctetString
import tech.lp2p.lite.cert.ASN1Primitive
import tech.lp2p.lite.cert.DEROctetString
import tech.lp2p.lite.cert.DERSequence
import java.security.PrivateKey

data class Certificate(
    private val encoded: ByteArray,
    private val certificateKey: PrivateKey,
    private val sigAlgName: String
) : tech.lp2p.lite.tls.Certificate {
    class SignedKey internal constructor(pubKey: ByteArray, signature: ByteArray) : ASN1Object() {
        private val pubKey: ASN1OctetString = DEROctetString(pubKey)
        private val signature: ASN1OctetString = DEROctetString(signature)

        override fun toASN1Primitive(): ASN1Primitive {
            val asn1Encodables = arrayOf<ASN1Encodable>(this.pubKey, this.signature)
            return DERSequence(asn1Encodables)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Certificate

        if (!encoded.contentEquals(other.encoded)) return false
        if (certificateKey != other.certificateKey) return false

        return true
    }

    override fun hashCode(): Int {
        var result = encoded.contentHashCode()
        result = 31 * result + certificateKey.hashCode()
        return result
    }

    override fun encoded(): ByteArray {
        return encoded
    }

    override fun sigAlgName(): String {
        return sigAlgName
    }

    override fun certificateKey(): PrivateKey {
        return certificateKey
    }
}