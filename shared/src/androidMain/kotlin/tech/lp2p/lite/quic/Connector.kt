package tech.lp2p.lite.quic

import tech.lp2p.lite.debug
import java.util.concurrent.ConcurrentHashMap


class Connector(private val reserve: (Any) -> Unit) {
    private val connections: MutableSet<ClientConnection> = ConcurrentHashMap.newKeySet()

    fun connections(): Set<ClientConnection> {
        val clientConnections: MutableSet<ClientConnection> = HashSet()
        for (connection in connections) {
            if (connection.isConnected) {
                clientConnections.add(connection)
            } else {
                removeConnection(connection)
            }
        }
        return clientConnections
    }

    suspend fun shutdown() {
        try {
            connections.forEach { obj: ClientConnection -> obj.close() }
            connections.clear()
        } catch (throwable: Throwable) {
            debug(throwable) // should not occur
        }
    }

    fun registerConnection(connection: ClientConnection) {
        connections.add(connection)
    }

    fun removeConnection(connection: ClientConnection) {
        connections.remove(connection)
        if (connection.isMarked()) {
            reserve.invoke(Any())
        }
    }
}
