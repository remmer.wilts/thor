package tech.lp2p.lite.quic


data class PacketHeader(
    val level: Level, val version: Int, val dcid: Number, val scid: Int?,
    val framesBytes: ByteArray, val packetNumber: Long, val updated: Keys?
) {
    fun hasUpdatedKeys(): Boolean {
        return updated != null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PacketHeader

        if (level != other.level) return false
        if (version != other.version) return false
        if (dcid != other.dcid) return false
        if (scid != other.scid) return false
        if (!framesBytes.contentEquals(other.framesBytes)) return false
        if (packetNumber != other.packetNumber) return false
        if (updated != other.updated) return false

        return true
    }

    override fun hashCode(): Int {
        var result = level.hashCode()
        result = 31 * result + version
        result = 31 * result + dcid.hashCode()
        result = 31 * result + (scid ?: 0)
        result = 31 * result + framesBytes.contentHashCode()
        result = 31 * result + packetNumber.hashCode()
        result = 31 * result + (updated?.hashCode() ?: 0)
        return result
    }
}
