package tech.lp2p.lite.quic

import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.updateAndGet
import tech.lp2p.lite.quic.FrameReceived.AckFrame
import java.util.concurrent.ConcurrentSkipListMap
import java.util.concurrent.ConcurrentSkipListSet
import kotlin.math.min


/**
 * Listens for received packets and generates ack frames for them.
 */
open class AckGenerator {
    private val rangesToAcknowledge = ConcurrentSkipListSet { obj: Long, aLong: Long ->
        obj.compareTo(
            aLong
        )
    }
    private val ackSentWithPacket =
        ConcurrentSkipListMap<Long, Array<Long>> { obj: Long, aLong: Long ->
            obj.compareTo(
                aLong
            )
        }
    private val newPacketsToAcknowlegdeSince = atomic(Settings.NOT_DEFINED.toLong())
    private val acksNotSend = atomic(0)


    // only invoked from the SENDER THREAD
    fun mustSendAck(level: Level): Boolean {
        val acknowlegdeSince = newPacketsToAcknowlegdeSince.value
        if (acknowlegdeSince != Settings.NOT_DEFINED.toLong()) {
            // precondition that new packages have arrived
            if (level == Level.App) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-13.2.2
                // "A receiver SHOULD send an ACK frame after receiving at least two
                // ack-eliciting packets."

                return if (acksNotSend.value >= Settings.ACK_FREQUENCY_TWO) {
                    true
                } else {
                    // NOTE: here is the case for 1 acknowledged packet
                    // check when the current time is greater then the first arrived package
                    // plus max ack delay, then a ack must send (returns true)
                    System.currentTimeMillis() > acknowlegdeSince + Settings.MAX_ACK_DELAY
                }
            }
            return true // for other level it is always without any delay
        }
        return false
    }


    // only invoked from the SENDER THREAD
    fun cleanup() {
        rangesToAcknowledge.clear()
        ackSentWithPacket.clear()
        acksNotSend.value = 0
        newPacketsToAcknowlegdeSince.value = Settings.NOT_DEFINED.toLong()
    }


    // only invoked from the RECEIVER THREAD
    fun packetReceived(
        level: Level,
        isAckEliciting: Boolean,
        packetNumber: Long,
        timeReceived: Long
    ) {
        rangesToAcknowledge.add(packetNumber)

        if (isAckEliciting) {
            newPacketsToAcknowlegdeSince.updateAndGet { operand: Long ->
                if (operand == Settings.NOT_DEFINED.toLong()) {
                    return@updateAndGet timeReceived
                }
                operand
            }

            if (level == Level.App) {
                acksNotSend.incrementAndGet()
            }
        }
    }


    // only invoked from the RECEIVER THREAD
    fun ackFrameReceived(receivedAck: AckFrame) {
        // Find max packet number that had an ack sent with it...

        var largestWithAck: Long = -1

        // it is ordered, packet status with highest packet number is at the top
        val acknowledgedRanges = receivedAck.acknowledgedRanges
        var i = 0
        while (i < acknowledgedRanges.size) {
            val to = acknowledgedRanges[i]
            i++
            val from = acknowledgedRanges[i]
            for (pn in to downTo from) {
                if (ackSentWithPacket.containsKey(pn)) {
                    largestWithAck = pn
                    break
                }
            }
            if (largestWithAck > 0) {
                break
            }
            i++
        }


        if (largestWithAck > 0) {
            // ... and for that max pn, all packets that where acked by it don't need to be acked again.

            val latestAcknowledgedAck = ackSentWithPacket[largestWithAck]

            if (latestAcknowledgedAck != null) {
                for (packet in latestAcknowledgedAck) {
                    rangesToAcknowledge.remove(packet)
                }
            }

            // And for all earlier sent packets (smaller packet numbers), the sent ack's can be discarded because
            // their ranges are a subset of the ones from the latestAcknowledgedAck and thus are now implicitly acked.
            val oldEntries: Set<Long> = ackSentWithPacket.headMap(largestWithAck).keys
            for (old in oldEntries) {
                ackSentWithPacket.remove(old)
            }
        }
    }

    // only invoked from the SENDER THREAD
    fun generateAck(packetNumber: Long, acceptLength: (Int) -> Boolean): Frame? {
        if (!rangesToAcknowledge.isEmpty()) {
            var delay = 0
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-13.2.1
            // "An endpoint MUST acknowledge all ack-eliciting Initial and Handshake packets immediately"
            val acknowlegdeSince = newPacketsToAcknowlegdeSince.value
            if (acknowlegdeSince != Settings.NOT_DEFINED.toLong()) {
                delay = min(
                    (System.currentTimeMillis() - acknowlegdeSince).toInt(), 0
                )
            }

            val packets = rangesToAcknowledge.toTypedArray()

            // Range list must not be modified during frame initialization
            // (guaranteed by this method being sync'd)
            val frame = createAckFrame(packets, delay)
            if (acceptLength.invoke(frame.frameLength())) {
                ackSentWithPacket[packetNumber] = packets
                newPacketsToAcknowlegdeSince.value = Settings.NOT_DEFINED.toLong()
                acksNotSend.value = 0
                return frame
            }
        }
        return null
    }

}

