package tech.lp2p.lite.cert

import java.security.PrivateKey
import java.security.Signature

data class JcaContentSignerBuilder(val signatureAlgorithm: String) {

    fun build(privateKey: PrivateKey): ContentSigner {
        val sigAlgId = find(signatureAlgorithm)


        val sigName = getAlgorithmName(sigAlgId)
        val sig = Signature.getInstance(sigName)
        sig.initSign(privateKey)

        return JcaContentSigner(sig, sigAlgId)
    }

    private class JcaContentSigner(
        private val sig: Signature,
        private val signatureAlgId: AlgorithmIdentifier
    ) :
        ContentSigner {

        override fun getAlgorithmIdentifier(): AlgorithmIdentifier {
            return signatureAlgId
        }

        override fun signature(): Signature {
            return sig
        }

        override fun getSignature(): ByteArray {
            return sig.sign()
        }
    }
}