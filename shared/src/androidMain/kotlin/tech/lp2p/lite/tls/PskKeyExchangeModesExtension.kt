/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

/**
 * TLS Pre-Shared Key Exchange Modes extension.
 * See [...](https://tools.ietf.org/html/rfc8446#section-4.2.9)
 */
data class PskKeyExchangeModesExtension(val keyExchangeModes: Array<PskKeyExchangeMode>) :
    Extension {
    override fun getBytes(): ByteArray {
        val extensionLength = (1 + keyExchangeModes.size).toShort()
        val buffer = Buffer()

        buffer.writeShort(ExtensionType.ASK_KEY_EXCHANGE_MODES.value)
        buffer.writeShort(extensionLength) // Extension payload length (in bytes)

        buffer.writeByte(keyExchangeModes.size.toByte())
        keyExchangeModes.forEach { mode: PskKeyExchangeMode -> buffer.writeByte(mode.value) }
        require(buffer.size.toInt() == 4 + extensionLength)
        return buffer.readByteArray()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PskKeyExchangeModesExtension

        return keyExchangeModes.contentEquals(other.keyExchangeModes)
    }

    override fun hashCode(): Int {
        return keyExchangeModes.contentHashCode()
    }

    companion object {
        fun createPskKeyExchangeModesExtension(keyExchangeMode: PskKeyExchangeMode): PskKeyExchangeModesExtension {
            val keyExchangeModes =
                arrayOf(keyExchangeMode)
            return PskKeyExchangeModesExtension(keyExchangeModes)
        }

        fun createPskKeyExchangeModesExtension(keyExchangeModes: Array<PskKeyExchangeMode>): PskKeyExchangeModesExtension {
            return PskKeyExchangeModesExtension(keyExchangeModes)
        }

        fun parse(buffer: Buffer, extensionLength: Int): PskKeyExchangeModesExtension {
            val keyExchangeModes: MutableList<PskKeyExchangeMode> = ArrayList()
            val extensionDataLength = validateExtensionHeader(
                buffer, extensionLength, 2
            )
            val pskKeyExchangeModesLength = buffer.readByte().toInt()
            if (extensionDataLength != 1 + pskKeyExchangeModesLength) {
                throw DecodeErrorException("inconsistent length")
            }
            repeat(pskKeyExchangeModesLength) {
                val modeByte = buffer.readByte().toInt()
                when (modeByte) {
                    PskKeyExchangeMode.PSK_KE.value.toInt() -> {
                        keyExchangeModes.add(PskKeyExchangeMode.PSK_KE)
                    }

                    PskKeyExchangeMode.PSK_DHE_KE.value.toInt() -> {
                        keyExchangeModes.add(PskKeyExchangeMode.PSK_DHE_KE)
                    }

                    else -> {
                        throw DecodeErrorException("invalid psk key exchange mocde")
                    }
                }
            }

            return PskKeyExchangeModesExtension(keyExchangeModes.toTypedArray())
        }
    }
}
