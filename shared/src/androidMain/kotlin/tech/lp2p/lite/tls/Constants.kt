package tech.lp2p.lite.tls


enum class PskKeyEstablishmentMode {
    NONE,
    PSK_ONLY,
    PSK_DHE,
    BOTH
}


@Suppress("unused")
enum class Status {
    Initial,
    ClientHelloSent,
    ServerHelloReceived,
    EncryptedExtensionsReceived,
    CertificateRequestReceived,
    CertificateReceived,
    CertificateVerifyReceived,
    Finished,
    ClientHelloReceived,
    ServerHelloSent,
    EncryptedExtensionsSent,
    CertificateRequestSent,
    CertificateSent,
    CertificateVerifySent,
    FinishedSent,
    FinishedReceived
}

@Suppress("unused")
enum class SignatureScheme(value: Int) {
    /* RSASSA-PKCS1-v1_5 algorithms */
    RSA_PKCS1_SHA256(0x0401),
    RSA_PKCS1_SHA384(0x0501),
    RSA_PKCS1_SHA512(0x0601),

    /* ECDSA algorithms */
    ECDSA_SECP256R1_SHA256(0x0403),
    ECDSA_SECP384R1_SHA384(0x0503),
    ECDSA_SECP521R1_SHA512(0x0603),

    /* RSASSA-PSS algorithms with public key OID rsaEncryption */
    RSA_PSS_RSAE_SHA256(0x0804),
    RSA_PSS_RSAE_SHA384(0x0805),
    RSA_PSS_RSAE_SHA512(0x0806),

    /* EdDSA algorithms */
    ED25519(0x0807),
    ED448(0x0808),

    /* RSASSA-PSS algorithms with public key OID RSASSA-PSS */
    RSA_PSS_PSS_SHA256(0x0809),
    RSA_PSS_PSS_SHA384(0x080a),
    RSA_PSS_PSS_SHA512(0x080b),

    /* Legacy algorithms */
    RSA_PKCS1_SHA1(0x0201),
    ECDSA_SHA1(0x0203),
    ;

    val value: Short = value.toShort()


    companion object {
        private val byValue: MutableMap<Short, SignatureScheme> = HashMap()

        init {
            for (t in entries) {
                byValue[t.value] = t
            }
        }


        fun get(value: Short): SignatureScheme {
            val signatureScheme = byValue[value]
                ?: throw DecodeErrorException("invalid signature scheme value")
            return signatureScheme
        }
    }
}

enum class ProtectionKeysType {
    None,
    Handshake,
    Application
}

@Suppress("RedundantSuppression", "unused")
internal enum class ExtendedHandshakeType {
    CLIENT_HELLO,
    SERVER_HELLO,
    NEW_SESSION_TICKET,
    END_OF_EARLY_DATA,
    ENCRYPTED_EXTENSIONS,
    CERTIFICATE,
    CERTIFICATE_REQUEST,
    CERTIFICATE_VERIFY,
    FINISHED,
    KEY_UPDATE,
    SERVER_CERTIFICATE,
    SERVER_CERTIFICATE_VERIFY,
    SERVER_FINISHED,
    CLIENT_CERTIFICATE,
    CLIENT_CERTIFICATE_VERIFY,
    CLIENT_FINISHED;

    companion object {
        private val byOrdinal: MutableMap<Int, ExtendedHandshakeType> = HashMap()

        init {
            for (t in entries) {
                byOrdinal[t.ordinal] = t
            }
        }


        fun get(ordinal: Int): ExtendedHandshakeType {
            val type = byOrdinal[ordinal]
            checkNotNull(type) { "ExtendedHandshakeType not found" }
            return type
        }
    }
}

@Suppress("RedundantSuppression", "unused")
enum class HandshakeType(value: Int) {
    CLIENT_HELLO(1),
    SERVER_HELLO(2),
    NEW_SESSION_TICKET(4),
    END_OF_EARLY_DATA(5),
    ENCRYPTED_EXTENSIONS(8),
    CERTIFICATE(11),
    CERTIFICATE_REQUEST(13),
    CERTIFICATE_VERIFY(15),
    FINISHED(20),
    KEY_UPDATE(24),
    MESSAGE_HASH(254),
    ;

    val value: Byte = value.toByte()


    companion object {
        private val byValue: MutableMap<Int, HandshakeType> = HashMap()

        init {
            for (t in entries) {
                byValue[t.value.toInt()] = t
            }
        }

        fun get(value: Int): HandshakeType? {
            return byValue[value]
        }
    }
}

@Suppress("RedundantSuppression", "unused")
enum class NamedGroup(value: Int) {
    /* Elliptic Curve Groups (ECDHE) */
    SECP256r1(0x0017), SECP384r1(0x0018), SECP521r1(0x0019),


    /* Finite Field Groups (DHE) */
    FFDHE2048(0x0100), FFDHE3072(0x0101), FFDHE4096(0x0102),
    FFDHE6144(0x0103), FFDHE8192(0x0104),
    ;

    val value: Short = value.toShort()

    companion object {
        private val byValue: MutableMap<Short, NamedGroup> = HashMap()

        init {
            for (t in entries) {
                byValue[t.value] = t
            }
        }


        fun get(value: Short): NamedGroup {
            val namedGroup = byValue[value]
                ?: throw DecodeErrorException("invalid group value")
            return namedGroup
        }
    }
}

@Suppress("RedundantSuppression", "unused")
enum class CipherSuite(value: Int) {
    TLS_AES_128_GCM_SHA256(0x1301),
    TLS_AES_256_GCM_SHA384(0x1302),
    TLS_AES_128_CCM_SHA256(0x1304),
    TLS_AES_128_CCM_8_SHA256(0x1305);

    val value: Short = value.toShort()

    companion object {
        private val byValue: MutableMap<Short, CipherSuite> = HashMap()

        init {
            for (t in entries) {
                byValue[t.value] = t
            }
        }

        fun get(value: Short): CipherSuite? {
            return byValue[value]
        }
    }
}


@Suppress("unused")
enum class ExtensionType(value: Int) {
    SERVER_NAME(0),  /* RFC 6066 */

    MAX_FRAGMENT_LENGTH(1),  /* RFC 6066 */

    STATUS_REQUEST(5),  /* RFC 6066 */
    SUPPORTED_GROUPS(10),  /* RFC 8422, 7919 */
    SIGNATURE_ALGORITHMS(13),  /* RFC 8446 */

    USE_SRTP(14),  /* RFC 5764 */

    HEARTBEAT(15),  /* RFC 6520 */
    APPLICATION_LAYER_PROTOCOL(16),  /* RFC 7301 */

    SIGNED_CERTIFICATE_TIMESTAMP(18),  /* RFC 6962 */

    CLIENT_CERTIFICATE_TYPE(19),  /* RFC 7250 */

    SERVER_CERTIFICATE_TYPE(20),  /* RFC 7250 */

    PADDING(21),  /* RFC 7685 */
    PRE_SHARED_KEY(41),  /* RFC 8446 */
    EARLY_DATA(42),  /* RFC 8446 */
    SUPPORTED_VERSIONS(43),  /* RFC 8446 */

    COOKIE(44),  /* RFC 8446 */
    ASK_KEY_EXCHANGE_MODES(45),  /* RFC 8446 */
    CERTIFICATE_AUTHORITIES(47),  /* RFC 8446 */

    OID_FILTERS(48),  /* RFC 8446 */

    POST_HANDSHAKE_AUTH(49),  /* RFC 8446 */

    SIGNATURE_ALGORITHMS_CERT(50),  /* RFC 8446 */
    KEY_SHARE(51),
    ;


    val value: Short = value.toShort()
}

@Suppress("RedundantSuppression", "unused")
enum class PskKeyExchangeMode(value: Int) {
    PSK_KE(0),
    PSK_DHE_KE(1);

    val value: Byte = value.toByte()
}


@Suppress("RedundantSuppression", "unused")
internal enum class AlertDescription(value: Int) {
    CLOSE_NOTIFY(0),
    UNEXPECTED_MESSAGE(10),

    BAD_RECORD_MAC(20),

    RECORD_OVERFLOW(22),
    HANDSHAKE_FAILURE(40),
    BAD_CERTIFICATE(42),

    UNSUPPORTED_CERTIFICATE(43),

    CERTIFICATE_REVOKE(44),

    CERTIFICATE_EXPIRED(45),
    CERTIFICATE_UNKNOWN(46),
    ILLEGAL_PARAMETER(47),

    UNKNOWN_CA(48),

    ACCESS_DENIED(49),
    DECODE_ERROR(50),
    DECRYPT_ERROR(51),

    PROTOCOL_VERSION(70),

    INSUFFICIENT_SECURITY(71),
    INTERNAL_ERROR(80),

    INAPPROPRIATE_FALLBACK(86),

    USER_CANCELED(90),
    MISSING_EXTENSION(109),
    UNSUPPORTED_EXTENSION(110),

    UNRECOGNIZED_NAME(112),

    BAD_CERTIFICATE_STATUS_RESPONSE(113),

    UNKNOWN_PSK_IDENTITY(115),

    CERTIFICATE_REQUIRED(116),
    NO_APPLICATION_PROTOCOL(120);

    private val value = value.toByte()

    fun value(): Byte {
        return value
    }
}

