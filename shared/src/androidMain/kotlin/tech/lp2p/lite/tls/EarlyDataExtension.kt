/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

/**
 * TLS Early Data Indication extension.
 * See [...](https://tools.ietf.org/html/rfc8446#section-4.2.10)
 */
data class EarlyDataExtension(val maxEarlyDataSize: Long) : Extension {
    override fun getBytes(): ByteArray {
        val extensionDataLength = if (maxEarlyDataSize == -1L) 0 else 4
        val buffer = Buffer()
        buffer.writeShort(ExtensionType.EARLY_DATA.value)
        buffer.writeShort(extensionDataLength.toShort())
        if (maxEarlyDataSize > -1) {
            buffer.writeInt(maxEarlyDataSize.toInt())
        }
        require(buffer.size.toInt() == 4 + extensionDataLength)
        return buffer.readByteArray()
    }

    companion object {

        fun parse(
            buffer: Buffer,
            extensionLength: Int,
            context: HandshakeType
        ): EarlyDataExtension {
            val extensionDataLength =
                validateExtensionHeader(
                    buffer,
                    extensionLength,
                    0
                )
            var maxEarlyDataSize: Long = -1
            // Only when used in New Session Ticket (message), the EarlyDataIndication value is non-empty.
            if (context == HandshakeType.NEW_SESSION_TICKET) {
                if (extensionDataLength == 4) {
                    maxEarlyDataSize = buffer.readInt().toLong() and 0xffffffffL
                } else {
                    throw DecodeErrorException("invalid extension data length")
                }
            } else if (extensionDataLength != 0) {
                throw DecodeErrorException("invalid extension data length")
            }
            return EarlyDataExtension(maxEarlyDataSize)
        }
    }
}
