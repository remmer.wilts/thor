/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

/**
 * TLS Pre-Shared Key Extension, ServerHello variant.
 * see [...](https://datatracker.ietf.org/doc/html/rfc8446#section-4.2.11)
 */
data class ServerPreSharedKeyExtension(val selectedIdentity: Int) : PreSharedKeyExtension {
    override fun getBytes(): ByteArray {
        val buffer = Buffer()
        buffer.writeShort(ExtensionType.PRE_SHARED_KEY.value)
        buffer.writeShort(0x02.toShort())
        buffer.writeShort(selectedIdentity.toShort())
        return buffer.readByteArray()
    }

    companion object {

        fun parse(buffer: Buffer, extensionLength: Int): ServerPreSharedKeyExtension {
            validateExtensionHeader(buffer, extensionLength, 2)
            return ServerPreSharedKeyExtension(buffer.readShort().toInt())
        }
    }
}
