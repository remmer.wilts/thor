/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer

// https://tools.ietf.org/html/rfc8446#section-4.3.1
data class EncryptedExtensions(val extensions: List<Extension>, override val bytes: ByteArray) :
    HandshakeMessage {
    override val type: HandshakeType
        get() = HandshakeType.ENCRYPTED_EXTENSIONS


    companion object {
        private const val MINIMAL_MESSAGE_LENGTH = 1 + 3 + 2


        fun parse(
            buffer: Buffer,
            customExtensionParser: ExtensionParser?,
            data: ByteArray
        ): EncryptedExtensions {
            if (buffer.size < MINIMAL_MESSAGE_LENGTH) {
                throw DecodeErrorException("Message too short")
            }

            val msgLength = ((buffer.readByte().toInt() and 0xff) shl 16) or ((buffer.readByte()
                .toInt() and 0xff) shl 8) or (buffer.readByte().toInt() and 0xff)
            if (buffer.size < msgLength || msgLength < 2) {
                throw DecodeErrorException("Incorrect message length")
            }

            val extensions = parseExtensions(
                buffer,
                HandshakeType.SERVER_HELLO, customExtensionParser
            )

            return EncryptedExtensions(extensions, data)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EncryptedExtensions

        if (extensions != other.extensions) return false
        if (!bytes.contentEquals(other.bytes)) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = extensions.hashCode()
        result = 31 * result + bytes.contentHashCode()
        result = 31 * result + type.hashCode()
        return result
    }

}
