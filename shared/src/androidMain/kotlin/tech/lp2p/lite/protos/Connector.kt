package tech.lp2p.lite.protos

import tech.lp2p.asen.Peeraddr
import tech.lp2p.lite.Lite
import tech.lp2p.lite.TIMEOUT
import tech.lp2p.lite.quic.ClientConnection
import tech.lp2p.lite.quic.Connection
import tech.lp2p.lite.quic.Protocols
import tech.lp2p.lite.quic.Responder
import tech.lp2p.lite.quic.Version
import tech.lp2p.lite.tls.CipherSuite
import java.net.InetAddress
import java.net.InetSocketAddress


suspend fun connect(lite: Lite, address: Peeraddr): Connection {

    val inetAddress = InetAddress.getByAddress(address.address)
    val remoteAddress = InetSocketAddress(inetAddress, address.port.toInt())

    val protocols = Protocols()
    protocols[MULTISTREAM_PROTOCOL] = StreamHandler()
    protocols[IDENTITY_PROTOCOL] = IdentifyHandler(lite.peerId())
    val responder = Responder(protocols)

    val clientConnection = ClientConnection(
        Version.V1, address, remoteAddress,
        listOf(CipherSuite.TLS_AES_128_GCM_SHA256), lite.certificate(),
        responder, lite.connector()
    )

    clientConnection.connect(TIMEOUT)

    return clientConnection
}
