package tech.lp2p.lite.protos


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Semaphore
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import tech.lp2p.asen.Key
import tech.lp2p.asen.Peeraddr
import tech.lp2p.lite.DHT_ALPHA
import tech.lp2p.lite.DHT_CONCURRENCY
import tech.lp2p.lite.Lite
import tech.lp2p.lite.TIMEOUT
import tech.lp2p.lite.quic.Connection
import tech.lp2p.lite.quic.Requester


private suspend fun evalClosestPeers(pms: Message, peers: DhtPeers, key: Key): List<DhtPeer> {
    val dhtPeers: MutableList<DhtPeer> = ArrayList()
    for (entry in pms.closerPeers) {
        if (entry.addrs.isNotEmpty()) {
            val reachable = reachablePeeraddr(
                entry.id, entry.addrs
            )

            if (reachable != null) {
                val dhtPeer: DhtPeer = createDhtPeer(reachable, true, key)
                val result = peers.enhance(dhtPeer)
                if (result) {
                    dhtPeers.add(dhtPeer)
                }
            }
        }
    }
    return dhtPeers
}


private suspend fun bootstrap(lite: Lite, key: Key): List<DhtPeer> {
    val peers: MutableList<DhtPeer> = ArrayList()

    val peeraddrs = lite.bootstrap()
    for (peeraddr in peeraddrs) {
        peers.add(createDhtPeer(peeraddr, false, key))
    }

    val stored = lite.peerStore().peeraddrs(DHT_ALPHA)

    for (peeraddr in stored) {
        peers.add(createDhtPeer(peeraddr, true, key))
    }

    return peers
}


internal suspend fun findClosestPeers(
    scope: CoroutineScope,
    channel: Channel<Connection>,
    lite: Lite,
    key: Key
) {
    val message = createFindNodeMessage(key)
    val peers = initialPeers(lite, key)
    runRequest(scope, channel, lite, key, peers, message)
}


private suspend fun request(
    lite: Lite, dhtPeer: DhtPeer,
    message: Message, channel: Channel<Connection>
): Message {
    val connection = connect(lite, dhtPeer.peeraddr)
    val msg = request(connection, message)

    if (dhtPeer.replaceable) {
        channel.send(connection)
    }
    return msg
}


private suspend fun initialPeers(lite: Lite, key: Key): DhtPeers {
    // now get the best result to limit 'Lite.DHT_ALPHA'

    val dhtPeers = DhtPeers(DHT_ALPHA)
    val pds = bootstrap(lite, key)

    for (dhtPeer in pds) {
        dhtPeers.fill(dhtPeer)
    }
    return dhtPeers
}


private fun runRequest(
    scope: CoroutineScope,
    channel: Channel<Connection>,
    lite: Lite, key: Key,
    dhtPeers: DhtPeers, message: Message
) {

    val semaphore = Semaphore(DHT_CONCURRENCY)
    do {
        val nextPeer = dhtPeers.nextPeer()

        val hasPeer = nextPeer != null
        if (hasPeer) {
            if (semaphore.tryAcquire()) {
                nextPeer.start()
                scope.launch {
                    try {
                        val pms = request(lite, nextPeer, message, channel)

                        val res = evalClosestPeers(pms, dhtPeers, key)
                        if (res.isNotEmpty()) {
                            if (nextPeer.replaceable) {
                                lite.peerStore().storePeeraddr(nextPeer.peeraddr)
                            }
                        }
                    } catch (_: Throwable) {
                        // ignore exceptions
                    } finally {
                        nextPeer.done()
                        semaphore.release()
                    }
                }
            }
        }
    } while (scope.isActive)

    channel.cancel() // finish the channel is important here
}

internal fun createDhtPeer(peeraddr: Peeraddr, replaceable: Boolean, key: Key): DhtPeer {
    val peerKey = createPeerIdKey(peeraddr.peerId)
    val distance = keyDistance(key, peerKey)
    return DhtPeer(peeraddr, replaceable, distance, DhtPeer.State())
}


private fun createFindNodeMessage(key: Key): Message {
    return Message(
        type = Message.MessageType.FIND_NODE,
        key = key.target
    )
}


@OptIn(ExperimentalSerializationApi::class)
private suspend fun request(connection: Connection, message: Message): Message {
    val msg = ProtoBuf.encodeToByteArray<Message>(message)
    val data = Requester.createStream(connection)
        .request(
            TIMEOUT.toLong(), encode(
                msg, MULTISTREAM_PROTOCOL, DHT_PROTOCOL
            )
        )
    val response = receiveResponse(data)
    return ProtoBuf.decodeFromByteArray<Message>(response)
}

