package tech.lp2p.lite.protos

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.concurrent.ConcurrentSkipListSet

class DhtPeers(private val saturation: Int) {
    private val peerSet = ConcurrentSkipListSet<DhtPeer>()
    private val mutex = Mutex()


    fun nextPeer(): DhtPeer? {
        // The peers we query next should be ones that we have only Heard about.

        for (dhtPeer in peerSet) {
            if (!dhtPeer.isStarted) {
                return dhtPeer
            }
        }
        return null
    }


    suspend fun enhance(dhtPeer: DhtPeer): Boolean {
        if (peerSet.size < saturation) {
            return peerSet.add(dhtPeer)
        } else {
            mutex.withLock {
                val last = last()
                // check if dhtQueryPeer is closer
                val value = dhtPeer.compareTo(last)
                // if value is negative dhtPeer is closer to key
                if (value < 0) {
                    // it is save now to remove last and insert
                    val result = peerSet.add(dhtPeer)
                    if (result) {
                        // the new peer is added, now remove the last entry
                        // when it is possible
                        if (last.isDone) {
                            peerSet.remove(last)
                        }
                    }
                    return result
                }
                // else case not added
                return false
            }
        }
    }

    fun size(): Int {
        return peerSet.size
    }

    fun fill(dhtPeer: DhtPeer) {
        peerSet.add(dhtPeer)

        if (peerSet.size > saturation) {
            val last = peerSet.last()
            peerSet.remove(last)
        }
    }

    fun first(): DhtPeer {
        return peerSet.first()
    }

    fun last(): DhtPeer {
        return peerSet.last()
    }
}
