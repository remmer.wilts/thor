package tech.lp2p.lite.protos

import kotlinx.serialization.Serializable

/* proto definition
syntax = "proto2";
message Identify {
    optional string protocolVersion = 5;
    optional string agentVersion = 6;
    optional bytes publicKey = 1;
    repeated bytes listenAddrs = 2;
    optional bytes observedAddr = 4;
    repeated string protocols = 3;
}
*/

@Serializable
data class Identify(
    val publicKey: ByteArray? = null,
    val listenAddrs: List<ByteArray> = emptyList<ByteArray>(),
    val protocols: List<String> = emptyList<String>(),
    val observedAddress: ByteArray? = null,
    val protocolVersion: String? = null,
    val agentVersion: String? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Identify

        if (!publicKey.contentEquals(other.publicKey)) return false
        if (listenAddrs != other.listenAddrs) return false
        if (protocols != other.protocols) return false
        if (!observedAddress.contentEquals(other.observedAddress)) return false
        if (protocolVersion != other.protocolVersion) return false
        if (agentVersion != other.agentVersion) return false

        return true
    }

    override fun hashCode(): Int {
        var result = publicKey?.contentHashCode() ?: 0
        result = 31 * result + listenAddrs.hashCode()
        result = 31 * result + protocols.hashCode()
        result = 31 * result + (observedAddress?.contentHashCode() ?: 0)
        result = 31 * result + (protocolVersion?.hashCode() ?: 0)
        result = 31 * result + (agentVersion?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Identify(publicKey=${publicKey.contentToString()}, listenAddrs=$listenAddrs, protocols=$protocols, observedAddress=${observedAddress.contentToString()}, protocolVersion=$protocolVersion, agentVersion=$agentVersion)"
    }
}