package tech.lp2p.idun

import kotlinx.coroutines.channels.Channel
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.Node
import tech.lp2p.idun.core.Peer
import tech.lp2p.idun.core.SHA1_HASH_LENGTH
import tech.lp2p.idun.core.createPeerFromAddress
import tech.lp2p.idun.core.createRandomKey
import tech.lp2p.idun.core.inetAddressFromNetworkInterfaces
import tech.lp2p.idun.tasks.PeerLookupTask
import tech.lp2p.idun.tasks.TaskListener
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.util.concurrent.ThreadLocalRandom
import kotlin.time.measureTime


class Idun internal constructor(
    val nodeId: ByteArray = createRandomKey(SHA1_HASH_LENGTH),
    val port: Int = nextFreePort()
) {
    private val node: Node = Node(nodeId, port)

    fun startup() {
        node.start()
        bootstrap().forEach { address: InetSocketAddress ->
            if (!address.isUnresolved) {
                node.ping(Address(address.address.address, address.port.toUShort()), null)
            }
        }
    }

    fun ping(address: InetSocketAddress, id: ByteArray) {
        require(!address.isUnresolved) { "Address must be resolved" }
        node.ping(Address(address.address.address, address.port.toUShort()), id)
    }


    fun storeKey(key: ByteArray) {
        val localAddress = inetAddressFromNetworkInterfaces
        node.database.store(
            key, createPeerFromAddress(localAddress.address, port)
        )
    }

    fun entries(key: ByteArray, maxEntries: Int): List<Peer> {
        return node.database.sample(key, maxEntries)
    }

    suspend fun shutdown() {
        node.shutdown()
    }

    fun address(): InetSocketAddress {
        return InetSocketAddress(inetAddressFromNetworkInterfaces, port)
    }

    suspend fun peers(infoHash: ByteArray, channel: Channel<Peer>) {
        System.err.println("Start peers request ...")
        val duration = measureTime {
            try {
                val lookup = PeerLookupTask(node, infoHash, channel)

                lookup.addListener(object : TaskListener {
                    override fun finished() {
                        System.err.println("TaskListener finished")
                        channel.cancel()
                    }
                })
                lookup.start()
            } catch (throwable: Throwable) {
                debug("Idun", throwable)
            }
        }
        System.err.println("Time peers request " + duration.inWholeSeconds + "[s]")

    }

}

fun newIdun(
    nodeId: ByteArray = createRandomKey(SHA1_HASH_LENGTH),
    port: Int = nextFreePort()
): Idun {
    return Idun(nodeId, port)
}


@Suppress("SameReturnValue")
val isDebug: Boolean
    get() = true

@Suppress("SameReturnValue")
private val isError: Boolean
    get() = true


internal fun debug(tag: String, message: String) {
    if (isDebug) {
        println("$tag $message")
    }
}

internal fun debug(tag: String, throwable: Throwable) {
    if (isError) {
        System.err.println(tag + " " + throwable.localizedMessage)
        throwable.printStackTrace(System.err)
    }
}

fun nextFreePort(): Int {
    var port = ThreadLocalRandom.current().nextInt(1001, 65535)
    while (true) {
        if (isLocalPortFree(port)) {
            return port
        } else {
            port = ThreadLocalRandom.current().nextInt(1001, 65535)
        }
    }
}

// returns only IPv6 addresses
fun bootstrap(): List<InetSocketAddress> {
    val result: MutableSet<InetSocketAddress> = mutableSetOf()
    try {
        val entries = listOf(
            InetSocketAddress.createUnresolved("dht.transmissionbt.com", 6881),
            InetSocketAddress.createUnresolved("router.bittorrent.com", 6881),
            InetSocketAddress.createUnresolved("router.utorrent.com", 6881),
            InetSocketAddress.createUnresolved("dht.aelitis.com", 6881)
        )

        entries.forEach { inetSocketAddress ->
            var addresses = InetAddress.getAllByName(inetSocketAddress.hostName)
            addresses.forEach { inet ->
                result.add(InetSocketAddress(inet, inetSocketAddress.port))
            }
        }
    } catch (throwable: Throwable) {
        debug("Idun", throwable)
    }
    return result.toList()
}


private fun isLocalPortFree(port: Int): Boolean { // todo remove
    try {
        ServerSocket(port).close()
        return true
    } catch (_: Exception) {
        return false
    }
}
