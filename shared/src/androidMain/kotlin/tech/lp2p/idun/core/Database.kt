package tech.lp2p.idun.core

import dev.whyoleg.cryptography.random.CryptographyRandom
import kotlinx.atomicfu.atomic
import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import kotlinx.io.writeUShort
import org.kotlincrypto.hash.sha1.SHA1
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit

class Database internal constructor() {
    private val timestampCurrent = atomic(0L)
    private val items: ConcurrentMap<Int, Peers> = ConcurrentHashMap(500)

    @Volatile
    private var timestampPrevious: Long = 0

    private val sessionSecret = CryptographyRandom.nextBytes(SHA1_HASH_LENGTH)

    fun clear() {
        items.clear()
    }

    /**
     * Store an entry in the database
     *
     * @param key The key
     * @param dbi The DBItem to store
     */
    fun store(key: ByteArray, dbi: Peer) {
        items.compute(key.contentHashCode()) { _: Int?, v: Peers? ->
            if (v != null) {
                v.add(dbi)
                return@compute v
            } else {
                val peers = Peers(key)
                peers.add(dbi)
                return@compute peers
            }

        }
    }


    fun sample(key: ByteArray, maxEntries: Int): List<Peer> {
        val keyEntry = items[key.contentHashCode()] ?: return emptyList()
        return keyEntry.snapshot().take(maxEntries)
    }


    fun insertForKeyAllowed(key: ByteArray): Boolean {
        val entries = items[key.contentHashCode()] ?: return true

        val size = entries.size()

        if (size < MAX_DB_ENTRIES_PER_KEY / 5) return true

        if (size >= MAX_DB_ENTRIES_PER_KEY) return false

        return size < ThreadLocalRandom.current().nextInt(MAX_DB_ENTRIES_PER_KEY)
    }

    /**
     * Generate a write token, which will give peers write access to the DB.
     *
     * @param ip   The IP of the peer
     * @param port The port of the peer
     * @return A Key
     */
    fun genToken(nodeId: ByteArray, ip: ByteArray, port: UShort, lookupKey: ByteArray): ByteArray {
        updateTokenTimestamps()

        val size =
            SHA1_HASH_LENGTH + ip.size + 2 + 8 +
                    SHA1_HASH_LENGTH + sessionSecret.size

        // generate a hash of the ip port and the current time
        // should prevent anybody from crapping things up
        val bb = Buffer()
        bb.write(nodeId)
        bb.write(ip)
        bb.writeUShort(port)
        bb.writeLong(timestampCurrent.value)
        bb.write(lookupKey)
        bb.write(sessionSecret)
        require(bb.size.toInt() == size)

        // shorten 4bytes to not waste packet size
        // the chance of guessing correctly would be 1 : 4 million
        // and only be valid for a single infohash
        return SHA1().digest(bb.readByteArray()).copyOf(4)
    }

    private fun updateTokenTimestamps() {
        var current = timestampCurrent.value
        val now = System.nanoTime()
        while (TimeUnit.NANOSECONDS.toMillis(now - current) > TOKEN_TIMEOUT) {
            if (timestampCurrent.compareAndSet(current, now)) {
                timestampPrevious = current
                break
            }
            current = timestampCurrent.value
        }
    }


    fun checkToken(
        token: ByteArray,
        nodeId: ByteArray,
        ip: ByteArray,
        port: UShort,
        lookup: ByteArray
    ): Boolean {
        updateTokenTimestamps()
        return checkToken(token, nodeId, ip, port, lookup, timestampCurrent.value)
                || checkToken(token, nodeId, ip, port, lookup, timestampPrevious)
    }

    private fun checkToken(
        token: ByteArray,
        nodeId: ByteArray,
        ip: ByteArray,
        port: UShort,
        lookup: ByteArray,
        timeStamp: Long
    ): Boolean {
        val tdata = SHA1_HASH_LENGTH + ip.size + 2 + 8 +
                SHA1_HASH_LENGTH + SHA1_HASH_LENGTH

        val bb = Buffer()
        bb.write(nodeId)
        bb.write(ip)
        bb.writeShort(port.toShort())
        bb.writeLong(timeStamp)
        bb.write(lookup)
        bb.write(sessionSecret)
        require(tdata.toInt() == bb.size.toInt())
        val rawToken = SHA1().digest(bb.readByteArray()).copyOf(4)


        return token.contentEquals(rawToken)
    }

}
