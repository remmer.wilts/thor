package tech.lp2p.idun.core

import dev.whyoleg.cryptography.random.CryptographyRandom

object Key {
    val MIN_KEY: ByteArray = ByteArray(SHA1_HASH_LENGTH)
    val MAX_KEY: ByteArray = ByteArray(SHA1_HASH_LENGTH)

    init {
        MAX_KEY.fill(0xFF.toByte())
    }
}


fun prettyKeyPrint(hash: ByteArray, nicePrint: Boolean): String {
    val b = StringBuilder(if (nicePrint) 44 else 40)
    for (i in hash.indices) {
        if (nicePrint && i % 4 == 0 && i > 0) {
            b.append(' ')
        }
        var nibble = (hash[i].toInt() and 0xF0) shr 4
        b.append((if (nibble < 0x0A) '0'.code + nibble else 'A'.code + nibble - 10).toChar())
        nibble = hash[i].toInt() and 0x0F
        b.append((if (nibble < 0x0A) '0'.code + nibble else 'A'.code + nibble - 10).toChar())
    }
    return b.toString()
}

fun getInt(hash: ByteArray, offset: Int): Int {
    return (hash[offset].toUInt() shl 24 or (
            hash[offset + 1].toUInt() shl 16) or (
            hash[offset + 2].toUInt() shl 8) or
            hash[offset + 3].toUInt()).toInt()
}

fun distance(a: ByteArray, b: ByteArray): ByteArray {
    val hash = ByteArray(SHA1_HASH_LENGTH)
    for (i in a.indices) {
        hash[i] = (a[i].toInt() xor b[i].toInt()).toByte()
    }
    return hash
}


fun add(a: ByteArray, b: ByteArray): ByteArray {
    var carry = 0
    val hash = a.clone()
    for (i in (SHA1_HASH_LENGTH - 1) downTo 0) {
        carry += (hash[i].toUInt() + b[i].toUInt()).toInt()
        hash[i] = (carry and 0xff).toByte()
        carry = carry ushr 8
    }

    return hash
}

fun setBitKey(idx: Int): ByteArray {
    val hash = ByteArray(SHA1_HASH_LENGTH)
    hash[idx / 8] = (0x80 ushr (idx % 8)).toByte()
    return hash
}

fun createRandomKey(length: Int): ByteArray {
    return CryptographyRandom.nextBytes(length)
}

/**
 * Compares the distance of two keys relative to this one using the XOR metric
 *
 * @return -1 if h1 is closer to this key, 0 if h1 and h2 are equidistant, 1 if h2 is closer
 */
fun threeWayDistance(h0: ByteArray, h1: ByteArray, h2: ByteArray): Int {

    val mmi = Arrays.mismatch(h1, h2)

    if (mmi == -1) return 0

    val h = h0[mmi].toUInt()
    val a = h1[mmi].toUInt()
    val b = h2[mmi].toUInt()

    return (a xor h).compareTo(b xor h)
}

