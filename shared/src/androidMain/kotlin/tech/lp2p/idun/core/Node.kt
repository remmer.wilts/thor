package tech.lp2p.idun.core

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import tech.lp2p.idun.debug
import tech.lp2p.idun.isDebug
import tech.lp2p.idun.messages.AnnounceRequest
import tech.lp2p.idun.messages.AnnounceResponse
import tech.lp2p.idun.messages.Error
import tech.lp2p.idun.messages.FindNodeRequest
import tech.lp2p.idun.messages.FindNodeResponse
import tech.lp2p.idun.messages.GENERIC_ERROR
import tech.lp2p.idun.messages.GetPeersRequest
import tech.lp2p.idun.messages.GetPeersResponse
import tech.lp2p.idun.messages.Message
import tech.lp2p.idun.messages.MessageException
import tech.lp2p.idun.messages.PROTOCOL_ERROR
import tech.lp2p.idun.messages.PingRequest
import tech.lp2p.idun.messages.PingResponse
import tech.lp2p.idun.messages.Request
import tech.lp2p.idun.messages.Response
import tech.lp2p.idun.messages.SERVER_ERROR
import tech.lp2p.idun.messages.parseMessage
import tech.lp2p.idun.tasks.Task
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.math.min

class Node(val nodeId: ByteArray, port: Int) {
    private var numEntriesInRoutingTable: Int = 0
    internal val scope = CoroutineScope(Dispatchers.IO)
    internal var datagramSocket = DatagramSocket(port)
    private val cowMutex = Mutex()
    private val unsolicitedThrottle = ConcurrentHashMap<Address, Long>()
    private val maintenanceTasks: MutableMap<Bucket, Task> = mutableMapOf()
    private val enqueueEventConsumers: MutableCollection<(Call) -> Unit> =
        CopyOnWriteArrayList()

    private val unverifiedLossrate =
        ExponentialWeightendMovingAverage().setWeight(0.01).setValue(0.5)
    private val verifiedEntryLossrate =
        ExponentialWeightendMovingAverage().setWeight(0.01).setValue(0.5)


    // keeps track of RTT histogram for nodes not in our routing table
    val timeoutFilter: ResponseTimeoutFilter = ResponseTimeoutFilter()
    val calls: MutableMap<Int, Call> = ConcurrentHashMap()

    val mismatchDetector: IDMismatchDetector = IDMismatchDetector(this)
    val unreachableCache: NonReachableCache = NonReachableCache()
    val database: Database = Database()
    val cache: AnnounceNodeCache = AnnounceNodeCache()
    private val mutex = Mutex()

    @Volatile
    var isShutdown: Boolean = false

    @Volatile
    var routingTable = RoutingTable()


    private val callListener: CallListener = object : CallListener {

        override suspend fun onTimeout(call: Call) {
            if (call.knownReachableAtCreationTime()) verifiedEntryLossrate.updateAverage(1.0)
            else unverifiedLossrate.updateAverage(1.0)
            calls.remove(call.request.tid.contentHashCode(), call)
            timeout(call)
        }

        override suspend fun stateTransition(
            call: Call,
            previous: CallState,
            current: CallState
        ) {

        }

        override fun onResponse(call: Call, rsp: Message) {
            if (call.knownReachableAtCreationTime()) verifiedEntryLossrate.updateAverage(0.0)
            else unverifiedLossrate.updateAverage(0.0)
        }
    }


    suspend fun shutdown() {
        if (isShutdown) return

        debug("Node", "Start Node shutdown")
        isShutdown = true

        calls.values.forEach { obj: Call -> obj.cancel() }

        enqueueEventConsumers.clear()
        calls.clear()

        debug(
            "Node",
            "Stopped Server " + " " + prettyKeyPrint(nodeId, false)
        )


        database.clear()
        mismatchDetector.clear()
        unreachableCache.clear()
        cache.clear()


        routingTable = RoutingTable()
        unsolicitedThrottle.clear()
        maintenanceTasks.clear()
        try {
            scope.cancel()
        } catch (throwable: Throwable) {
            debug("Node", throwable)
        }
        try {
            datagramSocket.close()
        } catch (throwable: Throwable) {
            debug("Node", throwable)
        }


        debug("Node", "End Node shutdown")
    }


    init {
        val callListener = object : CallListener {
            override suspend fun onTimeout(call: Call) {
            }

            override fun onResponse(call: Call, rsp: Message) {
            }

            override suspend fun stateTransition(
                call: Call,
                previous: CallState,
                current: CallState
            ) {
                if (current == CallState.RESPONDED) mismatchDetector.add(call)
                if (current == CallState.RESPONDED ||
                    current == CallState.TIMEOUT
                ) {
                    unreachableCache.onCallFinished(call)
                }

            }
        }
        enqueueEventConsumers.add { c: Call -> c.addListener(callListener) }
        enqueueEventConsumers.add { c: Call -> this.onOutgoingRequest(c) }

        debug("Node", "Starting Server " + prettyKeyPrint(nodeId, false))
    }


    fun start() {

        scope.launch {

            val receiveBuffer = ByteArray(1500)
            while (!isShutdown) {

                try {
                    val datagram = DatagramPacket(receiveBuffer, receiveBuffer.size)
                    datagramSocket.receive(datagram)

                    val data = datagram.data.copyOfRange(0, datagram.length)
                    val inet = datagram.address
                    val port = datagram.port
                    // * no conceivable DHT message is smaller than 10 bytes
                    // * all DHT messages start with a 'd' for dictionary
                    // * port 0 is reserved
                    // * address family may mismatch due to autoconversion
                    // from v4-mapped v6 addresses to Inet4Address
                    // -> immediately discard junk on the read loop, don't even allocate a buffer for it
                    if (data.size < 10 || data[0] != 'd'.code.toByte()
                        || port == 0
                    ) continue

                    val address = Address(inet.address, port.toUShort())

                    handlePacket(data, address)

                } catch (throwable: Throwable) {
                    debug("Node", throwable)
                }
            }
        }
    }

    suspend fun ping(request: PingRequest) {

        // ignore requests we get from ourself
        if (isLocalId(request.id)) {
            return
        }

        val rsp = PingResponse(request.address, nodeId, request.tid)

        sendMessage(rsp)

        recieved(request, null)
    }

    suspend fun findNode(request: FindNodeRequest) {
        // ignore requests we get from ourself
        if (isLocalId(request.id)) {
            return
        }

        val kns = ClosestNodesSearch(request.target, MAX_ENTRIES_PER_BUCKET, this)
        kns.fill()
        val response = FindNodeResponse(
            request.address, nodeId, request.tid,
            kns.inet4List(),
            kns.inet6List()
        )

        sendMessage(response)

        recieved(request, null)
    }


    suspend fun getPeers(r: GetPeersRequest) {
        // ignore requests we get from ourself
        if (isLocalId(r.id)) {
            return
        }

        val peers = database.sample(r.infoHash, MAX_PEERS_PER_ANNOUNCE)

        // generate a token
        var token: ByteArray? = null
        if (database.insertForKeyAllowed(r.infoHash)) token =
            database.genToken(r.id, r.address.address, r.address.port, r.infoHash)


        val kns = ClosestNodesSearch(r.infoHash, MAX_ENTRIES_PER_BUCKET, this)
        kns.fill()


        val resp = GetPeersResponse(
            r.address, nodeId, r.tid, token,
            kns.inet4List(),
            kns.inet6List(),
            peers
        )

        sendMessage(resp)

        recieved(r, null)
    }

    suspend fun announce(request: AnnounceRequest) {
        // ignore requests we get from ourself
        if (isLocalId(request.id)) {
            return
        }

        // first check if the token is OK
        if (!database.checkToken(
                request.token,
                request.id,
                request.address.address,
                request.address.port,
                request.infoHash
            )
        ) {
            sendError(
                request, PROTOCOL_ERROR,
                "Invalid Token; tokens expire after " + TOKEN_TIMEOUT + "ms; " +
                        "only valid for the IP/port to which it was issued;" +
                        " only valid for the infohash for which it was issued"
            )
            return
        }

        // everything OK, so store the value
        val item =
            createPeerFromAddress(request.address.address, request.port)

        database.store(request.infoHash, item)

        // send a proper response to indicate everything is OK
        val rsp = AnnounceResponse(request.address, nodeId, request.tid)
        sendMessage(rsp)

        recieved(request, null)
    }


    suspend fun sendError(origMsg: Message, code: Int, msg: String) {
        debug("Node", origMsg.toString())
        sendMessage(Error(origMsg.address, nodeId, origMsg.tid, code, msg))
    }

    suspend fun timeout(r: Call) {
        onTimeout(r)
    }

    suspend fun recieved(msg: Message, associatedCall: Call?) {
        val ip = msg.address
        val id = msg.id


        val expectedId = associatedCall?.expectedID

        // server only verifies IP equality for responses.
        // we only want remote nodes with stable ports in our routing table, so appley a stricter check here
        if (associatedCall != null &&
            associatedCall.request.address != associatedCall.response!!.address
        ) {
            return
        }

        val bucket = routingTable.entryForId(id).bucket
        val entryById = bucket.findByIPorID(null, id)

        // entry is claiming the same ID as entry with different IP in our routing table -> ignore
        if (entryById != null && entryById.address != ip) return

        // ID mismatch from call (not the same as ID mismatch from routing table)
        // it's fishy at least. don't insert even if it proves useful during a lookup
        if (entryById == null && expectedId != null && !expectedId.contentEquals(id)) return

        val newEntry = BucketEntry(msg.address, id)
        // throttle the insert-attempts for unsolicited requests, update-only once they exceed the threshold
        // does not apply to responses
        if (associatedCall == null && updateAndCheckThrottle(newEntry.address)) {
            refreshOnly(newEntry)
            return
        }

        if (associatedCall != null) {
            newEntry.signalResponse(associatedCall.rTT)
            newEntry.mergeRequestTime(associatedCall.sentTime)
        }


        // force trusted entry into the routing table (by splitting if necessary) if
        // it passed all preliminary tests and it's not yet in the table
        // although we can only trust responses, anything else might be
        // spoofed to clobber our routing table


        val opts: MutableSet<InsertOptions> = mutableSetOf()

        if (msg is Response) opts.add(InsertOptions.RELAXED_SPLIT)

        insertEntry(newEntry, opts)

        // we already should have the bucket. might be an old one by now due to splitting
        // but it doesn't matter, we just need to update the entry, which should stay the
        // same object across bucket splits
        if (msg is Response) {
            if (associatedCall != null) {
                bucket.notifyOfResponse(msg, associatedCall)
            }
        }
    }

    /**
     * @return true if it should be throttled
     */
    private fun updateAndCheckThrottle(addr: Address): Boolean {
        val oldVal = unsolicitedThrottle.merge(
            addr, THROTTLE_INCREMENT
        ) { _: Long?, v: Long ->
            min((v + THROTTLE_INCREMENT), THROTTLE_SATURATION)
        }!! - THROTTLE_INCREMENT

        return oldVal > THROTTLE_THRESHOLD
    }

    private fun refreshOnly(toRefresh: BucketEntry) {
        val bucket = routingTable.entryForId(toRefresh.id).bucket

        bucket.refresh(toRefresh)
    }

    private suspend fun insertEntry(toInsert: BucketEntry, opts: Set<InsertOptions>) {
        if (nodeId.contentEquals(toInsert.id)) return

        var currentTable = routingTable
        var tableEntry = currentTable.entryForId(toInsert.id)

        while (!opts.contains(InsertOptions.NEVER_SPLIT) && tableEntry.bucket.isFull && (opts.contains(
                InsertOptions.FORCE_INTO_MAIN_BUCKET
            ) || toInsert.verifiedReachable()) && tableEntry.prefix.depth < KEY_BITS - 1
        ) {
            if (!opts.contains(InsertOptions.ALWAYS_SPLIT_IF_FULL) && !canSplit(
                    tableEntry,
                    toInsert,
                    opts.contains(InsertOptions.RELAXED_SPLIT)
                )
            ) break

            splitEntry(currentTable, tableEntry)
            currentTable = routingTable
            tableEntry = currentTable.entryForId(toInsert.id)
        }

        val oldSize = tableEntry.bucket.numEntries

        var toRemove: BucketEntry? = null

        if (opts.contains(InsertOptions.REMOVE_IF_FULL)) {
            toRemove = tableEntry.bucket.getEntries()
                .maxWithOrNull(AGE_ORDER)
        }

        if (opts.contains(InsertOptions.FORCE_INTO_MAIN_BUCKET)) tableEntry.bucket.modifyMainBucket(
            toRemove,
            toInsert
        )
        else tableEntry.bucket.insertOrRefresh(toInsert)

        // add delta to the global counter. inaccurate, but will be rebuilt by the bucket checks
        numEntriesInRoutingTable += tableEntry.bucket.numEntries - oldSize
    }

    private fun canSplit(
        entry: RoutingTableEntry,
        toInsert: BucketEntry,
        relaxedSplitting: Boolean
    ): Boolean {
        if (isLocalBucket(entry.prefix)) return true

        if (!relaxedSplitting) return false

        val search = ClosestNodesSearch(
            nodeId, MAX_ENTRIES_PER_BUCKET,
            this
        )

        search.filter = { true }

        search.fill()
        val found = search.getEntries()

        if (found.size < MAX_ENTRIES_PER_BUCKET) return true

        val max = found[found.size - 1]

        return threeWayDistance(nodeId, max.id, toInsert.id) > 0
    }

    private suspend fun splitEntry(expect: RoutingTable, entry: RoutingTableEntry) {
        cowMutex.withLock {
            val current = routingTable
            if (current != expect) return

            val a = RoutingTableEntry(
                splitPrefixBranch(entry.prefix, false), Bucket()
            )
            val b = RoutingTableEntry(
                splitPrefixBranch(entry.prefix, true), Bucket()
            )

            routingTable = current.modify(listOf(entry), listOf(a, b))

            // suppress recursive splitting to relinquish the lock faster.
            // this method is generally called in a loop anyway
            for (e in entry.bucket.getEntries()) insertEntry(
                e,
                setOf(InsertOptions.NEVER_SPLIT, InsertOptions.FORCE_INTO_MAIN_BUCKET)
            )
        }

        // replacements are less important, transfer outside lock
        for (e in entry.bucket.replacementEntries) insertEntry(
            e, setOf()
        )
    }


    fun isLocalId(id: ByteArray): Boolean {
        return nodeId.contentEquals(id)
    }

    private fun isLocalBucket(p: Prefix?): Boolean {
        return isPrefixOf(p!!, nodeId)
    }


    /**
     * Increase the failed queries count of the bucket entry we sent the message to
     */
    suspend fun onTimeout(call: Call) {
        // don't timeout anything if we don't have a connection
        if (call.expectedID != null) {
            routingTable.entryForId(call.expectedID).bucket.onTimeout(call.request.address)
        }
    }

    private fun onOutgoingRequest(c: Call) {
        val expectedId = c.expectedID ?: return
        val bucket = routingTable.entryForId(expectedId).bucket
        var obj = bucket.findByIPorID(c.request.address, expectedId)
        obj?.signalScheduledRequest()
    }

    fun doCall(c: Call) {
        enqueueEventConsumers.forEach { callback: (Call) -> Unit -> callback.invoke(c) }

        calls.putIfAbsent(c.request.tid.contentHashCode(), c)
        dispatchCall(c)
    }

    fun ping(addr: Address, id: ByteArray?) {
        val mtid = createRandomKey(TID_LENGTH)
        val pr = PingRequest(addr, nodeId, mtid)
        doCall(Call(pr, id)) // expectedId can not be available (only address is known)
    }


    private suspend fun handlePacket(data: ByteArray, address: Address) {

        val bedata: Map<String, Any>
        val buffer = newReader(data)
        try {
            bedata = decode(buffer)
        } catch (e: Exception) {
            debug("Node", e)
            debug(
                "Node", "failed to decode message  " +
                        " (length:" + buffer.remaining() + ") from: "
                        + address + " reason:" + e.message
            )
            val err: Message = Error(
                address, nodeId, byteArrayOf(0, 0, 0, 0),
                PROTOCOL_ERROR, "invalid bencoding: " + e.message
            )

            sendMessage(err)
            return
        }


        val msg: Message
        try {
            msg = parseMessage(
                address,
                bedata
            ) { mtid: ByteArray -> calls[mtid.contentHashCode()]?.request }
        } catch (e: MessageException) {
            debug("Node", e)
            var tid = typedGet(bedata, "t")
            if (tid == null) {
                tid = ByteArray(TID_LENGTH)
            }
            sendMessage(Error(address, nodeId, tid, e.errorCode, e.message!!))
            return
        } catch (e: Exception) {
            debug("Node", e)
            return
        }

        debug("Node", "incoming <- $msg")

        // just respond to incoming requests, no need to match them to pending requests
        if (msg is Request) {
            when (msg) {
                is AnnounceRequest -> announce(msg)
                is FindNodeRequest -> findNode(msg)
                is GetPeersRequest -> getPeers(msg)
                is PingRequest -> ping(msg)
            }
            return
        }

        // now only response or error

        if (msg is Response && msg.tid.size != TID_LENGTH) {
            val mtid = msg.tid

            val err = Error(
                msg.address, nodeId,
                mtid, SERVER_ERROR,
                "received a response with a transaction id length of " +
                        mtid.size + " bytes, expected [implementation-specific]: " +
                        TID_LENGTH + " bytes"
            )

            sendMessage(err)
            return
        }


        // check if this is a response to an outstanding request
        val c = calls[msg.tid.contentHashCode()]


        // message matches transaction ID and origin == destination
        if (c != null) {
            // we only check the IP address here. the routing table applies more strict
            // checks to also verify a stable port
            if (c.request.address == msg.address) {
                // remove call first in case of exception
                if (calls.remove(msg.tid.contentHashCode(), c)) {
                    c.response(msg)

                    // apply after checking for a proper response
                    if (msg is Response) {
                        recieved(msg, c)
                    }
                }

                return
            }

            // 1. the message is not a request
            // 2. transaction ID matched
            // 3. request destination did not match response source!!
            // 4. we're using random 48 bit MTIDs
            // this happening by chance is exceedingly unlikely

            // indicates either port-mangling NAT, a multhomed host listening on
            // any-local address or some kind of attack
            // -> ignore response
            if (isDebug) {
                debug(
                    "Node",
                    "tid matched, socket address did not, ignoring message, request: "
                            + c.request.address + " -> response: " + msg.address

                )
            }

            if (msg !is Error) {
                // this is more likely due to incorrect binding implementation in ipv6. notify peers about that
                // don't bother with ipv4, there are too many complications
                val err: Message = Error(
                    c.request.address, nodeId,
                    msg.tid, GENERIC_ERROR,
                    "A request was sent to " + c.request.address +
                            " and a response with matching transaction id was received from "
                            + msg.address + " . Multihomed nodes should ensure that sockets are " +
                            "properly bound and responses are sent with the " +
                            "correct source socket address. See BEPs 32 and 45."
                )

                sendMessage(err)
            }

            // but expect an upcoming timeout if it's really just a misbehaving node
            c.setSocketMismatch()
            c.injectStall()

            return
        }

        // a) it's a response b) didn't find a call c) uptime is high enough that
        // it's not a stray from a restart
        // -> did not expect this response
        if (msg is Response) {

            val err = Error(
                msg.address, nodeId,
                msg.tid, SERVER_ERROR,
                "received a response message whose transaction ID did not " +
                        "match a pending request or transaction expired"
            )
            sendMessage(err)
            return
        }



        if (isDebug) {

            if (msg is Error) {
                val b = StringBuilder()
                b.append(" [").append(msg.code).append("] from: ").append(msg.address)
                b.append(" Message: \"").append(msg.message).append("\"")
                debug("Node", "ErrorMessage $b")
                return
            }


            debug("Node", "not sure how to handle message $msg")
        }
    }

    private fun dispatchCall(call: Call) {
        val msg = call.request
        call.addListener(callListener)

        // known nodes - routing table entries - keep track of their own RTTs
        // they are also biased towards lower RTTs compared to the general population
        // encountered during regular lookups
        // don't let them skew the measurement of the general node population
        if (!call.knownReachableAtCreationTime()) timeoutFilter.registerCall(call)

        scope.launch {
            send(EnqueuedSend(msg, call))
        }
    }

    suspend fun sendMessage(msg: Message) {
        requireNotNull(msg.address) { "message destination must not be null" }

        send(EnqueuedSend(msg, null))
    }


    private suspend fun send(es: EnqueuedSend) {
        // simply assume nobody else is writing and attempt to do it
        // if it fails it's the current writer's job to double-check after releasing the write lock
        mutex.withLock {
            debug("Node", "sending -> ${es.message}")
            try {
                val buffer = Buffer()
                es.message.encode(buffer)
                val size = buffer.size.toInt()
                val address = es.message.address

                require(buffer.size <= MAX_PACKET_SIZE) { "Data exceeds packet size" }
                val inet = InetAddress.getByAddress(address.address)

                val datagram = DatagramPacket(
                    buffer.readByteArray(), size, inet, address.port.toInt()
                )

                datagramSocket.send(datagram)

                es.associatedCall?.hasSend(this@Node)

            } catch (throwable: Throwable) {
                if (isShutdown) return

                if (datagramSocket.isClosed == true) return

                debug("Node", "Failed address " + prettyPrint(es.message.address))
                debug("Node", throwable)

                es.associatedCall?.sendFailed()
                return
            }
        }
    }


    private inner class EnqueuedSend(val message: Message, val associatedCall: Call?) {
        init {
            checkNotNull(message.address)
            decorateMessage()
        }

        fun decorateMessage() {
            if (associatedCall != null) {
                var configuredRTT = associatedCall.expectedRTT

                if (configuredRTT == -1L) {
                    configuredRTT = timeoutFilter.stallTimeout
                }

                associatedCall.expectedRTT = configuredRTT
            }
        }


    }

    internal enum class InsertOptions {
        ALWAYS_SPLIT_IF_FULL,
        NEVER_SPLIT,
        RELAXED_SPLIT,
        REMOVE_IF_FULL,
        FORCE_INTO_MAIN_BUCKET
    }
}


const val TID_LENGTH = 6

// time constants
val DHT_VERSION: String
    get() = "TH" + String(
        byteArrayOf(
            (11 shr 8 and 0xFF).toByte(),
            (11 and 0xff).toByte()
        ), Charsets.ISO_8859_1
    )


// DHT
const val MAX_ENTRIES_PER_BUCKET: Int = 8
const val MAX_CONCURRENT_REQUESTS: Int = 10
const val RPC_CALL_TIMEOUT_MAX: Int = 10 * 1000
const val RPC_CALL_TIMEOUT_BASELINE_MIN: Int = 100 // ms
const val TOKEN_TIMEOUT: Int = 5 * 60 * 1000
const val MAX_DB_ENTRIES_PER_KEY: Int = 6000
const val MAX_PEERS_PER_ANNOUNCE: Int = 10


// enter survival mode if we don't see new packets after this time
const val SHA1_HASH_LENGTH: Int = 20
const val KEY_BITS: Int = SHA1_HASH_LENGTH * 8


const val ADDRESS_LENGTH_IPV6 = 16 + 2
const val ADDRESS_LENGTH_IPV4 = 4 + 2
const val NODE_ENTRY_LENGTH_IPV6 = ADDRESS_LENGTH_IPV6 + SHA1_HASH_LENGTH
const val NODE_ENTRY_LENGTH_IPV4 = ADDRESS_LENGTH_IPV4 + SHA1_HASH_LENGTH
const val MAX_PACKET_SIZE = 1200

// -1 token per minute, 60 saturation, 30 threshold
// if we see more than 1 per minute then it'll take 30 minutes until an unsolicited request can go into a replacement bucket again
const val THROTTLE_INCREMENT: Long = 10

/*
* Verification Strategy:
*
* - trust incoming requests less than responses to outgoing requests
* - most outgoing requests will have an expected ID - expected ID may come from external nodes, so don't take it at face value
*  - if response does not match expected ID drop the packet for routing table accounting purposes without penalizing any existing routing table entry
* - map routing table entries to IP addresses
*  - verified responses trump unverified entries
*  - lookup all routing table entry for incoming messages based on IP address (not node ID!) and ignore them if ID does not match
*  - also ignore if port changed
*  - drop, not just ignore, if we are sure that the incoming message is not fake (tid-verified response)
* - allow duplicate addresses for unverified entries
*  - scrub later when one becomes verified
* - never hand out unverified entries to other nodes
*
* other stuff to keep in mind:
*
* - non-reachable nodes may spam -> floods replacements -> makes it hard to get proper replacements without active lookups
*
*/
const val THROTTLE_SATURATION: Long = 60
const val THROTTLE_THRESHOLD: Long = 30

/*
* all byte[]/buffer/String conversions use ISO_8859_1 by default because it's round-trip
* compatible to unicode codepoints 0-255. i.e. it's suitable for binary data of
* unspecified encodings.
*
* this will garble actual UTF-8 strings, decode those manually if it's meant to be
*  human-readable
*/
fun str2buf(input: String, out: Buffer) {
    for (element in input) {
        require(element.code <= 0xff) {
            "only strings with codepoints 0x00 - 0xff are supported. " +
                    "for proper unicode handling convert strings manually. attempted to encode: $input"
        }
        out.writeByte(element.code.toByte())
    }
}


fun toHex(toHex: ByteArray, builder: StringBuilder, maxBytes: Int) {
    var bytes = maxBytes
    if (toHex.size < bytes) bytes = toHex.size
    builder.ensureCapacity(bytes * 2)
    for (i in 0 until bytes) {
        var nibble = (toHex[i].toInt() and 0xF0) shr 4
        builder.append((if (nibble < 0x0A) '0'.code + nibble else 'A'.code + nibble - 10).toChar())
        nibble = toHex[i].toInt() and 0x0F
        builder.append((if (nibble < 0x0A) '0'.code + nibble else 'A'.code + nibble - 10).toChar())
    }
}

fun prettyPrint(o: Any): String {
    return prettyPrintInternal(o)
}


private fun containsControls(st: String): Boolean {
    return st.codePoints().anyMatch { i: Int -> i < 32 && i != '\r'.code && i != '\n'.code }
}


fun prettyPrintInternal(any: Any): String {
    var o = any
    val builder = StringBuilder()
    if (o is Map<*, *>) {
        val m = o

        builder.append("{")


        val it = m.entries.iterator()
        while (it.hasNext()) {
            val e: Map.Entry<*, *> = it.next()
            builder.append(prettyPrintInternal(e.key!!))
            builder.append(":")
            builder.append(prettyPrintInternal(e.value!!))
            if (it.hasNext()) {
                builder.append(", ")
            }
        }

        builder.append("}")
        return builder.toString()
    }

    if (o is List<*>) {
        builder.append("[")

        val it = o.iterator()

        var prev: Any? = null

        while (it.hasNext()) {
            val e = it.next()!!
            if (prev != null) {
                builder.append(", ")
            }
            builder.append(prettyPrintInternal(e))
            prev = e
        }

        builder.append("]")
        return builder.toString()
    }

    if (o is String) {
        if (containsControls(o)) {
            builder.append(prettyPrintInternal(o.toByteArray(Charsets.ISO_8859_1)))
            return builder.toString()
        }

        builder.append('"')
        builder.append(o)
        builder.append('"')
        return builder.toString()
    }

    if (o is Long || o is Int) {
        builder.append(o)
        return builder.toString()
    }



    if (o is ByteArray) {
        if (o.isEmpty()) {
            builder.append("\"\"")
            return builder.toString()
        }

        builder.append("0x")

        toHex(o, builder, Int.MAX_VALUE)


        if (o.size < 10) {
            builder.append('/')
            builder.append(o.toString())
        }

        return builder.toString()
    }

    return builder.append("unhandled type(").append(o).append(')').toString()
}

