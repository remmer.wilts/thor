package tech.lp2p.idun.core

import kotlin.math.max
import kotlin.math.min

class BucketEntry(val address: Address, val id: ByteArray) {

    private val avgRTT = ExponentialWeightendMovingAverage().setWeight(RTT_EMA_WEIGHT)

    var creationTime: Long
    var lastSeen: Long
    private var verified = false

    /**
     * -1 = never queried / learned about it from incoming requests
     * 0 = last query was a success
     * > 0 = query failed
     */
    private var failedQueries = 0
    private var lastSendTime: Long = -1


    init {
        lastSeen = System.currentTimeMillis()
        creationTime = lastSeen
    }


    override fun equals(other: Any?): Boolean {
        if (other is BucketEntry) return this.equals(other)
        return false
    }

    fun equals(other: BucketEntry?): Boolean {
        if (other == null) return false
        return id.contentEquals(other.id) && address == other.address
    }

    fun matchIPorID(other: BucketEntry?): Boolean {
        if (other == null) return false
        return id.contentEquals(other.id) || address == other.address
    }

    override fun hashCode(): Int {
        return id.hashCode() // note bucket entry
    }

    override fun toString(): String {
        val now = System.currentTimeMillis()
        val b = StringBuilder(80)
        b.append(prettyKeyPrint(id, true)).append("/").append(address)
        if (lastSendTime > 0) b.append(";sent:").append((now - lastSendTime))
        b.append(";seen:").append((now - lastSeen))
        b.append(";age:").append(now - creationTime)
        if (failedQueries != 0) b.append(";fail:").append(failedQueries)
        if (verified) b.append(";verified")
        val rtt = avgRTT.average
        if (!rtt.isNaN()) b.append(";rtt:").append(rtt)

        return b.toString()
    }

    fun eligibleForNodesList(): Boolean {
        // 1 timeout can occasionally happen. should be fine to hand
        // it out as long as we've verified it at least once
        return verifiedReachable() && failedQueries < 2
    }

    fun verifiedReachable(): Boolean {
        return verified
    }

    // old entries, e.g. from routing table reload
    private fun oldAndStale(): Boolean {
        return failedQueries > OLD_AND_STALE_TIMEOUTS && System.currentTimeMillis() - lastSeen > OLD_AND_STALE_TIME
    }

    fun needsReplacement(): Boolean {
        return (failedQueries > 1 && !verifiedReachable()) || failedQueries > MAX_TIMEOUTS || oldAndStale()
    }

    fun mergeInTimestamps(other: BucketEntry) {
        if (!this.equals(other) || this === other) return
        lastSeen = max(lastSeen, other.lastSeen)
        lastSendTime = max(lastSendTime, other.lastSendTime)
        creationTime = min(creationTime, other.creationTime)
        if (other.verifiedReachable()) setVerified()
        if (!other.avgRTT.average.isNaN()) avgRTT.updateAverage(other.avgRTT.average)
    }

    val rTT: Int
        get() = avgRTT.getAverage(RPC_CALL_TIMEOUT_MAX.toDouble())
            .toInt()

    /**
     * @param rtt > 0 in ms. -1 if unknown
     */
    fun signalResponse(rtt: Long) {
        lastSeen = System.currentTimeMillis()
        failedQueries = 0
        verified = true
        if (rtt > 0) avgRTT.updateAverage(rtt.toDouble())
    }

    fun mergeRequestTime(requestSent: Long) {
        lastSendTime = max(lastSendTime.toDouble(), requestSent.toDouble()).toLong()
    }

    fun signalScheduledRequest() {
        lastSendTime = System.currentTimeMillis()
    }

    /**
     * Should be called to signal that a request to this peer has timed out;
     */
    fun signalRequestTimeout() {
        failedQueries++
    }


    private fun setVerified() {
        verified = true
    }


    class DistanceOrder(val target: ByteArray) : Comparator<BucketEntry> {
        override fun compare(o1: BucketEntry, o2: BucketEntry): Int {
            return threeWayDistance(target, o1.id, o2.id)
        }
    }
}

/**
 * ascending order for timeCreated, i.e. the first value will be the oldest
 */

val AGE_ORDER: Comparator<BucketEntry> =
    Comparator.comparingLong { obj: BucketEntry -> obj.creationTime }

// 5 timeouts, used for exponential backoff as per kademlia paper
private const val MAX_TIMEOUTS = 5

// haven't seen it for a long time + timeout == evict sooner than pure timeout
// based threshold. e.g. for old entries that we haven't touched for a long time
private const val OLD_AND_STALE_TIME = 15 * 60 * 1000
private const val OLD_AND_STALE_TIMEOUTS = 2
private const val RTT_EMA_WEIGHT = 0.3