package tech.lp2p.idun.core

import tech.lp2p.idun.messages.Message

interface CallListener {
    suspend fun stateTransition(call: Call, previous: CallState, current: CallState)

    fun onResponse(call: Call, rsp: Message)

    suspend fun onTimeout(call: Call)
}
