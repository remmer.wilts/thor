package tech.lp2p.idun.core


data class Peers(val key: ByteArray) {
    private val items: MutableList<Peer> = mutableListOf()

    @Synchronized
    fun add(toAdd: Peer) {
        val idx = items.indexOf(toAdd)
        if (idx >= 0) {
            return
        }
        items.add(toAdd)
    }

    @Synchronized
    fun snapshot(): List<Peer> {
        return items.shuffled()
    }

    @Synchronized
    fun size(): Int {
        return items.size
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Peers

        if (!key.contentEquals(other.key)) return false
        if (items != other.items) return false

        return true
    }

    override fun hashCode(): Int {
        var result = key.contentHashCode()
        result = 31 * result + items.hashCode()
        return result
    }
}
