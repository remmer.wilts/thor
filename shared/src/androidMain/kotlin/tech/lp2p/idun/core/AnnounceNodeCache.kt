package tech.lp2p.idun.core

import tech.lp2p.idun.messages.Message
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.ConcurrentSkipListMap

class AnnounceNodeCache internal constructor() {
    private val anchors =
        ConcurrentSkipListMap<ByteArray, CacheAnchorPoint>(object : Comparator<ByteArray> {
            override fun compare(
                p0: ByteArray,
                p1: ByteArray
            ): Int {
                return Arrays.compareUnsigned(p0, p1)
            }
        })
    private val cache =
        ConcurrentSkipListMap<ByteArray, CacheBucket>(object : Comparator<ByteArray> {
            override fun compare(
                p0: ByteArray,
                p1: ByteArray
            ): Int {
                return Arrays.compareUnsigned(p0, p1)
            }
        })
    val callListener: CallListener = object : CallListener {
        override suspend fun stateTransition(call: Call, previous: CallState, current: CallState) {
        }

        override suspend fun onTimeout(call: Call) {
            val nodeId = call.expectedID ?: return

            val targetEntry = cache.floorEntry(nodeId)

            if (targetEntry == null || !isPrefixOf(targetEntry.value.prefix, nodeId)) return

            // remove an entry if the id matches
            // ignore the removal if we have heard from the node after the request has been
            // issued, it might be a spurious failure
            targetEntry.value.removeIf { e: BucketEntry ->
                e.id.contentEquals(nodeId) &&
                        (e.lastSeen < call.sentTime || call.sentTime == -1L)
            }
        }


        override fun onResponse(call: Call, rsp: Message) {
            if (!call.matchesExpectedID()) return
            val kbe = BucketEntry(rsp.address, rsp.id)
            kbe.signalResponse(call.rTT)
            add(kbe)
        }
    }

    init {
        val rootBucket = CacheBucket(Prefix(ByteArray(SHA1_HASH_LENGTH), -1))
        cache[rootBucket.prefix.hash] = rootBucket
    }

    fun clear() {
        cache.clear()
        anchors.clear()
    }

    fun register(target: ByteArray) {
        val anchor = CacheAnchorPoint(target)
        anchors[target] = anchor
    }

    /*
     * this insert procedure might cause minor inconsistencies (duplicate entries, too-large lists)
     * but those are self-healing under merge/split operations
     */
    private fun add(entryToInsert: BucketEntry) {
        val target = entryToInsert.id

        outer@ while (true) {
            val targetEntry = cache.floorEntry(target)

            if (targetEntry == null || !isPrefixOf(
                    targetEntry.value.prefix,
                    target
                )
            ) { // split/merge operation ongoing, retry
                Thread.yield()
                continue
            }

            val targetBucket = targetEntry.value

            var size = 0

            for (e in targetBucket) {
                size++
                // refresh timestamp, this is checked for removals
                if (e.id.contentEquals(entryToInsert.id)) {
                    e.mergeInTimestamps(entryToInsert)
                    break@outer
                }
            }

            if (size >= MAX_CONCURRENT_REQUESTS) {
                // cache entry full, see if we this bucket prefix covers any anchor
                val anchorEntry = anchors.ceilingEntry(targetBucket.prefix.hash)

                if (anchorEntry == null || !isPrefixOf(
                        targetBucket.prefix,
                        anchorEntry.value.key
                    )
                ) {
                    // if this bucket is full and cannot be split
                    val it = targetBucket.iterator()
                    while (it.hasNext()) {
                        val kbe = it.next()
                        if (entryToInsert.rTT < kbe!!.rTT) {
                            targetBucket.add(entryToInsert)
                            it.remove()
                            break@outer
                        }
                    }
                    break
                }


                // check for concurrent split/merge
                if (cache[targetBucket.prefix.hash] !== targetBucket) continue
                // perform split operation
                val lowerBucket = CacheBucket(splitPrefixBranch(targetBucket.prefix, false))
                val upperBucket = CacheBucket(splitPrefixBranch(targetBucket.prefix, true))

                // remove old entry. this leads to a temporary gap in the cache-keyspace!
                if (!cache.remove(targetEntry.key, targetBucket)) continue

                cache[upperBucket.prefix.hash] = upperBucket
                cache[lowerBucket.prefix.hash] = lowerBucket

                for (e in targetBucket) add(e)


                continue
            }

            targetBucket.add(entryToInsert)
            break
        }
    }

    fun get(target: ByteArray, targetSize: Int): List<BucketEntry> {
        val closestSet = ArrayList<BucketEntry>(2 * targetSize)

        var ceil = cache.ceilingEntry(target)
        var floor = cache.floorEntry(target)

        do {
            if (floor != null) {
                closestSet.addAll(floor.value)
                floor = cache.lowerEntry(floor.key)
            }

            if (ceil != null) {
                closestSet.addAll(ceil.value)
                ceil = cache.higherEntry(ceil.key)
            }
        } while (closestSet.size / 2 < targetSize && (floor != null || ceil != null))

        return closestSet
    }

    private class CacheAnchorPoint(val key: ByteArray)

    private class CacheBucket(val prefix: Prefix) : ConcurrentLinkedQueue<BucketEntry>()
}
