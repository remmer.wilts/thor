package tech.lp2p.idun.core

import kotlin.math.max

class ClosestNodesSearch(
    private val key: ByteArray,
    private val maxEntries: Int,
    private val node: Node
) {
    private val entries: MutableList<BucketEntry> =
        ArrayList(maxEntries + MAX_ENTRIES_PER_BUCKET)

    private val comp: Comparator<BucketEntry> = BucketEntry.DistanceOrder(key)

    var filter: (BucketEntry) -> Boolean = { obj: BucketEntry -> obj.eligibleForNodesList() }

    /**
     * consider the following routing table:
     *
     *
     * 0000000...
     * 0000001...
     * 000001...
     * 00001...
     * 0001...
     * 001...
     * 01...
     * 1...
     *
     *
     * now consider the following target key:
     *
     *
     * 1001101111011100000000011101011001111100001100000010111010111110101000100010101011101001101111010011011110000111010010001100001101011110100000010000011001101000
     *
     *
     * the first bucket we will want to pick values from is 1...
     * the second bucket with the next-higher xor distance actually is 0001...
     *
     *
     * This requires a non-contiguous search
     */
    private fun insertBucket(bucket: Bucket) {
        bucket.entries().filter(filter).forEach { e: BucketEntry -> entries.add(e) }
    }

    private fun shave() {
        val overshoot = entries.size - maxEntries

        if (overshoot <= 0) return

        val tail: List<BucketEntry> = entries.subList(
            max(0, (entries.size - MAX_ENTRIES_PER_BUCKET)), entries.size
        )
        tail.sortedWith(comp)
        entries.subList(entries.size - overshoot, entries.size).clear()
    }

    fun fill() {
        val table = node.routingTable


        val initialIdx = table.indexForId(key)
        var currentIdx = initialIdx

        var current: RoutingTableEntry? = table.get(initialIdx)


        while (true) {
            insertBucket(current!!.bucket)

            if (entries.size >= maxEntries) break

            val bucketPrefix = current.prefix
            val targetToBucketDistance = createPrefix(
                distance(key, bucketPrefix.hash),
                bucketPrefix.depth
            ) // translate into xor distance, trim trailing bits
            val incrementedDistance =
                add(
                    targetToBucketDistance.hash,
                    setBitKey(targetToBucketDistance.depth)
                ) // increment distance by least significant *prefix* bit
            val nextBucketTarget =
                distance(key, incrementedDistance) // translate back to natural distance

            // guess neighbor bucket that might be next in target order
            val dir =
                Integer.signum(Arrays.compareUnsigned(nextBucketTarget, current.prefix.hash))
            var idx: Int

            current = null

            idx = currentIdx + dir
            if (0 <= idx && idx < table.size()) current = table.get(idx)

            // do binary search if guess turned out incorrect
            if (current == null || !isPrefixOf(current.prefix, nextBucketTarget)) {
                idx = table.indexForId(nextBucketTarget)
                current = table.get(idx)
            }

            currentIdx = idx

            // quit if there are insufficient routing table entries to reach the desired size
            if (currentIdx == initialIdx) break
        }

        shave()
    }

    fun inet4List(): List<BucketEntry> {
        return entries.filter { e: BucketEntry ->
            e.address.address.size == 4
        }
    }

    fun inet6List(): List<BucketEntry> {
        return entries.filter { e: BucketEntry ->
            e.address.address.size == 16
        }
    }

    /**
     * @return a unmodifiable List of the entries
     */
    fun getEntries(): List<BucketEntry> {
        return entries.toList()
    }
}
