package tech.lp2p.idun.core

import kotlin.experimental.inv


data class Prefix(val hash: ByteArray, val depth: Int) {
    /**
     * identifies the first bit of a key that has to be equal to be considered as covered by this prefix
     * -1 = prefix matches whole keyspace
     * 0 = 0th bit must match
     * 1 = ...
     */
    override fun toString(): String {
        if (depth == -1) return "all"
        val builder = StringBuilder(depth + 3)
        for (i in 0..depth) builder.append(
            if ((hash[i / 8].toInt()
                        and (0x80 shr (i % 8))) != 0
            ) '1' else '0'
        )
        builder.append("...")
        return builder.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Prefix

        if (depth != other.depth) return false
        if (!hash.contentEquals(other.hash)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = depth
        result = 31 * result + hash.contentHashCode()
        return result
    }
}


fun createPrefixHash(hash: ByteArray, depth: Int): ByteArray {
    val hash = hash.clone()
    copyBits(hash, hash, depth)
    return hash
}

fun createPrefix(hash: ByteArray, depth: Int): Prefix {
    val hash = createPrefixHash(hash, depth)
    return Prefix(hash, depth)
}

fun splitPrefixBranch(prefix: Prefix, highBranch: Boolean): Prefix {

    val hash = prefix.hash.clone()
    val depth = prefix.depth + 1
    if (highBranch) hash[depth / 8] =
        (hash[depth / 8].toInt() or (0x80 shr (depth % 8)).toByte()
            .toInt()).toByte()
    else hash[depth / 8] =
        (hash[depth / 8].toInt() and (0x80 shr (depth % 8)).toByte()
            .inv()
            .toInt()).toByte()

    return Prefix(hash, depth)
}


fun isPrefixOf(prefix: Prefix, hash: ByteArray): Boolean {
    return bitsEqual(prefix.hash, hash, prefix.depth)
}

/**
 * @return true if the first bits up to the Nth bit of both keys are equal
 *
 * <pre>
 * n = -1 => no bits have to match
 * n = 0  => byte 0, MSB has to match
 *
</pre> */
private fun bitsEqual(h1: ByteArray, h2: ByteArray, n: Int): Boolean {
    if (n < 0) return true


    val lastToCheck = n ushr 3

    val mmi = Arrays.mismatch(h1, h2)

    val diff = (h1[lastToCheck].toInt() xor h2[lastToCheck].toInt()) and 0xff

    val lastByteDiff = (diff and (0xff80 ushr (n and 0x07))) == 0

    return if (mmi == lastToCheck) lastByteDiff else Integer.compareUnsigned(
        mmi,
        lastToCheck
    ) > 0
}

private fun copyBits(source: ByteArray, data: ByteArray, depth: Int) {
    if (depth < 0) return

    // copy over all complete bytes
    System.arraycopy(source, 0, data, 0, depth / 8)

    val idx = depth / 8
    val mask = 0xFF80 shr depth % 8

    // mask out the part we have to copy over from the last prefix byte
    data[idx] = (data[idx].toInt() and mask.toByte().inv().toInt()).toByte()
    // copy the bits from the last byte
    data[idx] =
        (data[idx].toInt() or (source[idx].toInt() and mask).toByte().toInt()).toByte()
}

