package tech.lp2p.idun.core


enum class CallState {
    UNSENT,
    SENT,
    STALLED,
    ERROR,
    TIMEOUT,
    RESPONDED
}
