package tech.lp2p.idun.core

data class RoutingTableEntry(val prefix: Prefix, val bucket: Bucket) :
    Comparable<RoutingTableEntry> {

    override fun compareTo(other: RoutingTableEntry): Int {
        return Arrays.compareUnsigned(prefix.hash, other.prefix.hash)
    }

    override fun toString(): String {
        return "$prefix $bucket"
    }
}