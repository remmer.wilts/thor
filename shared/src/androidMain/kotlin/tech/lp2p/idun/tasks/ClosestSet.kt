package tech.lp2p.idun.tasks

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import tech.lp2p.idun.core.BucketEntry
import tech.lp2p.idun.core.Key
import tech.lp2p.idun.core.MAX_ENTRIES_PER_BUCKET
import tech.lp2p.idun.core.distance
import java.util.concurrent.ConcurrentSkipListSet

/*
* We need to detect when the closest set is stable
*  - in principle we're done as soon as there is no request candidates
*/
class ClosestSet(private val key: ByteArray) {
    private val mutex = Mutex()
    private val closest: MutableSet<BucketEntry> = ConcurrentSkipListSet(
        BucketEntry.DistanceOrder(
            key
        )
    )

    var insertAttemptsSinceTailModification: Int = 0
    private var insertAttemptsSinceHeadModification = 0

    fun reachedTargetCapacity(): Boolean {
        return closest.size >= MAX_ENTRIES_PER_BUCKET
    }

    suspend fun insert(reply: BucketEntry) {
        mutex.withLock {
            closest.add(reply)
            if (closest.size > MAX_ENTRIES_PER_BUCKET) {
                val last = closest.last()
                closest.remove(last)
                if (last === reply) insertAttemptsSinceTailModification++
                else insertAttemptsSinceTailModification = 0
            }
            if (closest.first() === reply) {
                insertAttemptsSinceHeadModification = 0
            } else {
                insertAttemptsSinceHeadModification++
            }
        }
    }


    fun entries(): List<BucketEntry> {
        return closest.toList()
    }

    fun tail(): ByteArray {
        if (closest.isEmpty()) return distance(key, Key.MAX_KEY)

        return closest.last().id
    }

    fun head(): ByteArray {
        if (closest.isEmpty()) return distance(key, Key.MAX_KEY)

        return closest.first().id
    }

}
