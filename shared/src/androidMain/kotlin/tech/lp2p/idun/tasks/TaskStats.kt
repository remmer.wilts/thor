package tech.lp2p.idun.tasks


data class TaskStats(private val counters: IntArray = IntArray(CountedStat.entries.size)) {

    fun update(
        inc: Set<CountedStat>,
        dec: Set<CountedStat>,
        zero: Set<CountedStat>
    ): TaskStats {
        val p = cloneTaskStats()
        for (counter in inc) {
            p.counters[counter.ordinal]++
        }
        for (counter in dec) {
            p.counters[counter.ordinal]--
        }
        for (counter in zero) {
            p.counters[counter.ordinal] = 0
        }
        return p
    }

    fun get(countedStat: CountedStat): Int {
        return counters[countedStat.ordinal]
    }

    private fun cloneTaskStats(): TaskStats {
        return TaskStats(counters.clone())
    }

    private fun done(): Int {
        return get(CountedStat.FAILED) + get(CountedStat.RECEIVED)
    }

    fun unanswered(): Int {
        return get(CountedStat.SENT) - done()
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TaskStats

        return counters.contentEquals(other.counters)
    }

    override fun hashCode(): Int {
        return counters.contentHashCode()
    }
}
