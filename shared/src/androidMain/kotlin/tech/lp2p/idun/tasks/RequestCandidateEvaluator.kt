package tech.lp2p.idun.tasks

import tech.lp2p.idun.core.BucketEntry
import tech.lp2p.idun.core.Call
import tech.lp2p.idun.core.CallState
import tech.lp2p.idun.core.MAX_ENTRIES_PER_BUCKET
import tech.lp2p.idun.core.threeWayDistance


private fun activeInFlight(inFlight: Collection<Call>): Int {
    return inFlight.filter { c: Call ->
        val state = c.state()
        state == CallState.UNSENT || state == CallState.SENT
    }.map { obj: Call -> obj.expectedID!! }.count()
}

private fun inStabilization(closest: ClosestSet, todo: IterativeLookupCandidates): Boolean {
    val suggestedCounts = closest.entries().map { k: BucketEntry ->
        todo.nodeForEntry(
            k
        )!!.sources.size
    }

    return suggestedCounts.any { i: Int -> i >= 5 } ||
            suggestedCounts.count { i: Int -> i >= 4 } >= 2
}

private fun candidateAheadOfClosestSet(
    task: Task,
    closest: ClosestSet,
    candidate: BucketEntry
): Boolean {
    return !closest.reachedTargetCapacity() ||
            threeWayDistance(task.key, closest.head(), candidate.id) > 0
}

private fun candidateAheadOfClosestSetTail(
    task: Task, closest: ClosestSet, candidate: BucketEntry
): Boolean {
    return !closest.reachedTargetCapacity() ||
            threeWayDistance(task.key, closest.tail(), candidate.id) > 0
}

fun terminationPrecondition(
    task: Task, closest: ClosestSet,
    todo: IterativeLookupCandidates, candidate: BucketEntry
): Boolean {
    return !candidateAheadOfClosestSetTail(task, closest, candidate) && (
            inStabilization(closest, todo) ||
                    closest.insertAttemptsSinceTailModification > MAX_ENTRIES_PER_BUCKET)
}


/* algo:
* 1. check termination condition
* 2. allow if free slot
* 3. if stall slot check
* a) is candidate better than non-stalled in flight
* b) is candidate better than head (homing phase)
* c) is candidate better than tail (stabilizing phase)
*/
fun goodForRequest(
    task: Task,
    closest: ClosestSet,
    todo: IterativeLookupCandidates,
    candidate: BucketEntry,
    inFlight: Collection<Call>
): Boolean {

    var result = candidateAheadOfClosestSet(task, closest, candidate)

    if (candidateAheadOfClosestSetTail(task, closest, candidate) &&
        inStabilization(closest, todo)
    ) result = true
    if (!terminationPrecondition(task, closest, todo, candidate) &&
        activeInFlight(inFlight) == 0
    ) result = true

    return result
}

