package tech.lp2p.idun.tasks

import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.updateAndGet
import tech.lp2p.idun.core.Call
import tech.lp2p.idun.core.CallListener
import tech.lp2p.idun.core.CallState
import tech.lp2p.idun.core.Node
import tech.lp2p.idun.debug
import tech.lp2p.idun.messages.Message
import tech.lp2p.idun.messages.Request
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.abs
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

abstract class Task internal constructor(val key: ByteArray, val node: Node) : Comparable<Task> {
    val counts = atomic(TaskStats())
    val state = atomic(TaskState.INITIAL)
    val inFlight: MutableSet<Call> = ConcurrentHashMap.newKeySet()


    private val preProcessingListener: CallListener = object : CallListener {
        override suspend fun stateTransition(call: Call, previous: CallState, current: CallState) {
            counts.updateAndGet { cnt: TaskStats ->
                val inc = mutableSetOf<CountedStat>()
                val dec = mutableSetOf<CountedStat>()
                val zero = mutableSetOf<CountedStat>()

                if (previous == CallState.STALLED) dec.add(CountedStat.STALLED)
                if (current == CallState.STALLED) inc.add(CountedStat.STALLED)

                if (current == CallState.RESPONDED) {
                    inc.add(CountedStat.RECEIVED)
                    zero.add(CountedStat.SENT_SINCE_RECEIVE)
                }

                if (current == CallState.TIMEOUT || current == CallState.ERROR) inc.add(CountedStat.FAILED)
                cnt.update(inc, dec, zero)
            }

            when (current) {
                CallState.RESPONDED -> {
                    inFlight.remove(call)
                    if (!isFinished) callFinished(call, call.response!!)
                }

                CallState.ERROR -> inFlight.remove(call)
                CallState.TIMEOUT -> {
                    inFlight.remove(call)
                    if (!isFinished) timeout()
                }

                else -> {}
            }
        }

        override fun onResponse(call: Call, rsp: Message) {

        }

        override suspend fun onTimeout(call: Call) {

        }
    }

    private var startTime: Long = 0
    private var taskID = 0
    private var listeners: MutableList<TaskListener>? = null
    private var finishTime: Long = 0

    private val postProcessingListener: CallListener = object : CallListener {
        override suspend fun stateTransition(call: Call, previous: CallState, current: CallState) {
            when (current) {
                CallState.RESPONDED, CallState.TIMEOUT, CallState.STALLED,
                CallState.ERROR -> runStuff()

                else -> {}
            }
        }

        override fun onResponse(call: Call, rsp: Message) {

        }

        override suspend fun onTimeout(call: Call) {

        }
    }

    private fun setState(expected: Set<TaskState>, newState: TaskState): Boolean {
        var current: TaskState
        do {
            current = state.value
            if (!expected.contains(current)) return false
        } while (!state.compareAndSet(current, newState))

        return true
    }

    override fun compareTo(other: Task): Int {
        return taskID - other.taskID
    }


    open suspend fun start() {
        if (setState(setOf(TaskState.INITIAL, TaskState.QUEUED), TaskState.RUNNING)) {
            startTime = System.currentTimeMillis()
            try {
                runStuff()
            } catch (e: Exception) {
                debug("Task", e)
            }
        }
    }

    private suspend fun runStuff() {
        if (isDone()) finish()

        if (!isFinished) {
            update()

            if (isDone()) finish()
        }
    }

    /**
     * Will continue the task, this will be called every time we have
     * rpc slots available for this task. Should be implemented by derived classes.
     */
    abstract suspend fun update()

    abstract suspend fun callFinished(call: Call, rsp: Message)

    abstract fun timeout()


    fun rpcCall(
        request: Request,
        expectedID: ByteArray,
        modifyCallBeforeSubmit: (Call) -> Unit
    ) {

        val call = Call(request, expectedID)

        // bump counters early to ensure task stays alive
        counts.updateAndGet { cnt: TaskStats ->
            cnt.update(
                setOf(CountedStat.SENT, CountedStat.SENT_SINCE_RECEIVE),
                setOf(),
                setOf()
            )
        }

        call.addListener(preProcessingListener)

        modifyCallBeforeSubmit.invoke(call)

        call.addListener(postProcessingListener)

        inFlight.add(call)

        node.doCall(call)
    }


    val isFinished: Boolean
        /// Is the task finished
        get() = state.value.isTerminal


    abstract fun getTodoCount(): Int


    val numOutstandingRequests: Int
        /**
         * @return number of requests that still haven't reached their final state but might have stalled
         */
        get() = counts.value.unanswered()

    private fun finish() {
        if (setState(
                setOf(TaskState.INITIAL, TaskState.QUEUED, TaskState.RUNNING),
                TaskState.FINISHED
            )
        ) notifyCompletionListeners()
    }

    private fun notifyCompletionListeners() {
        finishTime = System.currentTimeMillis()

        if (listeners != null) {
            for (tl in listeners!!) {
                tl.finished()
            }
        }
    }

    abstract fun isDone(): Boolean


    fun addListener(listener: TaskListener) {
        if (listeners == null) {
            listeners = ArrayList(1)
        }
        // listener is added after the task already terminated, thus it won't get the event, trigger it manually
        if (state.value.isTerminal) listener.finished()
        listeners!!.add(listener)
    }

    fun age(): Duration {
        return abs(startTime - System.currentTimeMillis()).toDuration(DurationUnit.MILLISECONDS)
    }


    override fun toString(): String {
        val b = StringBuilder(100)
        val stats = counts.value
        b.append(javaClass.simpleName)
        b.append(' ').append(taskID)
        b.append(" target:").append(key)
        b.append(" todo:").append(getTodoCount())
        if (!state.value.preStart()) {
            b.append(" ").append(stats)
        }
        b.append(" srv: ").append(node.nodeId)

        b.append(' ').append(state.value.toString())

        if (startTime != 0L) {
            if (finishTime == 0L) b.append(" age:").append(age())
            else if (finishTime > 0) b.append(" time to finish:").append(finishTime - startTime)
        }
        return b.toString()
    }

    enum class TaskState {
        INITIAL,
        QUEUED,
        RUNNING,
        FINISHED,
        KILLED;

        val isTerminal: Boolean
            get() = this == FINISHED || this == KILLED


        fun preStart(): Boolean {
            return this == INITIAL || this == QUEUED
        }
    }


}
