package tech.lp2p.idun.tasks

interface TaskListener {
    fun finished()
}
