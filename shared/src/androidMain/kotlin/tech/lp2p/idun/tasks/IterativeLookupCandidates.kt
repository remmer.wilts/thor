package tech.lp2p.idun.tasks

import kotlinx.atomicfu.locks.reentrantLock
import kotlinx.atomicfu.locks.withLock
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.BucketEntry
import tech.lp2p.idun.core.Call
import tech.lp2p.idun.core.CallState
import tech.lp2p.idun.core.IDMismatchDetector
import tech.lp2p.idun.core.NonReachableCache
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ThreadLocalRandom
import kotlin.math.max
import kotlin.math.min

/*
* Issues:
*
* - spurious packet loss
* - remotes might fake IDs. possibly with collusion.
* - invalid results
*   - duplicate IPs
*   - duplicate IDs
*   - wrong IDs -> might trip up fake ID detection!
*   - IPs not belonging to DHT nodes -> DoS abuse
*
* Solution:
*
* - generally avoid querying an IP more than once
* - dedup result lists from each node
* - ignore responses with unexpected IDs. normally this could be abused to silence others, but...
* - allow duplicate requests if many *separate* sources suggest precisely the same <id, ip, port> tuple
*
* -> we can recover from all the above-listed issues because the terminal set of nodes should
* have some partial agreement about their neighbors
*
* TODO:
*  - global mutual exclusion for in-flight targets. bad peers polluting everyone's routing table are the #1 slowdown for bulk lookups now
*  - min-heaps (active + penalty box)? assign score once. recalculate as needed
*
*/
class IterativeLookupCandidates internal constructor(
    private val target: ByteArray,
    private val detector: IDMismatchDetector,
    private var nonReachableCache: NonReachableCache

) {
    val lookupFilter: (LookupGraphNode) -> Boolean

    // maybe split out call tracking
    private val calls: MutableMap<Call, BucketEntry> = ConcurrentHashMap()
    private val lock = reentrantLock()
    private val callsByIp: MutableMap<Address, MutableSet<Call>> = ConcurrentHashMap()
    private val acceptedInets: MutableCollection<Address> = mutableSetOf()
    private val acceptedKeys: MutableCollection<Int> = mutableSetOf()
    private val candidates: MutableMap<BucketEntry, LookupGraphNode> = ConcurrentHashMap()

    init {
        lookupFilter = { node: LookupGraphNode ->
            lookup(node)
        }
    }

    private fun lookup(node: LookupGraphNode): Boolean {
        val kbe = node.bucketEntry
        if (node.tainted || node.unreachable || node.throttled) return false


        // skip retransmits if we previously got a response but from the wrong socket address
        if (node.calls.isNotEmpty() &&
            node.calls.any { obj: Call -> obj.hasSocketMismatch() }
        ) return false


        val addr = kbe.address

        if (acceptedInets.contains(addr) || acceptedKeys.contains(kbe.id.contentHashCode()))
            return false

        // only do requests to nodes which have at least one source where the source has not given us lots of bogus candidates
        if (node.sources.isNotEmpty() && node.sources
                .none { source: LookupGraphNode -> source.nonSuccessfulDescendantCalls() < 3 }
        ) return false

        var dups = 0

        // also check other calls based on matching IP instead of strictly matching ip+port+id
        val byIp: Set<Call>? = callsByIp[addr]
        if (byIp != null) {
            lock.withLock {
                for (c in byIp) {
                    // in flight, not stalled
                    if (c.state() == CallState.SENT || c.state() == CallState.UNSENT) return false

                    // already got a response from that addr that does not match what we would expect from this candidate anyway
                    if (c.state() == CallState.RESPONDED && !c.response!!.id.contentEquals(
                            kbe.id
                        )
                    ) return false
                    // we don't strictly check the presence of IDs in error messages, so we can't compare those here
                    if (c.state() == CallState.ERROR) return false
                    dups++
                }
            }
        }
        // log2 scale
        val sources =
            max(1.0, (node.sources.size + (if (node.root) 1 else 0)).toDouble()).toInt()
        val scaledSources = 31 - Integer.numberOfLeadingZeros(sources)
        scaledSources >= dups
        return true
    }


    fun addCall(c: Call, kbe: BucketEntry) {
        calls[c] = kbe

        lock.withLock {
            val byIp = callsByIp.computeIfAbsent(
                c.request.address
            ) { _: Address? -> HashSet() }
            byIp.add(c)
        }


        checkNotNull(candidates[kbe]).addCall(c)
    }

    fun acceptResponse(c: Call): BucketEntry? {
        // we ignore on mismatch, node will get a 2nd chance if sourced from multiple
        // nodes and hasn't sent a successful reply yet
        lock.withLock {
            if (!c.matchesExpectedID()) return null
            val kbe = calls[c]
            checkNotNull(kbe)

            val node = candidates[kbe]
            checkNotNull(node)
            val insertOk = !acceptedInets.contains(kbe.address) && !acceptedKeys.contains(
                kbe.id.contentHashCode()
            )
            if (insertOk) {
                acceptedInets.add(kbe.address)
                acceptedKeys.add(kbe.id.contentHashCode())
                node.accept()
                return kbe
            }
            return null
        }
    }

    fun addCandidates(source: BucketEntry?, entries: Collection<BucketEntry>) {
        val dedup: MutableSet<Any> = HashSet()

        val sourceNode = if (source != null) candidates[source] else null

        val children: MutableList<LookupGraphNode> = ArrayList()

        for (e in entries) {
            if (!dedup.add(e.id.contentHashCode()) || !dedup.add(e.address)) continue


            val newNode = candidates.compute(e) { kbe: BucketEntry, lgn: LookupGraphNode? ->
                var node = lgn
                if (node == null) {
                    node = LookupGraphNode(kbe)
                    node.root = source == null
                    node.tainted = detector.isIdInconsistencyExpected(kbe.address, kbe.id)

                    val failures = nonReachableCache.getFailures(kbe.address)
                    // 0-20
                    val rnd = ThreadLocalRandom.current().nextInt(21)
                    // -2 - 19 -> 5% chance to let even the worst stuff still
                    // through to keep the counters going up
                    node.unreachable = min((failures - 2).toDouble(), 19.0) > rnd

                }
                if (sourceNode != null) node.addSource(sourceNode)
                node
            }!!

            children.add(newNode)
        }

        sourceNode?.addChildren(children)
    }

    private fun comp(): Comparator<LookupGraphNode> {
        val d: Comparator<BucketEntry> = BucketEntry.DistanceOrder(target)
        val s =
            Comparator { a: LookupGraphNode, b: LookupGraphNode -> b.sources.size - a.sources.size }
        return Comparator.comparing({ n: LookupGraphNode -> n.bucketEntry }, d).thenComparing(s)
    }

    fun next(): BucketEntry? {
        lock.withLock {
            val node = allCand().sortedWith(comp()).firstOrNull(lookupFilter)
            return node?.toKbe()
        }
    }

    fun next2(postFilter: (BucketEntry) -> Boolean): BucketEntry? {
        lock.withLock {
            // sort + filter + findAny should be faster than filter + min in
            // this case since findAny reduces the invocations of the filter,
            // and that is more expensive than the sorting
            var node = allCand().sortedWith(comp())
                .filter(retransmitFilter(false)).firstOrNull(lookupFilter)

            var kbe = node?.toKbe()
            if (kbe != null) {
                if (postFilter.invoke(kbe)) {
                    return kbe
                }
            }


            node = allCand().sortedWith(comp()).filter(lookupFilter).firstOrNull(
                retransmitFilter(true)
            )
            kbe = node?.bucketEntry
            if (kbe != null) {
                if (postFilter.invoke(kbe)) {
                    return kbe
                }
            }


            return null
        }
    }

    fun allCand(): Collection<LookupGraphNode> {
        return candidates.values
    }

    fun nodeForEntry(e: BucketEntry): LookupGraphNode? {
        return candidates[e]
    }

}

private fun retransmitFilter(retransmits: Boolean): (LookupGraphNode) -> Boolean {
    return { node: LookupGraphNode -> node.calls.isEmpty() || retransmits }
}