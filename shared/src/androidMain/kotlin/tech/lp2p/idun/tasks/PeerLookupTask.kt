package tech.lp2p.idun.tasks

import kotlinx.coroutines.channels.Channel
import tech.lp2p.idun.core.BucketEntry
import tech.lp2p.idun.core.Call
import tech.lp2p.idun.core.ClosestNodesSearch
import tech.lp2p.idun.core.MAX_CONCURRENT_REQUESTS
import tech.lp2p.idun.core.MAX_ENTRIES_PER_BUCKET
import tech.lp2p.idun.core.Node
import tech.lp2p.idun.core.Peer
import tech.lp2p.idun.core.RPC_CALL_TIMEOUT_MAX
import tech.lp2p.idun.core.TID_LENGTH
import tech.lp2p.idun.core.createRandomKey
import tech.lp2p.idun.messages.GetPeersRequest
import tech.lp2p.idun.messages.GetPeersResponse
import tech.lp2p.idun.messages.Message
import kotlin.math.min

class PeerLookupTask(node: Node, key: ByteArray, val channel: Channel<Peer>) :
    IteratingTask(key, node) {

    init {
        // register key even before the task is started so the cache can already accumulate entries
        node.cache.register(key)

        addListener(object : TaskListener {
            override fun finished() {
                todo.next()
            }
        })
    }


    override suspend fun callFinished(call: Call, rsp: Message) {
        if (rsp !is GetPeersResponse) return

        val match = todo.acceptResponse(call) ?: return

        val returnedNodes: MutableSet<BucketEntry> = HashSet()


        rsp.nodes6.filter { e: BucketEntry ->
            !node.isLocalId(e.id)
        }.forEach { e: BucketEntry -> returnedNodes.add(e) }

        rsp.nodes.filter { e: BucketEntry ->
            !node.isLocalId(e.id)
        }.forEach { e: BucketEntry -> returnedNodes.add(e) }

        todo.addCandidates(match, returnedNodes)


        for (item in rsp.items) {

            // also add the items to the returned_items list
            channel.send(item)
        }


        // if we scrape we don't care about tokens.
        // otherwise we're only done if we have found the closest nodes that also returned tokens
        if (rsp.token != null) {
            closest.insert(match)
        }
    }

    override fun timeout() {
        System.err.println("timeout") // todo
    }

    override suspend fun update() {
        // check if the cache has any closer nodes after the initial query
        if (USE_CACHE) {
            val cacheResults: Collection<BucketEntry> =
                node.cache.get(key, MAX_CONCURRENT_REQUESTS)
            todo.addCandidates(null, cacheResults)
        }

        while (true) {


            val e = todo.next2 { kbe: BucketEntry ->
                goodForRequest(this, closest, todo, kbe, inFlight)
            }

            if (e == null) return

            val tid = createRandomKey(TID_LENGTH)

            val gpr = GetPeersRequest(e.address, node.nodeId, tid, key)
            System.err.println("PLT Request $gpr")
            // we only request cross-seeding on find-node

            rpcCall(gpr, e.id) { call: Call ->
                if (USE_CACHE) call.addListener(node.cache.callListener)
                call.builtFromEntry(e)

                var rtt = e.rTT.toLong()
                val defaultTimeout = node.timeoutFilter.stallTimeout

                if (rtt < RPC_CALL_TIMEOUT_MAX) {
                    // the measured RTT is a mean and not the 90th percentile unlike the RPCServer's timeout filter
                    // -> add some safety margin to account for variance
                    rtt = (rtt * (if (rtt < defaultTimeout) 2.0 else 1.5)).toLong()

                    call.expectedRTT = min(
                        rtt.toDouble(),
                        RPC_CALL_TIMEOUT_MAX.toDouble()
                    ).toLong()
                }
                todo.addCall(call, e)
            }
        }
    }


    override fun isDone(): Boolean {
        val waitingFor = numOutstandingRequests

        if (waitingFor > 0) return false

        val closest = todo.next() ?: return true

        return terminationPrecondition(this, this.closest, todo, closest)
    }


    override suspend fun start() {

        val kns = ClosestNodesSearch(key, MAX_ENTRIES_PER_BUCKET * 4, node)
        // unlike NodeLookups we do not use unverified nodes here. this avoids
        // rewarding spoofers with useful lookup target IDs
        kns.fill()
        todo.addCandidates(null, kns.getEntries())

        if (USE_CACHE) {
            // re-register once we actually started
            node.cache.register(key)
            todo.addCandidates(
                null,
                node.cache.get(key, MAX_CONCURRENT_REQUESTS * 2)
            )
        }

        super.start()
    }

}

private const val USE_CACHE = false