package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import kotlinx.io.readUShort
import tech.lp2p.idun.core.ADDRESS_LENGTH_IPV4
import tech.lp2p.idun.core.ADDRESS_LENGTH_IPV6
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.BucketEntry
import tech.lp2p.idun.core.NODE_ENTRY_LENGTH_IPV4
import tech.lp2p.idun.core.NODE_ENTRY_LENGTH_IPV6
import tech.lp2p.idun.core.Peer
import tech.lp2p.idun.core.SHA1_HASH_LENGTH
import tech.lp2p.idun.core.TID_LENGTH
import tech.lp2p.idun.core.readBuckets
import tech.lp2p.idun.core.typedGet
import tech.lp2p.idun.debug
import tech.lp2p.idun.isDebug

private fun parseError(
    address: Address,
    map: Map<String, Any>
): Message {
    val error = map["e"]

    var tid: ByteArray? = null
    var errorCode = 0
    var errorMsg: String? = null

    if (error is ByteArray) errorMsg = getStringFromBytes(error)
    else if (error is List<*>) {
        val errmap = error as List<Any>
        try {
            errorCode = (errmap[0] as Long).toInt()
            errorMsg = getStringFromBytes(errmap[1] as ByteArray)
        } catch (_: Exception) {
            // do nothing
        }
    }
    if (errorMsg == null) errorMsg = ""

    val rawMtid = map["t"]

    tid = rawMtid as? ByteArray ?: ByteArray(TID_LENGTH)

    var id = typedGet(map, "id")
    if (id == null || id.size != SHA1_HASH_LENGTH) {
        id = ByteArray(SHA1_HASH_LENGTH)
    }

    return Error(address, id, tid, errorCode, errorMsg)
}

@Throws(MessageException::class)
private fun extractNodes6(
    args: Map<String, Any?>
): List<BucketEntry> {
    val raw = args["nodes6"] as ByteArray?
    if (raw == null) return emptyList()
    if (raw.size % NODE_ENTRY_LENGTH_IPV6 != 0) throw MessageException(
        "expected length to be a multiple of " +
                NODE_ENTRY_LENGTH_IPV6 + ", received " + raw.size,
        PROTOCOL_ERROR
    )
    return readBuckets(raw, ADDRESS_LENGTH_IPV6)
}


@Throws(MessageException::class)
private fun extractNodes(
    args: Map<String, Any?>
): List<BucketEntry> {
    val raw = args["nodes"] as ByteArray?
    if (raw == null) return emptyList()
    if (raw.size % NODE_ENTRY_LENGTH_IPV4 != 0) throw MessageException(
        "expected length to be a multiple of " +
                NODE_ENTRY_LENGTH_IPV4 + ", received " + raw.size,
        PROTOCOL_ERROR
    )
    return readBuckets(raw, ADDRESS_LENGTH_IPV4)
}

@Throws(MessageException::class)
fun parseMessage(
    address: Address,
    map: Map<String, Any>,
    tidMapper: (ByteArray) -> (Request?),
): Message {
    val msgType = getStringFromBytes(map["y"] as ByteArray?, true)

    if (msgType.isEmpty()) {
        throw MessageException("message type (y) missing", PROTOCOL_ERROR)
    }

    return when (msgType) {
        "q" -> {
            parseRequest(address, map)
        }

        "r" -> {
            parseResponse(address, map, tidMapper)
        }

        "e" -> {
            parseError(address, map)
        }

        else -> throw MessageException("unknown RPC type (y=$msgType)", GENERIC_ERROR)
    }

}

@Throws(MessageException::class)
private fun parseResponse(
    address: Address,
    map: Map<String, Any>,
    tidMapper: (ByteArray) -> (Request?)
): Message {
    val tid = map["t"] as ByteArray?
    if (tid == null || tid.isEmpty()) throw MessageException(
        "missing transaction ID",
        PROTOCOL_ERROR
    )

    // responses don't have explicit methods, need to match them to a request to figure that one out
    var request = tidMapper.invoke(tid)
    if (request == null) throw MessageException(
        "unknown message type",
        PROTOCOL_ERROR
    )
    return parseResponse(address, map, request, tid)
}


@Throws(MessageException::class)
private fun parseResponse(
    address: Address,
    map: Map<String, Any>,
    request: Request, tid: ByteArray
): Message {
    val args = map["r"] as Map<String, Any?>?
        ?: throw MessageException(
            "response did not contain a body",
            PROTOCOL_ERROR
        )

    val idArg = args["id"]
        ?: throw MessageException(
            "mandatory parameter 'id' missing",
            PROTOCOL_ERROR
        )
    val id = idArg as ByteArray



    if (id.size != SHA1_HASH_LENGTH) {
        throw MessageException("invalid or missing origin ID", PROTOCOL_ERROR)
    }

    val msg: Message

    when (request) {
        is PingRequest -> msg = PingResponse(address, id, tid)
        is AnnounceRequest -> msg = AnnounceResponse(address, id, tid)
        is FindNodeRequest -> {
            if (!args.containsKey("nodes") && !args.containsKey("nodes6")) throw MessageException(
                "received response to find_node request with " +
                        "neither 'nodes' nor 'nodes6' entry", PROTOCOL_ERROR
            )
            val nodes6 = extractNodes6(args)
            val nodes = extractNodes(args)
            msg = FindNodeResponse(address, id, tid, nodes, nodes6)
        }

        is GetPeersRequest -> {
            val token = typedGet(args, "token")
            val nodes6 = extractNodes6(args)
            val nodes = extractNodes(args)
            var peers: MutableList<Peer> = ArrayList()

            var vals: List<ByteArray> = ArrayList()
            val values = args["values"]
            if (values != null) {
                if (values is List<*>) {
                    vals = values as List<ByteArray>
                } else {
                    throw MessageException(
                        "expected 'values' " +
                                "field in get_peers to be list of strings",
                        PROTOCOL_ERROR
                    )
                }
            }


            if (vals.isNotEmpty()) {
                for (i in vals.indices) {
                    // only accept ipv4 or ipv6 for now
                    val length = vals[i].size
                    if (length == ADDRESS_LENGTH_IPV4 || length == ADDRESS_LENGTH_IPV6) {
                        peers.add(Peer(vals[i].clone()))
                    } else {
                        debug(
                            "MessageDecoder",
                            "not accepted node " + vals[i].contentToString()
                        )
                    }
                }
            }

            if (peers.isNotEmpty() || nodes6.isNotEmpty() || nodes.isNotEmpty()) {
                debug("MessageDecoder", nodes.toString())
                msg = GetPeersResponse(address, id, tid, token, nodes, nodes6, peers)
            } else {
                throw MessageException(
                    "Neither nodes nor values in get_peers response",
                    PROTOCOL_ERROR
                )
            }
        }

        else -> {
            throw MessageException(
                "not handled request response",
                PROTOCOL_ERROR
            )
        }
    }

    if (isDebug) {
        val ip = map["ip"] as ByteArray?
        if (ip != null) {
            val addr = unpackAddress(ip)
            if (addr != null) {
                debug("MessageDecoder", "External IP: $addr")
            }
        }
    }

    return msg
}


fun unpackAddress(raw: ByteArray): Address? {
    if (raw.size != 6 && raw.size != 18) return null
    val buffer = Buffer()
    buffer.write(raw)
    val rawIP = buffer.readByteArray(raw.size - 2)
    val port = buffer.readUShort()
    return Address(rawIP, port)
}


const val GENERIC_ERROR = 201
const val SERVER_ERROR = 202
const val PROTOCOL_ERROR = 203
const val METHOD_UNKNOWN = 204