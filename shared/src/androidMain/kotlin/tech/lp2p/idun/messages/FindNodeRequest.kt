package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.DHT_VERSION
import tech.lp2p.idun.core.encodeInto

data class FindNodeRequest(
    override val address: Address,
    override val id: ByteArray,
    override val tid: ByteArray,
    val target: ByteArray
) :
    Request {

    override fun encode(buffer: Buffer) {
        val base: MutableMap<String, Any> = mutableMapOf()
        base["a"] = mapOf<String, Any>("id" to id, "target" to target)

        // transaction ID
        base["t"] = tid
        // version
        base["v"] = DHT_VERSION
        // message type
        base["y"] = "q"
        // message method
        base["q"] = "find_node"

        encodeInto(base, buffer)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FindNodeRequest

        if (address != other.address) return false
        if (!id.contentEquals(other.id)) return false
        if (!tid.contentEquals(other.tid)) return false
        if (!target.contentEquals(other.target)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + id.contentHashCode()
        result = 31 * result + tid.contentHashCode()
        result = 31 * result + target.contentHashCode()
        return result
    }

}
