package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.DHT_VERSION
import tech.lp2p.idun.core.encodeInto


data class AnnounceRequest(
    override val address: Address,
    override val id: ByteArray,
    override val tid: ByteArray,
    val infoHash: ByteArray,
    val port: Int,
    val token: ByteArray,
    val name: ByteArray?,
) :
    Request {

    override fun encode(buffer: Buffer) {
        val base: MutableMap<String, Any> = mutableMapOf()
        val inner: MutableMap<String, Any> = mutableMapOf()

        inner["id"] = id
        inner["info_hash"] = infoHash
        inner["port"] = port
        inner["token"] = token
        if (name != null) inner["name"] = name
        base["a"] = inner

        // transaction ID
        base["t"] = tid
        // version
        base["v"] = DHT_VERSION
        // message type
        base["y"] = "q"

        // message method
        base["q"] = "announce_peer"

        encodeInto(base, buffer)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AnnounceRequest

        if (port != other.port) return false
        if (address != other.address) return false
        if (!id.contentEquals(other.id)) return false
        if (!tid.contentEquals(other.tid)) return false
        if (!infoHash.contentEquals(other.infoHash)) return false
        if (!token.contentEquals(other.token)) return false
        if (!name.contentEquals(other.name)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = port
        result = 31 * result + address.hashCode()
        result = 31 * result + id.contentHashCode()
        result = 31 * result + tid.contentHashCode()
        result = 31 * result + infoHash.contentHashCode()
        result = 31 * result + token.contentHashCode()
        result = 31 * result + (name?.contentHashCode() ?: 0)
        return result
    }
}
