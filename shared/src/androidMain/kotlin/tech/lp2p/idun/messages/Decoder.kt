package tech.lp2p.idun.messages

import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.SHA1_HASH_LENGTH
import tech.lp2p.idun.core.longGet
import tech.lp2p.idun.core.typedGet

fun getStringFromBytes(bytes: ByteArray?, preserveBytes: Boolean): String {
    if (bytes == null) {
        return ""
    }
    return String(
        bytes,
        if (preserveBytes) Charsets.ISO_8859_1 else Charsets.UTF_8
    )
}


fun getStringFromBytes(bytes: ByteArray?): String {
    return getStringFromBytes(bytes, false)
}

@Throws(MessageException::class)
fun parseRequest(address: Address, map: Map<String, Any>): Message {
    val root = map["a"] as? Map<*, *> ?: throw MessageException(
        "expected a bencoded dictionary under key a", PROTOCOL_ERROR
    )

    val args = root as Map<String, Any>

    val tid = typedGet(map, "t")
    checkNotNull(tid) { "missing transaction ID in request" }
    require(tid.isNotEmpty()) { "zero-length transaction ID in request" }

    var id = typedGet(args, "id")
    checkNotNull(id) { "missing id" }
    require(id.size == SHA1_HASH_LENGTH) { "invalid node id" }

    val requestMethod = getStringFromBytes(map["q"] as ByteArray?, true)

    return when (requestMethod) {
        "ping" -> PingRequest(address, id, tid)
        "find_node", "get_peers" -> {
            var target = args["target"]
            if (target == null) {
                target = args["info_hash"]
            }
            if (target == null) {
                throw MessageException(
                    "missing/invalid target key in request",
                    PROTOCOL_ERROR
                )
            }
            var hash = target as ByteArray

            if (hash.size != SHA1_HASH_LENGTH) {
                throw MessageException(
                    "invalid target key in request",
                    PROTOCOL_ERROR
                )
            }

            var explicitWants: List<ByteArray>? = null
            val want = args["want"]
            if (want != null) {
                if (want is List<*>) {
                    explicitWants = want as List<ByteArray>
                } else {
                    throw MessageException(
                        "expected 'want' " +
                                "field in get_peers to be list of strings",
                        PROTOCOL_ERROR
                    )
                }
            }

            return when (requestMethod) {
                "find_node" -> FindNodeRequest(address, id, tid, hash)

                "get_peers" -> GetPeersRequest(address, id, tid, hash)

                else -> throw IllegalStateException("not handled branch")
            }
        }

        "announce_peer" -> {
            var infoHash = typedGet(args, "info_hash")
            checkNotNull(infoHash) {
                "missing info_hash for announce"
            }
            require(infoHash.size == SHA1_HASH_LENGTH) { "invalid info_hash" }


            var port = longGet(args, "port")
            checkNotNull(port) { "missing port for announce" }
            require(port in 1..65535) { "invalid port" }


            val token = typedGet(args, "token")

            if (token == null) throw MessageException(
                "missing or invalid mandatory arguments (info_hash, port, token) for announce",
                PROTOCOL_ERROR
            )
            if (token.isEmpty()) throw MessageException(
                "zero-length token in announce_peer request. see BEP33 for reasons why " +
                        "tokens might not have been issued by get_peers response",
                PROTOCOL_ERROR
            )
            val name = typedGet(args, "name")
            AnnounceRequest(address, id, tid, infoHash, port.toInt(), token, name)

        }

        else -> {
            throw MessageException(
                "method unknown in request",
                METHOD_UNKNOWN
            )
        }
    }
}