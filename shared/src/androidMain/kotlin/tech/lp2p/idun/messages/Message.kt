package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import tech.lp2p.idun.core.Address


// remoteAddress: for outgoing messages this is the destination
// remoteAddress: for incoming messages this is the origin
interface Message {
    val address: Address
    val id: ByteArray
    val tid: ByteArray
    fun encode(buffer: Buffer)
}

interface Response : Message
interface Request : Message


class MessageException : Exception {
    val errorCode: Int

    constructor(message: String, code: Int) : super(message) {
        this.errorCode = code
    }
}