package tech.lp2p.idun.messages

import kotlinx.io.Buffer
import tech.lp2p.idun.core.Address
import tech.lp2p.idun.core.BucketEntry
import tech.lp2p.idun.core.DHT_VERSION
import tech.lp2p.idun.core.NODE_ENTRY_LENGTH_IPV4
import tech.lp2p.idun.core.NODE_ENTRY_LENGTH_IPV6
import tech.lp2p.idun.core.Peer
import tech.lp2p.idun.core.encodeInto
import tech.lp2p.idun.core.writeBuckets

data class GetPeersResponse(
    override val address: Address,
    override val id: ByteArray,
    override val tid: ByteArray,
    val token: ByteArray?,
    val nodes: List<BucketEntry>,
    val nodes6: List<BucketEntry>,
    val items: List<Peer>
) : Response {


    override fun encode(buffer: Buffer) {
        val base: MutableMap<String, Any> = mutableMapOf()
        val inner: MutableMap<String, Any> = mutableMapOf()
        inner["id"] = id
        if (token != null) inner["token"] = token
        if (nodes.isNotEmpty()) inner["nodes"] = writeBuckets(nodes, NODE_ENTRY_LENGTH_IPV4)
        if (nodes6.isNotEmpty()) inner["nodes6"] = writeBuckets(nodes6, NODE_ENTRY_LENGTH_IPV6)
        if (items.isNotEmpty()) {
            val values: List<ByteArray> = items.map { it -> it.item }
            inner["values"] = values
        }
        base["r"] = inner

        // transaction ID
        base["t"] = tid
        // version
        base["v"] = DHT_VERSION
        // message type
        base["y"] = "r"

        encodeInto(base, buffer)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GetPeersResponse

        if (address != other.address) return false
        if (!id.contentEquals(other.id)) return false
        if (!tid.contentEquals(other.tid)) return false
        if (!token.contentEquals(other.token)) return false
        if (nodes != other.nodes) return false
        if (nodes6 != other.nodes6) return false
        if (items != other.items) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + id.contentHashCode()
        result = 31 * result + tid.contentHashCode()
        result = 31 * result + (token?.contentHashCode() ?: 0)
        result = 31 * result + nodes.hashCode()
        result = 31 * result + nodes6.hashCode()
        result = 31 * result + items.hashCode()
        return result
    }


}
