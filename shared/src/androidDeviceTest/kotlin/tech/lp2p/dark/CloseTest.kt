package tech.lp2p.dark

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class CloseTest {
    @Test
    fun closeConnect(): Unit = runBlocking(Dispatchers.IO) {

        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        halo.root("Homepage".toByteArray())
        val raw = halo.root().cid()

        val request = TestEnv.loopbackRequest(server.peerId(), serverPort)

        val client = newDark()
        val rootUri = client.fetchRoot(request)
        assertNotNull(rootUri)
        val cid = extractCid(rootUri)
        assertEquals(cid, raw)
        client.shutdown()

        Thread.sleep(100)
        assertEquals(server.numIncomingConnections(), 0)
        server.shutdown()
        halo.delete()
    }


}