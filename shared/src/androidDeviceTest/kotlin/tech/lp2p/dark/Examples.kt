package tech.lp2p.dark

import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals

class Examples {
    @Test
    fun simpleRequestResponse(): Unit = runBlocking {
        // create local server and client instance
        val port = TestEnv.nextFreePort()
        val halo = newHalo()
        val raw = halo.storeText("Moin") // store some text

        val server = newDark()
        // run the service with the given port and with the data stored in halo
        server.runService(halo, port)

        val client = newDark()

        val request =
            createRequest(
                TestEnv.loopbackPeeraddr(server.peerId(), port),
                raw
            ) // request is a pns-URI

        val data = client.fetchData(request) // fetch request
        assertEquals(String(data), "Moin")

        client.shutdown()
        server.shutdown()
        halo.delete() // Note: this one deletes all content in the block store
    }
}