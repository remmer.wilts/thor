package tech.lp2p.dark

import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class FetchRequestsTest {
    @Test
    fun fetchRequests(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()

        server.runService(halo, serverPort)
        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)

        val content = "Moin Moin"
        val bytes = TestEnv.getRandomBytes(5000)

        val fid = TestEnv.createContent(halo, "index.txt", content.toByteArray())
        assertNotNull(fid)

        val bin = TestEnv.createContent(halo, "payload.bin", bytes)
        assertNotNull(bin)


        val client = newDark()
        var request = createRequest(loopback, fid)
        var data = client.channel(request).readAllBytes()

        assertNotNull(data)
        assertTrue(data.contentEquals(content.toByteArray()))


        request = createRequest(loopback, bin)
        data = client.channel(request).readAllBytes()

        assertNotNull(data)
        assertTrue(data.contentEquals(bytes))
        client.shutdown()
        server.shutdown()
        halo.delete()
    }
}