package tech.lp2p.dark

import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.SPLITTER_SIZE
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class FetchStressTest {
    @Test
    fun stressFetchCalls(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val iterations = 10

        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        var now = System.currentTimeMillis()

        val dataSize = SPLITTER_SIZE.toInt()
        val fid = TestEnv.createContent(
            halo, "text.bin",
            TestEnv.getRandomBytes(dataSize)
        )
        assertNotNull(fid)


        println(
            "Store Input Stream : " + fid +
                    " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]"
        )

        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)

        repeat(iterations) {
            now = System.currentTimeMillis()
            val client = newDark()

            assertTrue(client.reachable(loopback))

            val request = createRequest(loopback, fid)

            val data = client.channel(request).readAllBytes()
            assertEquals(fid.size().toInt(), data.size)

            println(
                "Read Data : " + data.size +
                        "[bytes] Time : " + (System.currentTimeMillis() - now) + "[ms]"
            )
            client.shutdown()
        }
        server.shutdown()
        halo.delete()
    }
}
