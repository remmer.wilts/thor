package tech.lp2p.dark

import kotlinx.io.Buffer
import kotlinx.io.buffered
import kotlinx.io.files.SystemFileSystem
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.Peeraddr
import tech.lp2p.halo.Halo
import tech.lp2p.halo.Node
import tech.lp2p.halo.SPLITTER_SIZE
import tech.lp2p.halo.core.OCTET_MIME_TYPE
import tech.lp2p.lite.createPeeraddr
import java.net.Inet6Address
import java.net.InetAddress
import java.net.NetworkInterface
import kotlin.random.Random


internal object TestEnv {
    const val ITERATIONS: Int = 4096
    val BOOTSTRAP: MutableList<Peeraddr> = mutableListOf()

    private const val DEBUG = true

    init {
        try {
            // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
            BOOTSTRAP.add(
                createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    InetAddress.getByName("104.131.131.82").address, 4001.toUShort()
                )
            )
        } catch (throwable: Throwable) {
            error(throwable)
        }
    }

    fun randomPeerId(): PeerId {
        return PeerId(getRandomBytes(32))
    }

    fun getRandomBytes(number: Int): ByteArray {
        val bytes = ByteArray(number)
        Random.nextBytes(bytes)
        return bytes
    }

    fun randomLong(): Long {
        return Random.nextLong()
    }

    fun createContent(halo: Halo, name: String, data: ByteArray): Node {
        val source = Buffer()
        source.write(data)

        return halo.storeSource(source, name, OCTET_MIME_TYPE)
    }

    fun createContent(halo: Halo, iteration: Int): Node {
        val temp = halo.tempFile()
        SystemFileSystem.sink(temp).buffered().use { source ->
            repeat(iteration) {
                source.write(getRandomBytes(SPLITTER_SIZE.toInt()))
            }
        }

        val node = halo.storeFile(temp, OCTET_MIME_TYPE)

        SystemFileSystem.delete(temp)
        return node
    }


    fun supportLongRunningTests(): Boolean {
        return DEBUG
    }

    internal fun isLanAddress(inetAddress: InetAddress): Boolean {
        return inetAddress.isAnyLocalAddress
                || inetAddress.isLinkLocalAddress
                || (inetAddress.isLoopbackAddress)
                || (inetAddress.isSiteLocalAddress)
    }


    internal fun publicAddresses(): List<InetAddress> {
        val inetAddresses: MutableList<InetAddress> = ArrayList()

        try {
            val interfaces = NetworkInterface.getNetworkInterfaces()

            for (networkInterface in interfaces) {
                if (networkInterface.isUp) {
                    val addresses = networkInterface.inetAddresses
                    for (inetAddress in addresses) {
                        if (inetAddress is Inet6Address) {
                            if (!isLanAddress(inetAddress)) {
                                inetAddresses.add(inetAddress)
                            }
                        }
                    }
                }
            }
        } catch (throwable: Throwable) {
            throw IllegalStateException(throwable)
        }
        return inetAddresses
    }


    internal fun loopbackPeeraddr(peerId: PeerId, port: Int): Peeraddr {
        val inetAddress = InetAddress.getLoopbackAddress()
        return Peeraddr(peerId, inetAddress.address, port.toUShort())
    }

    fun loopbackRequest(peerId: PeerId, port: Int): String {
        return createRequest(loopbackPeeraddr(peerId, port))
    }

    fun peeraddrs(peerId: PeerId, port: Int): List<Peeraddr> {
        val inetSocketAddresses: Collection<InetAddress> = publicAddresses()
        val result = mutableListOf<Peeraddr>()
        for (inetAddress in inetSocketAddresses) {
            result.add(Peeraddr(peerId, inetAddress.address, port.toUShort()))
        }
        return result
    }


    internal fun isLocalPortFree(port: Int): Boolean {
        try {
            java.net.ServerSocket(port).close()
            return true
        } catch (_: Throwable) {
            return false
        }
    }

    fun nextFreePort(): Int {
        val port = Random.nextInt(4001, 65535)
        return if (isLocalPortFree(port)) {
            port
        } else {
            nextFreePort()
        }
    }


    // Reservation feature is only possible when a public Inet6Address is available
    fun reservationFeaturePossible(): Boolean {
        return publicAddresses().isNotEmpty()
    }
}
