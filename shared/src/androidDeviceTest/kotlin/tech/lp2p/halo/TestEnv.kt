package tech.lp2p.halo

import kotlinx.io.Buffer
import kotlinx.io.buffered
import kotlinx.io.files.SystemFileSystem
import tech.lp2p.halo.core.OCTET_MIME_TYPE
import kotlin.random.Random


internal object TestEnv {



    fun getRandomBytes(number: Int): ByteArray {
        val bytes = ByteArray(number)
        Random.nextBytes(bytes)
        return bytes
    }

    fun randomLong(): Long {
        return Random.nextLong()
    }

    fun createContent(
        halo: Halo,
        name: String,
        mimeType: String,
        data: ByteArray
    ): Node {
        val source = Buffer()
        source.write(data)

        return halo.storeSource(source, name, mimeType)

    }

    fun createContent(halo: Halo, iteration: Int): Node {
        val temp = halo.tempFile()

        SystemFileSystem.sink(temp).buffered().use { source ->
            repeat(iteration) {
                source.write(getRandomBytes(SPLITTER_SIZE.toInt()))
            }
        }

        val node = halo.storeFile(temp, OCTET_MIME_TYPE)

        SystemFileSystem.delete(temp)
        return node
    }

    fun error(throwable: Throwable) {
        throwable.printStackTrace()
    }

}
