package tech.lp2p.halo

import kotlinx.coroutines.runBlocking
import kotlinx.io.Buffer
import kotlinx.io.buffered
import kotlinx.io.files.SystemFileSystem
import kotlinx.io.readByteArray
import tech.lp2p.halo.TestEnv.getRandomBytes
import tech.lp2p.halo.core.OCTET_MIME_TYPE
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class Examples {

    @Test
    fun storeAndGetBlock(): Unit = runBlocking {
        val halo = newHalo()
        val maxSize = 50

        // prepare data
        val bytes = Random.nextBytes(maxSize)
        val buffer = Buffer()
        buffer.write(bytes)

        // (1) store block
        val cid = halo.nextCid()
        halo.storeBlock(cid, buffer)

        // (2) get block
        val cmp = halo.fetchBlock(cid)
        assertNotNull(cmp)

        // tests
        val cmpBytes = cmp.readByteArray()
        assertContentEquals(bytes, cmpBytes)

        // cleanup
        halo.deleteBlock(cid)
        halo.delete()
    }

    @Test
    fun storeAndGetRaw(): Unit = runBlocking {
        val halo = newHalo()
        val maxSize = 50

        val random = Random
        val bytes = ByteArray(maxSize)
        random.nextBytes(bytes)

        // (1) store data
        val node = halo.storeData(bytes)

        // tests
        assertNotNull(node)
        assertEquals(node.size(), maxSize.toLong())


        // (2) get data
        val data = halo.fetchData(node)
        assertEquals(data, bytes)
        halo.delete()
    }

    @Test
    fun storeAndGetFile(): Unit = runBlocking {
        val halo = newHalo()

        val temp = halo.tempFile()
        val iteration = 10
        SystemFileSystem.sink(temp).buffered().use { source ->
            repeat(iteration) {
                source.write(getRandomBytes(SPLITTER_SIZE.toInt()))
            }
        }
        // (1) store the file
        val node = halo.storeFile(temp, OCTET_MIME_TYPE)

        // tests
        assertNotNull(node)
        assertEquals(node.size(), (iteration * SPLITTER_SIZE.toInt()).toLong())
        assertEquals(node.name(), temp.name)
        assertEquals(node.mimeType(), OCTET_MIME_TYPE)

        // (2) get the file
        val outputFile = halo.tempFile()
        halo.transferTo(node, outputFile)

        // tests
        assertEquals(
            SystemFileSystem.metadataOrNull(outputFile)?.size,
            SystemFileSystem.metadataOrNull(temp)?.size
        )

        // cleanup
        SystemFileSystem.delete(outputFile)
        SystemFileSystem.delete(temp)
        halo.delete()
    }
}