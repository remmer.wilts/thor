package tech.lp2p.lite

import kotlinx.coroutines.runBlocking
import kotlinx.io.Buffer
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.parsePeeraddr
import tech.lp2p.lite.protos.BYTES_EMPTY
import tech.lp2p.lite.protos.HopMessage
import tech.lp2p.lite.protos.Status
import tech.lp2p.lite.protos.createPeerIdKey
import tech.lp2p.lite.protos.keyDistance
import tech.lp2p.lite.protos.unsignedVariantSize
import tech.lp2p.lite.protos.writeUnsignedVariant
import tech.lp2p.lite.quic.readUnsignedVariant
import java.net.InetAddress
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class UtilsTest {

    @OptIn(ExperimentalSerializationApi::class)
    @Test
    fun testHopMessage() {

        val hopMessage = HopMessage(
            type = HopMessage.Type.STATUS,
            status = Status.OK
        )

        val data = ProtoBuf.encodeToByteArray<HopMessage>(hopMessage)

        val msg = ProtoBuf.decodeFromByteArray<HopMessage>(data)
        checkNotNull(msg)

        assertEquals(msg.type, HopMessage.Type.STATUS)
        assertEquals(msg.status, Status.OK)

    }

    @Test
    fun testBuilder() {

        val lite = Lite()
        assertNotNull(lite)
        assertNotNull(lite.certificate())
        assertNotNull(lite.peerStore())

    }


    @Test
    fun stressToBase58() {
        repeat(TestEnv.ITERATIONS) {
            val peerId = TestEnv.randomPeerId()
            val toBase58 = peerId.toBase58()
            assertNotNull(toBase58)
            val cmp = decodePeerId(toBase58)
            assertEquals(peerId, cmp)
        }
    }


    @Test
    fun peerIdRandom() {
        val peerId = TestEnv.randomPeerId()
        val bytes = peerId.hash
        assertEquals(PeerId(bytes), peerId)
    }

    @Test
    fun emptyData() {
        val data = BYTES_EMPTY

        val size = unsignedVariantSize(data.size.toLong()) + data.size

        val buffer = Buffer()
        writeUnsignedVariant(buffer, data.size.toLong())
        buffer.write(data)
        assertEquals(buffer.size, size.toLong())


        val dataSize = readUnsignedVariant(buffer)
        assertEquals(dataSize, 0)
        assertTrue(buffer.exhausted())

    }


    @Test
    fun distance(): Unit = runBlocking {
        val peerId = TestEnv.randomPeerId()


        val a = createPeerIdKey(peerId)
        val b = createPeerIdKey(peerId)


        val dist = keyDistance(a, b)
        assertEquals(dist.toLong(), 0L)


        val random = TestEnv.randomPeerId()

        val r1 = createPeerIdKey(random)

        val distCmp = keyDistance(a, r1)
        assertNotEquals(distCmp.toLong(), 0L)
    }


    @OptIn(ExperimentalEncodingApi::class)
    @Test
    fun tinkEd25519() {
        val keys = generateKeys()

        val peerId = keys.peerId

        val msg = "moin moin".toByteArray()
        val signature = sign(keys, msg)


        val privateKeyAsString = Base64.encode(keys.privateKey)
        assertNotNull(privateKeyAsString)
        val publicKeyAsString = Base64.encode(keys.peerId.hash)
        assertNotNull(publicKeyAsString)


        val privateKey = Base64.decode(privateKeyAsString)
        assertNotNull(privateKey)
        val publicKey = Base64.decode(publicKeyAsString)

        val peerIdCmp = PeerId(publicKey)

        assertEquals(peerId, peerIdCmp)

        verify(peerIdCmp, msg, signature)
    }


    @Test
    fun peerIdsDecoding() {
        val random = TestEnv.randomPeerId()

        // -- Peer ID (sha256) encoded as a raw base58btc multihash
        var peerId = decodePeerId("QmYyQSo1c1Ym7orWxLYvCrM2EmxFTANf8wXmmE7DWjhx5N")
        assertNotNull(peerId)

        //  -- Peer ID (ed25519, using the "identity" multihash) encoded as a raw base58btc multihash
        peerId = decodePeerId("12D3KooWD3eckifWpRn9wQpMG9R9hX3sD158z7EqHWmweQAJU5SA")
        assertNotNull(peerId)

        peerId = decodePeerId(random.toBase58())
        assertNotNull(peerId)
        assertEquals(peerId, random)
    }


    @Test
    fun ipv6Test() {
        assertNotNull(
            createPeeraddr(
                "12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU",
                InetAddress.getByName("2804:d41:432f:3f00:ccbd:8e0d:a023:376b").address,
                4001.toUShort()
            )
        )
    }

    @Test
    fun listenAddresses(): Unit = runBlocking {

        val server = newLite()
        val result = TestEnv.publicPeeraddrs(server.peerId(), 5001)
        assertNotNull(result)
        for (ma in result) {
            println("Listen Address : $ma")
        }
        assertNotNull(server.peerId())

        server.shutdown()

    }

    @Test
    fun peerStoreTest() {
        val port = 4001
        val random = TestEnv.randomPeerId()

        TestEnv.publicPeeraddrs(random, port).forEach { peeraddr ->
            assertNotNull(peeraddr)
            val data = peeraddr.encoded()
            assertNotNull(data)
            val cmp = parsePeeraddr(random, data)
            assertNotNull(cmp)
            assertEquals(peeraddr, cmp)
        }
    }

}
