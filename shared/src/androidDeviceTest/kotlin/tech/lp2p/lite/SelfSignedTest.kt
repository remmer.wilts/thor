package tech.lp2p.lite

import kotlinx.coroutines.runBlocking
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.identifyRaw
import tech.lp2p.lite.cert.ASN1OctetString
import tech.lp2p.lite.cert.ASN1Primitive
import tech.lp2p.lite.cert.DEROctetString
import tech.lp2p.lite.cert.DLSequence
import tech.lp2p.lite.cert.getSequenceInstance
import java.io.ByteArrayInputStream
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class SelfSignedTest {
    @Test
    fun certificateTest(): Unit = runBlocking {

        val lite = newLite()
        val encoded = lite.certificate().encoded()
        assertNotNull(encoded)

        /*
        val certText = """
             -----BEGIN CERTIFICATE-----
             ${Base64.encode(cert.encoded())}
             -----END CERTIFICATE-----

             """.trimIndent()

        val certificate = certText.toByteArray(Charsets.US_ASCII)

        ByteArrayInputStream(certificate).use { certificateInput ->
            var x509Certificate = CertificateFactory.getInstance("X509")
                .generateCertificate(certificateInput) as X509Certificate
            return Certificate(x509Certificate, key)
        }*/
        var x509Certificate = CertificateFactory.getInstance("X509")
            .generateCertificate(ByteArrayInputStream(encoded)) as X509Certificate

        val peerId = extractPeerIdFromCertificate(x509Certificate)
        assertNotNull(peerId)
        assertEquals(peerId, lite.peerId())
        lite.shutdown()

    }

    fun extractPeerIdFromCertificate(cert: X509Certificate): PeerId {
        try {
            val extension = cert.getExtensionValue(LIBP2P_CERTIFICATE_EXTENSION)
            checkNotNull(extension)

            return extractPeerIdFromCertificateExtension(extension)
        } catch (throwable: Throwable) {
            throw IllegalStateException(throwable)
        }
    }


    fun extractPeerIdFromCertificateExtension(extension: ByteArray): PeerId {
        val octs = ASN1Primitive.fromByteArray(extension) as ASN1OctetString
        val primitive = ASN1Primitive.fromByteArray(octs.octets)
        val sequence = getSequenceInstance(primitive) as DLSequence
        val pubKeyRaw = sequence.getObjectAt(0) as DEROctetString
        return identifyRaw(pubKeyRaw.octets)
    }
}