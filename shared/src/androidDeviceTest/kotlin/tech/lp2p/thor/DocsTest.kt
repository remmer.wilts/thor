package tech.lp2p.thor

import org.junit.Assert
import org.junit.Test

class DocsTest {
    @Test
    fun parseContentDisposition() {
        val filename =
            parseContentDisposition("attachment; filename=\"README.md\"; filename=\"UTF-8 README.md\"")
        Assert.assertEquals(filename, "README.md")
    }
}