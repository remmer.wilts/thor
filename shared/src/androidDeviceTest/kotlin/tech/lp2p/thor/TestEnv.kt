package tech.lp2p.thor

import kotlinx.io.Buffer
import kotlinx.io.buffered
import kotlinx.io.files.SystemFileSystem
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.Peeraddr
import tech.lp2p.halo.Halo
import tech.lp2p.halo.Node
import tech.lp2p.halo.core.OCTET_MIME_TYPE
import tech.lp2p.lite.createPeeraddr
import java.net.InetAddress
import java.net.ServerSocket
import kotlin.random.Random


internal object TestEnv {
    val BOOTSTRAP: MutableList<Peeraddr> = mutableListOf()


    init {
        try {
            // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
            BOOTSTRAP.add(
                createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    InetAddress.getByName("104.131.131.82").address, 4001.toUShort()
                )
            )
        } catch (throwable: Throwable) {
            error(throwable)
        }
    }

    fun getRandomBytes(number: Int): ByteArray {
        val bytes = ByteArray(number)
        Random.nextBytes(bytes)
        return bytes
    }

    fun createContent(halo: Halo, name: String, data: ByteArray): Node {
        val source = Buffer()
        source.write(data)

        return halo.storeSource(source, name, OCTET_MIME_TYPE)
    }

    fun createContent(halo: Halo, iteration: Int): Node {
        val temp = halo.tempFile()
        SystemFileSystem.sink(temp).buffered().use { source ->
            repeat(iteration) {
                source.write(getRandomBytes(Short.MAX_VALUE.toInt()))
            }
        }

        val fid = halo.storeFile(temp, OCTET_MIME_TYPE)

        SystemFileSystem.delete(temp)
        return fid
    }

    internal fun loopbackPeeraddr(peerId: PeerId, port: Int): Peeraddr {
        val inetAddress = InetAddress.getLoopbackAddress()
        return Peeraddr(peerId, inetAddress.address, port.toUShort())
    }


    internal fun isLocalPortFree(port: Int): Boolean {
        try {
            ServerSocket(port).close()
            return true
        } catch (_: Throwable) {
            return false
        }
    }

    fun nextFreePort(): Int {
        val port = Random.nextInt(4001, 65535)
        return if (isLocalPortFree(port)) {
            port
        } else {
            nextFreePort()
        }
    }
}
